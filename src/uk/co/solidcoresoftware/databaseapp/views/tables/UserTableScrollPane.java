package uk.co.solidcoresoftware.databaseapp.views.tables;

import java.awt.Dimension;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;

public class UserTableScrollPane extends JScrollPane {
	
	public final BaseTable table;

	public UserTableScrollPane(BaseTable table) {
		super(table);
		this.table = table;
		
		table.setScrollPane(this);
		
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		setVerticalScrollBarPolicy(VERTICAL_SCROLLBAR_AS_NEEDED);
		setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		addComponentListener(new ComponentListener() {
			
			@Override
			public void componentShown(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentResized(ComponentEvent e) {
				resizeTable();
			}
			
			@Override
			public void componentMoved(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void componentHidden(ComponentEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
	}
	
	public void turnToList() {
		table.setTableHeader(null);
		table.setOpaque(true);
		table.getRowSorter().toggleSortOrder(1);
		table.setRowMargin(0);
		table.showLines(false);
		
		getViewport().setBackground(table.getBackground());
		getViewport().setOpaque(true);
	}
	
	public void resizeTable() {
		if (this.table != null) {
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					table.resizeAllColumns(getWidth());
				}
			});
		}
	}
}
