package uk.co.solidcoresoftware.databaseapp.libraries.core.config;

public interface Config {
	public final static String ID_COLUMN = "id";
	public final static String ID_COLUMN_SQL_DATA_TYPE = "bigint(20)";
}
