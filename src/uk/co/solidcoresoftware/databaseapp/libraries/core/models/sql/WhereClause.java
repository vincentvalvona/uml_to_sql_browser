package uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;

public class WhereClause {

	public static final int WHERE_CLAUSE_TYPE_CUSTOM = -1;
	public static final int WHERE_CLAUSE_TYPE_EQUALS = 0;
	public static final int WHERE_CLAUSE_TYPE_NOT_EQUAL_TO = 1;
	public static final int WHERE_CLAUSE_TYPE_GREATER_THAN = 2;
	public static final int WHERE_CLAUSE_TYPE_GREATER_THAN_OR_EQUAL_TO = 3;
	public static final int WHERE_CLAUSE_TYPE_LESS_THAN = 4;
	public static final int WHERE_CLAUSE_TYPE_LESS_THAN_OR_EQUAL_TO = 5;
	
	private final Column column;
	private final Object value;
	private final int whereClauseType;
	private final String customSQL;
	
	public WhereClause(String customSQL) {
		this(null, null, WHERE_CLAUSE_TYPE_CUSTOM, customSQL);
	}
	
	public WhereClause(Column column, Object value) {
		this(column, value, WHERE_CLAUSE_TYPE_EQUALS);
	}

	public WhereClause(Column column, Object value, int whereClauseType) {
		this(column, value, whereClauseType, null);
	}
	
	public WhereClause(Column column, Object value, int whereClauseType, String customSQL) {
		this.column = column;
		this.value = value;
		this.whereClauseType = whereClauseType;
		this.customSQL = customSQL;
	}
	
	public String getPreparedStatementSegment() {
		
		if (WHERE_CLAUSE_TYPE_CUSTOM == whereClauseType) {
			return customSQL;
		}
		else {
			String comparator = " = ";
			
			switch (whereClauseType) {
				case WHERE_CLAUSE_TYPE_EQUALS: comparator = " = "; break;
				case WHERE_CLAUSE_TYPE_NOT_EQUAL_TO: comparator = " != "; break;
				case WHERE_CLAUSE_TYPE_GREATER_THAN: comparator = " > "; break;
				case WHERE_CLAUSE_TYPE_GREATER_THAN_OR_EQUAL_TO: comparator = " >= "; break;
				case WHERE_CLAUSE_TYPE_LESS_THAN: comparator = " < "; break;
				case WHERE_CLAUSE_TYPE_LESS_THAN_OR_EQUAL_TO: comparator = " <= "; break;
			}
			
			return column.tableDotColumnWithTicks() + comparator + "?";
		}
	}
	
	public static String getWhereStatement(WhereClause[] whereClauses) {
		if (whereClauses == null || whereClauses.length == 0) {
			return "";
		}
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" WHERE ");
		sqlBuilder.append(whereClauses[0].getPreparedStatementSegment());
		
		for (int i=1; i<whereClauses.length; i++) {
			sqlBuilder.append(" AND ");
			sqlBuilder.append(whereClauses[i].getPreparedStatementSegment());
		}
		
		return sqlBuilder.toString();
	}
	
	public Object getValue() { return value; }

	public boolean isCustomSQL() {
		return WHERE_CLAUSE_TYPE_CUSTOM == whereClauseType;
	}
}
