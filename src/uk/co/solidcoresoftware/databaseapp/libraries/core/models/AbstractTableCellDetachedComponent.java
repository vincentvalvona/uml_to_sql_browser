package uk.co.solidcoresoftware.databaseapp.libraries.core.models;

import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.InterfaceTableCellDetachedComponent;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;

public abstract class AbstractTableCellDetachedComponent implements InterfaceTableCellDetachedComponent {

	@Override
	public void setModelAndColumn(LightTableModel model, Column column) {}

	@Override
	public void setValue(int rowIndex, Object value) {
		updateComponent();
	}

	@Override
	public void setToNoRowSelected() {
		updateComponent();
	}

	@Override
	public void sendValueToTable() {}

	@Override
	public Column getColumn() { return null; }

	public abstract void updateComponent();
}
