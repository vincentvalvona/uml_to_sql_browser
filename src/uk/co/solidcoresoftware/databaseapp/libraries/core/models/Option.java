package uk.co.solidcoresoftware.databaseapp.libraries.core.models;

public class Option {
	public static final int OPTION_VALUE_GENERAL = 0;
	public static final int OPTION_VALUE_YES = 1;
	public static final int OPTION_VALUE_NO = 2;
	public static final int OPTION_VALUE_CANCEL = 3;
	public static final int OPTION_VALUE_RETRY = 4;
	public static final int OPTION_VALUE_OK = 5;
	public static final int OPTION_VALUE_CONFIRM = 6;
	public static final int OPTION_VALUE_REPLACE = 7;
	public static final int OPTION_VALUE_USE_MAIN_INPUT = 8;
	
	public final Object value;
	public final String stringValue;
	
	public Option(Object value, String stringValue) {
		this.value = value;
		this.stringValue = stringValue;
	}
	
	@Override
	public int hashCode() {
		return value.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		
		if (obj instanceof Option) {		
			Option objT = (Option) obj;
			
			if (this.value == null) return objT.value == null;
			
			return this.value.equals(objT.value);
		}
		
		return false;
	}
	
	@Override
	public String toString() {
		return this.stringValue;
	}
}
