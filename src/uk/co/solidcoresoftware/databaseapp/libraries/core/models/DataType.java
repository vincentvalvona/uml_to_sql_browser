package uk.co.solidcoresoftware.databaseapp.libraries.core.models;

import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Date;


import java.text.SimpleDateFormat;

public class DataType {

//	public final static int TYPE_STRING = 0;
//	public final static int java.sql.Types.INTEGER = 1;
//	public final static int java.sql.Types.BIGINT = 2;
//	public final static int java.sql.Types.BOOLEAN = 3;
//	public final static int java.sql.Types.DECIMAL = 4;
//	public final static int java.sql.Types.FLOAT = 5;

	public final static String CONSTRAINT_PRIMARY = "PRIMARY";
	
	public final static String S_TYPE_SERIAL = "SERIAL";
	public final static String S_TYPE_BIT = "BIT";
	public final static String S_TYPE_BOOLEAN = "BOOLEAN";
	public final static String S_TYPE_TINYINT = "TINYINT";
	public final static String S_TYPE_INT = "INT";
	public final static String S_TYPE_BIGINT = "BIGINT";

	public final static String S_TYPE_TINYTEXT = "TINYTEXT";
	public final static String S_TYPE_TEXT = "TEXT";
	public final static String S_TYPE_MEDIUMTEXT = "MEDIUMTEXT";
	public final static String S_TYPE_LONGTEXT = "LONGTEXT";

	public final static String S_TYPE_DATE = "DATE";
	public final static String S_TYPE_TIME = "TIME";
	public final static String S_TYPE_DATETIME = "DATETIME";
	public final static String S_TYPE_TIMESTAMP = "TIMESTAMP";
	
	public final static String S_TYPE_DECIMAL = "DECIMAL";
	public final static String S_TYPE_FLOAT = "FLOAT";
	public final static String S_TYPE_DOUBLE = "DOUBLE";
	
	public final static String S_TYPE_CHAR = "CHAR";
	public final static String S_TYPE_VARCHAR = "VARCHAR";
	
	public static final int INT_TYPE_DATETIME = 933001;
	public static final int INT_TYPE_TINYTEXT = 933002;
	public static final int INT_TYPE_TEXT = 933003;
	public static final int INT_TYPE_MEDIUMTEXT = 933004;
	public static final int INT_TYPE_LONGTEXT = 933005;

	public static final long INT_TYPE_TINYTEXT_MAX_LENGTH = 255l;
	public static final long INT_TYPE_TEXT_MAX_LENGTH = 65535l;
	public static final long INT_TYPE_MEDIUMTEXT_MAX_LENGTH = 16777215l;
	public static final long INT_TYPE_LONGTEXT_MAX_LENGTH = 4294967295l;
	
	public static Long getLengthIfText(int dataType) {
		switch (dataType) {
			case INT_TYPE_TINYTEXT: return INT_TYPE_TINYTEXT_MAX_LENGTH;
			case INT_TYPE_TEXT: return INT_TYPE_TEXT_MAX_LENGTH;
			case INT_TYPE_MEDIUMTEXT: return INT_TYPE_MEDIUMTEXT_MAX_LENGTH;
			case INT_TYPE_LONGTEXT: return INT_TYPE_LONGTEXT_MAX_LENGTH;
			default: return null;
		}
	}
	
	
	public static int sTypeToIntType(final String sType) {
		// Boolean
		if (sType.startsWith(S_TYPE_BOOLEAN) || sType.startsWith(S_TYPE_BIT) || sType.startsWith(S_TYPE_TINYINT)) {
			return java.sql.Types.BOOLEAN;
		}
		// Int
		else if (sType.startsWith(S_TYPE_INT)) {
			return java.sql.Types.INTEGER;
		}
		// Long
		else if (sType.startsWith(S_TYPE_BIGINT) || sType.startsWith(S_TYPE_SERIAL)) {
			return java.sql.Types.BIGINT;
		}
		// Decimal
		else if (sType.startsWith(S_TYPE_DECIMAL)) {
			return java.sql.Types.DECIMAL;
		}
		// Float
		else if (sType.startsWith(S_TYPE_FLOAT)) {
			return java.sql.Types.FLOAT;
		}
		// Double
		else if (sType.startsWith(S_TYPE_DOUBLE)) {
			return java.sql.Types.DOUBLE;
		}
		//
		else if (sType.equals(S_TYPE_DATE) ) {
			return java.sql.Types.DATE;
		}
		else if (sType.equals(S_TYPE_TIME) ) {
			return java.sql.Types.TIME;
		}
		else if (sType.equals(S_TYPE_TIMESTAMP) ) {
			return java.sql.Types.TIMESTAMP;
		}
		else if (sType.equals(S_TYPE_DATETIME) ) {
			return DataType.INT_TYPE_DATETIME;
		}
		else if (sType.equals(S_TYPE_TINYTEXT) ) {
			return DataType.INT_TYPE_TINYTEXT;
		}
		else if (sType.equals(S_TYPE_TEXT) ) {
			return DataType.INT_TYPE_TEXT;
		}
		else if (sType.equals(S_TYPE_MEDIUMTEXT) ) {
			return DataType.INT_TYPE_MEDIUMTEXT;
		}
		else if (sType.equals(S_TYPE_LONGTEXT) ) {
			return DataType.INT_TYPE_LONGTEXT;
		}
		else if (sType.startsWith(S_TYPE_CHAR) ) {
			return java.sql.Types.CHAR;
		}
		// String or other
		else {
			return java.sql.Types.VARCHAR;
		}
	}

//	static Object convertToDataType(Object value, int dataType) {
//	
//		if (value == null) return null;
//			
//		if (value instanceof Boolean) {
//			if (dataType == java.sql.Types.BOOLEAN) return value;
//			else if (dataType == java.sql.Types.INTEGER) return (boolean) value ? 1 : 0;
//			else if (dataType == java.sql.Types.BIGINT) return (boolean) value ? 1l : 0l;
//			else if (dataType == java.sql.Types.FLOAT) return (boolean) value ? 1f : 0f;
//			else if (dataType == java.sql.Types.DECIMAL) return (boolean) value ? BigDecimal.ONE : BigDecimal.ZERO;
//		}
//		else if (value instanceof Integer) {
//			if (dataType == java.sql.Types.BOOLEAN) return ((int) value) != 0;
//			else if (dataType == java.sql.Types.INTEGER) return value;
//			else if (dataType == java.sql.Types.BIGINT) ((Integer) value).longValue();
//			else if (dataType == java.sql.Types.FLOAT) ((Float) value).floatValue();
//			else if (dataType == java.sql.Types.DECIMAL) return new BigDecimal((Integer) value);
//		}
//		else if (value instanceof Long) {
//			if (dataType == java.sql.Types.BOOLEAN) return ((long) value) != 0;
//			else if (dataType == java.sql.Types.INTEGER) ((Long) value).intValue();
//			else if (dataType == java.sql.Types.BIGINT) return value;
//			else if (dataType == java.sql.Types.FLOAT) ((Long) value).floatValue();
//			else if (dataType == java.sql.Types.DECIMAL) return new BigDecimal((Long) value);
//		}
//		else if (value instanceof Float) {
//			if (dataType == java.sql.Types.BOOLEAN) return ((float) value) != 0;
//			else if (dataType == java.sql.Types.INTEGER) ((Float) value).intValue();
//			else if (dataType == java.sql.Types.BIGINT) ((Float) value).longValue();
//			else if (dataType == java.sql.Types.FLOAT) return value;
//			else if (dataType == java.sql.Types.DECIMAL) return new BigDecimal((Long) value);
//		}
//		else if (value instanceof BigDecimal) {
//			if (dataType == java.sql.Types.BOOLEAN) return ((BigDecimal) value).stripTrailingZeros().equals(BigDecimal.ZERO.stripTrailingZeros());
//			else if (dataType == java.sql.Types.INTEGER) return ((BigDecimal) value).intValueExact();
//			else if (dataType == java.sql.Types.BIGINT) return ((BigDecimal) value).longValueExact();
//			else if (dataType == java.sql.Types.FLOAT) return ((BigDecimal) value).floatValue();
//			else if (dataType == java.sql.Types.DECIMAL) return value;
//		}
//		else {
//			if (dataType == java.sql.Types.BOOLEAN) {
//				Boolean boolValue = null;
//				try { boolValue = Boolean.parseBoolean(value.toString()); }
//				catch (NumberFormatException e) { boolValue = null; }
//				return boolValue;
//			}
//			else if (dataType == java.sql.Types.INTEGER) {
//				Integer intValue = null;
//				try { intValue = Integer.parseInt(value.toString()); }
//				catch (NumberFormatException e) { intValue = null; }
//				return intValue;
//			}
//			else if (dataType == java.sql.Types.BIGINT) {
//				Long longValue = null;
//				try { longValue = Long.parseLong(value.toString()); }
//				catch (NumberFormatException e) { longValue = null; }
//				return longValue;
//			}
//			else if (dataType == java.sql.Types.DECIMAL) {
//	
//				BigDecimal decimalValue = BigDecimal.ZERO;
//				
//				try {
//					decimalValue = new BigDecimal(value.toString());
//				}
//				catch (NumberFormatException e) {
//					decimalValue = BigDecimal.ZERO;
//				}
//	
//				return decimalValue;
//			}
//		}
//	
//		return value.toString();
//	}
	
	static Object convertToDataType(Object value, int dataType) {
		
		String strValue = null;

		if (value == null) {
			strValue = null;
		}
		else if (value instanceof Boolean) {
			if (dataType == java.sql.Types.BOOLEAN) return value;
			strValue = Boolean.toString((Boolean) value);
		}
		else if (value instanceof Integer) {
			if (dataType == java.sql.Types.INTEGER) return value;
			strValue = Integer.toString((Integer) value);
		}
		else if (value instanceof Long) {
			if (dataType == java.sql.Types.BIGINT) return value;
			strValue = Long.toString((Long) value);
		}
		else if (value instanceof Float) {
			if (dataType == java.sql.Types.FLOAT) return value;
			strValue = Float.toString((Float) value);
		}
		else if (value instanceof Double) {
			if (dataType == java.sql.Types.DOUBLE) return value;
			strValue = Double.toString((Double) value);
		}
		else if (value instanceof BigDecimal) {
			if (dataType == java.sql.Types.DECIMAL) return value;
			strValue = ((BigDecimal) value).toPlainString();
		}
		else if (value instanceof Timestamp) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			if (dataType == java.sql.Types.TIMESTAMP
					|| dataType == DataType.INT_TYPE_DATETIME) return value;
			strValue = sdf.format((Timestamp) value);
		}
		else if (value instanceof Date) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			if (dataType == java.sql.Types.DATE) return value;
			strValue = sdf.format((Date) value);
		}
		else if (value instanceof Time) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			if (dataType == java.sql.Types.TIME) return value;
			strValue = sdf.format((Time) value);
		}
		else {
			strValue = value.toString();
		}

		Object returnValue = null;
		
		try {
			if (strValue == null) {
				returnValue = null;
			}
			else if (dataType == java.sql.Types.BOOLEAN) {
				returnValue = Boolean.parseBoolean(strValue);
			}
			else if (dataType == java.sql.Types.INTEGER) {
				returnValue = Integer.parseInt(strValue);
			}
			else if (dataType == java.sql.Types.BIGINT) {
				returnValue = Long.parseLong(strValue);
			}
			else if (dataType == java.sql.Types.FLOAT) {
				returnValue = Float.parseFloat(strValue);
			}
			else if (dataType == java.sql.Types.DOUBLE) {
				returnValue = Double.parseDouble(strValue);
			}
			else if (dataType == java.sql.Types.DECIMAL) {
				returnValue = new BigDecimal(strValue);
			}
			else if (dataType == DataType.INT_TYPE_DATETIME) {
				returnValue = value == null ? (Timestamp) null : Timestamp.valueOf(value.toString());
			}
			else if (dataType == java.sql.Types.TIMESTAMP) {
				returnValue = value == null ? (Timestamp) null : Timestamp.valueOf(value.toString());
			}
			else if (dataType == java.sql.Types.DATE) {
				returnValue = value == null ? (Date) null : Date.valueOf(value.toString());
			}
			else if (dataType == java.sql.Types.TIME) {
				returnValue = value == null ? (Time) null : Time.valueOf(value.toString());
			}
			else {
				returnValue = strValue;
			}
		}
		catch (NumberFormatException e) {
			returnValue = null;
		}
		catch (IllegalArgumentException e) {
			returnValue = null;
		}
		
		return returnValue;
	}
}