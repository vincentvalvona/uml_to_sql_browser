package uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql;

import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.regex.Pattern;

import growoffshore.DatabaseInfo;
import growoffshore.dialogs.ProgressBarDialog;
import growoffshore.generators.CalibratorGenerator;
import uk.co.solidcoresoftware.databaseapp.libraries.core.config.Config;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.DataType;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.UniqueColumnGroup;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.Row;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.SQLInsert;


public class SQLEngine {

	public static final String UNIQUE = "UNI";

	private final static String INFORMATION_SCHEMA = "information_schema";
	private final String CUSTOM_TABLE_PREFIX = "custom_table_t_";
	private final String CUSTOM_COLUMN_PREFIX = "col_";
	
	private final static int defaultPort = 3306;
	
	private String urlPrefix = "jdbc:mysql://localhost:3306";

	private String host;
    private String user = null;
    private String password = null;
	private String databaseName = null;
	
    private Connection con = null;
	private boolean commitAndRollbackEnabled = true;
    
    private static SQLEngine mainConnection = null;
    private static SQLEngine infoSchemaConnection = null;
    
    public static SQLEngine getMainConnection() {
    	mainConnection.openConnection();
    	return mainConnection;
    }

    public static SQLEngine getInfoSchemaConnection() {
    	infoSchemaConnection.openConnection();
    	return infoSchemaConnection;
    }
    
    public static void setMainConnection(SQLEngine connection) {
    	if (SQLEngine.mainConnection != null) {
    		SQLEngine.mainConnection.closeConnection();
    	}
    	
		SQLEngine.mainConnection = connection;
	}
    
    public static void setInfoSchemaConnection(SQLEngine connection) {
    	if (SQLEngine.infoSchemaConnection != null) {
    		SQLEngine.infoSchemaConnection.closeConnection();
    	}
    	
		SQLEngine.infoSchemaConnection = connection;
	}
    
    public static SQLEngine getSQLEngine(String host, String user, String password, String database) {
    	return new SQLEngine(host, user, password, database);
    }

    public static SQLEngine getSQLEngineForSchema(String host, String user, String password) {
    	return new SQLEngine(host, user, password, INFORMATION_SCHEMA);
    }
    
    private SQLEngine(String host, String user, String password, String database) {
		setConnectionDetails(host, user, password, database);
	}
    
    private boolean setConnectionDetails(String host, String user, String password, String database) {
    	this.host = host;
    	this.databaseName = database;
    	this.user = user;
    	this.password = password;
    	
    	String potentialURLPrefix = getURLPrefix(host);

    	if (potentialURLPrefix != null) {
    		urlPrefix = potentialURLPrefix;
    		return true;
    	}
    	
    	return false;
    }
    
    public String getUrlPrefix() {
		return urlPrefix;
	}
    
    private static String getURLPrefix(String host) {
    	try {
			URL url = new URL("http://" + host);
			
			int portFound = url.getPort();
			
			if (portFound == -1) {
				return "jdbc:mysql://" + host + ":" + defaultPort;
			}
			else {
				return "jdbc:mysql://" + host;
			}
			
		} catch (MalformedURLException e) {
//			e.printStackTrace();
			return null;
		}
    }

    public static String attemptToConnect(String host, String user, String password, String database) {
    	String urlPrefix = getURLPrefix(host);
    	
    	if (urlPrefix == null) {
    		return "Invalid URL for host";
    	}
    	
    	String url = urlPrefix + "/" + database;
    	
    	String errorMessage = null;
    	
    	Connection con = null;
    	
    	try {
			con = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			e.printStackTrace();
			errorMessage = "Can't connect to the server";
		}

		if (con != null) {
	    	try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
		
    	return errorMessage;
    }

    private boolean openConnection() {
    	String dbName = databaseName;
    	String urlStr = urlPrefix + "/" + dbName;
    	
    	try {
			if (con == null || con.isClosed()) {
				con = DriverManager.getConnection(urlStr, user, password);
				con.setAutoCommit(false);
				con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
			}
		} catch (SQLException e) {
			con = null;
			e.printStackTrace();
			return false;
		}
    	
    	return true;
    }
    
    public void closeConnection() {
//    	if (con != null) {
//	    	try {
//				con.close();
//			} catch (SQLException e) {
//				
//			}
//    	}
    }
    
    public int selectCount(Table table) {
    	
    	String sql = "SELECT COUNT(*) FROM " + table.tableNameWithTicks();
    	
    	try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			
			if (rs.next()) {
				return rs.getInt(1);
			}
			return 0;
		} catch (SQLException e) {
			return 0;
		}
    }
    
    /**
     * Selects an SQL select statement where the * after select is replaced with a
     * comma seperated list of the columns to select (if specified). The columns must
     * be ordered so all of the real columns are grouped together at the beginning of
     * the array and all of the fake columns grouped together at the end of the array. 
     * 
     * @param sql The sql SELECT statement to use.  
     * @param columns (null means remain as SELECT *).
     */
    @SuppressWarnings("unchecked")
	public ArrayList<Row> select(BaseTableModel model, String sql, Column[] columns, WhereClause[] whereClauses, Integer offset, Integer limit) {
    	return (ArrayList<Row>) select(false, model, sql, columns, whereClauses, offset, limit);
    }

    public int selectCount(BaseTableModel model, String sql, Column[] columns, WhereClause[] whereClauses, Integer offset, Integer limit) {
    	return (int) select(true, model, sql, columns, whereClauses, offset, limit);
    }
    
    private Object select(final boolean getCountInstead, BaseTableModel model, String sql, Column[] columns, WhereClause[] whereClauses, Integer offset, Integer limit) {
    	
    	if (columns == null) {
    		columns = new Column[0];
    	}
    	
    	// By default, it is assumed that all columns are real
    	int fakeColumnStart = columns.length;
    	
    	ArrayList<Column> columnsToSelectArrayList = new ArrayList<Column>();
    	
    	/*
    	 * When the first fake-column is found, fakeColumnStart is modified
    	 * to the position of this column.
    	 */
    	for (int i=0; i<columns.length; i++) {
    		if (columns[i].isFakeColumn()) {
    			fakeColumnStart = i;
    			break;
    		}
    		else {
    			columnsToSelectArrayList.add(columns[i]);
    		}
    	}
    	
    	Column[] columnsToSelect = columnsToSelectArrayList.toArray(new Column[fakeColumnStart]);
    	
    	final String stringToSearch = "SELECT *";
    	
    	String finalSQL;
    	
    	// If the string starts with stringToSearch then replace this.
    	if (getCountInstead && sql.subSequence(0, stringToSearch.length()).equals(stringToSearch)) {
    		StringBuilder alteredSQL = new StringBuilder("SELECT COUNT(*)");
    		alteredSQL.append(sql.subSequence(stringToSearch.length(), sql.length()));
    		finalSQL = alteredSQL.toString();
    	}
    	else if (columnsToSelect != null && columnsToSelect.length > 0 && sql.subSequence(0, stringToSearch.length()).equals(stringToSearch)) {
    		StringBuilder alteredSQL = new StringBuilder("SELECT ");
			alteredSQL.append(Column.joinTableAndColumnNamesWithAliases(columnsToSelect));
    		alteredSQL.append(sql.subSequence(stringToSearch.length(), sql.length()));
    		finalSQL = alteredSQL.toString();
    	}
    	else {
    		finalSQL = sql.toString();
    	}
    	
    	finalSQL += WhereClause.getWhereStatement(whereClauses);

    	// Adding order by clauses
    	if (model != null) {
    		ArrayList<OrderByClause> orderByClauses = model.getCustomOrderByClauses();
    		
    		if (orderByClauses.size() > 0) {
    			String orderBySQL = OrderByClause.getOrderByStatement(orderByClauses.toArray(new OrderByClause[orderByClauses.size()]));
    			finalSQL += orderBySQL;
    		}
    	}
    	
    	// Setting limits
    	if (!getCountInstead) {
	    	if (limit != null) { finalSQL += (" LIMIT " + limit); }
	    	if (offset != null) { finalSQL += (" OFFSET " + offset); }
    	}
    
		System.out.println("finalSQL: " + finalSQL);
    	
    	Object returnValue = null;
    	
    	try {
    		PreparedStatement st = con.prepareStatement(finalSQL);
    		
    		// Where values are set here
    		if (whereClauses != null) {
    			int setObjectIndex = 1;
				for (int i=0; i<whereClauses.length; i++) {
					WhereClause nextWhereClause = whereClauses[i];
					if (!nextWhereClause.isCustomSQL()) {
						st.setObject(setObjectIndex, whereClauses[i].getValue());
						setObjectIndex++;
					}
				}
    		}
    		
			ResultSet rs = st.executeQuery();
			
			if (getCountInstead) {
				int result = 0;
				
				while (rs.next()) {
					result += rs.getInt(1);
				}
				returnValue = result;
			}
			else {
				// For each row.
				ArrayList<Row> data = new ArrayList<>();
				
				while (rs.next()) {
					
					Object[] dataRow = new Object[columns.length];
					
					for (int i=0; i<fakeColumnStart; i++) {
						switch (columns[i].getType()) {
							case java.sql.Types.BOOLEAN: dataRow[i] = rs.getBoolean(i+1); break;
							case java.sql.Types.INTEGER: dataRow[i] = rs.getInt(i+1); break;
							case java.sql.Types.BIGINT: dataRow[i] = rs.getLong(i+1); break;
							case java.sql.Types.DECIMAL: dataRow[i] = rs.getBigDecimal(i+1); break;
							case java.sql.Types.TIME: 
								try{
									dataRow[i] = rs.getTime(i+1);
								}
								catch(SQLException e){
									System.out.println("[SQLEngine.java] Time error: row id=" + dataRow[0] +"\n "+ e.getMessage());
									dataRow[i]= new Time(0);
								}
								break;
							case java.sql.Types.DATE: 
								try{
									dataRow[i] = rs.getDate(i+1);
								}
								catch(SQLException e){
									System.out.println("[SQLEngine.java] Date error: row id=" + dataRow[0] +"\n "+ e.getMessage());
									dataRow[i]= new Date(0);
								}
								break;							
							case java.sql.Types.TIMESTAMP: 
							case DataType.INT_TYPE_DATETIME: 
								try{
									dataRow[i] = rs.getTimestamp(i+1);
								}
								catch(SQLException e){
									System.out.println("[SQLEngine.java] Timestamp error: row id=" + dataRow[0] +"\n "+ e.getMessage());
									dataRow[i]= new Timestamp(0);
								}
								
								break;
							default: dataRow[i] = rs.getString(i+1);
						}
						
						if (rs.wasNull()) {
							dataRow[i] = null;
							if (columns[i].isNullAllowed()) {
							}
							else {
								// if we get here there is a problem with the database
								System.out.println("[SQLEngine.java] null found in non-null entry: row id=" + dataRow[0] );

								//dataRow[i] = columns[i].getDefaultValue();
							}
						}
					}
					
					// If a model is specified then fake columns are updated as well
					if (model != null) {
						for (int i=fakeColumnStart; i<columns.length; i++) {
							columns[i].updateThisColumnFromOtherColumns(dataRow, model);
						}
					}
					
					data.add(new Row(dataRow));
				}
				returnValue = data;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
    	
		return returnValue;
    }

    /**
     * Gets the row position of a value in a specific columns. For now, we are using longs only.
     * 
     * @param idColumn
     * @param value
     * @return
     */
    public Integer getRowPosition(Column idColumn, Column displayColumn, Long value) {
    	
    	String tableNameInTicks = idColumn.table.tableNameWithTicks();
    	String columnNameInTicks = idColumn.columnNameWithTicks();
    	String displayColumnNameInTicks = displayColumn.columnNameWithTicks();
    	
    	StringBuilder sql = new StringBuilder("SELECT `x`." + columnNameInTicks + ", `x`.`position` FROM (SELECT `t`." + columnNameInTicks + ",");
    	sql.append(" @rownum := @rownum + 1 AS `position`");
    	sql.append("FROM ");
    	sql.append(tableNameInTicks);
    	sql.append(" `t`");
    	sql.append(" JOIN (SELECT @rownum := 0) `r` ORDER BY `t`." + displayColumnNameInTicks + ") `x`");
    	sql.append(" WHERE `x`." + columnNameInTicks + "=" + value + ";");

    	System.out.println("getRowPosition: " + sql.toString());

    	Statement st;
    	ResultSet rs;
		try {
			st = con.createStatement();
	    	rs = st.executeQuery(sql.toString());

	    	if (rs.next()) {
	    		return rs.getInt(2);
	    	}
		} catch (SQLException e) {
			// Do nothing
		}
    	
    	return null;
    }
    
    

    
    public HashMap<Long, String> getDisplayValues(Column pKeyColumn, Column displayColumn, HashSet<Long> values) {
    	// SELECT `id`, `name` FROM `Milestone` WHERE `id`=1 OR `id`=2
    	
    	HashMap<Long, String> valueToString = new HashMap<>();
    	
    	if (values.size() > 0) {
    		
    		String pKeyTableDotColumn = pKeyColumn.tableDotColumnWithTicks();
    		
    		StringBuilder sql = new StringBuilder("SELECT ");
    		sql.append(pKeyTableDotColumn);
    		sql.append(',');
    		sql.append(displayColumn.tableDotColumnWithTicks());
    		sql.append(" FROM ");
    		sql.append(pKeyColumn.table.tableNameWithTicks());
    		
    		boolean firstEntry = true;
	    	Iterator<Long> valuesIt = values.iterator();

	    	Long minValue = null;
	    	Long maxValue = null;
	    	
	    	while (valuesIt.hasNext()) {
	    		Long nextValue = valuesIt.next();
	    		if (nextValue == null) continue;
	    		
	    		if (firstEntry) {
	    			firstEntry= false;
	    			minValue = nextValue;
	    			maxValue = nextValue;
	    		}
	    		else {
	    			if (nextValue < minValue) minValue = nextValue;
	    			else if (nextValue > maxValue) maxValue = nextValue;
	    		}
	    	}
	    	
	    	if (minValue != null && maxValue != null) {

	    		sql.append(" WHERE ");
	    		sql.append(pKeyTableDotColumn);
	    		sql.append(">=");
	    		sql.append(minValue);
	
	    		sql.append(" AND ");
	    		sql.append(pKeyTableDotColumn);
	    		sql.append("<=");
	    		sql.append(maxValue);
	    		
				try {
					System.out.println("getDisplayValues sql: " + sql.toString());
					openConnection();
					Statement st = con.createStatement();
			    	ResultSet rs = st.executeQuery(sql.toString());
			    	
			    	while (rs.next()) {
			    		valueToString.put(rs.getLong(1), rs.getString(2));
			    	}
				} catch (SQLException e) {
					e.printStackTrace();
				}
	    	}
    	}
    	
    	return valueToString;
    }
    
    /**
     * Inserts columns into a specific table. The table is detected
     * automatically from the columns.
     * 
     * @param column Must not be null and an array of at least 1 column with
     * all columns being from the same table.
     * @param values The number of values must match the number of columns.
     * @return Whether the row was inserted successfully or not.
     */
    public SQLInsert insert(Column[] columns, Object[] values, Object extras) {
    	
    	Column firstColumn = columns[0];
    	
    	Table table = firstColumn.table;
    	
    	StringBuilder sql = new StringBuilder("INSERT INTO "); sql.append(table.tableNameWithTicks());
    	sql.append(" ("); sql.append(Column.joinColumnNamesWithTicks(columns)); sql.append(')');
    	sql.append(" VALUES ("); sql.append(Column.joinQuestionMarks(columns)); sql.append(")");

    	SQLInsert sqlInsert = new SQLInsert(columns.length);
    	
    	PreparedStatement ps;
		try {
			ps = con.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			for (int i=0; i<values.length; i++) {
				
				Object valueToInsert;
				
				Column nextColumn = columns[i];
				
				int nextColumnType = nextColumn.getType();
				
				if (nextColumnType == java.sql.Types.TIMESTAMP
						|| nextColumnType == java.sql.Types.TIME
						|| nextColumnType == java.sql.Types.DATE
						|| nextColumnType == DataType.INT_TYPE_DATETIME) {
					
					java.util.Date modifiedValue = nextColumn.getCurrentTimeValue(values[i]);
					sqlInsert.setValueToBeModified(i, modifiedValue);
					valueToInsert = modifiedValue;
				}
				else {
					valueToInsert = values[i];
				}
				
				// If a string is entered as an empty string then regard this as null
				if (valueToInsert instanceof String) {
					String str = (String) valueToInsert;
					if (str.trim().length() == 0) {
						valueToInsert = null;
					}
				}
				
				ps.setObject(i+1, valueToInsert);
			}
	    	ps.execute();
	    	ResultSet rs = ps.getGeneratedKeys();
	    	if (rs.first()) {
		    	sqlInsert.setGeneratedKey(rs.getLong(1));
	    	}
		} catch (SQLException e) {
			if (extras instanceof SQLEngineExceptionWrapper) {
				((SQLEngineExceptionWrapper) extras).setException(e);
			}
//			e.printStackTrace();
		}
    	
    	return sqlInsert;
    }

	/**
	 * 
	 * @param columns This must be a non-null array with a length of at least 1. All columns
	 * must be of the same table
	 * @param primaryKeyValue
	 * @param newValues The new values for this update. This must be a non-null array with its
	 * length equal to that of columns
	 * @return
	 */
    public boolean update(Column[] columns, Object primaryKeyValue, Object[] newValues, Object extras) {
    	assert(columns.length == newValues.length);
    	
    	String sql = " UPDATE " + columns[0].table.tableNameWithTicks()
			+ "  SET " + Column.joinColumnNamesWithTicksAndSuffix(columns, "=?", false)
    			+ " WHERE " + columns[0].table.idColumnNameWithTicks() + "=? ";
    	
    	try {
			PreparedStatement ps = con.prepareStatement(sql);
			
			int setObjectIndex=1;
			
			for (int i=0; i<columns.length; i++) {
				
				Object valueToUpdate = newValues[i];
				

				if (valueToUpdate == null && !columns[i].isNullAllowed()) {
					valueToUpdate = columns[i].getDefaultValue();
				}
				

				// If a string is entered as an empty string then regard this as null
				if (valueToUpdate instanceof String) {
					String str = (String) valueToUpdate;
					if (str.trim().length() == 0) {
						valueToUpdate = null;
					}
				}
				ps.setObject(setObjectIndex++, valueToUpdate);
			}

			ps.setObject(setObjectIndex, primaryKeyValue);
//			return true;
			
			int numberOfRowUpdated = ps.executeUpdate();
			return numberOfRowUpdated != 0;

		} catch (SQLException e) {
			if (extras instanceof SQLEngineExceptionWrapper) {
				((SQLEngineExceptionWrapper) extras).setException(e);
				System.out.println("[SQLEngine.java] Update error: \n" + e.getMessage());
			}
//			e.printStackTrace();
		}
    	
    	return false;
    }
    
    public boolean delete(Table table, Object primaryKeyValue, Object extras) {
    	
    	String sql = "DELETE FROM " + table.tableNameWithTicks() + " WHERE " + table.idColumnNameWithTicks() + "=?";
    	
        	try {
    			PreparedStatement ps = con.prepareStatement(sql);
    			
    			ps.setObject(1, primaryKeyValue);
    			
    			int numberOfRowsUpdated = ps.executeUpdate();
    			return numberOfRowsUpdated != 0;

    		} catch (SQLException e) {
    			if (extras instanceof SQLEngineExceptionWrapper) {
    				((SQLEngineExceptionWrapper) extras).setException(e);
    			}
//    			e.printStackTrace();
    		}
        	
        	return false;
    }
    
    public ArrayList<String> getTableNames(String prefix) {
    	
    	
    	ArrayList<String> tableNames = new ArrayList<String>(50);
    	
    	try {
    		DatabaseMetaData meta = con.getMetaData();
    		ResultSet metaColumns = meta.getTables(null, databaseName, prefix + "%", null);

	    	while (metaColumns.next()) {
	    		tableNames.add(metaColumns.getString("TABLE_NAME"));
	    	}
		} catch (SQLException e) {
			return null;
		}
    	
    	return tableNames;
    }
    
    public ArrayList<String> getCustomTableNames() {
    	return getTableNames(CUSTOM_TABLE_PREFIX);
    }

    public final static String META_COLUMNS_COLUMN_NAME = "COLUMN_NAME";
    public final static String META_COLUMNS_DATA_TYPE = "DATA_TYPE";
    public final static String META_COLUMNS_NUMERIC_PRECISION = "NUMERIC_PRECISION";
    public final static String META_COLUMNS_NUMERIC_SCALE = "NUMERIC_SCALE";
    public final static String META_COLUMNS_IS_NULLABLE = "IS_NULLABLE";
    public final static String META_COLUMNS_COLUMN_DEFAULT = "COLUMN_DEFAULT";
    public final static String META_COLUMNS_COLUMN_KEY = "COLUMN_KEY";
    public final static String META_COLUMNS_CHARACTER_MAXIMUM_LENGTH = "CHARACTER_MAXIMUM_LENGTH";
    public final static String META_COLUMNS_CHARACTER_OCTET_LENGTH = "CHARACTER_OCTET_LENGTH";
	public final static String META_COLUMNS_CONSTRAINT_CATALOG = "CONSTRAINT_CATALOG";
	public final static String META_COLUMNS_CONSTRAINT_SCHEMA = "CONSTRAINT_SCHEMA";
	public final static String META_COLUMNS_CONSTRAINT_NAME = "CONSTRAINT_NAME";
	public final static String META_COLUMNS_TABLE_CATALOG = "TABLE_CATALOG";
	public final static String META_COLUMNS_TABLE_SCHEMA = "TABLE_SCHEMA";
	public final static String META_COLUMNS_TABLE_NAME = "TABLE_NAME";
	public final static String META_COLUMNS_ORDINAL_POSITION = "ORDINAL_POSITION";
	public final static String META_COLUMNS_POSITION_IN_UNIQUE_CONSTRAINT = "POSITION_IN_UNIQUE_CONSTRAINT";
	public final static String META_COLUMNS_REFERENCED_TABLE_SCHEMA = "REFERENCED_TABLE_SCHEMA";
	public final static String META_COLUMNS_REFERENCED_TABLE_NAME = "REFERENCED_TABLE_NAME";
	public final static String META_COLUMNS_REFERENCED_COLUMN_NAME = "REFERENCED_COLUMN_NAME";

    public ArrayList<Object[]> getColumnInfo(String metaColumnName, Table table, String[] metaColumnNames) {
    	
    	openConnection();
    	Connection con = getInfoSchemaConnection().con;
    	
    	ArrayList<Object[]> columnInfo = new ArrayList<>();
    	
    	// Converting column names to actual columns
    	Column[] metaColumns = new Column[metaColumnNames.length]; 
    	
    	for (int i=0; i<metaColumnNames.length; i++) {
    		metaColumns[i] = new Column(metaColumnName, metaColumnNames[i]);
    	}
    	
    	try {
    		
    		String tableSQLPart = (table == null) ? "" : " AND `TABLE_NAME`=\""+table.tableName+"\"";
    		
    		String sql = "SELECT " + Column.joinColumnNamesWithTicks(metaColumns) + " FROM `COLUMNS` WHERE `TABLE_SCHEMA`=\""
    				+ databaseName + "\"" + tableSQLPart;
    		
    		Statement st = con.createStatement();
    		ResultSet metaColumnsRs = st.executeQuery(sql);
    		
    		// Fills the data to return
    		while (metaColumnsRs.next()) {
    			Object[] columnInfoEntry = new Object[metaColumnNames.length];
    			for (int i=0; i<metaColumnNames.length; i++) {
    				columnInfoEntry[i] = metaColumnsRs.getObject(metaColumnNames[i]);
    			}

				columnInfo.add(columnInfoEntry);
    		}
    		
    		
		} catch (SQLException e) {
//			e.printStackTrace();
			closeConnection();
			return null;
		}

    	for (int i=0; i<columnInfo.size(); i++) {
    		Object[] nextSet = columnInfo.get(i);
    		for (int j=0; j<nextSet.length; j++) {
    		}
    	}

		closeConnection();
    	
    	return columnInfo;
    }
    
    public ArrayList<String> getCustomColumnNames(String tableName) {
    	
    	
    	ArrayList<String> columnNames = new ArrayList<String>(50);
    	
    	try {
    		DatabaseMetaData meta = con.getMetaData();
    		ResultSet metaColumns = meta.getColumns(null, databaseName, tableName, CUSTOM_COLUMN_PREFIX + "%");

	    	while (metaColumns.next()) {
	    		columnNames.add(metaColumns.getString("COLUMN_NAME"));
	    	}
		} catch (SQLException e) {
			return null;
		}

    	return columnNames;
    }
    
    public String convertToLowercaseUnderscore(String inputWord) {
    	String inputWordSpacesToUnderscores = inputWord.toLowerCase().trim().replaceAll(" ", "_");
    	return inputWordSpacesToUnderscores.replaceAll("[^A-Za-z0-9_]", "");
    }
    
    public boolean createCustomTable(String userFriendlyTableName) {
    	
    	ArrayList<String> currentTableNames = getCustomTableNames();
    	
    	long number = 1;
    	
    	String tableNameOriginal = CUSTOM_TABLE_PREFIX + convertToLowercaseUnderscore(userFriendlyTableName);
    	String tableNameToUse = tableNameOriginal;
    	
    	while (currentTableNames.contains(tableNameToUse)) {
    		tableNameToUse = tableNameOriginal + "_" + number++;
    	}

    	String sql = "CREATE TABLE " + tableNameToUse + " ( " + Config.ID_COLUMN + " " + Config.ID_COLUMN_SQL_DATA_TYPE + " NOT NULL AUTO_INCREMENT, PRIMARY KEY (" + Config.ID_COLUMN + ")) ENGINE = INNODB";

    	return true;
    }
    
    public boolean createCustomColumn(String tableName,  String userFriendlyColumnName) {
    	
    	ArrayList<String> currentColumnNames = getCustomColumnNames(tableName);
    	
    	long number = 0;
    	
    	String columnNameOriginal = CUSTOM_COLUMN_PREFIX + convertToLowercaseUnderscore(userFriendlyColumnName);
    	String columnNameToUse = columnNameOriginal;
    	
    	while (currentColumnNames.contains(columnNameToUse)) {
    		columnNameToUse = columnNameOriginal + "_" + number++;
    	}
    	
    	String sql = "CREATE TABLE " + columnNameToUse + " ( " + Config.ID_COLUMN + " " + Config.ID_COLUMN_SQL_DATA_TYPE + " NOT NULL AUTO_INCREMENT, PRIMARY KEY (" + Config.ID_COLUMN + "))";

    	return true;
    }
    
    public boolean createTableIfNotExist(Table table) {
    	return createTable(table, true);
    }
    
    public boolean createTable(Table table, boolean onlyIfNotExists) {
    	
    	String sql = onlyIfNotExists ? "CREATE TABLE IF NOT EXISTS " : "CREATE TABLE ";
    	
    	sql += table.tableNameWithTicks()
    			+ "("
    			+ " " + table.idColumnNameWithTicks() + " SERIAL PRIMARY KEY"
    			+ " ) ENGINE = INNODB";

    	boolean successful = true;
    	
    	try {
			Statement statement = con.createStatement();
			statement.execute(sql);
		} catch (SQLException e) {
			successful = false;
		}
    	
    	return successful;
    }
    
    public boolean createTable(Table table, boolean onlyIfNotExists, Column[] columns, ArrayList<UniqueColumnGroup> uniqueColumnGroups) {
    	
    	StringBuilder sql = new StringBuilder();
    	
    	if (onlyIfNotExists) {
    		sql.append("CREATE TABLE IF NOT EXISTS ");
    	}
    	else {
    		sql.append("CREATE TABLE ");
    	}
    	
    	sql.append(table.tableNameWithTicks());
    	sql.append("(");
    	sql.append(table.idColumnNameWithTicks());
    	sql.append(" SERIAL PRIMARY KEY");
    	
    	for (int i=0; i<columns.length; i++) {
    		if (columns[i] != null) {
	    		sql.append(",");
	    		sql.append(getSQLForNewColumn(columns[i], uniqueColumnGroups != null));
    		}
    	}
    	
    	if (uniqueColumnGroups != null) {
    		int uniqueColumnGroupsCount = uniqueColumnGroups.size();
    		for (int i=0; i<uniqueColumnGroupsCount; i++) {
    			UniqueColumnGroup nextGroup = uniqueColumnGroups.get(i);
    			sql.append(",");
    			sql.append("UNIQUE KEY(");
    			sql.append(nextGroup.getColumnsNamesJoinedWithBackticks());
    			sql.append(")");
    		}
    	}
    	
    	sql.append(")");
    	sql.append(" ENGINE = INNODB");
    	
    	boolean successful = true;
    	
    	try {
			Statement statement = con.createStatement();
			statement.execute(sql.toString());
		} catch (SQLException e) {
//			e.printStackTrace();
			successful = false;
		}
    	
    	return successful;
    }

    public boolean createColumnIfNotExist(Column column) {
    	return createColumn(column, true);
    }
    
    private String getSQLForNewColumn(Column column, boolean ignoreUniques) {
    	int type = column.getType();
    	
    	String typePart;
    	
		if (type == java.sql.Types.BOOLEAN) {
			typePart = "TINYINT(1)";
		}
		else if (type == java.sql.Types.DECIMAL) {
			typePart = "DECIMAL(65," + column.getDecimalScale() + ")";
		}
		else if (type == java.sql.Types.INTEGER) {
			typePart = "INT(11)";
		}
		else if (type == java.sql.Types.BIGINT) {
			typePart = "BIGINT(20)";
		}
		else {
			typePart = "VARCHAR(" + column.getMaxStringLength() +")";
		}
		
		StringBuilder sql = new StringBuilder(column.columnNameWithTicks());
		sql.append(" ");

		if (column.getTypeEnum() != null) {
			sql.append(column.getTypeEnum().getEnumAsType());
		}
		else if (column.getTypeString() != null) {
			sql.append(column.getTypeString());
		}
		else {
			sql.append(typePart);
		}
		sql.append(" ");
		if (ignoreUniques) {
			sql.append(column.getNullSQL());
		}
		else {
			sql.append(column.getUniqueAndNullSQL());
	    }
		
		Object defaultValue = column.getDefaultValue();
		
		if (column.defaultAvailable()) {
			if (defaultValue == null) {
				if (column.getType() == java.sql.Types.TIMESTAMP) {
					sql.append(" DEFAULT CURRENT_TIMESTAMP");
				}
			}
			else {
				if (column.isHasDefaultValue()) {
					sql.append(" DEFAULT ");
					sql.append(column.getSafeSQLValue(defaultValue));
				}
			}
		}
		
		return sql.toString();
    }
    
    public boolean createColumn(Column column, boolean onlyIfNotExists) {
    	
		StringBuilder sql = new StringBuilder("ALTER TABLE ");
		sql.append(column.table.tableNameWithTicks());
		sql.append(" ADD ");
		sql.append(getSQLForNewColumn(column, false));
		sql.append(";");
    	
    	boolean successful = true;
    	
    	try {
			Statement statement = con.createStatement();
			statement.execute(sql.toString());
		} catch (SQLException e) {
			successful = false;
//			e.printStackTrace();
		}
    	
    	return successful;
    }
    
    public void setCommitAndRollbackEnabled(boolean commitAndRollbackEnabled) {
		this.commitAndRollbackEnabled = commitAndRollbackEnabled;
	}
    
    public boolean commit() {
    	if (commitAndRollbackEnabled) {
	    	try {
	    		if (!con.getAutoCommit()) {
	    			con.commit();
	    		}
			} catch (SQLException e) {
				rollback();
				return false;
			}
    	}
    	
    	return true;
    }
    
    public boolean rollback() {
    	if (commitAndRollbackEnabled) {
	    	try {
	    		if (!con.getAutoCommit()) {
	    			con.rollback();
	    		}
			} catch (SQLException e) {
				return false;
			}
    	}

    	return true;
    }
    
	public boolean dropAllTables(ProgressBarDialog progressDialog) {
		
		DatabaseMetaData meta;
		ResultSet tables;
		
		try {
			meta = con.getMetaData();
			tables = meta.getTables(null, null, null, null);
		} catch (SQLException e) {
//			e.printStackTrace();
			return false;
		}
		
		try {
			progressDialog.setNewMaxValue(tables.getFetchSize());
			
			while (tables.next()) {
				if (progressDialog.pendingCancelled()) {
					return false;
				}
				
				String tableName = tables.getString("TABLE_NAME");

				progressDialog.setMessage("Deleting table `" + tableName + "`");
				
				Statement statement = con.createStatement();
				statement.execute("DROP TABLE `" + tableName + "`");
				
				progressDialog.increment();
			}
		} catch (SQLException e) {
			return false;
		}
		
		
		return true;
	}
	
	public void createStatement() {
		
	}
	
	public boolean removeForeignKeyConstraint(String tableNameRaw, String constraintRaw) {
		

		String constraint = Column.makeAlphanumericAndUnderscores(constraintRaw);
		String tableName = Column.makeAlphanumericAndUnderscores(tableNameRaw);
		String sql = "ALTER TABLE `" + tableName + "` DROP FOREIGN KEY `" + constraint + "`";
		
		Statement st;
		try {
			st = con.createStatement();
			boolean rs = st.execute(sql);
			return rs;
		} catch (SQLException e) {
//			e.printStackTrace();
			return false;
		}
	}

	public boolean setColumnAsForeignKeyToTable(Column column, Table primaryKeyTable) {
		String tableDotColumnWithDoubleUnderscores = column.table.tableName.replaceAll("_", "__") + "_"
				+ column.columnName.replaceAll("_", "__"); 
		
		StringBuilder sql = new StringBuilder("ALTER TABLE ");
		sql.append(column.tableNameWithTicks());
		sql.append(" ADD CONSTRAINT ");
		sql.append("`fk_"); sql.append(tableDotColumnWithDoubleUnderscores); sql.append("`");
		sql.append(" FOREIGN KEY ("); sql.append(column.columnNameWithTicks()); sql.append(")");
		sql.append(" REFERENCES "); sql.append(primaryKeyTable.tableNameWithTicks());
		sql.append(" ("); sql.append(primaryKeyTable.idColumnNameWithTicks()); sql.append(")");
		sql.append(" ON DELETE RESTRICT ON UPDATE RESTRICT;");
		
		Statement st;
		try {
			st = con.createStatement();
			st.execute(sql.toString());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	/*
	 * ALTER TABLE  `Turbine` CHANGE  `blade`  `blade` BIGINT( 20 ) UNSIGNED NULL DEFAULT NULL ;
	 */
	public boolean setColumnDataTypeToMatchPrimaryKey(Column column) {
		StringBuilder sql = new StringBuilder("ALTER TABLE ");
		sql.append(column.tableNameWithTicks());
		sql.append(" CHANGE ");
		sql.append(column.columnNameWithTicks());
		sql.append(" ");
		sql.append(column.columnNameWithTicks());
		sql.append(" BIGINT( 20 ) UNSIGNED NULL DEFAULT NULL;");
		
		Statement st;
		try {
			st = con.createStatement();
			st.execute(sql.toString());
		} catch (SQLException e) {
			return false;
		}
		
		return true;
	}

	public boolean dropAllForiegnKeys(ProgressBarDialog progressDialog) {
		
		Connection schemaCon = getInfoSchemaConnection().con;
		
		String sql = "SELECT * FROM `KEY_COLUMN_USAGE` WHERE `TABLE_SCHEMA`=\""+databaseName+"\" AND `REFERENCED_TABLE_NAME` IS NOT NULL AND `REFERENCED_COLUMN_NAME` IS NOT NULL";
		
		Statement st; ResultSet rs;
		
		try {
			st = schemaCon.createStatement();
			rs = st.executeQuery(sql);
			
			progressDialog.setNewMaxValue(rs.getFetchSize());

			while (rs.next()) {
				
				if (progressDialog.pendingCancelled()) {
					return false;
				}
				
				String tableName = rs.getString(META_COLUMNS_TABLE_NAME);
				String constraintName = rs.getString(META_COLUMNS_CONSTRAINT_NAME);
				
				progressDialog.setMessage("Deleting constraint \"" + constraintName + "\" from table \"" + constraintName + "\"");
				
				removeForeignKeyConstraint(Column.makeAlphanumericAndUnderscores(tableName), constraintName);

				progressDialog.increment();
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
//			e.printStackTrace();
			return false;
		}

		return true;
	}

	public String getHost() {
		return host;
	}
	
	public String getUser() {
		return user;
	}
	
	public String getPassword() {
		return password;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public String getShowCreate(Table table) {
		String sql = "SHOW CREATE TABLE " + table.tableNameWithTicks();
			
		Statement st;
		
		String result = null;
		
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			
			if (rs.getFetchSize() > 0) {
				rs.next();
				
				result = rs.getString(1);
			}
			
		} catch (SQLException e) {
//			e.printStackTrace();
		}
		
		return result;
	}

	public Column[] getShowColumns(Table table) {
		String sql = "SHOW COLUMNS FROM " + table.tableNameWithTicks();
		
		Statement st;
		
		Column[] result = new Column[0];
		
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			
			result = new Column[rs.getFetchSize()];
			
			int index = 0;
			
			while (rs.next()) {
				String nextColumnName = rs.getString(1);
				result[index] = new Column(table, nextColumnName);
				
				index++;
			}
		} catch (SQLException e) {
//			e.printStackTrace();
		}
		
		return result;
	}
	
	public Column[] getAllColumnsInTable(Table table) {
		ArrayList<Object[]> columnInfo = getColumnInfo("COLUMNS", table, new String[] {
				SQLEngine.META_COLUMNS_COLUMN_NAME,
				SQLEngine.META_COLUMNS_DATA_TYPE,
				SQLEngine.META_COLUMNS_IS_NULLABLE,
				SQLEngine.META_COLUMNS_COLUMN_DEFAULT,
				SQLEngine.META_COLUMNS_COLUMN_KEY,
				SQLEngine.META_COLUMNS_NUMERIC_PRECISION,
				SQLEngine.META_COLUMNS_NUMERIC_SCALE,
				SQLEngine.META_COLUMNS_CHARACTER_MAXIMUM_LENGTH});
		
		int columnNameIndex = 0; int dataTypeIndex = 1; int isNullableIndex = 2;
		int columnDefaultIndex = 3; int columnKeyIndex = 4; int numericalPrecisionIndex = 5;
		int numericalScaleIndex = 6; int charMaxLengthIndex = 7;

		int columnInfoCount = columnInfo.size();
		
		final Column[] columns = new Column[columnInfoCount];
		
		for (int i=0; i<columnInfoCount; i++) {
			Object[] nextInfoSet = columnInfo.get(i);
			
			String columnName = (String) nextInfoSet[columnNameIndex];
			String dataType = (String) nextInfoSet[dataTypeIndex];
			String isNullable = (String) nextInfoSet[isNullableIndex];
			String columnDefault = (String) nextInfoSet[columnDefaultIndex];
			String columnKey = (String) nextInfoSet[columnKeyIndex];
			BigInteger numericalPrecision = (BigInteger) nextInfoSet[numericalPrecisionIndex];
			BigInteger numericalScale = (BigInteger) nextInfoSet[numericalScaleIndex];
			BigInteger charMaxLength = (BigInteger) nextInfoSet[charMaxLengthIndex];
			
			Column column = new Column(table, columnName);
			column.setTypeString(dataType);
			
			CalibratorGenerator.addCalibrator(column);
			

			
			if (columnName.toLowerCase().equals("id")) {
				column.setType(java.sql.Types.BIGINT);
				column.setNullAllowed(true);
				column.setDefaultValue(null);
			}
			else {
				column.setNullAllowed(isNullable.toUpperCase().equals("YES"));
				column.setUnique(columnKey.toUpperCase().equals(UNIQUE));

				if (columnDefault != null && columnDefault.equals("CURRENT_TIMESTAMP")) {
					column.setDefaultValue(null);
				}
				else {
					column.setDefaultValue(columnDefault);
				}
			}
			
			if (charMaxLength != null) {
				column.setMaxStringLength(((BigInteger) charMaxLength).intValue());
			}
			
			if (numericalPrecision != null && numericalScale != null) {
				int scale = ((BigInteger) numericalScale).intValue();
				int precision = ((BigInteger) numericalPrecision).intValue();
				column.setDecimalCountRightOfPoint(scale);
				column.setDecimalCountLeftOfPoint(precision - scale);
			}
			
			columns[i] = column;
		}
		
		return columns;
	}

	public HashMap<UniqueColumnGroup, Table> getAllUniqueColumnGroups(String databaseName) {
		
		String sql = "SELECT GROUP_CONCAT(CONCAT(`TABLE_NAME`,\".\",`COLUMN_NAME`) SEPARATOR \",\")"
	      + " FROM `STATISTICS`"
	      + " WHERE `TABLE_SCHEMA`=\"" + databaseName + "\""
	      + " AND `NON_UNIQUE`=\"0\""
	      + " AND `INDEX_NAME`!=\"PRIMARY\""
	      + " GROUP BY `TABLE_NAME`,`INDEX_NAME`";
		
		Statement st;
		
		final String COMMA_REGEX = Pattern.quote(",");
		final String DOT_REGEX = Pattern.quote(".");
		
		HashMap<UniqueColumnGroup, Table> allUniqueGroups = new HashMap<>();
		
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(sql);
			
			while (rs.next()) {
				String nextGroupStr = rs.getString(1);
				String[] nextGroupArray = nextGroupStr.split(COMMA_REGEX);
				
				UniqueColumnGroup nextGroup = new UniqueColumnGroup();
				
				for (int i=0; i<nextGroupArray.length; i++) {
					String[] tableThenColumn = nextGroupArray[i].split(DOT_REGEX);
					String tableName = tableThenColumn[0], columnName = tableThenColumn[1];

					Column nextColumn = new Column(tableName, columnName);
					nextGroup.addColumn(nextColumn);
				}
				
				allUniqueGroups.put(nextGroup, nextGroup.getColumnAt(0).table);
			}
			
		} catch (SQLException e) {
//			e.printStackTrace();
		}
		
		
		// TODO Auto-generated method stub
		return allUniqueGroups;
	}
}
