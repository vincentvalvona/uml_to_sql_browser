package uk.co.solidcoresoftware.databaseapp.libraries.core.models;

public interface Data {
	public Object[][] getData();
	public void update();
}
