package uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql;

import java.sql.SQLException;

public class SQLEngineExceptionWrapper {
	
	private SQLException exception;

	public void setException(SQLException exception) {
		this.exception = exception;
	}
}
