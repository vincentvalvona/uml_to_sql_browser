package uk.co.solidcoresoftware.databaseapp.libraries.core.models;

import java.util.ArrayList;

public class UniqueColumnGroup {
	private final ArrayList<Column> columns = new ArrayList<>();
	
	public void addColumn(Column column) {
		columns.add(column);
	}
	
	public boolean isEmpty() {
		return columns.isEmpty();
	}
	
	public int getColumnCount() {
		return columns.size();
	}
	
	public Column getColumnAt(int index) {
		return columns.get(index);
	}
	
	public String getColumnsNamesJoinedWithBackticks() {
		return Column.joinColumnNamesWithTicks(Column.arrayListToArray(columns), true);
	}

	public boolean containsSinglePrimaryKey() {
		
		if (getColumnCount() == 1) {
			Column onlyColumn = columns.get(0);
			
			if (onlyColumn.isPrimary()) {
				return true;
			}
		}
		
		return false;
	}
}
