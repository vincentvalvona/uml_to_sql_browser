package uk.co.solidcoresoftware.databaseapp.libraries.core.models;

public class ValueToAlwaysSave {
	
	public final Object value; 
	
	public ValueToAlwaysSave(Object value) {
		this.value = value;
	}
}
