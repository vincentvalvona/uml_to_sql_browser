package uk.co.solidcoresoftware.databaseapp.libraries.core.models.items;

import java.math.BigDecimal;
import java.util.HashMap;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;

public abstract class AllItemsOfType<T extends ItemOfType> {
	
	// All items are stored here
	//protected ArrayList<T> allItems = new ArrayList<T>();
	protected HashMap<Long, T> idToItemMap = new HashMap<Long, T>();
	
	public int columnIdIndex = -1;
	public int columnUnitPriceIndex = -1;
	public int columnUnitTimeIndex = -1;
	public int columnTotalTimeIndex = -1;
	
	public void addItem(T item, long id) {
		idToItemMap.put(id, item);
	}
	
	public T getItem(long id) {
		return idToItemMap.get(id);
	}
	

	public void clearItems() {
		idToItemMap.clear();
	}
	
	private void addItems(LightTableModel tableModel) {
		Object[][] data = tableModel.getData();

		for (int i=0; i<data.length; i++) {
			Object[] row = data[i];
			
			// Getting the id of this row
			Long id = (Long) row[columnIdIndex];
			
			BigDecimal unitPrice;
			Long unitTime;
			Long totalTime;
			
			if (columnUnitPriceIndex == -1) {
				unitPrice = BigDecimal.ZERO;
			}
			else {
				unitPrice = (BigDecimal) row[columnUnitPriceIndex]; 
			}

			if (columnUnitTimeIndex == -1) {
				unitTime = 0l;
			}
			else {
				unitTime = (Long) row[columnUnitTimeIndex];
			}
			

			if (columnTotalTimeIndex == -1) {
				totalTime = 0l;
			}
			else {
				totalTime = (Long) row[columnTotalTimeIndex];
			}
			
			// If the id is null, then this is skipped
			if (id == null) continue;
			
			T item = idToItemMap.get(id);
			if (item == null) {
				item = createItem(id);
			}

			item.setUnitPrice(unitPrice);
			item.setUnitTime(unitTime);
			item.setTotalTime(totalTime);
			item.setRow(row);
			
			idToItemMap.put(item.id, item);
		}
	}
	
	protected abstract T createItem(long id);

	
	protected void populateItems(LightTableModel tableModel, Column idColumn, Column unitPriceColumn, Column unitTimeColumn, Column totalTimeColumn) {
		
		clearItems();
		
		columnIdIndex = tableModel.getColumnIndex(idColumn);

		if (unitPriceColumn == null) {
			columnUnitPriceIndex = -1;
		}
		else {
			columnUnitPriceIndex = tableModel.getColumnIndex(unitPriceColumn);
		}
		
		if (unitTimeColumn == null) {
			columnUnitTimeIndex = -1;
		}
		else {
			columnUnitTimeIndex = tableModel.getColumnIndex(unitTimeColumn);
		}
		

		if (totalTimeColumn == null) {
			columnTotalTimeIndex = -1;
		}
		else {
			columnTotalTimeIndex = tableModel.getColumnIndex(totalTimeColumn);
		}
		
		// Adding all items to the list
		addItems(tableModel);
	}
	
	public abstract void populateItems();
}
