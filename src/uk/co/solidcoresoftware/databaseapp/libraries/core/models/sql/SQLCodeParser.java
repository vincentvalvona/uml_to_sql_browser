package uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class SQLCodeParser {

	boolean fileFailedToLoad = false;
	private String[] statements;

	private LinkedList<Segment> segments = new LinkedList<>();
	Segment currentSegment;
	
	private int charPos;
	private int bracketLevel;
	private int currentStatus;
	
	private static int statusCount = 0;
	private final static int STATUS_NORMAL = statusCount++;
	private final static int STATUS_IN_SINGLE_Q = statusCount++;
	private final static int STATUS_IN_DOUBLE_Q = statusCount++;
	private final static int STATUS_IN_COMMENT = statusCount++;
	private final static int STATUS_IN_LINE_COMMENT = statusCount++;

	private static int segmentTypeCount = 0;
	public final static int SEGMENT_TYPE_NORMAL = segmentTypeCount++;
	public final static int SEGMENT_TYPE_COMMENT = segmentTypeCount++;
	public final static int SEGMENT_TYPE_COMMENT_LINE = segmentTypeCount++;
	public final static int SEGMENT_TYPE_STRING_SINGLE_Q = segmentTypeCount++;
	public final static int SEGMENT_TYPE_STRING_DOUBLE_Q = segmentTypeCount++;
	public final static int SEGMENT_TYPE_SEMICOLON = segmentTypeCount++;
	public final static int SEGMENT_TYPE_OPEN_BRACKET = segmentTypeCount++;
	public final static int SEGMENT_TYPE_CLOSED_BRACKET = segmentTypeCount++;
	public final static int SEGMENT_TYPE_COMMA = segmentTypeCount++;
	
	
	private final static String LINE_SEP = System.lineSeparator();
	private final static int LINE_SEP_LENGTH = LINE_SEP.length();
	
	
	private void newSegment(int segmentType, int charPos) {
		currentSegment = new Segment(segmentType, charPos);
		currentSegment.setBracketLevel(bracketLevel);
		segments.add(currentSegment);
	}
	
	private void readNextLine(String nextLine) {
		if (STATUS_IN_LINE_COMMENT == currentStatus) {
			currentStatus = STATUS_NORMAL;
			newSegment(SEGMENT_TYPE_NORMAL, charPos);
		}
		else {
			newSegment(currentSegment.segmentType, charPos);
		}
		
		char[] chars = nextLine.toCharArray();

		for (int charIndex=0; charIndex<chars.length; charIndex++) {
			char c = chars[charIndex];
			
			boolean addCharAtEnd = true;
			
			if (STATUS_IN_COMMENT == currentStatus) {
				// Leaving a comment, */
				if (c == '/') {
					if (charIndex != 0 && chars[charIndex-1] == '*') {
						currentSegment.addChar(c);
						addCharAtEnd = false;
						currentStatus = STATUS_NORMAL; newSegment(SEGMENT_TYPE_NORMAL, charPos+1);
					}
				}
			}
			else if (STATUS_IN_SINGLE_Q == currentStatus) {
				if (c == '\\') {
					currentSegment.toggleInsideEscape();
				}
				else if (currentSegment.isInsideEscape()) {
					currentSegment.toggleInsideEscape();
				}
				// Leaving a single quote string.
				else if (c == '\'') {
					currentSegment.addChar(c);
					addCharAtEnd = false;
					currentStatus = STATUS_NORMAL; newSegment(SEGMENT_TYPE_NORMAL, charPos+1);
				}
			}
			else if (STATUS_IN_DOUBLE_Q == currentStatus) {
				if (c == '\\') {
					currentSegment.toggleInsideEscape();
				}
				else if (currentSegment.isInsideEscape()) {
					currentSegment.toggleInsideEscape();
				}
				// Leaving a double quote string.
				else if (c == '\"') {
					currentSegment.addChar(c);
					addCharAtEnd = false;
					currentStatus = STATUS_NORMAL; newSegment(SEGMENT_TYPE_NORMAL, charPos+1);
				}
			}
			else if (STATUS_NORMAL == currentStatus){
				// About to enter a single quote string
				if (c == '\'') {
					addCharAtEnd = false;
					currentStatus = STATUS_IN_SINGLE_Q; newSegment(SEGMENT_TYPE_STRING_SINGLE_Q, charPos);
					currentSegment.addChar(c);
				}
				// About to enter a double quote string
				else if (c == '\"') {
					addCharAtEnd = false;
					currentStatus = STATUS_IN_DOUBLE_Q; newSegment(SEGMENT_TYPE_STRING_DOUBLE_Q, charPos);
					currentSegment.addChar(c);
				}
				// About to enter a comment quote. /*
				else if (c == '*') {
					if (charIndex != 0 && chars[charIndex-1] == '/') {
						addCharAtEnd = false;
						currentSegment.removeLastChar();
						currentStatus = STATUS_IN_COMMENT; newSegment(SEGMENT_TYPE_COMMENT, charPos-1);
						currentSegment.addChar('/'); currentSegment.addChar('*');
					}
				}
				else if (c == '-') {
					if (charIndex != 0 && chars[charIndex-1] == '-') {
						addCharAtEnd = false;
						currentSegment.removeLastChar();
						currentStatus = STATUS_IN_LINE_COMMENT; newSegment(SEGMENT_TYPE_COMMENT_LINE, charPos-1);
						currentSegment.addChar('-'); currentSegment.addChar('-');
					}
				}
				else if (c == '#') {
					addCharAtEnd = false;
					currentStatus = STATUS_IN_LINE_COMMENT; newSegment(SEGMENT_TYPE_COMMENT_LINE, charPos);
					currentSegment.addChar('#');
				}
				else if (c == ';') {
					newSegment(SEGMENT_TYPE_SEMICOLON, charPos);
					currentSegment.addChar(c);
					addCharAtEnd = false;
					newSegment(SEGMENT_TYPE_NORMAL, charPos+1);
				}
				else if (c == '(') {
					newSegment(SEGMENT_TYPE_OPEN_BRACKET, charPos);
					currentSegment.addChar(c);
					bracketLevel++;
					addCharAtEnd = false;
					newSegment(SEGMENT_TYPE_NORMAL, charPos+1);
				}
				else if (c == ')') {
					bracketLevel--;
					newSegment(SEGMENT_TYPE_CLOSED_BRACKET, charPos);
					currentSegment.addChar(c);
					addCharAtEnd = false;
					newSegment(SEGMENT_TYPE_NORMAL, charPos+1);
				}
				else if (c == ',') {
					newSegment(SEGMENT_TYPE_COMMA, charPos);
					currentSegment.addChar(c);
					addCharAtEnd = false;
					newSegment(SEGMENT_TYPE_NORMAL, charPos+1);
				}
			}
			
			// Adds character if allowed
			if (addCharAtEnd) {
				currentSegment.addChar(c);
			}
			charPos++;
		}
		
		charPos += LINE_SEP_LENGTH;
		currentSegment.increaseLineSepChars(LINE_SEP_LENGTH);
	}
	
	private void resetFields() {
		charPos = 0;
		bracketLevel = 0;
		
		currentStatus = STATUS_NORMAL;

		segments.clear();
		newSegment(SEGMENT_TYPE_NORMAL, charPos);
	}
	
	public void parseString(String str) {
		resetFields();
		
		String[] strAsLines = str.split(Pattern.quote(LINE_SEP));
		
		StringBuilder sb = new StringBuilder();
		sb.ensureCapacity(2000);
		
		
		for (int i=0; i<strAsLines.length; i++) {
			readNextLine(strAsLines[i]);
		}
	}
	
	public void parseFile(File sqlFile) {
		resetFields();
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(sqlFile));
			
			StringBuilder sb = new StringBuilder();
			sb.ensureCapacity(2000);
			
			String nextLine;
			while ((nextLine = reader.readLine()) != null) {
				readNextLine(nextLine);
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			fileFailedToLoad = true;
		} catch (IOException e) {
			e.printStackTrace();
			fileFailedToLoad = true;
		}
	}
	
	
	public String[] getStatements() {
		return statements;
	}
	
	public class Segment {
		public final int segmentType;
		public final int startCharPos;
		
		private boolean insideEscape = false;
		
		private final LinkedList<Character> chars;
		private int totalLineSepChars;
		private int bracketLevel;
		
		public Segment(int segmentType, int startCharPos) {
			this.segmentType = segmentType;
			this.startCharPos = startCharPos;
			chars = new LinkedList<>();
		}
		
		public void increaseLineSepChars(int lineSepLength) {
			this.totalLineSepChars += lineSepLength;
		}

		public void toggleInsideEscape() {
			insideEscape = !insideEscape;
		}
		
		public boolean isInsideEscape() {
			return insideEscape;
		}

		public void removeLastChar() {
			chars.removeLast();
		}

		public void addChar(char c) {
			chars.add(c);
		}
		
		public int length() {
			return chars.size();
		}

		public int lengthIncludingLineSeps() {
			return chars.size() + totalLineSepChars;
		}

		public int getTotalLineSepChars() {
			return totalLineSepChars;
		}
		

		public void setBracketLevel(int bracketLevel) {
			this.bracketLevel = bracketLevel;
		}
		
		public int getBracketLevel() {
			return bracketLevel;
		}
		
		@Override
		public String toString() {
			
			StringBuilder sb = new StringBuilder(chars.size());
			
			Iterator<Character> charsIt = chars.iterator();
			
			while (charsIt.hasNext()) {
				sb.append(charsIt.next());
			}
			return sb.toString();
		}
	}
	
	public LinkedList<Segment> getSegments() {
		return segments;
	}
}
