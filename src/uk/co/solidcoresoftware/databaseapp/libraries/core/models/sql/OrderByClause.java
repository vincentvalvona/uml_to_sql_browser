package uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;

public class OrderByClause {
	
	public final static int ORDER_BY_ASC = 0;
	public final static int ORDER_BY_DESC = 1;
	
	private final Column column;
	private final int orderBy; 
	
	public OrderByClause(Column column, int orderBy) {
		this.column = column;
		this.orderBy = orderBy;
	}
	
	public String getSQL() {
		return column.tableDotColumnWithTicks() + (ORDER_BY_DESC == orderBy ? " DESC" : " ASC");
	}
	
	public static String getOrderByStatement(OrderByClause[] orderByClauses) {
		if (orderByClauses == null || orderByClauses.length == 0) {
			return "";
		}
		// ORDER BY key_part1 DESC, key_part2 DESC;
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(" ORDER BY ");
		sqlBuilder.append(orderByClauses[0].getSQL());
		
		for (int i=1; i<orderByClauses.length; i++) {
			sqlBuilder.append(", ");
			sqlBuilder.append(orderByClauses[i].getSQL());
		}
		
		return sqlBuilder.toString();
	}
}
