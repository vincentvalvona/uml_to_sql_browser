package uk.co.solidcoresoftware.databaseapp.libraries.core.models;


public abstract class AbstractColumnCalibrator {
	

	protected Column column;
	
	public final void setColumn(Column column) {
		this.column = column;
	}
	
	public abstract Object calibrateToValue(Object input);
	
	public abstract Object calibrateFromValue(Object input);
	
	public abstract String getCellEditingText(Object input);
	
	public abstract String getCellRenderingText(Object input);
}
