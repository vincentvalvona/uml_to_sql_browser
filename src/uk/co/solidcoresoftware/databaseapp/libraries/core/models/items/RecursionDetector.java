package uk.co.solidcoresoftware.databaseapp.libraries.core.models.items;

public class RecursionDetector {
	
	/* For simplicity, a set it assumed to be recursive if it's
	 * at least this many levels deep
	 * 
	 */
	private final Object[][] data;
	private final int deepestLevelAllowed;
	private final int containingIndex;
	private final int containedIndex;
	
	private int deepestLevel = 0;
	
	private boolean exceededDeepestLevel = false;

	public RecursionDetector(Object[][] data, int deepestLevelAllowed, int containingIndex, int containedIndex) {
		this.data = data;
		this.deepestLevelAllowed = deepestLevelAllowed;
		this.containingIndex = containingIndex;
		this.containedIndex = containedIndex;
	}
	
	public boolean isRecursive(Object initialContainingValue) {
		iterateOverArray(0, initialContainingValue);
		
		return exceededDeepestLevel;
	}
	
	private void iterateOverArray(int level, Object containingValueToCompare) {
		
		if (level > deepestLevelAllowed) {
			exceededDeepestLevel = true; return;
		}
		
		// Making sure the deepest level is the max level explored
		if (level > deepestLevel) deepestLevel = level;
		
		for (int i=0; i<data.length; i++) {
			Object[] row = data[i];
			
			Object containingValue = row[containingIndex];
			
			if (containingValue == null) continue;
			else if (containingValue.equals(containingValueToCompare)) {
				Object containedValue = row[containedIndex];
				
				if (containedValue == null) continue;
				
				iterateOverArray(level+1, containedValue);
			}
		}
	}
}
