package uk.co.solidcoresoftware.databaseapp.libraries.core.models;


public class Scenario {

	public final Option[] options;
	public final String question;
	public final String title;
	
	public final static Scenario[] noScenarios = new Scenario[0];
	
	private String suggestion = null;
	private boolean suggestionReadOnly = false;
	
	public Scenario(Option[] options, String question, String title) {
		this.options = options;
		this.question = question;
		this.title = title;
	}
	
	public boolean suggestionExists() {
		return suggestion != null;
	}
	
	public String getSuggestion() {
		return suggestion;
	}
	
	public void setSuggestion(String suggestion) {
		this.suggestion = suggestion;
	}
	

	public boolean getSuggestionReadOnly() {
		return suggestionReadOnly;
	}
	
	public void setSuggestionReadOnly(boolean readOnly) {
		this.suggestionReadOnly = readOnly;
	}
	
}
