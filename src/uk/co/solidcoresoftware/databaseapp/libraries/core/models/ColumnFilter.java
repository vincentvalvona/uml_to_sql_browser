package uk.co.solidcoresoftware.databaseapp.libraries.core.models;


public class ColumnFilter {

	private final static int EXACT_MATCH = 0;
	private final static int MATCHES_LEFT = 1;
	private final static int MATCHES_RIGHT = 2;
	
	private final DataType column;
	private final String filter;
	private final int filterType;
	
	public ColumnFilter(DataType column, String filter) {
		this(column, filter, EXACT_MATCH);
	}
	
	
	public ColumnFilter(DataType column, String filter, int filterType) {
		this.column = column;
		this.filter = filter;
		this.filterType = filterType;
	}
}
