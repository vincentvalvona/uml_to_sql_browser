package uk.co.solidcoresoftware.databaseapp.libraries.core.models;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.regex.Pattern;

import growoffshore.Enumeration;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLEngine;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLInjectionEscaper;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;

public class Column {
	
	
	public final Table table;
	public final String columnName;
	
	private boolean columnIsFake = false;
	
	private int type = java.sql.Types.VARCHAR;

	private String validFormat = null;
	private Pattern validFormatChecker = null;
	
	private boolean valueReadOnly = false; 
	
	public static final String VALID_FORMAT_INTEGER = "\\-?[0-9]+";
	public static final String VALID_FORMAT_CURRENCY_POUNDS = "\\£?\\-?[0-9]*\\.?[0-9]*p?";
	public static final String VALID_FORMAT_DECIMAL = "[0-9]*\\.?{0,1}[0-9]*";
	
	private static int valueRangeCount = 0;
	public static final int VALUE_RANGE_OK = valueRangeCount++;
	public static final int VALUE_RANGE_ABOVE_MAX = valueRangeCount++;
	public static final int VALUE_RANGE_BELOW_MIN = valueRangeCount++;
	public static final int VALUE_RANGE_STRING_TOO_LONG = valueRangeCount++;
	
	boolean hasDefaultValue = false;
	private Object defaultValue = null;
	private boolean resetValue = true;
	
	private String title = null;
	
	private ArrayList<Column> otherColumnsToModify = new ArrayList<Column>();
	
	private ArrayList<String> foreignKeys = new ArrayList<String>();
	
	private String aliasOf = null;
	private Object autoValue;
	private boolean autoValueSet;
	
	private int decimalScale;
	private boolean decimalScalingOn;
	private boolean defaultValueValid = true;
	
	private AbstractColumnCalibrator calibrator;
	
	// Null substitute
	private Object nullSubstitute = null;
	private boolean usesNullSubstitute = false;
	private String typeString;
	private Enumeration typeEnum;
	private boolean nullAllowed = true;
	private boolean unique = false;
	
	private Integer maxStringLength = null;
	
	private BigDecimal maxValue; private boolean inclusiveOfMaxValue;
	private BigDecimal minValue; private boolean inclusiveOfMinValue;

	public Column(String tableName, String columnName) {
		this(new Table(tableName), columnName);
	}
	
	public Column(Table table, String columnNameRaw) {
		/* Unneeded characters are stipped out. This is
		 * to prevent SQL injection.
		 */
		this.table = table;
		this.columnName = makeAlphanumericAndUnderscores(columnNameRaw);
		
		if (columnName.equals(table.idColumnName)) {
			setValueReadOnly(true);
			setType(java.sql.Types.BIGINT);
			setDefaultValue(null);
		}
		else {
			setType(java.sql.Types.VARCHAR);
		}
	}
	

	public Column(String tableName, String columnName, String validFormat) {
		this(tableName, columnName);
		setValidFormat(validFormat);
	}
	

	public Column(String tableName, String columnName, String validFormat, String title) {
		this(tableName, columnName);
		setValidFormat(validFormat);
		setTitle(title);
	}
	

	public static String makeAlphanumericAndUnderscores(String str) {
		return str.replaceAll("[^a-zA-Z0-9_]","");
	}
	
	public static String makeAlphanumeric(String str) {
		return str.replaceAll("[^a-zA-Z0-9]","");
	}
	
	public boolean isAutomaticValueSet() {
		return autoValueSet;
	}
	
	public Object getAutomaticValue() {
		return autoValue;
	}
	
	public void setAutomaticValue(Object value) {
		autoValue = value;
		autoValueSet = true;
	}
	
	public void removeAutomaticValue() {
		autoValueSet = false;
	}
	
	public void setValidFormat(String validFormat) {
		this.validFormat = validFormat;
		this.validFormatChecker = validFormat == null ? null : Pattern.compile(validFormat);
	}
	
	public void setValueReadOnly(boolean readOnly) {
		this.valueReadOnly = readOnly;
	}
	
	public boolean isValueReadOnly() {
		return valueReadOnly;
	}
	
	public boolean isCellActive(Object[] row) {
		return true;
	}
	
	public int getType() {
		return type;
	}
	
	
	public Integer convertInputToInteger(String input) {
		return Integer.parseInt(input);
	}
	

	public Long convertInputToLong(String input) {
		return Long.parseLong(input);
	}
	
	public void setType(int type) {
		this.type = type;
		if (isNullAllowed()) {
			setDefaultValue(null);
		}
		else {
			setDefaultValue(getAbsoluteDefault());
		}
	}
	
	public Object getAbsoluteDefault() {
		Object defaultValue;
		
		switch (type) {
			case java.sql.Types.BOOLEAN: defaultValue = new Boolean(false); break;
			case java.sql.Types.INTEGER: defaultValue = new Integer(0); break;
			case java.sql.Types.BIGINT: defaultValue = new Long(0); break;
			case java.sql.Types.FLOAT: defaultValue = new Float(0); break;
			case java.sql.Types.DECIMAL: defaultValue = decimalToCorrectFormat(BigDecimal.ZERO); break;
			case DataType.INT_TYPE_DATETIME: defaultValue = null;
			case java.sql.Types.TIMESTAMP: defaultValue = null;
			case java.sql.Types.TIME: defaultValue = null;
			case java.sql.Types.DATE: defaultValue = null;
			default: defaultValue = ""; break;
		}
		
		return defaultValue;
	}
	
	private boolean needsToBeEscaped() {
		switch (type) {
			case java.sql.Types.BOOLEAN: case java.sql.Types.INTEGER: case java.sql.Types.BIGINT:
			case java.sql.Types.FLOAT: case java.sql.Types.DECIMAL: case DataType.INT_TYPE_DATETIME:
			case java.sql.Types.TIMESTAMP: case java.sql.Types.TIME: case java.sql.Types.DATE:
				// Numeric types, dates and times don't need escaping
				return false;
			default:
				// Any String or other data-types need to be escaped
				return true;
		}
	}
	
	public boolean isTextType() {
		return needsToBeEscaped();
				
	}
	
	public boolean isDateOrTime() {
		return DataType.INT_TYPE_DATETIME == type
				|| java.sql.Types.TIMESTAMP == type
				|| java.sql.Types.TIME == type
				|| java.sql.Types.DATE == type;
	}
	
	public boolean isBoolean() {
		return java.sql.Types.BOOLEAN == type;
	}
	
	public boolean isNonBooleanNumericInput() {
		switch (type) {
			case java.sql.Types.INTEGER: case java.sql.Types.BIGINT:
			case java.sql.Types.FLOAT: case java.sql.Types.DECIMAL:
				return true;
			default:
				// Strings, dates and times need quotes
				return false;
		}
	}
	
	public BigDecimal getBigDecimalValue(Object obj) {
		
		if (obj instanceof BigDecimal) {
			return (BigDecimal) obj;
		}
		if (obj instanceof Long) {
			return new BigDecimal((Long) obj);
		}
		else if (obj instanceof Integer) {
			return new BigDecimal((Integer) obj);
		}
		else if (obj instanceof Float) {
			return new BigDecimal((Float) obj);
		}
		else if (obj instanceof Double) {
			return new BigDecimal((Double) obj);
		}
		
		return BigDecimal.ZERO;
	}
	

	private boolean needsQuotes() {
		switch (type) {
			case java.sql.Types.BOOLEAN: case java.sql.Types.INTEGER: case java.sql.Types.BIGINT:
			case java.sql.Types.FLOAT: case java.sql.Types.DECIMAL:
				return false;
			default:
				// Strings, dates and times need quotes
				return true;
		}
	}
	
	public void setTypeString (String type) {
		
		type = type.toUpperCase();
		
		// This is a boolean
		if (type.equals(DataType.S_TYPE_TINYINT) || type.equals(DataType.S_TYPE_BIT)) {
			this.typeString = "BOOLEAN";
			setType(java.sql.Types.BOOLEAN);
		}
		else {
			this.typeString = type.replaceAll("[^A-Z0-9_\\(\\)],","");
			setType(DataType.sTypeToIntType(typeString));
		}
		
	}
	
	public void setTypeEnum (Enumeration type) {
		this.typeEnum = type;
		if (type != null) {
			setType(java.sql.Types.VARCHAR);
		}
	}
	
	public String getTypeString() {
		
		if (typeString == null) return null;
		
		String typeStringTrim = typeString.trim();
		
		if (typeStringTrim.equals(DataType.S_TYPE_VARCHAR) || typeStringTrim.equals(DataType.S_TYPE_CHAR)) {
			return typeStringTrim + "(" + getMaxStringLength() + ")";
		}
		
		return typeString;
	}
	
	public Enumeration getTypeEnum() {
		return typeEnum;
	}

	
	public String getCustomTypeInDatabase() {
		return typeString;
	}
	
	public boolean hasCustomTypeInDatebase() {
		return typeString != null;
	}
	
	public final void makeColumnFake() {
		columnIsFake = true;
	}
	
	public final boolean isFakeColumn() {
		return columnIsFake;
	}
	
	
	public final String tableDotPrimaryColumn() {
		return table.tableName + '.' + table.idColumnName;
	}

	public final String tableDotColumn() {
		return table.tableName + '.' + columnName;
	}
	
	@Override
	public int hashCode() {
		return toString().hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Column) {
			return toString().equals(obj.toString());
		}
		
		return false;
	}
	
	@Override
	public String toString() {
		return tableDotColumn();
	}
	
	public static final String joinTableNames(Column[] columns) {
		if (columns.length == 1) {
			return columns[0].table.tableNameWithTicks();
		}
		else {
			StringBuilder joinedNames = new StringBuilder(columns[0].table.tableNameWithTicks());
			
			for (int i=1; i<columns.length; i++) {
				joinedNames.append(',');
				joinedNames.append(columns[i].table.tableNameWithTicks());
			}
			
			return joinedNames.toString();
		}
	}

	

	public static final String joinColumnNamesWithTicks(Column[] columns) {
		return joinColumnNamesWithTicks(columns, false);
	}
	
	public static final String joinColumnNamesWithTicks(Column[] columns, final boolean uniqueLengthForText) {
		return joinColumnNamesWithTicksAndSuffix(columns, "", uniqueLengthForText);
	}
	
	public static final String joinColumnNamesWithTicksAndSuffix(Column[] columns, String suffix, final boolean uniqueLengthForText) {
		StringBuilder joinedNames = new StringBuilder();
		
		for (int i=0; i<columns.length; i++) {
			if (i != 0) {
				joinedNames.append(',');
			}
			joinedNames.append(columns[i].columnNameWithTicks());
			
			if (uniqueLengthForText) {
				joinedNames.append(columns[i].uniqueLengthInBracketsIfText());
			}
			joinedNames.append(suffix);
		}
		
		return joinedNames.toString();
	}


	private String uniqueLengthInBracketsIfText() {
		Long maxLengthIfText = DataType.getLengthIfText(type);
		if (null != maxLengthIfText) {
			return "(" + maxLengthIfText + ")";
		}
		
		return "";
	}

	public void setAsAliasOf(String aliasOf) {
		this.aliasOf = aliasOf;
		setValueReadOnly(true);
	}
	
	public static final String joinTableAndColumnNamesWithAliases(Column[] columns) {
		if (columns.length == 1) {
			return columns[0].tableDotColumnWithTicks();
		}
		else {
			StringBuilder joinedNames = new StringBuilder(columns[0].tableDotColumnWithTicks());
			
			for (int i=1; i<columns.length; i++) {
				joinedNames.append(',');
				if (columns[i].aliasOf == null) {
					joinedNames.append(columns[i].tableDotColumnWithTicks());
				}
				else {
					joinedNames.append(columns[i].aliasOfWithTicks());
//					joinedNames.append(" AS ");
//					joinedNames.append(columns[i].toString());
				}
			}
			
			return joinedNames.toString();
		}
	}
	
	public static final String joinTableAndColumnNames(Column[] columns) {
		if (columns.length == 1) {
			return columns[0].tableDotColumnWithTicks();
		}
		else {
			StringBuilder joinedNames = new StringBuilder(columns[0].tableDotColumnWithTicks());
			
			for (int i=1; i<columns.length; i++) {
				joinedNames.append(',');
				joinedNames.append(columns[i].tableDotColumnWithTicks());
			}
			
			return joinedNames.toString();
		}
	}
	

	public static final String joinQuestionMarks(Column[] columns) {
		if (columns.length == 1) {
			return "?";
		}
		else {
			StringBuilder joinedNames = new StringBuilder("?");
			
			for (int i=1; i<columns.length; i++) {
				joinedNames.append(",?");
			}
			
			return joinedNames.toString();
		}
	}
	
	public Object getDefaultValue() {
		Object defaultValueToUse;
		
		if (isAutomaticValueSet()) {
			defaultValueToUse = getAutomaticValue();
		}
		else {
			defaultValueToUse = this.defaultValue;
		}
		
		if (defaultValueToUse == null && !isNullAllowed()) {
			return getAbsoluteDefault();
		}
		else {
			return defaultValueToUse;
		}
	}
	
	/**
	 * Sets the default value of this column. The value's type is
	 * also set if it's not null.
	 * 
	 * @param value The default value.
	 */
	public void setDefaultValue(Object value) {
		if (getType() == java.sql.Types.BOOLEAN) {
			this.defaultValue = false;
			if (value != null){
				if(value.equals("true")){
					this.defaultValue = true;
				}
			}

		}
		else {
			this.defaultValue = value;
		}
	}
	
	public void setResetValue(boolean resetValue) {
		this.resetValue = resetValue;
	}
	
	public boolean getResetValue() {
		return this.resetValue;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return this.title;
	}

	public boolean isValidInput(Object input) {
		return isValidInput(input, null);
	}
	
	public boolean isValidInput(Object input, String validFormat) {
		if (valueReadOnly) return false;
		
		if (!(input instanceof String)) return true;
		
		if (validFormat == null) {
			if (this.validFormatChecker == null) return true;
			return this.validFormatChecker.matcher(input.toString()).matches();
		}
		else {
			Pattern validFormatChecker = Pattern.compile(validFormat);
			return validFormatChecker.matcher(input.toString()).matches();
		}
	}
	
	/**
	 * If a value is meant to be compared against a set of data, then this should be overridden.
	 * 
	 * @param value
	 * @return
	 */
	public boolean isInputPartOfSet(Object value) {
		return true;
	}
	
	public Object modifyInput(Object rawValueObj) {
		if (rawValueObj == null) return rawValueObj;

		if (!(rawValueObj instanceof String)) return rawValueObj;

		
		String rawValue = rawValueObj.toString();
		
		String modifiedValue = null;

		if (VALID_FORMAT_INTEGER == validFormat) {
			rawValue = rawValue.toString().trim();
			
			// Trailing zeros are removed here (even if a minus in front)
			if (!rawValue.equals("")) {
				StringBuilder modifications = new StringBuilder(rawValue);
				boolean hasMinus = modifications.charAt(0) == '-';
				
				while(modifications.length() > 0) {
					char firstChar = modifications.charAt(0);
					if (firstChar == '-' || firstChar == '0') {
						modifications.deleteCharAt(0);
					}
					else {
						break;
					}
				}
				
				if (hasMinus) modifications.insert(0, '-');
				
				rawValue = modifications.toString();
			}
			
			if (rawValue.equals("") || rawValue.equals("-")) modifiedValue = "0";
			

			if (rawValue.equals("") || rawValue.equals("-")) modifiedValue = "0";
		}
		else if (VALID_FORMAT_DECIMAL == validFormat) {
			rawValue = rawValue.toString().trim();

			if (rawValue.equals("") || rawValue.equals(".")) {
				modifiedValue = "0";
			}
			else if (isValidInput(rawValue, VALID_FORMAT_DECIMAL)) {
				BigDecimal bDec = new BigDecimal(rawValue);
				modifiedValue = bDec.toPlainString();
			}
		}
		else if (VALID_FORMAT_CURRENCY_POUNDS == validFormat) {
			rawValue = rawValue.toString().trim();

			// Checking for minus sign
			boolean hasMinus = rawValue.contains("-");
			if (hasMinus) rawValue = rawValue.replaceAll("\\-", "");
			
			// Checking for £, $, etc at beginning
			if (rawValue.length() > 0) {
				char char1 = rawValue.charAt(0);
				if (char1 != '.' && (char1 < '0' || char1 > '9')) {
					rawValue = rawValue.substring(1, rawValue.length());
				}
			}
				
			boolean doDivision = false;
			
			// Checking for p at the end
			if (rawValue.endsWith("p")) {
				rawValue = "0" + rawValue.substring(0, rawValue.length() - "p".length());
				doDivision = true;
			}
			else if (rawValue.endsWith("pence")) {
				rawValue = "0" + rawValue.substring(0, rawValue.length() - "pence".length());
				doDivision = true;
			}
			
			if (doDivision) {
				BigDecimal dec = new BigDecimal(rawValue);
				dec = dec.divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
				rawValue = dec.toPlainString();
			}
			
			if (rawValue.equals("") || rawValue.equals(".")) {
				modifiedValue = "0.00";
			}
			else if (isValidInput(rawValue, VALID_FORMAT_DECIMAL)) {
				BigDecimal bDec = new BigDecimal(rawValue);
				bDec = bDec.setScale(2, RoundingMode.HALF_UP);
				modifiedValue = bDec.toPlainString();
			}
			if (hasMinus) modifiedValue = "-" + modifiedValue;
		}
		
		Object value = modifiedValue == null ? rawValue : modifiedValue;
		
		return value;
	}
	
	
	public void addForeignKey(String foreignKey) {
		foreignKeys.add(foreignKey);
	}
	
	
	public String[] getForeignKeys() {
		return foreignKeys.toArray(new String[foreignKeys.size()]);
	}
	
	public Scenario[] getScenarios(Data model) {
		return Scenario.noScenarios;
	}
	
	
	
	
	/**
	 * Optional overide to 
	 * 
	 * @return
	 */
	public DataType[] otherColumnsToUpdate() {
			return null;
	}
	
	public void addOtherColumnToModify(Column column) {
		otherColumnsToModify.add(column);
	}
	
	
	/**
	 * You can optionally override this method where you can make modifications to other columns
	 * should you wish. For example, if this is a fake column with the table.column as
	 * customer.full_name, then two real columns customer.first_name and customer.surname can have
	 * their real values set based on what the full name is.
	 * @param newRow 
	 * @param newValue 
	 */
	public final void invokeUpdatesForOtherColumns(Object[] newRow, Object newValue, LightTableModel model) {
		Iterator<Column> it = otherColumnsToModify.iterator();

		while (it.hasNext()) {
			it.next().updateThisColumnFromOtherColumns(newRow, model);
		}
	}
	
	/**
	 * Called when certain columns are edited by the user (any column which has this column in its otherColumnsToModify list).
	 * Also called when a selection for this table is made from the database.
	 * 
	 * @param newRow The row being modified.
	 * @param model The model for this row.
	 */
	public void updateThisColumnFromOtherColumns(Object[] newRow, LightTableModel model) {}
	
	/**
	 * Called when the user directly changes a value in the database for this column.
	 * 
	 * @param newRow The row being modified.
	 * @param newValue The newValue for this this column in the row
	 * @param model The model for this row.
	 */
	public void updateOtherColumnsFromThisColumn(Object[] newRow, Object newValue, LightTableModel model) {}
	
	public boolean canResetValue() {
		return resetValue;
	}

	public boolean isNullAllowed() {
		return nullAllowed;
	}


	public boolean isAlias() {
		return aliasOf != null;
	}


	public BigDecimal decimalToCorrectFormat(BigDecimal decimalValue) {
		
		if (decimalScalingOn) {
			decimalValue = decimalValue.setScale(decimalScale, RoundingMode.HALF_UP);
		}
		
		return decimalValue;
	}
	
	public Object convertToCorrectDataType(Object value) {
		
		value = DataType.convertToDataType(value, this.type); 
		
		if (value instanceof BigDecimal) {
			value = decimalToCorrectFormat((BigDecimal) value);
		}
		return value;
	}


	public void setDecimalCountRightOfPoint(int decimalScale) {
		this.decimalScale = decimalScale;
		this.decimalScalingOn = true;
//		defaultValue = decimalToCorrectFormat(BigDecimal.ZERO);
	}
	
	public void removeDecimalScale() {
		this.decimalScalingOn = false;
	}
	
	public int getDecimalScale() {
		return decimalScale;
	}
	
	public boolean isDecimalScalingOn() {
		return decimalScalingOn;
	}


	public boolean isDefaultValueValid() {
		return defaultValueValid;
	}
	
	public void setDefaultValueValid(boolean defaultValueValid) {
		this.defaultValueValid = defaultValueValid;
	}


	public boolean isDefaultValue(Object newValue) {
		if (newValue == null) {
			return defaultValue == null;
		}
		
		return newValue.equals(defaultValue);
	}


	public boolean acceptModifiedInput(Object[] newRow, Object newValue) {
		return true;
	}
	
	public void setCalibrator(AbstractColumnCalibrator calibrator) {
		calibrator.setColumn(this);
		this.calibrator = calibrator;
	}

	public void setCalibratorAndUpdate(AbstractColumnCalibrator calibrator) {
		setCalibrator(calibrator);
	}
	
	public boolean hasCalibrator() {
		return this.calibrator != null;
	}
	
	public AbstractColumnCalibrator gppetCalibrator() {
		return this.calibrator;
	}

	public void setNullSubstitute(Object value) {
		nullSubstitute = value;
		usesNullSubstitute = true;
	}
	
	public void removeNullSubstitute() {
		usesNullSubstitute = false;
	}
	
	public Object getNullSubstitute() {
		return nullSubstitute;
	}
	
	public boolean isUsingNullSubstitute() {
		return usesNullSubstitute;
	}


	public Integer getMaxStringLength() {
		return maxStringLength;
	}
	
	public void setMaxStringLength(Integer maxStringLength) {
		this.maxStringLength = maxStringLength;
	}
	
	public AbstractColumnCalibrator getCalibrator() {
		return calibrator;
	}
	
	public String tableDotColumnWithTicks() {
		return "`" + table.tableName + "`.`" + columnName + "`";
	}

	public String tableNameWithTicks() {
		return "`" + table.tableName + "`";
	}
	
	public String columnNameWithTicks() {
		return "`" + columnName + "`";
	}
	
	public String aliasOfWithTicks() {
		return "`" + aliasOf + "`";
	}
	
	public void setNullAllowed(boolean nullAllowed) {
		this.nullAllowed = nullAllowed;
	}
	
	public void setUnique(boolean unique) {
		this.unique = unique;
	}
	
	public boolean isUnique() {
		return unique;
	}
	
	public String getUniqueAndNullSQL() {
		
		String returnValue;
		
		if (unique) {
			if (nullAllowed) {
				returnValue = "UNIQUE NULL";
			}
			else {
				returnValue = "UNIQUE NOT NULL";
			}
		}
		else {
			if (nullAllowed) {
				returnValue = "";
			}
			else {
				returnValue = "NOT NULL";
			}
		}
		
		return returnValue;
	}
	
	public String getNullSQL() {
		
		String returnValue;
		
		if (nullAllowed) {
			returnValue = "";
		}
		else {
			returnValue = "NOT NULL";
		}
		
		return returnValue;
	}
	
	public String getSafeSQLValue(Object value) {
		
		String returnValue;
		
		if (value == null) {
			switch (type) {
				case java.sql.Types.TIMESTAMP: returnValue = "CURRENT_TIMESTAMP";
				default: returnValue = "NULL";
			}
		}
		else {
			returnValue = value.toString();
			
			if (needsToBeEscaped()) {
				returnValue = SQLInjectionEscaper.escapeString(returnValue, true);
			}
			
			if (needsQuotes()) {
				returnValue = "\'" + returnValue + "\'";
			}
		}
		
		return returnValue;
	}
	
	public boolean defaultAvailable() {
		switch (type) {
			case java.sql.Types.BOOLEAN: case java.sql.Types.INTEGER: case java.sql.Types.BIGINT:
			case java.sql.Types.FLOAT: case java.sql.Types.DECIMAL: case DataType.INT_TYPE_DATETIME:
			case java.sql.Types.TIMESTAMP: case java.sql.Types.TIME: case java.sql.Types.DATE:
			case java.sql.Types.VARCHAR: case java.sql.Types.CHAR:
				return true;
			default:
				return false;
		}
	}

	public static Column firstUniqueColumn(Column[] columns) {
		for (int i=0; i<columns.length; i++) {
			Column nextColumn = columns[i];
			
			if (!nextColumn.isPrimary()) {
				if (nextColumn.unique) {
					return nextColumn;
				}
			}
		}
		
		return null;
	}

	public static Column firstUniqueAndNotNullColumn(Column[] columns) {
		for (int i=0; i<columns.length; i++) {
			Column nextColumn = columns[i];

			if (!nextColumn.isPrimary()) {
				if (nextColumn.unique && !nextColumn.nullAllowed) {
					return nextColumn;
				}
			}
		}
		
		return null;
	}
	
	public static Column[] arrayListToArray(ArrayList<Column> columnsAList) {
		return columnsAList.toArray(new Column[columnsAList.size()]);
	}
	
	public java.util.Date getCurrentTimeValue(Object value) {
		int columnType = getType();
		
		java.util.Date dateOrTimevalue = null;
		
		if (value instanceof Long) {
			// Do nothing
		}
		// A a value is already a real date object, nothing needs to be done to it.
		else if (value instanceof java.util.Date) {
			dateOrTimevalue = (java.util.Date) value;
		}
		else if (value != null || (value instanceof String && value.toString().trim().equals(""))) {
			try {
				if (java.sql.Types.TIMESTAMP == columnType) {
					dateOrTimevalue = Timestamp.valueOf(value.toString());
				}
				else if (java.sql.Types.TIME == columnType) {
					dateOrTimevalue = Time.valueOf(value.toString());
				}
				else if (java.sql.Types.DATE == columnType) {
					dateOrTimevalue = Date.valueOf(value.toString());
				}
				else {
					dateOrTimevalue = Timestamp.valueOf(value.toString());
				}
			}
			catch(IllegalArgumentException e) {
				value = null;
				dateOrTimevalue = null;
			}
		}
		
		if (dateOrTimevalue == null) {
			long longValue = 0;
			
			// we already have a milliseconds value
			if (value != null && (value instanceof Long)) {
				longValue = (long) value;
			}
			
			//convert millisecond value to required time or date type
			if (java.sql.Types.TIME == columnType) {
				dateOrTimevalue = new Time(longValue);
			}
			else if (java.sql.Types.DATE == columnType) {
				dateOrTimevalue = new Date(longValue);
			}
			else {
				// default to a timestamp entry
				dateOrTimevalue = new Timestamp(longValue);
			}
			
		}
		
		return dateOrTimevalue;
	}

	public boolean isHasDefaultValue() {
		return hasDefaultValue;
	}
	
	public void setHasDefaultValue(boolean hasDefaultValue) {
		this.hasDefaultValue = hasDefaultValue;
	}

	public boolean isPrimary() {
		return (columnName.equals(table.idColumnName));
	}
	
	public void setMaxValue(Object maxValue, boolean inclusiveOfMaxValue) {
		this.maxValue = getBigDecimalValue(maxValue);
		this.inclusiveOfMaxValue = inclusiveOfMaxValue;
	}
	
	public void setMinValue(Object minValue, boolean inclusiveOfMinValue) {
		this.minValue = getBigDecimalValue(minValue);
		this.inclusiveOfMinValue = inclusiveOfMinValue;
	}
	

	public void setDecimalCountLeftOfPoint(int digitsOnLeft) {
		long newMaxValue = 1;
		
		for (int i=0; i<digitsOnLeft; i++) {
			newMaxValue *= 10;
		}
		
		setMaxValue(newMaxValue, false);
		setMinValue(-newMaxValue, false);
	}
	
	public int valueRangeStatus(Object obj) {
		
		if (obj instanceof String) {
			String str = (String) obj;
			
			if (maxStringLength != null) {
				if (str.length() > maxStringLength) {
					return VALUE_RANGE_STRING_TOO_LONG;
				}
			}
		}
		else if (isNonBooleanNumericInput()) {
			BigDecimal decimal = getBigDecimalValue(obj);

			if (maxValue != null) {
				if (decimal.compareTo(maxValue) == 1) {
					return VALUE_RANGE_ABOVE_MAX;
				}
				else if (decimal.compareTo(maxValue) == 0 && !inclusiveOfMaxValue) {
					return VALUE_RANGE_ABOVE_MAX;
				}
			}
			else if (minValue != null) {
				if (decimal.compareTo(minValue) == -1) {
					return VALUE_RANGE_BELOW_MIN;
				}
				else if (decimal.compareTo(minValue) == 0 && !inclusiveOfMinValue) {
					return VALUE_RANGE_BELOW_MIN;
				}
			}
		}
		
		return VALUE_RANGE_OK;
	}

}
