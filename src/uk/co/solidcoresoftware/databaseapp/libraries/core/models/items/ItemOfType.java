package uk.co.solidcoresoftware.databaseapp.libraries.core.models.items;

import java.math.BigDecimal;

public class ItemOfType {
	
	public final long id;
	private BigDecimal unitPrice = BigDecimal.ZERO;
	private Long unitTime = 0l;
	private Long totalTime = 0l;
	
	private Object[] row;

	public ItemOfType(long id) {
		this.id = id;
	}
	
	public void setRow(Object[] row) {
		this.row = row;
	}
	
	public void setUnitPrice(BigDecimal unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	public BigDecimal getUnitPrice() {
		return unitPrice;
	}


	public void setUnitTime(Long unitTime) {
		this.unitTime = unitTime;
	}
	
	public Long getUnitTime() {
		return unitTime;
	}
	
	public void setTotalTime(Long totalTime) {
		this.totalTime = totalTime;
	}
	
	public Long getTotalTime() {
		return totalTime;
	}
}
