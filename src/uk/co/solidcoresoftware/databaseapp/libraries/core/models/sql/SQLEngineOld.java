package uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql;


public class SQLEngineOld {
//
//	private static final String INFORMATION_SCHEMA = "information_schema";
//	private static final String CUSTOM_TABLE_PREFIX = "custom_table_t_";
//	private static final String CUSTOM_COLUMN_PREFIX = "col_";
//	
//	private static String urlPrefix = "jdbc:mysql://localhost:3306";
//	private static String dbNameDefault = null;
//
//    private static String user = null;
//    private static String password = null;
//	
//    private static Connection con = null;
//    
//    public static void setConnectionDetails(String host, String database, String user, String password) {
//    	urlPrefix = "jdbc:mysql://" + host +":3306";
//    	dbNameDefault = database;
//    	SQLEngineOld.user = user;
//    	SQLEngineOld.password = password;
//    }
//
//
//    public static void openConnection() {
//    	getConnection(dbNameDefault, true);
//    }
//    
//    public static Connection getConnection(String dbName, boolean changeMasterConnection) {
//    	String url = urlPrefix + "/" + dbName;
//    	
//    	Connection newCon = changeMasterConnection ? con : null;
//    	
//    	try {
//			if (newCon == null || newCon.isClosed()) {
//				newCon = DriverManager.getConnection(url, user, password);
//				newCon.setAutoCommit(false);
//				newCon.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
//			}
//		} catch (SQLException e) {
//			newCon = null;
//			e.printStackTrace();
//		}
//    	
//    	if (changeMasterConnection) {
//    		con = newCon;
//    	}
//    	
//    	return newCon;
//    }
//    
//    
//    public static void closeConnection() {
//    	if (con != null) {
//	    	try {
//				con.close();
//			} catch (SQLException e) {
//				
//			}
//    	}
//    }
//    
//
//    public static void closeConnection(Connection con) {
//    	if (con != null) {
//	    	try {
//				con.close();
//			} catch (SQLException e) {
//				
//			}
//    	}
//    }
//    
//    
//    public static ArrayList<Object[]> select(InterfaceTableModel model, String sql, Table table, Column[] columns, WhereClause[] whereClauses) {
//    	return select(false, model, sql, columns, whereClauses);
//    }
//        
//
//    public static ArrayList<Object[]> selectFromInfo(InterfaceTableModel model, String sql, Table table, Column[] columns, WhereClause[] whereClauses) {
//    	return select(true, model, sql, columns, whereClauses);
//    }	
//    
//    /**
//     * Selects an SQL select statement where the * after select is replaced with a
//     * comma seperated list of the columns to select (if specified). The columns must
//     * be ordered so all of the real columns are grouped together at the beginning of
//     * the array and all of the fake columns grouped together at the end of the array. 
//     * 
//     * @param sql The sql SELECT statement to use.  
//     * @param columns (null means remain as SELECT *).
//     */
//    private static ArrayList<Object[]> select(boolean fromInfoSchema, InterfaceTableModel model, String sql, Column[] columns, WhereClause[] whereClauses) {
//
//    	Connection con;
//    	
//    	if (fromInfoSchema) {
//    		con = getConnection(INFORMATION_SCHEMA, false);
//    	}
//    	else {
//    		con = SQLEngineOld.con;
//    	}
//    	
////	    	if (sql == null && table != null) {
////	    		// TODO later
////	    	}
//    	
//    	// By default, it is assumed that all columns are real
//    	int fakeColumnStart = columns.length;
//    	
//    	ArrayList<Column> columnsToSelectArrayList = new ArrayList<Column>();
//    	
//    	/*
//    	 * When the first fake-column is found, fakeColumnStart is modified
//    	 * to the position of this column.
//    	 */
//    	for (int i=0; i<columns.length; i++) {
//    		if (columns[i].isFakeColumn()) {
//    			fakeColumnStart = i;
//    			break;
//    		}
//    		else {
//    			columnsToSelectArrayList.add(columns[i]);
//    		}
//    	}
//    	
//    	Column[] columnsToSelect = columnsToSelectArrayList.toArray(new Column[fakeColumnStart]);
//    	
//    	
//    	
//    	final String stringToSearch = "SELECT *";
//    	
//    	String finalSQL;
//    	
//    	// If the string starts with stringToSearch then replace this.
//    	if (columnsToSelect != null && columnsToSelect.length > 0 &&
//    			sql.subSequence(0, stringToSearch.length()).equals(stringToSearch)) {
//    		StringBuilder alteredSQL = new StringBuilder("SELECT ");
//    		alteredSQL.append(Column.joinTableAndColumnNamesWithAliases(columnsToSelect));
//    		alteredSQL.append(sql.subSequence(stringToSearch.length(), sql.length()));
//    		finalSQL = alteredSQL.toString();
//    	}
//    	else {
//    		finalSQL = sql.toString();
//    	}
//    	
//    	finalSQL += WhereClause.getWhereStatement(whereClauses);
//    	
//    	ArrayList<Object[]> data = null;
//    	
//    	try {
//    		System.out.println(finalSQL);
//    		PreparedStatement st = con.prepareStatement(finalSQL);
//    		
//    		
//    		// Where values are set here
//    		if (whereClauses != null) {
//				for (int i=0; i<whereClauses.length; i++) {
//					st.setObject(i+1, whereClauses[i].getValue());
//				}
//    		}
//    		
//			ResultSet rs = st.executeQuery();
//			con.commit();
//			
//			// For each row.
//			data = new ArrayList<>();
//			
//			while (rs.next()) {
//				
//				Object[] dataRow = new Object[columns.length];
//				
//				for (int i=0; i<fakeColumnStart; i++) {
//					switch (columns[i].getType()) {
//						case java.sql.Types.BOOLEAN: dataRow[i] = rs.getBoolean(i+1); break;
//						case java.sql.Types.INTEGER: dataRow[i] = rs.getInt(i+1); break;
//						case java.sql.Types.BIGINT: dataRow[i] = rs.getLong(i+1); break;
//						case java.sql.Types.DECIMAL: dataRow[i] = rs.getBigDecimal(i+1); break;
//						case java.sql.Types.TIME: dataRow[i] = rs.getTime(i+1); break;
//						case java.sql.Types.DATE: dataRow[i] = rs.getDate(i+1); break;
//						case java.sql.Types.TIMESTAMP: dataRow[i] = rs.getTimestamp(i+1); break;
//						case DataType.INT_TYPE_DATETIME: dataRow[i] = rs.getTimestamp(i+1); break;
//						default: dataRow[i] = rs.getString(i+1); break;
//					}
//					
//					if (rs.wasNull()) {
//						if (columns[i].isNullAllowed()) {
//							dataRow[i] = null;
//						}
//						else {
//							dataRow[i] = columns[i].getDefaultValue();
//						}
//					}
//				}
//				
//				// If a model is specified then fake columns are updated as well
//				if (model != null) {
//					for (int i=fakeColumnStart; i<columns.length; i++) {
//						columns[i].updateThisColumnFromOtherColumns(dataRow, model);
//					}
//				}
//				
//				data.add(dataRow);
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//			data = null;
//		}
//
//		if (fromInfoSchema) {
//    		try { con.close(); } catch (SQLException e) {}
//    	}
//		
//		return data;
//    }
//    
////	    public static void select(ListInterface list, String sql) {
////	    	select(list, sql, DataType.TYPE_STRING);
////	    }
////    
////
////	    public static void select(ListInterface list, String sql, int dataType) {
////	    	
////	    	OpenConnection();
////	    	    	
////	    	try {
////				Statement st = con.createStatement();
////				ResultSet rs = st.executeQuery(sql);
////				con.commit();
////				
////				ResultSetMetaData metaData = rs.getMetaData();
////				int columnCount = metaData.getColumnCount();
////				int columnCountMinus1 = columnCount - 1;
////
////				list.clear();
////				
////				while (rs.next()) {
////					
////					Object value;
////					String[] restOfRow = new String[columnCount - 1];
////					
////					// Setting value
////					switch (dataType) {
////						case DataType.TYPE_BOOLEAN: value = rs.getBoolean(1); break;
////						case DataType.TYPE_INT: value = rs.getInt(1); break;
////						case DataType.TYPE_LONG: value = rs.getLong(1); break;
////						default: value = rs.getString(1); break;
////					}
////					
////					if (rs.wasNull()) value = null;
////					
////					
////					// Setting rest of row
////					for (int i=0; i<columnCountMinus1; i++) {
////						
////						restOfRow[i] = rs.getString(i+2);
////						if (rs.wasNull()) restOfRow[i] = null;
////					}
////					
////					list.addItem(value, restOfRow);
////				}
////				
////				list.refresh();
////				
////			} catch (SQLException e) {
////				// TODO Auto-generated catch block
////				e.printStackTrace();
////			}
////	    }
//    
//    /**
//     * Inserts columns into a specific table. The table is detected
//     * automatically from the columns.
//     * 
//     * @param column Must not be null and an array of at least 1 column with
//     * all columns being from the same table.
//     * @param values The number of values must match the number of columns.
//     * @return Whether the row was inserted successfully or not.
//     */
//    public static SQLInsert insert(Column[] columns, Object[] values) {
//    	
//    	openConnection();
//    	
//    	Column firstColumn = columns[0];
//    	
//    	Table table = firstColumn.table;
//    	
//    	StringBuilder sql = new StringBuilder("INSERT INTO "); sql.append(table.tableName);
//    	sql.append(" ("); sql.append(Column.joinColumnNames(columns)); sql.append(')');
//    	sql.append(" VALUES ("); sql.append(Column.joinQuestionMarks(columns)); sql.append(")");
//
//    	SQLInsert sqlInsert = new SQLInsert(columns.length);
//    	
//    	PreparedStatement ps;
//		try {
//			ps = con.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
//			for (int i=0; i<values.length; i++) {
//				
//				if (columns[i].getType() == java.sql.Types.TIMESTAMP) {
//					System.out.println("This is a timestamp");
//					Date date= new java.util.Date();
//					sqlInsert.setValueToBeModified(i, new Timestamp(date.getTime()).toString());
//				}
//				
//				ps.setObject(i+1, values[i]);
//			}
//			System.out.println("sql.toString(): " + sql.toString());
//	    	ps.execute();
//			con.commit();
//	    	ResultSet rs = ps.getGeneratedKeys();
//	    	if (rs.first()) {
//		    	sqlInsert.setGeneratedKey(rs.getLong(1));
//	    	}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//    	
//    	return sqlInsert;
//    }
//    
//    
////		public static boolean update(Column column, Object primaryKeyValue, Object newValue) {
////			return update(new Column[] {column}, primaryKeyValue, new Object[] {newValue});
////		}
//
//	/**
//	 * 
//	 * @param columns This must be a non-null array with a length of at least 1. All columns
//	 * must be of the same table
//	 * @param primaryKeyValue
//	 * @param newValues The new values for this update. This must be a non-null array with its
//	 * length equal to that of columns
//	 * @return
//	 */
//    public static boolean update(Column[] columns, Object primaryKeyValue, Object[] newValues) {
//    	assert(columns.length == newValues.length);
//    	
//    	openConnection();
//    	
//    	String sql = "UPDATE " + columns[0].table.tableNameWithTicks()
//			+ " SET " + Column.joinColumnNamesWithSuffix(columns, "=?")
//    			+ " WHERE `" + columns[0].table.idColumnName + "`=?";
//    	System.out.println("  " + sql);
//    	
//    	try {
//			PreparedStatement ps = con.prepareStatement(sql);
//			
//			int setObjectIndex=1;
//			
//			for (int i=0; i<columns.length; i++) {
//				
//				if (newValues[i] == null) {
//					ps.setNull(setObjectIndex++, columns[i].getType());
//				}
//				else {
//					ps.setObject(setObjectIndex++, newValues[i]);
//				}
//			}
//
//			ps.setObject(setObjectIndex, primaryKeyValue);
////				return true;
//			
//			int numberOfRowUpdated = ps.executeUpdate();
//			return numberOfRowUpdated != 0;
//
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//    	
//    	return false;
//    }
//    
//    
//    public static boolean delete(Table table, Object primaryKeyValue) {
//    	
//    	openConnection();
//
//    	String sql = "DELETE FROM " + table.tableName + " WHERE `" + table.idColumnName + "`=?";
//    	
//        	try {
//    			PreparedStatement ps = con.prepareStatement(sql);
//    			
//    			ps.setObject(1, primaryKeyValue);
//    			
//    			int numberOfRowsUpdated = ps.executeUpdate();
//    			return numberOfRowsUpdated != 0;
//
//    		} catch (SQLException e) {
//    			e.printStackTrace();
//    		}
//        	
//        	return false;
//    }
//    
//    
//    
//    /*
//     * SQL to get all custom tables
//     * 
//     	SELECT `TABLE_CATALOG`, `TABLE_SCHEMA`, `TABLE_NAME`, `COLUMN_NAME`, `ORDINAL_POSITION`, `COLUMN_DEFAULT`, `IS_NULLABLE`, `DATA_TYPE`, `CHARACTER_MAXIMUM_LENGTH`, `CHARACTER_OCTET_LENGTH`, `NUMERIC_PRECISION`, `NUMERIC_SCALE`, `CHARACTER_SET_NAME`, `COLLATION_NAME`, `COLUMN_TYPE`, `COLUMN_KEY`, `EXTRA`, `PRIVILEGES`, `COLUMN_COMMENT` FROM `COLUMNS` WHERE `TABLE_SCHEMA`="db_example" AND `TABLE_NAME` LIKE "custom_table_t_%"
//     * 
//     */
//    
//    
//    
//
//    public static ArrayList<String> getTableNames(String prefix) {
//    	openConnection();
//    	
//    	ArrayList<String> tableNames = new ArrayList<String>(50);
//    	
//    	try {
//    		DatabaseMetaData meta = con.getMetaData();
//    		ResultSet metaColumns = meta.getTables(null, dbNameDefault, prefix + "%", null);
//
//	    	while (metaColumns.next()) {
//	    		tableNames.add(metaColumns.getString("TABLE_NAME"));
//	    	}
//		} catch (SQLException e) {
//			return null;
//		}
//    	
//    	return tableNames;
//    }
//    
//    
//    public static ArrayList<String> getCustomTableNames() {
//    	return getTableNames(CUSTOM_TABLE_PREFIX);
//    }
//
//
//    public static final String META_COLUMNS_COLUMN_NAME = "COLUMN_NAME";
//    public static final String META_COLUMNS_DATA_TYPE = "DATA_TYPE";
//    public static final String META_COLUMNS_NUMERIC_PRECISION = "NUMERIC_PRECISION";
//    public static final String META_COLUMNS_NUMERIC_SCALE = "NUMERIC_SCALE";
//    public static final String META_COLUMNS_IS_NULLABLE = "IS_NULLABLE";
//    public static final String META_COLUMNS_COLUMN_DEFAULT = "COLUMN_DEFAULT";
//    public static final String META_COLUMNS_CHARACTER_MAXIMUM_LENGTH = "CHARACTER_MAXIMUM_LENGTH";
//    public static final String META_COLUMNS_CHARACTER_OCTET_LENGTH = "CHARACTER_OCTET_LENGTH";
//	public static final String META_COLUMNS_CONSTRAINT_CATALOG = "CONSTRAINT_CATALOG";
//	public static final String META_COLUMNS_CONSTRAINT_SCHEMA = "CONSTRAINT_SCHEMA";
//	public static final String META_COLUMNS_CONSTRAINT_NAME = "CONSTRAINT_NAME";
//	public static final String META_COLUMNS_TABLE_CATALOG = "TABLE_CATALOG";
//	public static final String META_COLUMNS_TABLE_SCHEMA = "TABLE_SCHEMA";
//	public static final String META_COLUMNS_TABLE_NAME = "TABLE_NAME";
//	public static final String META_COLUMNS_ORDINAL_POSITION = "ORDINAL_POSITION";
//	public static final String META_COLUMNS_POSITION_IN_UNIQUE_CONSTRAINT = "POSITION_IN_UNIQUE_CONSTRAINT";
//	public static final String META_COLUMNS_REFERENCED_TABLE_SCHEMA = "REFERENCED_TABLE_SCHEMA";
//	public static final String META_COLUMNS_REFERENCED_TABLE_NAME = "REFERENCED_TABLE_NAME";
//	public static final String META_COLUMNS_REFERENCED_COLUMN_NAME = "REFERENCED_COLUMN_NAME";
//
//    public static ArrayList<Object[]> getColumnInfo(String metaColumnName, Table table, String[] metaColumnNames) {
//    	
//    	Connection con = getConnection(INFORMATION_SCHEMA, false);
//    	
//    	ArrayList<Object[]> columnInfo = new ArrayList<>();
//    	
//    	// Converting column names to actual columns
//    	Column[] metaColumns = new Column[metaColumnNames.length]; 
//    	
//    	for (int i=0; i<metaColumnNames.length; i++) {
//    		metaColumns[i] = new Column(metaColumnName, metaColumnNames[i]);
//    	}
//    	
//    	try {
//    		
//    		String tableSQLPart = (table == null) ? "" : " AND `TABLE_NAME`=\""+table.tableName+"\"";
//    		
//    		String sql = "SELECT " + Column.joinColumnNames(metaColumns) + " FROM `COLUMNS` WHERE `TABLE_SCHEMA`=\""
//    				+ dbNameDefault + "\"" + tableSQLPart;
//    		
//    		Statement st = con.createStatement();
//    		ResultSet metaColumnsRs = st.executeQuery(sql);
//    		
//    		// Fills the data to return
//    		while (metaColumnsRs.next()) {
//    			Object[] columnInfoEntry = new Object[metaColumnNames.length];
//    			for (int i=0; i<metaColumnNames.length; i++) {
//    				columnInfoEntry[i] = metaColumnsRs.getObject(metaColumnNames[i]);
//    			}
//
//				columnInfo.add(columnInfoEntry);
//    		}
//    		
//    		
//		} catch (SQLException e) {
//			e.printStackTrace();
//			closeConnection(con);
//			return null;
//		}
//
//    	for (int i=0; i<columnInfo.size(); i++) {
//    		Object[] nextSet = columnInfo.get(i);
//    		for (int j=0; j<nextSet.length; j++) {
//    		}
//    	}
//
//		closeConnection(con);
//    	
//    	return columnInfo;
//    }
//    
//    
//    
//    public static ArrayList<String> getCustomColumnNames(String tableName) {
//    	openConnection();
//    	
//    	ArrayList<String> columnNames = new ArrayList<String>(50);
//    	
//    	try {
//    		DatabaseMetaData meta = con.getMetaData();
//    		ResultSet metaColumns = meta.getColumns(null, dbNameDefault, tableName, CUSTOM_COLUMN_PREFIX + "%");
//
//	    	while (metaColumns.next()) {
//	    		columnNames.add(metaColumns.getString("COLUMN_NAME"));
//	    	}
//		} catch (SQLException e) {
//			return null;
//		}
//
//    	return columnNames;
//    }
//    
//    public static String convertToLowercaseUnderscore(String inputWord) {
//    	String inputWordSpacesToUnderscores = inputWord.toLowerCase().trim().replaceAll(" ", "_");
//    	return inputWordSpacesToUnderscores.replaceAll("[^A-Za-z0-9_]", "");
//    }
//    
//    
//    public static boolean createCustomTable(String userFriendlyTableName) {
//    	
//    	ArrayList<String> currentTableNames = getCustomTableNames();
//    	
//    	long number = 1;
//    	
//    	String tableNameOriginal = CUSTOM_TABLE_PREFIX + convertToLowercaseUnderscore(userFriendlyTableName);
//    	String tableNameToUse = tableNameOriginal;
//    	
//    	while (currentTableNames.contains(tableNameToUse)) {
//    		tableNameToUse = tableNameOriginal + "_" + number++;
//    	}
//
//    	String sql = "CREATE TABLE " + tableNameToUse + " ( " + Config.ID_COLUMN + " " + Config.ID_COLUMN_SQL_DATA_TYPE + " NOT NULL AUTO_INCREMENT, PRIMARY KEY (" + Config.ID_COLUMN + ")) ENGINE = INNODB";
//
//    	return true;
//    }
//    
//    public static boolean createCustomColumn(String tableName,  String userFriendlyColumnName) {
//    	
//    	ArrayList<String> currentColumnNames = getCustomColumnNames(tableName);
//    	
//    	long number = 0;
//    	
//    	String columnNameOriginal = CUSTOM_COLUMN_PREFIX + convertToLowercaseUnderscore(userFriendlyColumnName);
//    	String columnNameToUse = columnNameOriginal;
//    	
//    	while (currentColumnNames.contains(columnNameToUse)) {
//    		columnNameToUse = columnNameOriginal + "_" + number++;
//    	}
//    	
//    	String sql = "CREATE TABLE " + columnNameToUse + " ( " + Config.ID_COLUMN + " " + Config.ID_COLUMN_SQL_DATA_TYPE + " NOT NULL AUTO_INCREMENT, PRIMARY KEY (" + Config.ID_COLUMN + "))";
//
//    	return true;
//    }
//    
//    
//
//    public static boolean createTableIfNotExist(Table table) {
//    	return createTable(table, true);
//    }
//    
//
//    public static boolean createTable(Table table, boolean onlyIfNotExists) {
//    	
//    	String sql = onlyIfNotExists ? "CREATE TABLE IF NOT EXISTS " : "CREATE TABLE ";
//    	
//    	sql += table.tableName
//    			+ "("
//    			+ " " + table.idColumnName + " SERIAL PRIMARY KEY"
//    			+ " ) ENGINE = INNODB";
//
//    	openConnection();
//    	
//    	boolean successful = true;
//    	
//    	try {
//			Statement statement = con.createStatement();
//			statement.execute(sql);
//		} catch (SQLException e) {
//			successful = false;
//		}
//    	
//    	return successful;
//    }
//
//    public static boolean createColumnIfNotExist(Column column) {
//    	return createColumn(column, true);
//    }
//    public static boolean createColumn(Column column, boolean onlyIfNotExists) {
//    	
//    	int type = column.getType();
//    	
//    	String typePart;
//    	
//		if (type == java.sql.Types.BOOLEAN) {
//			typePart = "TINYINT(1)";
//		}
//		else if (type == java.sql.Types.DECIMAL) {
//			typePart = "DECIMAL(65," + column.getDecimalScale() + ")";
//		}
//		else if (type == java.sql.Types.INTEGER) {
//			typePart = "INT(11)";
//		}
//		else if (type == java.sql.Types.BIGINT) {
//			typePart = "BIGINT(20)";
//		}
//		else {
//			typePart = "VARCHAR(" + column.getMaxStringLength() +")";
//		}
//		
//		StringBuilder sql = new StringBuilder("ALTER TABLE `");
//		sql.append(column.table.tableName);
//		sql.append("` ADD `");
//		sql.append(column.columnName);
//		sql.append("` ");
//
//		if (column.getTypeEnum() != null) {
//			sql.append(column.getTypeEnum().getEnumAsType());
//		}
//		else if (column.getTypeString() != null) {
//			sql.append(column.getTypeString());
//		}
//		else {
//			sql.append(typePart);
//		}
//		
//		sql.append(";");
//    	
//    	openConnection();
//    	
//    	boolean successful = true;
//    	
//    	try {
//			Statement statement = con.createStatement();
//			System.out.println("sql.toString(): " + sql.toString());
//			statement.execute(sql.toString());
//		} catch (SQLException e) {
//			successful = false;
//			e.printStackTrace();
//		}
//    	
//    	return successful;
//    }
//    
//    
//    public static boolean commit() {
//    	try {
//			con.commit();
//		} catch (SQLException e) {
//			return false;
//		}
//    	
//    	return true;
//    }
//    
//    public static boolean rollback() {
//    	try {
//			con.rollback();
//		} catch (SQLException e) {
//			return false;
//		}
//
//    	return true;
//    }
//    
//
////		public static void connectToDatabase(String[] args) {
////			connectToDatabase(args, dbNameDefault);
////		}
//    
////		public static void connectToDatabase(String[] args, String dbName) {
////	        Connection con = null;
////	        Statement st = null;
////	        ResultSet rs = null;
////
////	        String url = urlPrefix + "/" + dbName;
////	        
////	        try {
////	            con = DriverManager.getConnection(url, user, password);
////	            st = con.createStatement();
////	            rs = st.executeQuery("SELECT VERSION()");
////
////	        } catch (SQLException ex) {
////	            Logger lgr = Logger.getLogger(SQLEngine.class.getName());
////	            lgr.log(Level.SEVERE, ex.getMessage(), ex);
////
////	        } finally {
////	            try {
////	                if (rs != null) { rs.close(); }
////	                if (st != null) { st.close(); }
////	                if (con != null) { con.close(); }
////
////	            } catch (SQLException ex) {
////	                Logger lgr = Logger.getLogger(SQLEngine.class.getName());
////	                lgr.log(Level.WARNING, ex.getMessage(), ex);
////	            }
////	        }
////	    }
//	
//	public static boolean dropAllTables() {
//		
//		openConnection();
//		
//		DatabaseMetaData meta;
//		ResultSet tables;
//		
//		try {
//			meta = con.getMetaData();
//			tables = meta.getTables(null, null, null, null);
//		} catch (SQLException e) {
//			e.printStackTrace();
//			return false;
//		}
//		
//		try {
//			while (tables.next()) {
//				String tableName = tables.getString("TABLE_NAME");
//				Statement statement = con.createStatement();
//				statement.execute("DROP TABLE " + tableName);
//			}
//		} catch (SQLException e) {
//			return false;
//		}
//		
//		
//		return true;
//	}
//	
//	public static void createStatement() {
//		
//	}
//
//	/*
//	 * ALTER TABLE katalog 
//		ADD CONSTRAINT `fk_katalog_sprache` 
//		FOREIGN KEY (`Sprache`)
//		REFERENCES `Sprache` (`ID`)
//		ON DELETE RESTRICT
//		ON UPDATE RESTRICT;
//	 */
//	
//	public static boolean removeForeignKeyConstraint(String tableNameRaw, String constraintRaw) {
//		openConnection();
//
//		String constraint = Column.makeAlphanumericAndUnderscores(constraintRaw);
//		String tableName = Column.makeAlphanumericAndUnderscores(tableNameRaw);
//		String sql = "ALTER TABLE `" + tableName + "` DROP FOREIGN KEY `" + constraint + "`";
//		System.out.println("removeForeignKeyConstraint: " + sql);
//		
//		Statement st;
//		try {
//			st = con.createStatement();
//			boolean rs = st.execute(sql);
//			return rs;
//		} catch (SQLException e) {
//			e.printStackTrace();
//			return false;
//		}
//	}
//
//	public static boolean setColumnAsForeignKeyToTable(Column column, Table primaryKeyTable) {
//		openConnection();
//		
//		String tableDotColumnWithDoubleUnderscores = column.table.tableName.replaceAll("_", "__") + "_"
//				+ column.columnName.replaceAll("_", "__"); 
//		
//		StringBuilder sql = new StringBuilder("ALTER TABLE ");
//		sql.append(column.tableNameWithTicks());
//		sql.append(" ADD CONSTRAINT ");
//		sql.append("`fk_"); sql.append(tableDotColumnWithDoubleUnderscores); sql.append("`");
//		sql.append(" FOREIGN KEY ("); sql.append(column.columnNameWithTicks()); sql.append(")");
//		sql.append(" REFERENCES "); sql.append(primaryKeyTable.tableNameWithTicks());
//		sql.append(" ("); sql.append(primaryKeyTable.idColumnNameWithTicks()); sql.append(")");
//		sql.append(" ON DELETE RESTRICT ON UPDATE RESTRICT;");
//		
//		System.out.println(sql);
//
//		Statement st;
//		try {
//			st = con.createStatement();
//			st.execute(sql.toString());
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			return false;
//		}
//		
//		return true;
//	}
//	
//	/*
//	 * ALTER TABLE  `Turbine` CHANGE  `blade`  `blade` BIGINT( 20 ) UNSIGNED NULL DEFAULT NULL ;
//	 */
//	public static boolean setColumnDataTypeToMatchPrimaryKey(Column column) {
//		openConnection();
//		
//		StringBuilder sql = new StringBuilder("ALTER TABLE ");
//		sql.append(column.tableNameWithTicks());
//		sql.append(" CHANGE ");
//		sql.append(column.columnNameWithTicks());
//		sql.append(" ");
//		sql.append(column.columnNameWithTicks());
//		sql.append(" BIGINT( 20 ) UNSIGNED NULL DEFAULT NULL;");
//		
//		Statement st;
//		try {
//			st = con.createStatement();
//			st.execute(sql.toString());
//		} catch (SQLException e) {
//			return false;
//		}
//		
//		return true;
//	}
//
//
//	public static boolean dropAllForiegnKeys() {
//		
//		
//		Connection con = getConnection(INFORMATION_SCHEMA, false);
//		
//		String sql = "SELECT * FROM `KEY_COLUMN_USAGE` WHERE `TABLE_SCHEMA`=\""+dbNameDefault+"\" AND `REFERENCED_TABLE_NAME` IS NOT NULL AND `REFERENCED_COLUMN_NAME` IS NOT NULL";
//		
//		Statement st; ResultSet rs;
//		
//		try {
//			st = con.createStatement();
//			rs = st.executeQuery(sql);
//
//			while (rs.next()) {
//				String tableName = rs.getString(META_COLUMNS_TABLE_NAME);
//				String constraintName = rs.getString(META_COLUMNS_CONSTRAINT_NAME);
//				
//				removeForeignKeyConstraint(Column.makeAlphanumericAndUnderscores(tableName), constraintName);
//			}
//			
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//			closeConnection(con);
//			return false;
//		}
//
//		closeConnection(con);
//		return true;
//	}
//
//
//	public static Object getDBName() {
//		return dbNameDefault;
//	}
}
