package uk.co.solidcoresoftware.databaseapp.libraries.core.models;

import java.util.ArrayList;
import java.util.HashSet;

public class DataHub {
	
	private static HashSet<String> validSQLStatements = new HashSet<String>();

	// Valid SQL statements.
	public final static String SQL_STATEMENT_GET_CLIENTS
				= "SELECT * FROM clients LEFT JOIN clients_poss"
				+ " ON clients.id=clients_poss.client_id"
				+ " LEFT JOIN clients_notes ON clients.id=clients_notes.client_id"
				+ " LEFT JOIN practitioners ON clients.practitioner_id=practitioners.id"
				+ " GROUP BY clients.id";

	public static final String SQL_STATEMENT_GET_COMPONENTS
				= "SELECT * FROM components";

	public static final String SQL_STATEMENT_GET_PRODUCTS
				= "SELECT * FROM products";
	
	public static final String SQL_STATEMENT_GET_PROCESSES
				= "SELECT * FROM processes";

	public static final String SQL_STATEMENT_GET_PRODUCTS_CONTAINED_ITEMS
				= "SELECT * FROM products_contained_items";
	

	public static final String SQL_STATEMENT_GET_PRODUCTS_CONTAINED_ITEMS_TOTAL_TIMES
				= "SELECT products.id, SUM(total_time) AS total_time"
					+ " FROM products LEFT JOIN products_contained_items"
					+ " ON products.id = products_contained_items.product_id"
					+ " AND products_contained_items.contained_item_table = \"processes\""
					+ " GROUP BY products.id";
	/*
	 *
	 SELECT products.id, SUM(total_time) AS total_time
 FROM products LEFT JOIN products_contained_items
 ON products.id = products_contained_items.product_id
 AND products_contained_items.contained_item_table = "processes"
 GROUP BY products.id
	 */
	
	
	public static final String f
				= "SET @row_number:=0;"
				+ " SELECT @row_number:=@row_number+1 as id, \"components\" as contained_item_table, id as item_id, name FROM components"
				+ " UNION ALL"
				+ " SELECT @row_number:=@row_number+1 as id, \"products\" as contained_item_table, id as item_id, name FROM products"
				+ " UNION ALL"
				+ " SELECT @row_number:=@row_number+1 as id, \"processes\" as contained_item_table, id as item_id, name FROM processes";
	

	public static final String ProductsContainedItemsSelection
				= "(SELECT id, name, 'components' FROM components as all_items)"
				+ " UNION ALL"
				+ " (SELECT id, name, 'products' FROM products as all_items)"
				+ " UNION ALL"
				+ " (SELECT id, name, 'processes' FROM processes as all_items)";
	

	public static final String ItemsToSellSelection
				= "(SELECT id, name, 'products', unit_cost FROM products as all_items)";
	
	
	
	
	static {
		validSQLStatements.add(SQL_STATEMENT_GET_CLIENTS);
	}
	
	
	/**
	 * Performs the following type of SQL command
	 * 
	 * UPDATE table SET column=newValue WHERE id=id
	 * 
	 * 
	 * @param column For getting the table and column names
	 * @param id The id of the row
	 * @param newValue
	 */
	public final static void update(DataType column, int id, Object newValue) {
		
		
	}
	
	
	public final static void get(final String sqlStatement, Object[] params) {
		
		// The SQL statement must be from one of the valid SQL statements (see above)
		if (validSQLStatements.contains(sqlStatement)) {
			
		}
		else {
			
		}
	}
	
	private static String[] allFKeys = {"Red", "Green", "Blue", "Yellow"};
	
	private static ArrayList<String> fKeysList = new ArrayList<String>();
	
	public static void addFKey() {
		int fKeysCount = fKeysList.size();
		
		if (fKeysCount < allFKeys.length) {
			fKeysList.add(allFKeys[fKeysCount]);
		}
	}
	
	static {
		addFKey();
	}
	
	public static String[] getFKeys() {
		String[] fKeys = new String[fKeysList.size()];
		fKeysList.toArray(fKeys);
		
		return fKeys;
	}
}
