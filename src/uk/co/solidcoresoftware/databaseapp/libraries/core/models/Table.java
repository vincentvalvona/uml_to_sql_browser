package uk.co.solidcoresoftware.databaseapp.libraries.core.models;

import java.util.Collection;
import java.util.HashMap;

import uk.co.solidcoresoftware.databaseapp.libraries.core.config.Config;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableModel;

public class Table {
	
	private final static HashMap<String, Table> allTables = new HashMap<String, Table>();
	
	public final String tableName;
	public final String idColumnName;

	public Table(String tableName) {
		this(tableName, Config.ID_COLUMN);
	}
	
	public Table(String tableName, Column idColumn) {
		this.tableName = Column.makeAlphanumericAndUnderscores(tableName);
		this.idColumnName = idColumn.columnName;
	}
	
	public Table(String tableName, String idColumnName) {
		this.tableName = Column.makeAlphanumericAndUnderscores(tableName);
		this.idColumnName = idColumnName;
	}

	public static Table addTable(String tableName, String idColumnName) {
		Table table = new Table(tableName, idColumnName);
		allTables.put(tableName, table);
		return table;
	}
	
	private static void addTable(String tableName) {
		addTable(tableName, Config.ID_COLUMN);
	}
	
	public static Table getTable(String tableName) {
		
		Table table = allTables.get(tableName);
		
		if (table == null) {
			table = new Table(tableName, Config.ID_COLUMN);
		}
		
		return table;
	}
	

	public static String getIdColumn(String tableName) {
		return allTables.get(tableName).idColumnName;
	}

	public String idColumnNameWithTicks() {
		return "`" + idColumnName + "`";
	}
	
	public static void setupTables() {
	}
	
	public static Collection<Table> getAlltables() {
		return allTables.values();
	}
	
	public String tableNameWithTicks() {
		return "`" + tableName + "`";
	}

	public String primaryKeyTableDotColumn() {
		return tableName + "." + idColumnName;
	}

	public String primaryKeyTableDotColumnWithTicks() {
		return "`" + tableName + "`.`" + idColumnName + "`";
	}
	
	@Override
	public int hashCode() {
		return tableName.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		
		Table objCast = (Table) obj;
		return tableName.equals(objCast.tableName);
	}
	
	@Override
	public String toString() {
		return "name: " + tableNameWithTicks() + "  primary: " + idColumnNameWithTicks();
	}
}
