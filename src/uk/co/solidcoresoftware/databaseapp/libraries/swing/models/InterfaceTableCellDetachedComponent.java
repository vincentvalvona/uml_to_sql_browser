package uk.co.solidcoresoftware.databaseapp.libraries.swing.models;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.InterfaceTableModel;

public interface InterfaceTableCellDetachedComponent {

	public void setModelAndColumn(LightTableModel lightTableModel, Column column);
	public void setToNoRowSelected();
	public Column getColumn();
	public void setValue(int selectedRowIndex, Object value);
	void sendValueToTable();

}
