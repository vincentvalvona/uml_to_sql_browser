package uk.co.solidcoresoftware.databaseapp.libraries.swing.models;

import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.ScrollPaneConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TableColumnModelEvent;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;
import uk.co.solidcoresoftware.databaseapp.views.tables.UserTableScrollPane;

public class BaseTableColumnModel extends DefaultTableColumnModel implements InterfaceBaseTableColumnModel {

	public final static int MIN_COLUMN_WIDTH = 100;
	
	private ArrayList<BaseTableColumn> allColumns = new ArrayList<BaseTableColumn>();
	private ArrayList<BaseTableColumn> visibleColumns = new ArrayList<BaseTableColumn>();

	public final BaseTable baseTable;

	private int tableWidth;
	
	public BaseTableColumnModel(BaseTable baseTable) {
		this.baseTable = baseTable;
		addColumnModelListener(new TableColumnModelListener() {
			
			@Override
			public void columnSelectionChanged(ListSelectionEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void columnRemoved(TableColumnModelEvent e) {
				setWidths(tableWidth);
			}
			
			@Override
			public void columnMoved(TableColumnModelEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void columnMarginChanged(ChangeEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void columnAdded(TableColumnModelEvent e) {
				setWidths(tableWidth);
			}
		});
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.views.tables.InterfaceBaseTableColumnModel#moveColumn(int, int)
	 */
	@Override
	public void moveColumn(int oldIndex, int newIndex) {
		
		if (oldIndex == newIndex) {
			super.moveColumn(oldIndex, newIndex);
			return;
		}
		
		BaseTableColumn columnToMove = getColumn(oldIndex);
		BaseTableColumn columnToMoveTo = getColumn(newIndex);

		int newIndexAll = allColumns.indexOf(columnToMoveTo);
		
		allColumns.remove(columnToMove);
		allColumns.add(newIndexAll, columnToMove);
		
		super.moveColumn(oldIndex, newIndex);
	}
	
	@Override
	public BaseTableColumn getColumn(int columnIndex) {
		if (columnIndex >= 0 && columnIndex < getColumnCount()) {
			return (BaseTableColumn) super.getColumn(columnIndex);
		}
		else {
			return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.views.tables.InterfaceBaseTableColumnModel#setColumnVisible(javax.swing.table.TableColumn, boolean)
	 */
	public void setColumnVisible(BaseTableColumn aTableColumn, boolean visible) {
		if (visible) {
			visibleColumns.add(aTableColumn);
		}
		else {
			visibleColumns.remove(aTableColumn);
		}
		
		rearrangeColumns();
		updateList();
	}
	
	public void showOnlyThisColumn(BaseTableColumn aTableColumn) {

		if (visibleColumns.contains(aTableColumn)){
		
			BaseTableColumn[] allVisibleColumnsArray = visibleColumns.toArray(new BaseTableColumn[visibleColumns.size()]);
			
			for (int i=0; i<allVisibleColumnsArray.length; i++) {
				BaseTableColumn nextTableColumn = allVisibleColumnsArray[i];
				if (aTableColumn != allVisibleColumnsArray[i]) {
					visibleColumns.remove(allVisibleColumnsArray[i]);
				}
			}
			
			rearrangeColumns();
			updateList();
		}
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.views.tables.InterfaceBaseTableColumnModel#showAllColumns()
	 */
	public void showAllColumns() {
		visibleColumns.clear();
		visibleColumns.addAll(allColumns);
		updateList();
	}

	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.views.tables.InterfaceBaseTableColumnModel#columnVisible(javax.swing.table.TableColumn)
	 */
	public boolean columnVisible(BaseTableColumn aTableColumn) {
		return visibleColumns.contains(aTableColumn);
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.views.tables.InterfaceBaseTableColumnModel#getAllColumns()
	 */
	public BaseTableColumn[] getAllColumns() {
		BaseTableColumn[] allColumnsToReturn = new BaseTableColumn[allColumns.size()];
		
		for (int i=0; i<allColumns.size(); i++) {
			allColumnsToReturn[i] = allColumns.get(i);
		}
		
		return allColumnsToReturn;
	}
	
	public Column[] getAllRealColumns() {
		Column[] allColumnsToReturn = new Column[allColumns.size()];
		
		for (int i=0; i<allColumns.size(); i++) {
			allColumnsToReturn[i] = allColumns.get(i).getColumn();
		}
		
		return allColumnsToReturn;
	}

	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.views.tables.InterfaceBaseTableColumnModel#rearrangeColumns()
	 */
	public void rearrangeColumns() {
		ArrayList<BaseTableColumn> allVisibleColumns = (ArrayList<BaseTableColumn>) visibleColumns.clone();
		
		visibleColumns.clear();
		visibleColumns.addAll(allColumns);
		visibleColumns.retainAll(allVisibleColumns);
	}
	
	public void addColumn(BaseTableColumn column) {
		allColumns.add(column);
		allColumns.ensureCapacity(allColumns.size());
		visibleColumns.ensureCapacity(allColumns.size());
	}
	
	public void removeColumn(BaseTableColumn column) {
		allColumns.remove(column);
		visibleColumns.remove(column);
		super.removeColumn(column);
	}
	
	public void removeAllColumns() {
		Iterator<BaseTableColumn> it = allColumns.iterator();
		while (it.hasNext()) {
			super.removeColumn(it.next());
		}
		
		allColumns.clear();
		visibleColumns.clear();
	}

	private void updateList() {
		Iterator<BaseTableColumn> it = allColumns.iterator();
		while (it.hasNext()) {
			super.removeColumn(it.next());
		}
		
		it = visibleColumns.iterator();
		while (it.hasNext()) {
			super.addColumn(it.next());
		}
	}

	@Override
	public void setColumnVisible(InterfaceBaseTableColumn aTableColumn, boolean visible) {
		setColumnVisible((BaseTableColumn) aTableColumn, visible);
	}

	@Override
	public boolean columnVisible(InterfaceBaseTableColumn aTableColumn) {
		// TODO Auto-generated method stub
		return columnVisible((BaseTableColumn) aTableColumn);
	}

	@Override
	public void addColumn(InterfaceBaseTableColumn column) {
		addColumn((BaseTableColumn) column);
	}

	@Override
	public void removeColumn(InterfaceBaseTableColumn column) {
		removeColumn((BaseTableColumn) column);
	}

	
	public void setWidths(int tableWidth) {
		this.tableWidth = tableWidth;
		
		int visibleColumnsCount = visibleColumns.size();
		
		if (visibleColumnsCount > 0) {
		
			int totalWeighting = 0;
			
			for (int i=0; i<visibleColumnsCount; i++) {
				totalWeighting += visibleColumns.get(i).getMinWidthWeighting();
			}
			
			int standardColumnWidth = tableWidth / totalWeighting;
			
			if (standardColumnWidth < MIN_COLUMN_WIDTH) standardColumnWidth = MIN_COLUMN_WIDTH;
			
			for (int i=0; i<visibleColumnsCount; i++) {
				BaseTableColumn nextColumn = visibleColumns.get(i);
				nextColumn.setWidths(standardColumnWidth * nextColumn.getMinWidthWeighting());
			}
	
			UserTableScrollPane scrollPane = baseTable.getScrollPane();
			if (standardColumnWidth == MIN_COLUMN_WIDTH) {
				scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
			}
			else {
				scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
			}
		}
	}
}
