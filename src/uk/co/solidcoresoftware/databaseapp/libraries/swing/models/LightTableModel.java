package uk.co.solidcoresoftware.databaseapp.libraries.swing.models;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javax.print.DocFlavor.STRING;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

import growoffshore.PasteModifier;
import growoffshore.UserAlerter;
import growoffshore.mainwindow.LightDetachedComponent;
import uk.co.solidcoresoftware.databaseapp.libraries.core.config.Config;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.AbstractColumnCalibrator;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Data;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.DataType;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.ValueToAlwaysSave;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.ValueToNeverSave;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.models.ComboBoxSelectedAddRow;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.models.CommandSelection;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.Prompt;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.WrappedValue;

public abstract class LightTableModel extends AbstractTableModel implements Data, Config {

	protected HashMap<Column, Integer> columnsToModelIndex = new HashMap<Column, Integer>();
	
	// Converting the column number into columns
	protected ArrayList<Column> modelIndexesToColumn = new ArrayList<Column>();
	protected ArrayList<Row> data = new ArrayList<>(); // The data as rows in the database
	
	// Getting a column from a string in the form table.column
	protected HashMap<String, Column> stringsToColumn = new HashMap<>();

	protected ArrayList<Table> allTables = new ArrayList<Table>();

	protected ArrayList<Column> columnsInsertedRaw = new ArrayList<Column>();

	protected int selectedRowIndex = -1;
	
	protected boolean rowAddingEnabled = true;
	protected boolean rowDeletingEnabled = true;
	
	private boolean allowPreservedDataTypes = false;
	
	protected ArrayList<InterfaceTableCellDetachedComponent> detachedComponents = new ArrayList<InterfaceTableCellDetachedComponent>();

	protected ArrayList<ModelDataChangedListener> modelChangedListeners = new ArrayList<>();
	
	public LightTableModel() {
		this(false);
	}
	
	protected LightTableModel(boolean emptyConstructor) {
		if (!emptyConstructor) {
			setupModel();
			activateColumns();
			setupCalibrators();
			defineIndices();
		}
	}
	
	/**
	 * Orders the columns so that fake columns are put at the end and makes them
	 * ready for adding rows. Rows can't be added before this.
	 */
	public final void activateColumns() {
		int numberOfColumns = columnsInsertedRaw.size();
		
		// First adds the real columns to the correct places
		for (int i=0; i<numberOfColumns; i++) {
			Column column = columnsInsertedRaw.get(i);
			
			if (!column.isFakeColumn()) activateColumn(column);
		}

		// Then adds the fake columns.
		for (int i=0; i<numberOfColumns; i++) {
			Column column = columnsInsertedRaw.get(i);
			
			if (column.isFakeColumn()) activateColumn(column);
		}
	}
	
	protected void activateColumn(Column column) {
		columnsToModelIndex.put(column, columnsToModelIndex.size());
		modelIndexesToColumn.add(column);
		stringsToColumn.put(column.toString(), column);
		
		if (!allTables.contains(column.table)) {
			allTables.add(column.table);
		}
	}
	
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#addColumn(uk.co.solidcoresoftware.databaseapp.models.Column)
	 */
	public void addColumn(Column column) {
		columnsInsertedRaw.add(column);
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#getColumn(int)
	 */
	public Column getColumn(int columnIndex) {
		return modelIndexesToColumn.get(columnIndex);
	}
	
	public Column[] getColumns(int[] columnIndices) {
		Column[] columns = new Column[columnIndices.length];
		
		for (int i=0; i<columnIndices.length; i++) {
			columns[i] = getColumn(columnIndices[i]);
		}
		
		return columns;
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#getColumnIndex(uk.co.solidcoresoftware.databaseapp.models.Column)
	 */
	public int getColumnIndex(Column column) {
		Integer modelIndex = columnsToModelIndex.get(column);
		
		return modelIndex == null ? -1 : modelIndex;
	}

	public int[] getColumnIndices(Column[] columns) {
		int[] indices = new int[columns.length];
		
		for (int i=0; i<columns.length; i++) {
			indices[i] = getColumnIndex(columns[i]);
		}
		
		return indices;
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#getColumnIndex(java.lang.String)
	 */
	public int getColumnIndex(String tableDotColumn) {
		Column column = getColumn(tableDotColumn);
		return getColumnIndex(column);
	}
	

	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#getColumnIndex(java.lang.String, java.lang.String)
	 */
	public int getColumnIndex(String tableName, String columnName) {
		Column column = getColumn(tableName, columnName);
		return getColumnIndex(column);
	}

	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#getModelIndex(java.lang.String)
	 */
	public int getModelIndex(String tableAndColumnName) {
		return getColumnIndex(getColumn(tableAndColumnName));
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#getColumnCount()
	 */
	@Override
	public int getColumnCount() {
		return modelIndexesToColumn.size();
	}

	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#getRowCount()
	 */
	@Override
	public int getRowCount() {
		return data.size();
	}

	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#isCellEditable(int, int)
	 */
	public boolean isCellEditable(int row, int column) {
	    return true;
	}

	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#saveSelectedRow()
	 */
	public void saveSelectedRow() {
		saveRowAt(getSelectedRowIndex());
		
	}
	
	public void saveRowAt(int rowIndex) {
		Table firstTable = allTables.get(0);
		
		int columnIndex = getColumnIndex(firstTable.tableName, firstTable.idColumnName);
		
		Object valueToSave = getValueAt(rowIndex, columnIndex);
		setValueAt(new ValueToAlwaysSave(valueToSave), rowIndex, columnIndex);
	}

	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#setValueAt(java.lang.Object, int, int)
	 */
	@Override
	public void setValueAt(Object newValue, int rowIndex, int columnIndex) {
		setValueAt(newValue, rowIndex, columnIndex, null);
	}
	

	public final void setValueAt(Object newValue, int rowIndex, int columnIndex, Object[] newRowData) {
		setValueAt(newValue, rowIndex, columnIndex, newRowData, true);
	}
	
	public final void setValueAt(Object newValue, int rowIndex, int columnIndex, Object[] newRowData, boolean saveRow) {

		// The row of this cell is selected
		Row currentRow = data.get(rowIndex);
		Object[] currentRowData = currentRow.getRowData();
		
		Column column = modelIndexesToColumn.get(columnIndex);
		
		Object extras = null;

		if (newRowData == null) {
			newRowData = Arrays.copyOf(currentRowData, currentRowData.length);
		}
		
		newValue = modifyRow(newValue, extras, rowIndex, columnIndex, column, newRowData, currentRow);
		
		int rangeCheck = column.valueRangeStatus(newValue);
		
		boolean canSaveRow = !(newValue instanceof ValueToNeverSave) && (Column.VALUE_RANGE_OK == rangeCheck);
		
		if (canSaveRow && saveRow) {
			/* An attempt to save the new values of this row is done here. 
			 * If saving is successful then the current row has its values
			 * updated to that of the new row.
			 */
			saveWithUpdates(newValue, rowIndex, columnIndex, newRowData, currentRow, extras);
		}
	}
	
	
	
	public void saveWithUpdates(Object newValue, int rowIndex, int columnIndex, Object[] newRowData, Row currentRow, Object extras) {
		
		Object[] currentRowData = currentRow.getRowData();
		
		boolean rowSaved = saveRow(currentRowData, newRowData, extras);

		Iterator<ModelDataChangedListener> modelChangedListenersIt = modelChangedListeners.iterator();
		
		while (modelChangedListenersIt.hasNext()) {
			modelChangedListenersIt.next().beforeDataChanged(newValue, rowIndex, columnIndex, newRowData, currentRow, extras);
		}
		
		// If the data is invalid then 
		if (rowSaved) {
			currentRow.setRowInvalid(false);
		}
		else {
			currentRow.setRowInvalid(true);
			Object[] dataBeforeInvalid = currentRow.getBeforeInvalidRowData();
			
			for (int i=0; i<newRowData.length; i++) {
				dataBeforeInvalid[i] = currentRowData[i];
			}
		}

		
		for (int i=0; i<newRowData.length; i++) {
			currentRowData[i] = newRowData[i];
		}
		
		if (rowSaved) {
			currentRow.setRowInvalid(false);
			
			if (extras instanceof UserAlerter && extras != null) {
				Prompt prompt = new Prompt(Prompt.STATUS_SUCCESSFUL, newValue, rowIndex, columnIndex);
				((UserAlerter) extras).onPrompt(prompt);
			}
		}
		else {
			currentRow.setRowInvalid(true);
			if (extras instanceof UserAlerter && extras != null) {
				Prompt prompt = new Prompt(Prompt.STATUS_FAILED, newValue, rowIndex, columnIndex);
				((UserAlerter) extras).onPrompt(prompt);
			}
		}
		
		modelChangedListenersIt = modelChangedListeners.iterator();
		
		while (modelChangedListenersIt.hasNext()) {
			modelChangedListenersIt.next().afterDataChanged(newValue, rowIndex, columnIndex, newRowData, currentRow, extras);
		}
		
		doPostSetValueAt();
		
		// The affected row is updated in the JTable
		fireTableRowsUpdated(rowIndex, rowIndex);
		
		// Detached components are updated.
		updateValuesOfDetachedComponents();
	}
	
	private Object modifyRow(Object newValue, Object extras, int rowIndex, int columnIndex, Column column, Object[] newRow, Row currentRow) {
		
		boolean alwaysSave = false;
		
		// First check if alerter is attached
		if (newValue instanceof WrappedValue) {
			WrappedValue valueWA = (WrappedValue) newValue;
			extras = valueWA.extras;
			newValue = valueWA.newValue;
		}

		if (newValue instanceof CommandSelection) {
			((CommandSelection) newValue).invokeCommand(this, newValue, rowIndex, columnIndex);
			return new ValueToNeverSave();
		}

		if (extras instanceof UserAlerter && extras != null) {
			Prompt prompt = new Prompt(Prompt.STATUS_ATTEMPTING_TO_SAVE, newValue, rowIndex, columnIndex);
			((UserAlerter) extras).onPrompt(prompt);
		}
		
		/* If a value is an instance of ValueToAlwaysSave, then this value is always
		 * saved regardless of what the previous value was.
		 */
		if (newValue instanceof ValueToAlwaysSave) {
			newValue = ((ValueToAlwaysSave) newValue).value;
			alwaysSave = true;
		}
		else if (newValue instanceof ValueToNeverSave) {
			return new ValueToNeverSave();
		}
		else if (newValue instanceof BadForeignConstraintValue) {
			if (extras instanceof UserAlerter && extras != null) {
				Prompt prompt = new Prompt(Prompt.STATUS_FAILED, newValue, rowIndex, columnIndex);
				((UserAlerter) extras).onPrompt(prompt);
			}
			if (allowPreservedDataTypes) {
				data.get(rowIndex).getRowData()[columnIndex] = newValue;
				return new ValueToNeverSave();
			}
			return new ValueToNeverSave();
		}
		
		if (column.hasCalibrator()) {
			AbstractColumnCalibrator calibrator = column.getCalibrator();
			newValue = calibrator.calibrateToValue(newValue);
		}
		
		newValue = column.convertToCorrectDataType(newValue);
		
		// An invalid input means the data isn't modified
		if (!alwaysSave) {
			if (!column.isValidInput(newValue)) return new ValueToNeverSave();
			else if (!column.isInputPartOfSet(newValue)) return new ValueToNeverSave();
		}

		// Values are modified to the correct format
		newValue = newValue == null ? newValue : column.modifyInput(newValue);

		// Gets the current value of this cell
		Object currentValue = getValueAt(rowIndex, columnIndex);
		currentValue = currentValue == null ? currentValue : currentValue.toString();

		if (!alwaysSave) {
			boolean valuesAreEqual;
			if (currentValue == null) {
				valuesAreEqual = newValue == null;
			}
			else {
				valuesAreEqual = (newValue != null) && currentValue.toString().equals(newValue.toString());
			}
	
			// Changes are discarded here
			if (valuesAreEqual) return new ValueToNeverSave();
		}
		
		// TO DO NEW
		if (newValue == null) {
			newValue = column.getDefaultValue();
		}
		
		
		/* Creates the new row as a shallow copy (values are immutable and so
		 * a deep copy isn't needed). The column to update has the new value
		 * replacing the old value.
		
		 */
	
		newRow[columnIndex] = newValue;

		// Here, a column can decide which inputs to accept or reject for whatever reason it decides.
		if (!column.acceptModifiedInput(newRow, newValue)) return new ValueToNeverSave();

		// Custom updates are done here, often for fake columns
		column.invokeUpdatesForOtherColumns(newRow, newValue, this);
		column.updateOtherColumnsFromThisColumn(newRow, newValue, this);
		
		return newValue;
	}

	protected void doPostSetValueAt() {
		
	}

	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#getValueAt(int, int)
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Object value = data.get(rowIndex).getRowData()[columnIndex];
		
		if (value == null) {
			Column column = getColumn(columnIndex);
			if (column.isUsingNullSubstitute()) {
				value = column.getNullSubstitute();
			}
		}
		
		return value;
	}
	

	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#getValueAt(int, uk.co.solidcoresoftware.databaseapp.models.Column)
	 */
	public Object getValueAt(int rowIndex, Column column) {
		return getValueAt(rowIndex, columnsToModelIndex.get(column));
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#getRowAt(int)
	 */
	public Row getRowAt(int rowIndex) {
		return data.get(rowIndex);
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#addRow()
	 */
	public Integer addRow() {
		if (rowAddingEnabled) {
			Object[] newRow = new Object[modelIndexesToColumn.size()];
	
			for (int i=0; i<newRow.length; i++) {
				newRow[i] = modelIndexesToColumn.get(i).getDefaultValue();
			}
			
			data.add(new Row(newRow));
	
			int lastRowIndex = getRowCount() - 1;
			fireTableRowsInserted(lastRowIndex, lastRowIndex);
			
			return lastRowIndex;
		}
		return null;
	}
	
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#isRowInDatabase(java.lang.Object[])
	 */
	public boolean isRowInDatabase(Object[] row) {
		
		// A row cannot be in database if no tables exist
		if (allTables.size() == 0) return false;
		
		Iterator<Table> tables = allTables.iterator();

		while (tables.hasNext()) {
			Table table = tables.next();
			String tableAndColumnName = table.tableName + "." + table.idColumnName;
			
			Column idColumn = getColumn(tableAndColumnName);
			Object primaryKeyValue = row[columnsToModelIndex.get(idColumn)];
			
			if (primaryKeyValue == null) {
				return false;
			}
		}
		
		return true;
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#saveRow(java.lang.Object[], java.lang.Object[])
	 */
	public boolean saveRow(Object[] currentRow, Object[] newRow, Object extras) {
		for (int i=0; i<newRow.length; i++) {
			currentRow[i] = newRow[i];
		}
		
		return true;
	}

	public boolean deleteRowAt(final int modelRowIndex, Object extras) {
		return deleteRows(new int[]{modelRowIndex}, extras);
	}
	
	public boolean deleteRows(final int[] modelRowIndices, Object extras) {
		if (rowDeletingEnabled) {
			int count = data.size();
			
			// First determining indices to keep
			boolean[] indicesToKeep = new boolean[count];
			
			
			for (int i=0; i<indicesToKeep.length; i++) {
				indicesToKeep[i] = true;
			}
			
			for (int i=0; i<modelRowIndices.length; i++) {
				int nextIndex = modelRowIndices[i];
				indicesToKeep[nextIndex] = false;
			}
			
			// Next filling up new data
			ArrayList<Row> newData = new ArrayList<>();
			
			for (int i=0; i<indicesToKeep.length; i++) {
				if (indicesToKeep[i]) {
					newData.add(data.get(i));
				}
			}
	
			data = newData;
	
			doPostSetValueAt();

			Iterator<ModelDataChangedListener> modelChangedListenersIt = modelChangedListeners.iterator();
			
			while (modelChangedListenersIt.hasNext()) {
				modelChangedListenersIt.next().afterDataDeleted();
			}
			
			fireTableDataChanged();
			
			return true;
		}
		return false;
	}
	
	protected boolean deleteRow(int rowIndex, Object extras) {
		data.remove(rowIndex);
		
		Iterator<ModelDataChangedListener> modelChangedListenersIt = modelChangedListeners.iterator();
		
		while (modelChangedListenersIt.hasNext()) {
			modelChangedListenersIt.next().afterDataDeleted();
		}
		
		return true;
	}
	

	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#update()
	 */
	@Override
	public void update() {
		update(true);
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#update(boolean)
	 */
	public void update(boolean invokePostUpdate) {
		if (invokePostUpdate) {
			postUpdate();
		}

		Iterator<ModelDataChangedListener> modelChangedListenersIt = modelChangedListeners.iterator();
		
		while (modelChangedListenersIt.hasNext()) {
			modelChangedListenersIt.next().afterDataRefreshed();
		}
		
		fireTableDataChanged();
		
		// Detached components are updated.
		updateValuesOfDetachedComponents();
	}

	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#updateWithNoSelection()
	 */
	public void updateWithNoSelection() {
		data.clear();
		
		Iterator<ModelDataChangedListener> modelChangedListenersIt = modelChangedListeners.iterator();
		
		while (modelChangedListenersIt.hasNext()) {
			modelChangedListenersIt.next().afterDataRefreshed();
		}
		
		fireTableDataChanged();
		
		// Detached components are updated.
		updateValuesOfDetachedComponents();
	}
	
	protected void postUpdate() {
		
	}

	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#getData()
	 */
	@Override
	public Object[][] getData() {
		
		Object[][] dataAs2DArray = new Object[data.size()][];
		
		int rowCount = getRowCount();
		
		for (int i=0; i<rowCount; i++) {
			dataAs2DArray[i] = data.get(i).getRowData();
		}
		
		return dataAs2DArray;
	}

	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#getColumn(java.lang.String, java.lang.String)
	 */
	public Column getColumn(String tableName, String columnName) {
		return getColumn(tableName + '.' + columnName);
	}

	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#getColumn(java.lang.String)
	 */
	public Column getColumn(String tableAndColumnName) {
		return stringsToColumn.get(tableAndColumnName);
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#setSelectedRow(int)
	 */
	public void setSelectedRow(int selectedModelIndex) {
		this.selectedRowIndex = selectedModelIndex;
		updateValuesOfDetachedComponents();
	}
	
	protected void updateValuesOfDetachedComponents() {
		Iterator<InterfaceTableCellDetachedComponent> it = detachedComponents.iterator();
		
		while (it.hasNext()) {
			updateValueOfDetachedComponent(it.next());
		}
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#attachDetachedComponent(uk.co.solidcoresoftware.databaseapp.models.TableCellDetachedComponent, uk.co.solidcoresoftware.databaseapp.models.Column)
	 */
	public void attachDetachedComponent(InterfaceTableCellDetachedComponent component, Column column) {
		component.setModelAndColumn(this, column);
		detachedComponents.add(component);
		updateValueOfDetachedComponent(component);
	}
	
	public void attachDetachedComponent(LightDetachedComponent component) {
		attachDetachedComponent(component, null);
	}
	
	protected void updateValueOfDetachedComponent(InterfaceTableCellDetachedComponent component) {
		
		if (selectedRowIndex == -1) {
			component.setToNoRowSelected();
		}
		else {
			Column column = component.getColumn();
			
			Object value = null;
			if (column != null) {
				int columnIndex = getColumnIndex(column);
				value = data.get(selectedRowIndex).getRowData()[columnIndex];
			}
			
			component.setValue(selectedRowIndex, value);
		}
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#getSelectedRowIndex()
	 */
	public int getSelectedRowIndex() {
		return selectedRowIndex;
	}

	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#updateValueFromDetachedComponent(int, uk.co.solidcoresoftware.databaseapp.models.Column, java.lang.Object)
	 */
	public void updateValueFromDetachedComponent(int rowIndex, Column column, Object value) {
		if (rowIndex == -1) return;
		
		int columnIndex = getColumnIndex(column);
		setValueAt(value, rowIndex, columnIndex);
	}

	protected abstract void setupModel();

	protected abstract void setupCalibrators();
	protected abstract void defineIndices();

	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#getColumnClass(int)
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
			Column column = getColumn(columnIndex);
			
			int dataType = column.getType();

			switch (dataType) {
				case java.sql.Types.INTEGER: return Integer.class;
				case java.sql.Types.BIGINT: return Long.class;
				case java.sql.Types.FLOAT: return Float.class;
				case java.sql.Types.BOOLEAN: return Boolean.class;
				case java.sql.Types.DECIMAL: return BigDecimal.class;
				case java.sql.Types.VARCHAR: return String.class;
				case java.sql.Types.CHAR: return String.class;
				case DataType.INT_TYPE_TINYTEXT: return String.class;
				case DataType.INT_TYPE_TEXT: return String.class;
				case DataType.INT_TYPE_MEDIUMTEXT: return String.class;
				case DataType.INT_TYPE_LONGTEXT: return String.class;
			}
			
			return Object.class;
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#isActive()
	 */
	public boolean isActive() {
		return true;
	}

	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#updateAllRows()
	 */
	public void updateAllRows() {
		fireTableRowsUpdated(0, data.size()-1);
	}
	
	public Column[] getAllColumns() {
		return modelIndexesToColumn.toArray(new Column[getColumnCount()]);
	}
	
	public void setRowAddingEnabled(boolean rowAddingEnabled) {
		this.rowAddingEnabled = rowAddingEnabled;
	}
	
	public void setRowDeletingEnabled(boolean rowDeletingEnabled) {
		this.rowDeletingEnabled = rowDeletingEnabled;
	}
	
	public boolean isRowAddingEnabled() {
		return rowAddingEnabled;
	}
	
	public boolean isRowDeletingEnabled() {
		return rowDeletingEnabled;
	}
	
	public void pasteDataToModel(int[] modelRowIndices, int[] modelColumnIndices, Object[][] dataToPaste, UserAlerter alerter) {

		// Do not paste if data is empty
		if (dataToPaste == null || dataToPaste.length == 0 || dataToPaste[0] == null || dataToPaste[0].length == 0) return;
		
		int numberOfRowsToPaste = dataToPaste.length;
		int numberOfColsToPaste = dataToPaste[0].length;
		
		for (int i=0; i<modelRowIndices.length; i++) {
			int modelRowIndex = modelRowIndices[i]; int dataToPasteI = i % numberOfRowsToPaste;
			for (int j=0; j<modelColumnIndices.length; j++) {
				int modelColIndex = modelColumnIndices[j]; int dataToPasteJ = j % numberOfColsToPaste;
				
				Object cellToPaste = dataToPaste[dataToPasteI][dataToPasteJ];
				
				if (alerter == null) {
					setValueAt(cellToPaste, modelRowIndex, modelColIndex);
				}
				else {
					setValueAt(new WrappedValue(cellToPaste, alerter), modelRowIndex, modelColIndex);
				}
			}
		}
	}

	public void setAllowPreservedDataTypes(boolean allowPreservedDataTypes) {
		this.allowPreservedDataTypes = allowPreservedDataTypes;
	}
	
	public boolean isAllowPreservedDataTypes() {
		return allowPreservedDataTypes;
	}

//	public boolean valuesExistInColumn(Object[] values, int skipRowIndex, int[] columnIndices) {
//		
//	}
	
	public boolean valueExistsInColumn(Object value, int skipRowIndex, int columnIndex) {
		int rowCount = data.size();
		
		if (value == null) {
			for (int i=0; i<rowCount; i++) {
				if (i==skipRowIndex) continue;
				
				Object[] nextRow = data.get(i).getRowData();
				Object nextValue = nextRow[columnIndex];
				if (nextValue == null) return true;
			}
		}
		else {
			String valueToString = value.toString();
			
			Class<? extends Object> valueClass = value.getClass();
			
			for (int i=0; i<rowCount; i++) {
				if (i==skipRowIndex) continue;
				
				Object[] nextRow = data.get(i).getRowData();
				Object nextValue = nextRow[columnIndex];
				
				if (nextValue == null) {
					continue;
				}
				else if (nextValue.getClass().equals(valueClass)) {
					if (nextValue.equals(value)) return true;
				}
				else {
					if (nextValue.toString().equals(valueToString)) return true;
				}
			}
		}
		
		return false;
	}

	public HashSet<Long> getAllUniqueValuesInColumn(Column column) {
		HashSet<Long> values = new HashSet<>();
		
		int colIndex = getColumnIndex(column);
		
		int dataSize = data.size();
		
		for (int dIndex=0; dIndex<dataSize; dIndex++) {
			Row row = data.get(dIndex);
			Object value = row.getRowData()[colIndex];
			values.add((Long) value);
		}
		
		return values;
	}
	
	public boolean valueExistsInColumn(Object value, int skipRowIndex, Column column) {
		return valueExistsInColumn(value, skipRowIndex, getColumnIndex(column));
	}

	public void addModelDataChangedListener(ModelDataChangedListener l) {
		modelChangedListeners.add(l);
	}
	
	public Column[] getUniqueColumns() {
		
		int allColumnsCount = getColumnCount();

		// List of unique columns
		ArrayList<Column> allUniqueColumns = new ArrayList<>(allColumnsCount);
		
		for (int i=0; i<allColumnsCount; i++) {
			Column nextColumn = getColumn(i);
			
			// All unique columns are returned except for the primary-key column
			if (nextColumn.isUnique() && !nextColumn.table.idColumnName.equals(nextColumn.columnName)) {
				allUniqueColumns.add(nextColumn);
			}
		}
		
		return allUniqueColumns.toArray(new Column[allUniqueColumns.size()]);
	}

	public void haveUniqueSwitchedOn(boolean on) {
		
	}
	
	public boolean allRowsValid() {
		int rowCount = getRowCount();
		
		for (int i=0; i<rowCount; i++) {
			if (data.get(i).isRowInvalid()) {
				return false;
			}
		}
		
		return true;
	}
	
	public void revertInvalidRows() {
		int rowCount = getRowCount();
		
		for (int i=0; i<rowCount; i++) {
			
			Row nextRow = data.get(i);
			
			if (nextRow.isRowInvalid()) {
				nextRow.revertToValidRow();
			}
		}

		Iterator<ModelDataChangedListener> modelChangedListenersIt = modelChangedListeners.iterator();
		
		while (modelChangedListenersIt.hasNext()) {
			modelChangedListenersIt.next().afterDataRefreshed();
		}
		
		fireTableDataChanged();
		
		// Detached components are updated.
		updateValuesOfDetachedComponents();
	}

	public boolean allPrimaryKeysSet(final Row row) {
		Column[] allColumns = getAllColumns();
		Object[] allValues = row.getRowData();
		
		int lastIndex = Math.min(allColumns.length, allValues.length);
		
		for (int i=0; i<lastIndex; i++) {
			Column nextColumn = allColumns[i];
			if (nextColumn.isPrimary()) {
				if (allValues[i] == null) {
					return false;
				}
			}
		}
		
		return true;
	}

	public Object[] getRowDataWithBlankPrimarys(int modelIndex) {
		
		Column[] allColumns = getAllColumns();
		
		int lastIndex = allColumns.length;
		
		Object[] copiedRow = getRowAt(modelIndex).getRowData().clone();
		
		for (int i=0; i<lastIndex; i++) {
			Column nextColumn = allColumns[i];
			if (nextColumn.isPrimary()) {
				copiedRow[i] = null;
			}
		}
		
		
		return copiedRow;
	}
}