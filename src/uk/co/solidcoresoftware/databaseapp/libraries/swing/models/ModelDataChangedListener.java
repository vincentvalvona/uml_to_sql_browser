package uk.co.solidcoresoftware.databaseapp.libraries.swing.models;

public interface ModelDataChangedListener {
	public void afterDataChanged(Object newValue, int rowIndex, int columnIndex, Object[] newRow, Row currentRow, Object extras);
	public void beforeDataChanged(Object newValue, int rowIndex, int columnIndex, Object[] newRow, Row currentRow, Object extras);
	public void afterDataRefreshed();
	public void afterDataDeleted();
}
