package uk.co.solidcoresoftware.databaseapp.libraries.swing.models;

import java.util.ArrayList;
import java.util.Iterator;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.OrderByClause;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLEngine;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.WhereClause;

public abstract class BaseTableModel extends LightTableModel {

	protected String sqlForSelect;
	private boolean appendWhereForAutoValues = false;
	
	private Integer pageNumber = 1; 
	private Integer rowsPerPage = null; // null means show all rows
	
	protected String customWhereSql = null;

	protected ArrayList<String> customJoinSQL = new ArrayList<>();
	protected ArrayList<WhereClause> customWhereClauses = new ArrayList<>();
	protected ArrayList<OrderByClause> customOrderByClauses = new ArrayList<>();

	public BaseTableModel(String sqlForSelect) {
		super(true);
		this.sqlForSelect = sqlForSelect;
		setupModel();
		activateColumns();		
		setupEnsureTablesAndColumnsExist();
		setupCalibrators();
		defineIndices();
	}
	
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#saveRow(java.lang.Object[], java.lang.Object[])
	 */
	@Override
	public boolean saveRow(Object[] currentRow, Object[] newRow, Object extras) {

		Iterator<Table> tables = allTables.iterator();
		
		while (tables.hasNext()) {
			Table table = tables.next();
			String tableAndColumnName = table.tableName + "." + table.idColumnName;
			
			Column idColumn = getColumn(tableAndColumnName);
			Object primaryKeyValue = currentRow[columnsToModelIndex.get(idColumn)];
			
			if (primaryKeyValue == null) {
				Long newId = insertRowPart(currentRow, newRow, table, extras);
				if (newId == null) {
					SQLEngine.getMainConnection().rollback();
					return false;
				}
				else {
					updateForeignKeys(idColumn, newRow, newId);
				}
			}
			else {
				int columnIndex = getModelIndex(tableAndColumnName);
				updateForeignKeys(idColumn, newRow, primaryKeyValue);
				if(!updateRowPart(currentRow, newRow, currentRow[columnIndex], table.tableName, extras)) {
					SQLEngine.getMainConnection().rollback();
					return false;
				}
			}
		}
		
		return SQLEngine.getMainConnection().commit();
	}
	
	private Long insertRowPart(Object[] currentRow, Object[] newRow, Table table, Object extras) {
		int numberOfColumns = modelIndexesToColumn.size();
		ArrayList<Column> relevantColumns = new ArrayList<Column>();
		ArrayList<Object> relevantRow = new ArrayList<Object>();
		
		for (int i=0; i<numberOfColumns; i++) {
			Column thisColumn = modelIndexesToColumn.get(i);
			
			// All fake columns and alias columns are skipped
			if (thisColumn.isFakeColumn() || thisColumn.isAlias()) continue;
			
			/* If thisColumn has the same table-name as column and the column
			 * of thisColumn is not the primary key then use this column for inserting.
			 */
			if (thisColumn.table.tableName.equals(table.tableName)) {
				
				// An automatic value overrides any manual value set
				if (thisColumn.isAutomaticValueSet()) {
					newRow[i] = thisColumn.getAutomaticValue();
				}
				
				relevantColumns.add(thisColumn);
				relevantRow.add(newRow[i]);
			}
		}
		
		Object[] rowToInsert = relevantRow.toArray(new Object[relevantRow.size()]);
		Column[] columnsToInsert = relevantColumns.toArray(new Column[rowToInsert.length]);
		
		SQLInsert sqlInsert = SQLEngine.getMainConnection().insert(columnsToInsert, rowToInsert, extras);
		
		for(int i=0; i<sqlInsert.insertValueIsBeModified.length; i++) {
			if (sqlInsert.insertValueIsBeModified[i]) {
				newRow[i] = sqlInsert.newInsertValues[i];
			}
		}
		
		/* The primary-key ID is changed to that of the auto-increment value
		 * if it was inserted successfully. 
		 */
		if (sqlInsert.getGeneratedKey() != null) {
			Column idColumn = getColumn(table.tableName, table.idColumnName);
			newRow[columnsToModelIndex.get(idColumn)] = sqlInsert.getGeneratedKey();
		}

		return sqlInsert.getGeneratedKey();
	}
	
	private void updateForeignKeys(Column idColumn, Object[] newRow, Object newId) {
		// Updating foreign keys
		String[] fKeys = idColumn.getForeignKeys();
		
		for (int i=0; i<fKeys.length; i++) {
			int fKeyColumnIndex = getColumnIndex(getColumn(fKeys[i]));
			newRow[fKeyColumnIndex] = newId;
		}
	}
	
	private boolean updateRowPart(Object[] currentRow, Object[] newRow, Object primaryKeyValue, String tableName, Object extras) {
		
		ArrayList<Column> columnsToUpdate = new ArrayList<Column>();
		ArrayList<Object> newValuesToUpdate = new ArrayList<Object>();
		
		for (int i=0; i<newRow.length; i++) {
			
			Column thisColumn = modelIndexesToColumn.get(i);
			

			// This thisColumn must belong to the correct table.
			if (!thisColumn.table.tableName.equals(tableName)) continue;
			
			// Fake columns and aliases are skipped
			if (thisColumn.isFakeColumn() || thisColumn.isAlias()) continue;
			
			// If a new value is equal to an old value, this is skipped
			if (newRow[i] == null) {
				if (currentRow[i] == null) continue;
			}
//			else {
//				if (currentRow[i] != null && newRow[i].toString().equals(currentRow[i].toString())) continue;
//			}
			
			// An automatic value overrides any manual value set
			if (thisColumn.isAutomaticValueSet()) {
				newRow[i] = thisColumn.getAutomaticValue();
			}

			columnsToUpdate.add(thisColumn); newValuesToUpdate.add(newRow[i]);
		}
		
		int numberOfColumnsToUpdate = columnsToUpdate.size();
		if (numberOfColumnsToUpdate == 0) return true;

		return SQLEngine.getMainConnection().update(columnsToUpdate.toArray(new Column[numberOfColumnsToUpdate]), primaryKeyValue, newValuesToUpdate.toArray(), extras);
	}
	

	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#deleteRows(int[])
	 */
	@Override
	public boolean deleteRows(int[] modelRowIndices, Object extras) {
		if (rowDeletingEnabled) {
			for (int i=0; i<modelRowIndices.length; i++) {
				if (!deleteRow(modelRowIndices[i], extras)) {
					SQLEngine.getMainConnection().rollback();
					return false;
				}
			}
	
			if (!SQLEngine.getMainConnection().commit()) {
				return false;
			}
			
			for (int i=0; i<modelRowIndices.length; i++) {
				data.set(modelRowIndices[i], null);
			}
			
			while (data.remove(null)); // Removes all null elements (removed rows) from the list 
			
			doPostSetValueAt();

			Iterator<ModelDataChangedListener> modelChangedListenersIt = modelChangedListeners.iterator();
			
			while (modelChangedListenersIt.hasNext()) {
				modelChangedListenersIt.next().afterDataDeleted();
			}
			
			fireTableDataChanged();
			
			return true;
		}
		return false;
	}
	

	@Override
	protected boolean deleteRow(int rowIndex, Object extras) {
		Table[] tableNames = this.allTables.toArray(new Table[this.allTables.size()]);
		
		Object[] currentRow = data.get(rowIndex).getRowData();
		
		if (tableNames.length == 0) return true;
		
		for (int i=tableNames.length-1; i>=0; i--) {
			String tableAndColumnName = tableNames[i].tableName + "." + tableNames[i].idColumnName;
			int columnIndex = getModelIndex(tableAndColumnName);
			
			if (currentRow[columnIndex] == null) continue;
			
			if (!deleteRowPart(tableNames[i], currentRow[columnIndex], extras)) {
				return false;
			}
		}
		
		Iterator<ModelDataChangedListener> modelChangedListenersIt = modelChangedListeners.iterator();
		
		while (modelChangedListenersIt.hasNext()) {
			modelChangedListenersIt.next().afterDataDeleted();
		}
		
		return true;
	}
	

	private boolean deleteRowPart(Table table, Object primaryKeyValue, Object extras) {
		return SQLEngine.getMainConnection().delete(table, primaryKeyValue, extras);
	}
	

	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#update()
	 */
	@Override
	public void update() {
		update(true);
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.models.InterfaceBaseTableModel#update(boolean)
	 */
	@Override
	public void update(boolean invokePostUpdate) {
		update(true, sqlForSelect);
	}
	

	protected void update(String sqlForSelect) {
		update(true, sqlForSelect);
	}

	protected ArrayList<WhereClause> getWhereClausesList(Column[] columns) {
		
		int customWhereClausesSize = customWhereClauses.size();
		ArrayList<WhereClause> whereClausesList = new ArrayList<WhereClause>(columns.length + 1 + customWhereClausesSize);
		
		if (appendWhereForAutoValues) {
			for (int i=0; i<columns.length; i++) {
				Column column = columns[i];
				
				if (column.isAutomaticValueSet()) {
					WhereClause whereClause = new WhereClause(column, column.getAutomaticValue());
					whereClausesList.add(whereClause);
				}
			}
		}
		
		for (int i=0; i<customWhereClausesSize; i++) {
			WhereClause nextCustomWhereClause = customWhereClauses.get(i);
			whereClausesList.add(nextCustomWhereClause);
		}
		
		if (customWhereSql != null) {
			whereClausesList.add(new WhereClause(customWhereSql));
		}
		
		return whereClausesList;
	}

	public void setCustomWhereSQL(String customWhereSql) {
		this.customWhereSql = customWhereSql;
	}
	
	protected Integer getLimit() {
		return rowsPerPage;
	}
	
	protected Integer getOffset() {
		if (pageNumber == null) return null;
		
		Integer limit = getLimit();
		Integer offset = null;
		
		if (limit != null) {
			offset = limit * (pageNumber - 1);
		}
		
		return offset;
	}
	
	protected void update(boolean invokePostUpdate, String sqlForSelect) {
		
		int customJoinSQLSize = customJoinSQL.size();
		
		System.out.println("customJoinSQLSize: " + customJoinSQLSize);
		
		StringBuilder allJoinSQL = new StringBuilder();
		for (int i=0; i<customJoinSQLSize; i++) {
			String nextJoinSQL = customJoinSQL.get(i);
			allJoinSQL.append(" ");
			allJoinSQL.append(nextJoinSQL);
		}
		
		Column[] columns = new Column[modelIndexesToColumn.size()];
		modelIndexesToColumn.toArray(columns);
		
		ArrayList<WhereClause> whereClausesList = getWhereClausesList(columns);
		
		Integer limit = getLimit(); Integer offset = getOffset();
		
		data = SQLEngine.getMainConnection().select(this, sqlForSelect + allJoinSQL.toString(), columns,
				whereClausesList.toArray(new WhereClause[whereClausesList.size()]), offset, limit);
		
		// Looking for fake id columns to generate id
		//TODO
		
		if (invokePostUpdate) {
			postUpdate();
		}
		
		Iterator<ModelDataChangedListener> modelChangedListenersIt = modelChangedListeners.iterator();
		
		while (modelChangedListenersIt.hasNext()) {
			modelChangedListenersIt.next().afterDataRefreshed();
		}
		
		fireTableDataChanged();
		
		// Detached components are updated.
		updateValuesOfDetachedComponents();
	}
	
	private void setupEnsureTablesAndColumnsExist() {
		// Making sure tables exist
		Iterator<Table> tablesIt = allTables.iterator();
		
		while (tablesIt.hasNext()) {
			Table table = tablesIt.next();
			SQLEngine.getMainConnection().createTableIfNotExist(table);
		}
		
		// Making sure columns exist
		Iterator<Column> columnsIt = columnsInsertedRaw.iterator();
		
		while (columnsIt.hasNext()) {
			Column column = columnsIt.next();
			
			if (!column.isFakeColumn()) {
				SQLEngine.getMainConnection().createColumnIfNotExist(column);
			}
		}

		SQLEngine.getMainConnection().commit();
	}

	public void setAppendWhereForAutoValues(boolean appendWhereForAutoValues) {
		this.appendWhereForAutoValues = appendWhereForAutoValues;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}
	
	public void setRowsPerPage(Integer rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}
		
	
	public int getNumberOfRowsInTable(Table table) {
		return SQLEngine.getMainConnection().selectCount(table);
	}
	
	public int getRowIndexOfForCurrentPage(int absRowIndex) {
		int rowIndex = absRowIndex % rowsPerPage;
		
		if (rowIndex == 0) rowIndex += rowsPerPage;
		
		return rowIndex;
	}
	
	public int getNumberOfPagesInTable(int numberOfRows) {
		int numberOfPages;
		
		if (numberOfRows == 0) {
			numberOfPages = 1;
		}
		else {
			numberOfPages = ((numberOfRows - 1) / rowsPerPage) + 1;
		}

		return numberOfPages;
	}
	
	public int getPageNumber() {
		return pageNumber;
	}
	
	public Integer getRowsPerPage() {
		return rowsPerPage;
	}
	

	public void clearCustomJoinSQL() {
		customJoinSQL.clear();
	}

	public void addCustomJoinSQL(String joinSQL) {
		customJoinSQL.add(joinSQL);
	}
	
	public void clearCustomWhereClauses() {
		customWhereClauses.clear();
	}
	
	public void addCustomWhereClause(WhereClause whereClause) {
		customWhereClauses.add(whereClause);
	}
	
	public void clearCustomOrderByClauses() {
		customOrderByClauses.clear();
	}
	
	public void addCustomOrderByClause(OrderByClause orderByClause) {
		customOrderByClauses.add(orderByClause);
	}
	
	public ArrayList<OrderByClause> getCustomOrderByClauses() {
		return customOrderByClauses;
	}
}
