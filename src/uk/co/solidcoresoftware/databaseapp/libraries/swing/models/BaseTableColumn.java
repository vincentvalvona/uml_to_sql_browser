package uk.co.solidcoresoftware.databaseapp.libraries.swing.models;

import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.DataType;

public class BaseTableColumn extends TableColumn implements InterfaceBaseTableColumn {
	
	private Column column = null;
	
	/* The type of column determines the min width weighting. For example, a date and time column will in general 
	 * be bigger than others, so a the minimum width will be twice the normal mininum width if the min width
	 * weighting is 2.
	 */
	private int minWidthWeighting = 1;
	
	public final static int DEFAULT_MIN_WIDTH_WEIGHTING = 1;
	
	public BaseTableColumn(int modelIndex, int width, TableCellRenderer cellRenderer, TableCellEditor cellEditor) {
		super(modelIndex, width, cellRenderer, cellEditor);
		setMinWidth(100);
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.views.tables.InterfaceBaseTableColumn#setColumn(uk.co.solidcoresoftware.databaseapp.models.Column)
	 */
	public void setColumn(Column column) {
		this.column = column;
		
		int columnType = column.getType();
		
		if (java.sql.Types.TIMESTAMP == columnType
				|| DataType.INT_TYPE_DATETIME == columnType) {
			setMinWidthWeighting(2);
		}
		else {
			setMinWidthWeighting(DEFAULT_MIN_WIDTH_WEIGHTING);
		}
	}
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.views.tables.InterfaceBaseTableColumn#getColumn()
	 */
	public Column getColumn() {
		return this.column;
	}

	public void setWidths(int width) {
		setMinWidth(width);
		setPreferredWidth(width);
		setMaxWidth(width);
	}
	
	public void setMinWidthWeighting(int minWidthWeighting) {
		this.minWidthWeighting = minWidthWeighting;
	}
	
	public int getMinWidthWeighting() {
		return minWidthWeighting;
	}
}
