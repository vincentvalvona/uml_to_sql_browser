package uk.co.solidcoresoftware.databaseapp.libraries.swing.models;

public class Row {
	
	private boolean rowInvalid = false;
	
	private Object[] rowDataBeforeInvalid;
	private final Object[] rowData;
	
	public Row(int size) {
		this.rowData = new Object[size];
		createBeforeInvalidRowData();
	}
	
	public Row(Object[] rowData) {
		this.rowData = rowData;
		createBeforeInvalidRowData();
	}
	
	private void createBeforeInvalidRowData() {
		this.rowDataBeforeInvalid = new Object[this.rowData.length];
	}
	
	public Object[] getRowData() {
		return rowData;
	}
	
	public Object[] getBeforeInvalidRowData() {
		return rowDataBeforeInvalid;
	}
	
	public void setRowInvalid(boolean rowInvalid) {
		this.rowInvalid = rowInvalid;
	}
	
	public boolean isRowInvalid() {
		return rowInvalid;
	}

	public void revertToValidRow() {
		for (int i=0; i<rowData.length; i++) {
			rowData[i] = rowDataBeforeInvalid[i];
		}
		setRowInvalid(false);
	}
}
