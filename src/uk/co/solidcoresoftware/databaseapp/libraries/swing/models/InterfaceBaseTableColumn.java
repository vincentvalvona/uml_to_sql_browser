package uk.co.solidcoresoftware.databaseapp.libraries.swing.models;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;

public interface InterfaceBaseTableColumn {

	public void setColumn(Column column);
	public Column getColumn();
	public Object getHeaderValue();
}
