package uk.co.solidcoresoftware.databaseapp.libraries.swing.models;

public class ModifySingleCellWithPreservedDataType {
	
	public final Object value; 
	
	public ModifySingleCellWithPreservedDataType(Object value) {
		this.value = value;
	}
}
