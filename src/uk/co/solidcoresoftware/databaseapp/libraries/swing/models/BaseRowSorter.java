package uk.co.solidcoresoftware.databaseapp.libraries.swing.models;

import java.math.BigDecimal;
import java.util.Comparator;

import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.DataType;

public class BaseRowSorter extends TableRowSorter<TableModel> implements InterfaceBaseRowSorter {
	
	final LightTableModel tableModel;
	
	public BaseRowSorter(LightTableModel tableModel) {
		super(tableModel);
		this.tableModel = tableModel;
	}
	
	
	
	/* (non-Javadoc)
	 * @see uk.co.solidcoresoftware.databaseapp.views.tables.InterfaceBaseRowSorter#getComparator(int)
	 */
	@Override
	public Comparator<?> getComparator(int columnIndex) {
		Column column = tableModel.getColumn(columnIndex);
		
		int dataType = column.getType();
		
		switch (dataType) {
			case java.sql.Types.INTEGER: return intCompare;
			case java.sql.Types.BIGINT: return longCompare;
			case java.sql.Types.BOOLEAN: return booleanCompare;
			case java.sql.Types.DECIMAL: return bigDecimalCompare;
			case java.sql.Types.VARCHAR: return stringCompare;
			case java.sql.Types.CHAR: return stringCompare;
			case DataType.INT_TYPE_TINYTEXT: return stringCompare;
			case DataType.INT_TYPE_TEXT: return stringCompare;
			case DataType.INT_TYPE_MEDIUMTEXT: return stringCompare;
			case DataType.INT_TYPE_LONGTEXT: return stringCompare;
		}
		
		return objectCompare;
	}
	
	public static Comparator<Integer> intCompare = new Comparator<Integer>() {
		@Override
		public int compare(Integer value1, Integer value2) {
			return value1.compareTo(value2);
		}
	};
	

	public static Comparator<Long> longCompare = new Comparator<Long>() {
		@Override
		public int compare(Long value1, Long value2) {
			return value1.compareTo(value2);
		}
	};
	
	public static Comparator<Boolean> booleanCompare = new Comparator<Boolean>() {
		@Override
		public int compare(Boolean value1, Boolean value2) {
			return value1.compareTo(value2);
		}
	};
	
	public static Comparator<BigDecimal> bigDecimalCompare = new Comparator<BigDecimal>() {
		@Override
		public int compare(BigDecimal value1, BigDecimal value2) {
			return value1.compareTo(value2);
		}
	};
	
	public static Comparator<String> stringCompare = new Comparator<String>() {
		@Override
		public int compare(String value1, String value2) {
			return value1.compareTo(value2);
		}
	};

	public static Comparator<Object> objectCompare = new Comparator<Object>() {
		@Override
		public int compare(Object value1, Object value2) {
			if (value1 == null) return 1;
			return value1.toString().compareTo(value2.toString());
		}
	};
}
