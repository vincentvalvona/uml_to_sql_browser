package uk.co.solidcoresoftware.databaseapp.libraries.swing.models;

public class ThreadTracer {

	public static void printStackTrace(Thread currentThread, String traceName) {
		StackTraceElement[] stackTrace = currentThread.getStackTrace();
		
		System.out.println("traceName: " + traceName);
		for (int i=0; i<stackTrace.length; i++) {
			StackTraceElement nextStack = stackTrace[i];
			System.out.println(nextStack.getClassName() + "." + nextStack.getMethodName() + ", line: " + nextStack.getLineNumber());
		}
	}

}
