package uk.co.solidcoresoftware.databaseapp.libraries.swing.models;

public class BadForeignConstraintValue {
	public final Object value;

	public BadForeignConstraintValue(Object value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return value == null ? "" : value.toString();
	}
}
