package uk.co.solidcoresoftware.databaseapp.libraries.swing.models;

public class SQLInsert {
	
	private Long generatedKey = null;
	
	public final boolean[] insertValueIsBeModified;
	public final Object[] newInsertValues;
	
	public SQLInsert(int numberOfValues) {
		insertValueIsBeModified = new boolean[numberOfValues];
		newInsertValues = new Object[numberOfValues];
	}
	
	public void setValueToBeModified(int index, Object value) {
		insertValueIsBeModified[index] = true;
		newInsertValues[index] = value;
	}
	
	public void setGeneratedKey(Long generatedKey) {
		this.generatedKey = generatedKey;
	}
	
	public Long getGeneratedKey() {
		return generatedKey;
	}
}
