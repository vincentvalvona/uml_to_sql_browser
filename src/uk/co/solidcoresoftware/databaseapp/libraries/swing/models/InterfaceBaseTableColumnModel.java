package uk.co.solidcoresoftware.databaseapp.libraries.swing.models;

public interface InterfaceBaseTableColumnModel {
	void setColumnVisible(InterfaceBaseTableColumn aTableColumn, boolean visible);
	boolean columnVisible(InterfaceBaseTableColumn aTableColumn);
	void addColumn(InterfaceBaseTableColumn column);
	void removeColumn(InterfaceBaseTableColumn column);
}
