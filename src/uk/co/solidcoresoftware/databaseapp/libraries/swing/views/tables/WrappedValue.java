package uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables;

import growoffshore.UserAlerter;

public class WrappedValue {

	public final Object newValue;
	public final Object extras;

	public WrappedValue(Object newValue, Object extras) {
		this.newValue = newValue;
		this.extras = extras;
	}
}
