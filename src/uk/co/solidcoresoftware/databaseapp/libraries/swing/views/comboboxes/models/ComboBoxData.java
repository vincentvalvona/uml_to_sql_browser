package uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.models;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.ComboBoxEditor;
import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JComboBox.KeySelectionManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableCellEditor;

import growoffshore.cellEditorsAndRenderers.ComboBoxDataCellEditor;
import growoffshore.cellEditorsAndRenderers.ComboBoxDataCellEditor.SpecialComboBox;

import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Data;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Option;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.ValueToNeverSave;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLEngine;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.Row;

public abstract class ComboBoxData implements Data {

	// The columns are stored here
	protected ArrayList<Column> columnsArrayList = new ArrayList<Column>();
	private HashMap<String, Integer> columnToIndex = new HashMap<String, Integer>();
	
	// The data is updated to and stored here
	protected ArrayList<Row> data = null;
	
	// The physical combobox
	private final ComboBoxDataCellEditor.SpecialComboBox<Option> comboBox = new ComboBoxDataCellEditor.SpecialComboBox<>();

	public final ArrayList<Option> allOptions = new ArrayList<Option>();
	

	private JTextField textFilter = new JTextField();
	
	boolean itemChangeListenerOn = true;
	
	// The query to use
	protected String query;
	private ComboBoxDataCellEditor cellEditor;
	

	private boolean eventsOn = false;
	
	// Support for autocompletion of the ComboBox

	private void addTextListeners() {
		textFilter.getDocument().addDocumentListener(new DocumentListener() {
			
			private void onAnyUpdate(DocumentEvent e) {
				if (eventsOn) {
					itemChangeListenerOn = false;
					updateListOptions();
					comboBox.setPopupVisible(false);
					comboBox.setPopupVisible(true);
					itemChangeListenerOn = true;
				}
			}
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				onAnyUpdate(e);
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				onAnyUpdate(e);
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				onAnyUpdate(e);
			}
		});
	}
	
	public ComboBoxData(String query) {
		this.query = query;
		
		addTextListeners();
		
		ComboBoxEditor cBoxEditor = new ComboBoxEditor() {
			@Override
			public void setItem(Object anObject) {
//				eventsOn = false;
////				itemChangeListenerOn = false;
//				System.out.println("anObject: " + anObject);
////				textFilter.setText("");
////				itemChangeListenerOn = true;
//				eventsOn = true;
			}
			
			@Override
			public void selectAll() {
				textFilter.selectAll();;
			}
			
			
			@Override
			public Object getItem() {
				if (textFilter.getText().trim().equals("")) {
					return new ValueToNeverSave();
				}
				
				return new Option(null, "");
			}
			
			@Override
			public Component getEditorComponent() {
//				resetTextField();
//				eventsOn = false;
//				itemChangeListenerOn = false;
//				textFilter = new JTextField("");
//				addTextListeners();
//				itemChangeListenerOn = true;
//				eventsOn = true;
				eventsOn = true;
				return textFilter;
			}
			
			@Override
			public void addActionListener(ActionListener l) {
				textFilter.addActionListener(l);
			}

			@Override
			public void removeActionListener(ActionListener l) {
				textFilter.removeActionListener(l);
			}
		};
		
		comboBox.setEditable(true);
		comboBox.setEditor(cBoxEditor);
		
		comboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (eventsOn && itemChangeListenerOn && e.getActionCommand().equals("comboBoxChanged")) {
					itemChangeListenerOn = false;
					comboBoxActionPerformed();
					itemChangeListenerOn = true;
				}
			}
		});
	}
	
	public Object[] getRow(int index) {
		return data.get(index).getRowData();
	}
	
	public abstract Option getOptionFromValue(Object value);

	public Object[][] getDataWithExtraRowAtEnd() {
		return data.toArray(new Object[data.size()+1][]);
	}

	@Override
	public Object[][] getData() {
		return data.toArray(new Object[data.size()][]);
	}

	public void addColumn(Column column) {
		int columnCount = columnsArrayList.size();
		
		columnToIndex.put(column.tableDotColumn(), columnCount);
		columnsArrayList.add(column);
	}
	
	public int getColumnIndex(String tableDotColumn) {
		Integer indexObj = columnToIndex.get(tableDotColumn);
		
		return indexObj == null ? -1 : indexObj;
	}
	
	public Column getColumn(String tableDotColumn) {
		int columnIndex = getColumnIndex(tableDotColumn);
		
		if (columnIndex == -1) return null;
		
		return columnsArrayList.get(columnIndex);
	}
	
	public void updateData() {
		updateData(query);
	}
	
	protected void updateData(String query) {
		data = SQLEngine.getMainConnection().select(null, query, columnsArrayList.toArray(new Column[columnsArrayList.size()]), null, null, null);
		postUpdateData();
		

		allOptions.clear();
		allOptions.ensureCapacity(data.size()+2);
		

		allOptions.add(new Option(null, getComboBoxDisplay(null)));
		allOptions.add(new Option(new ComboBoxSelectedAddRow(cellEditor), "New item..."));
		
		if (data != null) {
			Iterator<Row> dataIt = data.iterator();
			
			int index = 0;
			while(dataIt.hasNext()) {
				Object[] row = dataIt.next().getRowData();
				Option option = new Option(getValue(row), getComboBoxDisplay(row));
				allOptions.add(option);
			}
		}
	}
	
	
	
	
	protected abstract void postUpdateData();

	public abstract Object getValue(Object[] row);
	
	public abstract String getComboBoxDisplay(Object[] row);
	
	
	public SpecialComboBox<Option> getComboBox() {
		return comboBox;
	}

	public int getSelectedIndex() {
		return comboBox.getSelectedIndex();
	}
	
	public Option getSelectedItem() {
		
		Object selectedItemRaw = comboBox.getSelectedItem();
		
		Option selectedItem = null;
		
		if (selectedItemRaw instanceof Option) {
			selectedItem = (Option) selectedItemRaw;
		}
		
		return selectedItem;
	}

	private void updateListOptions() {
		comboBox.removeAllItems();

		Iterator<Option> allOptionsIt = allOptions.iterator();

		String textFilterTextLower = textFilter.getText().toLowerCase().trim();
		
		while (allOptionsIt.hasNext()) {
			Option nextOption = allOptionsIt.next();
			
			if (nextOption.value == null) {
				if (textFilterTextLower.equals("")) {
					
				}
			}
			
			String nextOptionStringLower = nextOption.stringValue.toLowerCase();
			
			if (nextOptionStringLower.contains(textFilterTextLower)) {
				comboBox.addItem(nextOption);
			}
		}
	}
	
	protected void comboBoxActionPerformed() {
		
	}
	
	@Override
	public void update() {
		updateData();
		updateListOptions();
	}

	public void setCellEditor(ComboBoxDataCellEditor cellEditor) {
		this.cellEditor = cellEditor;
	}

	public void resetTextField() {
//		eventsOn = false;
		itemChangeListenerOn = false;
		textFilter.setText("");
		itemChangeListenerOn = true;
//		eventsOn = true;
	}

	public String getTextValue() {
		return textFilter.getText();
	}
	
	public void setEventsOn(boolean eventsOn) {
		this.eventsOn = eventsOn;
	}
	
	public boolean isEventsOn() {
		return eventsOn;
	}
}
