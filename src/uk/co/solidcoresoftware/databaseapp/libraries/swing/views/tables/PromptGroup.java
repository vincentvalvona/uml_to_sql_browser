package uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables;

import java.util.ArrayList;

public class PromptGroup {
	private ArrayList<Prompt> allPrompts = new ArrayList<>();
	
	public void addPrompt(Prompt prompt) {
		allPrompts.add(prompt);
	}
	
	public ArrayList<Prompt> allPrompts() {
		return allPrompts;
	}
	
	public Prompt lastPrompt() {
		return allPrompts.get(allPrompts.size()-1);
	}
}
