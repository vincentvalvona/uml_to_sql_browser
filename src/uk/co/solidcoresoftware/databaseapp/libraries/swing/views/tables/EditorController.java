package uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables;

public interface EditorController {
	
	public final int CELL_IS_EDITABLE_INHERIT  = 0;
	public final int CELL_IS_EDITABLE_NO = 1;
	public final int CELL_IS_EDITABLE_YES = 2;
	
	public int isCellEditable(BaseTable baseTable, int row, int column);
}
