package uk.co.solidcoresoftware.databaseapp.libraries.swing.views.functions;

import java.awt.Component;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.AbstractButton;

public final class Functions {

	public final static int ACTION_SYSTEM_CUT = 1;
	public final static int ACTION_SYSTEM_COPY = 2;
	public final static int ACTION_SYSTEM_PASTE = 3;
	public final static int ACTION_SYSTEM_SELECT_ALL = 4;
	
	private Functions() {}
	
	public static Object[][] getClipboardDataAs2DArray() {
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		
		Transferable tran = null;
		
		try {
			tran = clipboard.getContents(null);
		}
		catch(IllegalStateException e) {
			tran = null;
		}
		
		if (tran != null) {
			if (tran.isDataFlavorSupported(DataFlavor.stringFlavor)) {
				try {
					String dataToPaste = (String) tran.getTransferData(DataFlavor.stringFlavor);
					
					String dataToPasteModified = dataToPaste.replace("\\r\\n", "\\n").replace('\r', '\n');
					
					Object[] dataToPasteSplit = dataToPasteModified.split("\\n");
					
					Object[][] dataToPasteCells = new Object[dataToPasteSplit.length][];
					
					for (int i=0; i<dataToPasteSplit.length; i++) {
						dataToPasteCells[i] = ((String) dataToPasteSplit[i]).split("\\t", -1);
					}
					
					return dataToPasteCells;
				} catch (UnsupportedFlavorException | IOException e) {
					return null;
				}
			}
		}
		
		return null;
	}
	

	public static void addSystemAction(AbstractButton button, final int action) {
		addSystemAction(button, action);
	}
	
	public static void addSystemAction(AbstractButton button, Window window, final int action) {
		
		if (window == null) {
			window = (Window) button.getTopLevelAncestor();
		}
		
		final Window windowFinal = window;
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				Component focusOwner = windowFinal.getFocusOwner();
				
				if (focusOwner instanceof EditFunctions) {

					EditFunctions focusOwnerCasted = ((EditFunctions) focusOwner);
					
					switch(action) {
						case ACTION_SYSTEM_CUT: focusOwnerCasted.doCut(); break;
						case ACTION_SYSTEM_COPY: focusOwnerCasted.doCopy(); break;
						case ACTION_SYSTEM_PASTE: focusOwnerCasted.doPaste(); break;
						case ACTION_SYSTEM_SELECT_ALL: focusOwnerCasted.doSelectAll(); break;
					}
				}
				
			}
		});
		
		
		Component ancestor = button;
		
		do {
			ancestor = ancestor.getParent();	
		}
		while (ancestor != null);
		
		
		
		
		
	}
}
