package uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.models;

import growoffshore.cellEditorsAndRenderers.ComboBoxDataCellEditor;
import growoffshore.cellEditorsAndRenderers.EditRowPopupCommander;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;

public class ComboBoxSelectedAddRow implements CommandSelection {
	
	private EditRowPopupCommander cellEditor;

	public ComboBoxSelectedAddRow(EditRowPopupCommander cellEditor) {
		this.cellEditor = cellEditor;
	}

	@Override
	public void invokeCommand(LightTableModel model, Object newValue, int rowIndex, int columnIndex) {
		cellEditor.addRowOptionSelected(model, newValue, rowIndex, columnIndex);
	}
}
