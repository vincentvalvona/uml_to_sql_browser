package uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables;

import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.EventObject;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import growoffshore.ContainsManualStop;
import growoffshore.DateChooserForPopup;
import growoffshore.PasteModifier;
import growoffshore.UserAlerter;
import growoffshore.cellEditorsAndRenderers.BaseTableColourer;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Data;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.DataType;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Option;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Scenario;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLEngine;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseRowSorter;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableColumn;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableColumnModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableColumn;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.ModifySingleCellWithPreservedDataType;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.Row;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.BaseCellEditor;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.BaseCellRenderer;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.CustomDialog;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.functions.EditFunctions;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.functions.Functions;
import uk.co.solidcoresoftware.databaseapp.views.tables.UserTableScrollPane;

public abstract class BaseTable extends JTable implements EditFunctions, InterfaceBaseTable, BaseTableColourer {

	protected static final String DIALOG_TITLE_ERROR_PASTING = "Error pasting data to some fields!";
	protected static final String DIALOG_TITLE_ERROR_DUPLICATING_ROWS = "Error duplicating some rows!";
	protected static final String DIALOG_TITLE_ADD_NEW_ROW = "Add new row...";
	protected static final String DIALOG_TITLE_ERROR_ADDING_NEW_ROW = "Error adding new row!";
	
	private static int numberOfModifyModes = 0;
	public final static int MODIFY_DATA_NONE = numberOfModifyModes++;
	public final static int MODIFY_DATA_REAL_VALUE_TO_STRING_VALUE = numberOfModifyModes++;
	public final static int MODIFY_DATA_STRING_VALUE_TO_REAL_VALUE = numberOfModifyModes++;
	
	public LightTableModel model;
	protected BaseTableColumnModel tableColumnModel;
	
	public boolean startEditingKeepValue = false;
	
	private Window window;
	protected UserAlerter alerter;
	
	private ColourOverrider defaultColourOverrider = new ColourOverrider() {
		public Color setColourOverrides(Component comp, Object value, boolean isSelected, boolean hasFocus, int rowIndex, int columnIndex) {

			Row row = model.getRowAt(rowIndex);
			
			Color colour;
			
			if (row.isRowInvalid()) {
				if (isRowSelected(rowIndex) && isColumnSelected(columnIndex)) colour = INVALID_COLOUR_SELECTED;
				else if (isRowSelected(rowIndex)) colour = INVALID_COLOUR_HIGHLIGHTED;
				else colour = INVALID_COLOUR_NORMAL;
			}
			else {
				if (isRowSelected(rowIndex) && isColumnSelected(columnIndex)) colour = COLOUR_SELECTED;
				else if (isRowSelected(rowIndex)) colour = COLOUR_HIGHLIGHTED;
				else colour = COLOUR_NORMAL;
			}
			
			return colour;
		}
	};
	private ColourOverrider colourOverrider = defaultColourOverrider;

	

	private EditorController defaultEditorController = new EditorController() {
		
		@Override
		public int isCellEditable(BaseTable baseTable, int row, int column) {
			if (baseTable.isEditing()) {
				TableCellEditor currentCellEditor = getCellEditor();
				if (currentCellEditor != null) currentCellEditor.stopCellEditing();
				return CELL_IS_EDITABLE_NO;
			}
			
			return CELL_IS_EDITABLE_INHERIT;
		}
	};
	private EditorController editorController = defaultEditorController;
	
	protected Color COLOUR_SELECTED = new Color(255, 255, 0);
	protected Color COLOUR_HIGHLIGHTED = new Color(255, 255, 200);
	protected Color COLOUR_NORMAL = new Color(255, 255, 255);
	
	protected Color INVALID_COLOUR_SELECTED = new Color(255, 128, 0);
	protected Color INVALID_COLOUR_HIGHLIGHTED = new Color(255, 128, 64);
	protected Color INVALID_COLOUR_NORMAL = new Color(255, 128, 128);
	protected Color INVALID_DISABLED_SELECTED = new Color(180, 180, 180);
	protected Color INVALID_DISABLED = new Color(128, 128, 128);
	
	protected Color UNSAVED_COLOUR_SELECTED = new Color(255, 255, 0);
	protected Color UNSAVED_COLOUR_HIGHLIGHTED = new Color(0, 255, 255);
	protected Color UNSAVED_COLOUR_NORMAL = new Color(0, 255, 255);
	
	private UserTableScrollPane userTableScrollPane;
	
	@Override
	public boolean isCellEditable(int row, int column) {
		
		int isCellEditable = editorController.isCellEditable(this, row, column);
		
		switch (isCellEditable) {
			case EditorController.CELL_IS_EDITABLE_YES: return true;
			case EditorController.CELL_IS_EDITABLE_NO: return false;
			default: return super.isCellEditable(row, column);
		}
	}
	
	public BaseTable(Window window, final LightTableModel model, UserAlerter alerter) {
		this.model = model;
		this.tableColumnModel = new BaseTableColumnModel(this);
		this.alerter = alerter;

		setColumnModel(this.tableColumnModel);
		
		setAutoCreateColumnsFromModel(false);
		setSelectionBackground(Color.YELLOW);
		
		setFocusable(true);
		
		setAutoCreateRowSorter(false);
//		setRowSorter(new BaseRowSorter(model));

		getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting()) {
					int rowIndex = BaseTable.this.getSelectedRow();
					
					if (rowIndex == -1) {
						model.setSelectedRow(rowIndex);
					}
					else {
						int modelRowIndex = convertRowIndexToModel(rowIndex);
						model.setSelectedRow(modelRowIndex);
					}
				}
			}
		});
		
		
		this.window = window;
		
		putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		
		setModel(model);
		
		setVisible(true);
		
		setupTable();
		showAllColumns();

		setRowSelectionAllowed(false);
		setColumnSelectionAllowed(false);
		setCellSelectionEnabled(true);
		
		//model.update();
		
		getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0),
				"enter");
		getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0),
				"enter");
		getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0),
				"deleteSelectedCells");
		getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_V, InputEvent.CTRL_DOWN_MASK),
				"pasteFromClipboard");
		getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_DOWN_MASK),
				"copyToClipboard");
		

		getActionMap().put("editCellKeepValue", new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				BaseTable.this.startEditingKeepValue = true;
				getActionMap().get("startEditing").actionPerformed(e);
			}
		});
		
		getActionMap().put("deleteSelectedCells", new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if (isEditing()) return;
				
				int[] selectedRows = BaseTable.this.getSelectedRows();
				int[] selectedColumns = BaseTable.this.getSelectedColumns();
				
				Column[] columns = new Column[selectedColumns.length];
				
				// Converting column indices to the model's equivilent.
				for (int colNum=0; colNum<selectedColumns.length; colNum++) {
					int columnIndex = selectedColumns[colNum];
					columns[colNum] = model.getColumn(convertColumnIndexToModel(columnIndex));
				}
				
				for (int rowNum=0; rowNum<selectedRows.length; rowNum++) {
					int rowIndex = selectedRows[rowNum];
					
					for (int colNum=0; colNum<selectedColumns.length; colNum++) {
						int columnIndex = selectedColumns[colNum];
						
						Column column = columns[colNum];
						
						if (!column.canResetValue()) continue;
						
						Object newValue = column.modifyInput(column.getDefaultValue());
						
						BaseTable.this.setValueAt(newValue, rowIndex, columnIndex);
					}
				}
			}
		});
		
		getActionMap().put("copyToClipboard", new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent event) {
				doCopy();
			}
		});
		

		getActionMap().put("pasteFromClipboard", new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent event) {
				doPaste();
			}
		});
		
		getActionMap().put("enter", new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int currentRow = getSelectedRow();
				if (currentRow == getRowCount()-1) {
					if (model.isRowAddingEnabled()) {
						Integer rowIndex = addRow();
						
						if (rowIndex == null) return;
					}
				}
				
				getActionMap().get("selectNextRow").actionPerformed(e);
			}
		});

//		setTableHeader(new JTableHeader(this.tableColumnModel) {
//			
//			@Override
//			protected void processMouseMotionEvent(MouseEvent e) {
//				if (e.getButton() != MouseEvent.BUTTON1 && e.getID() == MouseEvent.MOUSE_DRAGGED) {
//					// Do nothing
//				}
//				else {
//					super.processMouseMotionEvent(e);
//				}
//			}
//		});
		
		getTableHeader().addMouseListener(new MouseAdapter() {
			@Override
	    	public void mousePressed(MouseEvent mouseEvent) {	    	
		    	if (mouseEvent.getButton() == MouseEvent.BUTTON3) {
			        int index = columnAtPoint(mouseEvent.getPoint());
			        
			        SwingUtilities.invokeLater(new Runnable() {
						
						@Override
						public void run() {
					        if (index >= 0) {
					        	BaseTableColumn[] allColumns = getAllColumns();
			
					        	if (index >= allColumns.length) {
					        		return;
					        	}
					        	
					        	JPopupMenu menu = new JPopupMenu();
					        	ArrayList<JMenuItem> visibleMenuItems = new ArrayList<>(allColumns.length);
			  
					        	int numberOfVisibleColumns = 0;
					        	
								for (int i=0; i<allColumns.length; i++) {
									final BaseTableColumn aTableColumn = allColumns[i];
									final boolean columnVisible = tableColumnModel.columnVisible(aTableColumn);
									
									
									JMenuItem menuItem = menu.add(new JCheckBoxMenuItem(aTableColumn.getHeaderValue().toString()));
									menuItem.setSelected(columnVisible);
									menuItem.addActionListener(new ActionListener() {
										@Override
										public void actionPerformed(ActionEvent e) {
											tableColumnModel.setColumnVisible(aTableColumn, !columnVisible);
										}
									});

									if (columnVisible) {
										numberOfVisibleColumns++;
										visibleMenuItems.add(menuItem);
									}
								}
								
								menu.add(new JSeparator());
								
								BaseTableColumn selectedColumn = tableColumnModel.getColumn(index);
								JMenuItem showOnlyThisItem = menu.add(new JMenuItem("Show only " + selectedColumn.getHeaderValue().toString()));
								showOnlyThisItem.addActionListener(new ActionListener() {
									@Override
									public void actionPerformed(ActionEvent e) {
										tableColumnModel.showOnlyThisColumn(selectedColumn);
									}
								});
								

								JMenuItem showAllItems = menu.add(new JMenuItem("Show all columns"));
								showAllItems.addActionListener(new ActionListener() {
									@Override
									public void actionPerformed(ActionEvent e) {
										tableColumnModel.showAllColumns();
									}
								});
								
								/* If less than two columns are visible then disable the remaining items so that it's
								 *  impossible to make all columns invisible.
								 */
								if (numberOfVisibleColumns <= 1) {
									
									int numberOfVisibleMenuItems = visibleMenuItems.size();
									
									for (int i=0; i<numberOfVisibleMenuItems; i++) {
										visibleMenuItems.get(i).setEnabled(false);
									}
								}
						          
						        addCustomMenuOptions(menu, index, allColumns[index].getColumn());
					          
						        menu.show(getTableHeader(), mouseEvent.getX(), mouseEvent.getY());
						    }
						}
					});
			    }
	    	
			}
	    });
		
		addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent mouseEvent) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent mouseEvent) {
				if (mouseEvent.getButton() == MouseEvent.BUTTON1) {
					int clickedRow = rowAtPoint(mouseEvent.getPoint());
					int clickedColumn = columnAtPoint(mouseEvent.getPoint());
					
					if (clickedRow != -1 && clickedColumn != -1) {
						if (!(getEditingRow() == clickedRow && getEditingColumn() == clickedColumn)) {
							TableCellEditor currentCellEditor = getCellEditor();
							if (currentCellEditor != null) {
								currentCellEditor.stopCellEditing();
								if (currentCellEditor instanceof ContainsManualStop) {
									((ContainsManualStop) currentCellEditor).manuallyEditingStopped();
								}
								else {
									currentCellEditor.stopCellEditing();
								}
							}
						}
					}
				}
			}
			
			@Override
			public void mouseExited(MouseEvent mouseEvent) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent mouseEvent) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent mouseEvent) {
				
			}
		});
	}
	
	protected void addCustomMenuOptions(JPopupMenu menu, int index, Column column) {};

	/**
	 * Used to setup the unique features of each subclass of UserTable.
	 */
	protected abstract void setupTable();

	/** The model always returns the new row number added (a value >= 0). You can override this
	 * method to control whether a row is added or not. Always return null if you don't add a row
	 * to the model, otherwise always return the row added in the model.
	 *  
	 * @return The index of the added row in the model.
	 */
	public Integer addRow() {
		return convertRowIndexToView(model.addRow());
	}
	
	@Override
	public Class<?> getColumnClass(int columnIndex) {
		
		Column column = model.getColumn(columnIndex);

		switch (column.getType()) {
			case java.sql.Types.BOOLEAN: return Boolean.class;
			case java.sql.Types.INTEGER: return Integer.class;
			case java.sql.Types.BIGINT: return Long.class;
			case java.sql.Types.FLOAT: return Float.class;
			case java.sql.Types.DOUBLE: return Double.class;
			case java.sql.Types.DATE: return Date.class;
			case java.sql.Types.TIME: return Time.class;
			case java.sql.Types.TIMESTAMP: return Timestamp.class;
			case DataType.INT_TYPE_DATETIME: return Timestamp.class;
			default: return String.class;
		}
	}
	
	public LightTableModel getTableModel() {
		return model;
	}
	
	public void addColumn(String tableAndColumn, String columnTitle, int width, int columnType, TableCellRenderer cellRenderer, TableCellEditor cellEditor) {

		int modelIndex = model.getModelIndex(tableAndColumn);
		Column column = model.getColumn(tableAndColumn);
		
		if (cellRenderer == null) {
			cellRenderer = new BaseCellRenderer(this, model);
			((BaseCellRenderer) cellRenderer).setColumn(model.getColumn(tableAndColumn));
		}
		if (cellEditor == null) {
			cellEditor = new BaseCellEditor(new JTextField());
			
			BaseCellEditor cellEditorCast = (BaseCellEditor) cellEditor;
			cellEditorCast.setColumn(column);
			cellEditorCast.setTable(model);
		}
		
		BaseTableColumn userTableColumn = new BaseTableColumn(modelIndex, width, cellRenderer, cellEditor);
		userTableColumn.setColumn(column);
		
		if (column != null) {
			if (columnTitle != null) {
				userTableColumn.setHeaderValue(columnTitle);
			}
		}
		
		tableColumnModel.addColumn(userTableColumn);
	}
	
	public void addColumn(String tableAndColumn, String columnTitle, int width, int columnType, InterfaceTableCellRenderer cellRenderer, InterfaceTableCellEditor cellEditor) {
		addColumn(tableAndColumn, columnTitle, width, columnType, (TableCellRenderer) cellRenderer, (TableCellEditor) cellEditor);
	}
	
	public void removeAllColumns() {
		tableColumnModel.removeAllColumns();
	}

	
	public void showAllColumns() {
		tableColumnModel.showAllColumns();
	}
	
	public CustomDialog showQuestion(JComponent comp, Scenario scenario) {
		
		Option[] options = scenario.options;
		
		CustomDialog dialog = new CustomDialog(window);
		
		for (int i=0; i<options.length; i++) {
			dialog.addButton(options[i].stringValue, (int) options[i].value);
		}

		dialog.setTitle(scenario.title);
		dialog.setMessage(scenario.question);
		
		// Sets suggestion input if necessary
		if (scenario.suggestionExists()) {
			dialog.setInput(scenario.getSuggestion());
			dialog.setInputVisible(!scenario.getSuggestionReadOnly());
		}
		
		dialog.display();
		
		return dialog;
	}
	
	
	
	@Override
	public void setValueAt(Object newValue, int rowIndex, int columnIndex) {
		int modelRowIndex = convertRowIndexToModel(rowIndex);
		int modelColumnIndex = convertColumnIndexToModel(columnIndex);
		
		Object oldValue = model.getValueAt(modelRowIndex, modelColumnIndex);
		
		if (newValue == null && oldValue == null) return;
		else if (newValue != null && newValue.equals(oldValue)) return;
		
		Column column = model.getColumn(modelColumnIndex);
		if (column.isDefaultValue(newValue) && !column.isDefaultValueValid()) return;

		/* If this table contains alerter then wrap this value with extra information.
		 * This value is unwrapped again in the model.
		 */
		if (!(newValue instanceof WrappedValue) && alerter != null) {
			WrappedValue wrappedValue = new WrappedValue(newValue, alerter);
			newValue = wrappedValue;
		}
		
		super.setValueAt(newValue, rowIndex, columnIndex);
	}

	public void deleteSelectedRows() {
		int mainSelectedRow = getSelectedRow();
		int mainSelectedColumn = getSelectedColumn();
		
		int[] selectedRows = BaseTable.this.getSelectedRows();
		int[] modelRowIndices = new int[selectedRows.length];
		
		for (int rowNum=0; rowNum<selectedRows.length; rowNum++) {
			int rowIndex = selectedRows[rowNum];
			modelRowIndices[rowNum] = convertRowIndexToModel(rowIndex);
		}
		
		model.deleteRows(modelRowIndices, null);
	
		selectRow(mainSelectedRow);
		selectColumn(mainSelectedColumn);
		requestFocus();
	}
	
	public void selectRow(int rowToSelect) {
		int rowCount = getRowCount();
		
		// Select no rows
		if (rowCount == 0 || rowToSelect == -1) {
			clearSelection();
		}
		// Select current row
		else if (rowToSelect < rowCount) {
			setRowSelectionInterval(rowToSelect, rowToSelect);
		}
		// Select last row if row to select is too big
		else {
			setRowSelectionInterval(rowCount-1, rowCount-1);
		}
	}
	

	public void selectColumn(int columnToSelect) {
		int columnCount = getColumnCount();
		
		// Select no columns
		if (columnCount == 0 || columnToSelect == -1) {
			clearSelection();
		}
		// Select current column
		else if (columnToSelect < columnCount) {
			setColumnSelectionInterval(columnToSelect, columnToSelect);
		}
		// Select last column if column to select is too big
		else {
			setColumnSelectionInterval(columnCount-1, columnCount-1);
		}
	}
	


	public void showLines(boolean b) {
		showHorizontalLines = b; showVerticalLines = b;
	}

	@Override
	public void doCopy() {
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		
		final int[] selectedRowIndices = getSelectedRows();
		final int[] selectedColumnIndices = getSelectedColumns();

		final PasteModifier[] selectedColumnPasteModifiers = getPasteModifiers(selectedColumnIndices);
		
		final int[] modelRowIndices = convertRowIndexToModel(selectedRowIndices);
		final int[] modelColumnIndices = convertColumnIndexToModel(selectedColumnIndices);
		
		String[] dataAsArray = new String[modelRowIndices.length];
		
		for (int rowIndex=0; rowIndex<modelRowIndices.length; rowIndex++) {
			
			int modelRowIndex = modelRowIndices[rowIndex];
			
			String[] rowStrArray = new String[modelColumnIndices.length];
			for (int colIndex=0; colIndex<modelColumnIndices.length; colIndex++) {
				int modelColIndex = modelColumnIndices[colIndex];
				
				Object value = model.getValueAt(modelRowIndex, modelColIndex);
				
				PasteModifier nextPasteModifier = selectedColumnPasteModifiers[colIndex];
				String cellToCopy = nextPasteModifier == null ? (value == null ? "" : value.toString()) : nextPasteModifier.convertFromRealValueToStringValue(value);
				rowStrArray[colIndex] = cellToCopy;
			}
			
			dataAsArray[rowIndex] = String.join("\t", rowStrArray);
		}
		
		String dataToCopy = String.join("\n", dataAsArray);
		
		StringSelection stringSelection = new StringSelection(dataToCopy);
		clipboard.setContents(stringSelection, null);
	}

	@Override
	public void doCut() {
		
	}

	
	
	protected boolean dataEmpty(Object[][] dataToPaste) {
		return (dataToPaste == null || dataToPaste.length == 0 || dataToPaste[0] == null || dataToPaste[0].length == 0);
	}
	

	/**
	 * For the model version of pasteDataToModel, the dimensions of dataToPaste must be 
	 * String[selectedRowIndices.length][selectedColumnIndices.length]
	 * 
	 * @param modelRowIndices
	 * @param modelColumnIndices
	 * @param dataToPaste
	 * @param alerter
	 */
	protected void pasteDataToModel(int[] modelRowIndices, int[] modelColumnIndices, Object[][] dataToPaste, UserAlerter alerter) {
		for (int i=0; i<modelRowIndices.length; i++) {
			int modelRowIndex = modelRowIndices[i];
			for (int j=0; j<modelColumnIndices.length; j++) {
				int modelColIndex = modelColumnIndices[j];
				Object cellToPaste = dataToPaste[i][j];
				if (alerter == null) {
					model.setValueAt(cellToPaste, modelRowIndex, modelColIndex);
				}
				else {
					model.setValueAt(new WrappedValue(cellToPaste, alerter), modelRowIndex, modelColIndex);
				}
			}
		}
	}
	
	protected Object[][] pasteData(int[] selectedRowIndices, int[] selectedColumnIndices, Object[][] dataToPasteRaw, UserAlerter alerter,
			int modifyDataMode, String errorDialogTitle) {
		
		// Nothing happens if the data is empty (ie, null or zero length array)
		if (selectedRowIndices == null || selectedRowIndices.length == 0) return dataToPasteRaw;
		if (selectedColumnIndices == null || selectedColumnIndices.length == 0) return dataToPasteRaw;
		if (dataEmpty(dataToPasteRaw)) return dataToPasteRaw;
		

		int iCount = dataToPasteRaw.length;
		int jCount = dataToPasteRaw[0].length;
		
		Object[][] dataToPaste = new Object[iCount][jCount];
		
		for (int i=0; i<iCount; i++) {
			for (int j=0; j<jCount; j++) {
				dataToPaste[i][j] = dataToPasteRaw[i][j];
			}
		}
		
		// Making sure the number of rows and columns are big enough to fit selection (if possible)
		selectedRowIndices = matchIndicesLengthToSelection(selectedRowIndices, dataToPaste);
		
		Object[] dataToPasteColumns = dataToPaste[0]; 
		selectedColumnIndices = matchIndicesLengthToSelection(selectedColumnIndices, dataToPasteColumns, getColumnCount()-1);
		
		/* Getting the appropriate rows indices from the model or a new index
		 * if row doesn't yet exist
		 */
		Integer[] modelRowIndicesToPasteTo = new Integer[selectedRowIndices.length];
		
		int numberOfRowsInTable = getRowCount();
		
		for (int i=0; i<selectedRowIndices.length; i++) {
			int tableRowIndex = selectedRowIndices[i];
			
			/* Row doesn't exist in model so the index is the new row index
			 * or null if a row isn't added.
			 */
			if (tableRowIndex >= numberOfRowsInTable) {
				modelRowIndicesToPasteTo[i] = addRow();
			}
			// Row exists in model
			else {
				modelRowIndicesToPasteTo[i] = convertRowIndexToModel(selectedRowIndices[i]);
			}
		}
		
		// Getting the appropriate columns from the model.
		int[] modelColumnIndicesToPasteTo = new int[selectedColumnIndices.length];

		int numberOfColumns = getColumnCount();
		for (int i=0; i<selectedColumnIndices.length; i++) {
			
			if (selectedColumnIndices[i] >= numberOfColumns) {
				break;
			}
			modelColumnIndicesToPasteTo[i] = convertColumnIndexToModel(selectedColumnIndices[i]);
		}
		
		// Getting the paste modifiers for modifying data if cell-editor allows it.
		PasteModifier[] selectedColumnPasteModifiers = getPasteModifiers(selectedColumnIndices);
		
		// Now pasting the data
		int dataToPasteIndex = 0;
		
		for (int i=0; i<selectedRowIndices.length; i++) {
			if (dataToPasteIndex == dataToPaste.length) {
				dataToPasteIndex = 0;
			}
			
			Object[] rowToPaste = dataToPaste[dataToPasteIndex];

			int rowToPasteColumn = 0;

			if (modelRowIndicesToPasteTo[i] == null) break;
			
			int modelRowIndex = modelRowIndicesToPasteTo[i];
			
			Row currentRow = model.getRowAt(modelRowIndex);
			Object[] currentRowData = currentRow.getRowData();
			Object[] newRow = currentRowData.clone();
			
			for (int j=0; j<selectedColumnIndices.length; j++) {
				if (rowToPasteColumn == rowToPaste.length) {
					rowToPasteColumn = 0;
				}
				
				// Modifying data to paste if applicable
				PasteModifier pasteModifier = selectedColumnPasteModifiers[j];
				
				Object cellToPaste;
				
				if (modifyDataMode == MODIFY_DATA_REAL_VALUE_TO_STRING_VALUE) {
					cellToPaste = pasteModifier == null ? rowToPaste[rowToPasteColumn] : 
						pasteModifier.convertFromRealValueToStringValue(rowToPaste[rowToPasteColumn]);
				}
				else if (modifyDataMode == MODIFY_DATA_STRING_VALUE_TO_REAL_VALUE) {
					cellToPaste = pasteModifier == null ? rowToPaste[rowToPasteColumn] : 
						pasteModifier.convertFromStringValueToRealValue((String) rowToPaste[rowToPasteColumn]);
				}
				else {
					cellToPaste = rowToPaste[rowToPasteColumn];
				}

				rowToPaste[rowToPasteColumn] = cellToPaste;
				
				int modelColumnIndex = modelColumnIndicesToPasteTo[j];
				
				if (isCellEditable(selectedRowIndices[i], selectedColumnIndices[j])) {
					if (alerter == null) {
						model.setValueAt(cellToPaste, modelRowIndex, modelColumnIndex, newRow, false);
					}
					else {
						model.setValueAt(cellToPaste, modelRowIndex, modelColumnIndex, newRow, false);
					}
				}
				
				// Moving onto the next data to paste column
				rowToPasteColumn++;
			}

			model.haveUniqueSwitchedOn(false);
			model.saveWithUpdates(null, modelRowIndex, -1, newRow, currentRow, alerter);
			model.haveUniqueSwitchedOn(true);
			
			// Moving onto the next data to paste row
			dataToPasteIndex++;
		}
		
		return dataToPaste;
	}


	protected PasteModifier[] getPasteModifiers(int[] selectedColumnIndices) {
		PasteModifier[] selectedColumnPasteModifiers = new PasteModifier[selectedColumnIndices.length];
		
		for (int i=0; i<selectedColumnIndices.length; i++) {
			int nextColumnIndex = selectedColumnIndices[i];
			BaseTableColumn nextTableColumn = tableColumnModel.getColumn(nextColumnIndex);
			TableCellEditor nextCellEditor = nextTableColumn.getCellEditor();
			
			if (nextCellEditor instanceof PasteModifier) {
				selectedColumnPasteModifiers[i] = (PasteModifier) nextCellEditor;
			}
		}
		return selectedColumnPasteModifiers;
	}

	protected void setSelection(int[] rowIndices, int[] colIndices) {
		clearSelection();
		
		for (int i=0; i<rowIndices.length; i++) {
			addRowSelectionInterval(rowIndices[i], rowIndices[i]);
		}
		
		for (int j=0; j<colIndices.length; j++) {
			addColumnSelectionInterval(colIndices[j], colIndices[j]);
		}
	}
	

	protected int[] matchIndicesLengthToSelection(int[] selectedIndices, Object[] dataToPaste) {
		return matchIndicesLengthToSelection(selectedIndices, dataToPaste, null);
	}
	
	protected int[] matchIndicesLengthToSelection(int[] selectedIndices, Object[] dataToPaste, Integer maxIndex) {

		if (selectedIndices.length < dataToPaste.length) {
			ArrayList<Integer> selectedRowIndicesNew = new ArrayList<>(dataToPaste.length);
			
			// First copying current indices
			for (int i=0; i<selectedIndices.length; i++) {
				selectedRowIndicesNew.add(selectedIndices[i]);
			}
			
			// Then filling with new indices
			int lastIndex = selectedIndices[selectedIndices.length-1] + 1;
			
			for (int i=selectedIndices.length; i<dataToPaste.length; i++) {
				
				if (maxIndex == null) {
					selectedRowIndicesNew.add(lastIndex);
				}
				else if (maxIndex != null && lastIndex <= maxIndex) {
					selectedRowIndicesNew.add(lastIndex);
				}
				else {
					break;
				}

				lastIndex++;
			}
			
			int selectedRowIndicesSize = selectedRowIndicesNew.size();
			
			selectedIndices = new int[selectedRowIndicesSize];
			
			for (int i=0; i<selectedRowIndicesSize; i++) {
				selectedIndices[i] = selectedRowIndicesNew.get(i);
			}
		}
		
		return selectedIndices;
	}

	@Override
	public void doSelectAll() {
		selectAll();
	}
	
	public void setAlerter(UserAlerter alerter) {
		this.alerter = alerter;
	}
	
	public int[] convertRowIndexToModel(int[] viewRowIndices) {
		int[] modelIndices = new int[viewRowIndices.length];
		
		for (int i=0; i<viewRowIndices.length; i++) {
			modelIndices[i] = convertRowIndexToModel(viewRowIndices[i]);
		}
		
		return modelIndices;
	}
	
	public int[] convertColumnIndexToModel(int[] viewColumnIndices) {
		int[] modelIndices = new int[viewColumnIndices.length];
		
		for (int i=0; i<viewColumnIndices.length; i++) {
			modelIndices[i] = convertColumnIndexToModel(viewColumnIndices[i]);
		}
		
		return modelIndices;
	}
	
	public BaseTableColumn[] getAllColumns() {
		return (BaseTableColumn[]) tableColumnModel.getAllColumns();
	}
	
	public void setColour(Component comp, Object value, boolean isSelected, boolean hasFocus, int rowIndex, int columnIndex) {
		Color colour = colourOverrider.setColourOverrides(comp, value, isSelected, hasFocus, rowIndex, columnIndex);
		
		if (comp instanceof JComponent) {
			JComponent jComp = (JComponent) comp;
			jComp.setOpaque(true);
			jComp.setBackground(colour);
		}
		
		comp.setBackground(colour);
		
		if (comp instanceof Container) {
			Container container = (Container) comp;
			Component[] allComps = container.getComponents();
			for (int i=0; i<allComps.length; i++) {
				Component nextComp = allComps[i];
				setColour(nextComp, value, isSelected, hasFocus, rowIndex, columnIndex);
			}
		}
	}
	

	public boolean isCellEditable(EventObject anEvent) {
		return isCellEditable(anEvent, false);
	}

	public boolean isCellEditable(EventObject anEvent, boolean canUseKeys) {	
    	TableCellEditor editor = getCellEditor();

    	if (editor != null) {
    		if (editor instanceof ContainsManualStop) {
    			ContainsManualStop manualStopEditor = (ContainsManualStop) editor;
    			manualStopEditor.manuallyEditingStopped();
    		}
    		
    		editor.cancelCellEditing();
    	}
		
        if (anEvent instanceof MouseEvent) {
            MouseEvent mouseEvent = (MouseEvent) anEvent;
            return mouseEvent.getClickCount() >= 1;
        }
        else if (anEvent instanceof KeyEvent) {
            KeyEvent keyEvent = (KeyEvent) anEvent;

            if (canUseKeys) {
            	return true;
            }
            else {
            	return keyEvent.getKeyCode() == KeyEvent.VK_F2;
            }
        }
        
		return false;
	}
	
	public void setColourOverrider(ColourOverrider colourOverrider) {
		this.colourOverrider = colourOverrider == null ? defaultColourOverrider : colourOverrider;
	}
	
	public ColourOverrider getColourOverrider() {
		return colourOverrider;
	}
	
	public ColourOverrider getDefaultColourOverrider() {
		return defaultColourOverrider;
	}
	
	public void setEditorController(EditorController editorController) {
		this.editorController = editorController == null ? defaultEditorController : editorController;
	}
	
	public EditorController getEditorController() {
		return editorController;
	}
	
	public EditorController getDefaultEditorController() {
		return defaultEditorController;
	}

	public void addRowWithDialog() {
		
		int[] rowIndicesToPasteFrom = new int[1];
		rowIndicesToPasteFrom[0] = addRow();
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				// Setting column indices to use
				int[] columnIndicesToPasteFrom = new int[getColumnCount()];
				
				for (int i=0; i<columnIndicesToPasteFrom.length; i++) {
					columnIndicesToPasteFrom[i] = i;
				}
				
				Object[][] allData = getData(rowIndicesToPasteFrom, columnIndicesToPasteFrom);
				
				Object[][] dataToPaste = Array2DObjectToString(allData, rowIndicesToPasteFrom.length, columnIndicesToPasteFrom.length);
				
				pasteData(rowIndicesToPasteFrom, columnIndicesToPasteFrom, dataToPaste, alerter, MODIFY_DATA_NONE, DIALOG_TITLE_ADD_NEW_ROW);
			}
		});
	}

	public void duplicateSelectedRows() {
		
		// Adding new rows
		int[] allSelectedRows = getSelectedRows();
		int[] allRowsToPasteInto = new int[allSelectedRows.length];
		
		for (int i=0; i<allRowsToPasteInto.length; i++) {
			allRowsToPasteInto[i] = addRow();
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				// Setting column indices to use
				int[] allColumnIndices = new int[getColumnCount()];
				
				for (int i=0; i<allColumnIndices.length; i++) {
					allColumnIndices[i] = i;
				}
				
				Object[][] dataToPaste = Array2DObjectToString(getData(allSelectedRows, allColumnIndices),
						allSelectedRows.length, allColumnIndices.length);
				
				pasteData(allRowsToPasteInto, allColumnIndices, dataToPaste,
						alerter, MODIFY_DATA_NONE, DIALOG_TITLE_ERROR_DUPLICATING_ROWS);
			}
		});
	}
	@Override
	public void doPaste() {
		if (isEditing()) return;
		
		// Adding new rows
		int[] allSelectedRows = getSelectedRows();
		int[] allRowsToPasteInto = allSelectedRows;
	
		// Setting column indices to use
		int[] allColumnIndices = getSelectedColumns();
		
		Object[][] dataToPaste = Functions.getClipboardDataAs2DArray();
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				pasteData(allRowsToPasteInto, allColumnIndices, dataToPaste,
						alerter, MODIFY_DATA_STRING_VALUE_TO_REAL_VALUE, DIALOG_TITLE_ERROR_PASTING);
			}
		});
		
//		// Can only paste data if there are rows selected
//		int[] rowIndicesToPasteFrom = getSelectedRows();
//		if (rowIndicesToPasteFrom.length == 0) return;
//
//		// Can only paste data if there are columns selected
//		int[] columnIndicesToPasteFrom = getSelectedColumns();
//		if (columnIndicesToPasteFrom.length == 0) return;
//		
//		// Gets data to paste as 2D array.
//		String[][] dataToPaste = Functions.getClipboardDataAs2DArray();
//
//		// Cannot paste with no data.
//		if (!dataEmpty(dataToPaste)) {
//			SQLEngine.getMainConnection().setCommitAndRollbackEnabled(false);
//			pasteData(rowIndicesToPasteFrom, columnIndicesToPasteFrom, dataToPaste, alerter, MODIFY_DATA_STRING_VALUE_TO_REAL_VALUE, DIALOG_TITLE_ERROR_PASTING);
//			SQLEngine.getMainConnection().setCommitAndRollbackEnabled(true);
//			SQLEngine.getMainConnection().commit();
//		}
//		
//		model.fireTableDataChanged();
		
	}
	public Object[][] getData(int[] rowIndices, int[] colIndices) {

		int[] allModelRowIndices = convertRowIndexToModel(rowIndices);
		int[] allModelColIndices = convertColumnIndexToModel(colIndices);

		Object[][] data = new Object[allModelRowIndices.length][allModelColIndices.length];
		
		for (int rowIndex = 0; rowIndex<allModelRowIndices.length; rowIndex++) {
			for (int colIndex = 0; colIndex<allModelColIndices.length; colIndex++) {
				data[rowIndex][colIndex] = model.getValueAt(allModelRowIndices[rowIndex], allModelColIndices[colIndex]);
			}
		}
		
		return data;
	}
	
	public static Object[][] Array2DObjectToString(Object[][] allDataObject, int rowCount, int columnCount) {
		Object[][] allData = new String[rowCount][columnCount];
		
		for (int rowIndex=0; rowIndex<rowCount; rowIndex++) {
			for (int colIndex=0; colIndex<columnCount; colIndex++) {
				Object nextCellObj = allDataObject[rowIndex][colIndex];
				allData[rowIndex][colIndex] = nextCellObj == null ? null : nextCellObj.toString();
			}
		}
		return allData;
	}

	public static Object[][] Array2DObject(Object[][] allDataObject, int rowCount, int columnCount) {
		Object[][] allData = new Object[rowCount][columnCount];
		
		for (int rowIndex=0; rowIndex<rowCount; rowIndex++) {
			for (int colIndex=0; colIndex<columnCount; colIndex++) {
				Object nextCellObj = allDataObject[rowIndex][colIndex];
				allData[rowIndex][colIndex] = nextCellObj;
			}
		}
		return allData;
	}

	
	public void resizeAllColumns(int tableWidth) {
		tableColumnModel.setWidths(tableWidth);
	}


	public void setScrollPane(UserTableScrollPane userTableScrollPane) {
		this.userTableScrollPane = userTableScrollPane;
	}
	
	public UserTableScrollPane getScrollPane() {
		return userTableScrollPane;
	}
	
	public BaseTableColumnModel getTableColumnModel() {
		return tableColumnModel;
	}


	
	public void selectAndShowRow(int rowIndex) {
		if (rowIndex == -1) {
			rowIndex = getSelectedRow();
		}
		
		int columnCount = getColumnCount();
		
		if (rowIndex >= 0 && columnCount >= 1) {
			changeSelection(rowIndex, columnCount, false, false);
			changeSelection(rowIndex, 0, false, true);
		}
	}
}
