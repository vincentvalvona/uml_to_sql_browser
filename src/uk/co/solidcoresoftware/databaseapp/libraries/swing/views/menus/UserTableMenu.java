package uk.co.solidcoresoftware.databaseapp.libraries.swing.views.menus;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

public class UserTableMenu extends JPopupMenu {
	
	JMenuItem showOrHide = new JMenuItem();
	
	boolean subMenuAdded = false;
	
	public UserTableMenu() {
		add(showOrHide);
	}
}
