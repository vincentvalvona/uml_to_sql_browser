package uk.co.solidcoresoftware.databaseapp.libraries.swing.views;

import java.awt.Dialog;

import javax.swing.JDialog;

import java.awt.FlowLayout;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

import java.awt.BorderLayout;
import java.awt.CardLayout;

import javax.swing.SpringLayout;
import javax.swing.BoxLayout;

import java.awt.Frame;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;


import javax.swing.JSplitPane;
import javax.swing.JPanel;

import java.awt.GridLayout;


import javax.swing.JTextPane;

import java.awt.Panel;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Option;

public class CustomDialog extends JDialog implements InterfaceCustomDialog {
	
	// The icon, message and text input boxes.
	private JLabel icon;
	private JLabel messageLabel;
	private JTextField inputBox;
	
	// The panel to contain the above
	private JPanel otherInputsPanel;
	
	// The panel for the buttons
	private JPanel buttonsPanel;
	
	// The text of the selected button.
	private String selectedButtonCaption = null;
	private int selectedButtonType = Option.OPTION_VALUE_CANCEL;
	
	private JComponent defaultFocusElement = null;
	
	public CustomDialog(Window owner) {
		super(owner, Dialog.ModalityType.APPLICATION_MODAL);
		setResizable(false);
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
		
		Panel panel = new Panel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		getContentPane().add(panel);
		
		// The icon label. No icon by default and also hidden.
		icon = new JLabel("");
		icon.setVisible(false);
		panel.add(icon);
		
		// The panel for other inputs.
		otherInputsPanel = new JPanel();
		panel.add(otherInputsPanel);
		otherInputsPanel.setLayout(new BoxLayout(otherInputsPanel, BoxLayout.Y_AXIS));
		
		messageLabel = new JLabel("New label");
		otherInputsPanel.add(messageLabel);
		
		inputBox = new JTextField();
		inputBox.setVisible(false);
		otherInputsPanel.add(inputBox);
		inputBox.setColumns(10);
		
		buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		getContentPane().add(buttonsPanel);
		
		ActionMap am = getRootPane().getActionMap();
        InputMap im = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        Object windowCloseKey = new Object();
        KeyStroke windowCloseStroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
        
        Action windowCloseAction = new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                dispose();
            }
        };
        im.put(windowCloseStroke, windowCloseKey);
        am.put(windowCloseKey, windowCloseAction);
	}
	
	public void display() {
		pack();
		
		// Sets this element as having focus just before showing dialog
		if (defaultFocusElement != null) {
			defaultFocusElement.requestFocusInWindow();
			
			if (defaultFocusElement == inputBox) {
				inputBox.selectAll();
			}
		}
		setVisible(true);
	}
	
	public void setIcon(ImageIcon img) {
		icon.setIcon(img);
		icon.setVisible(true);
	}
	
	public void setTitle(String title) {
		super.setTitle(title);
	}
	
	public void setMessage(String message) {
		messageLabel.setText(message);
	}
	
	public void setDefaultButtonCaption(String caption) {
		this.selectedButtonCaption = caption;
	}
	
	/**
	 * Allows the input box to be shown and sets the default input
	 * to show to the user.
	 * 
	 * @param input
	 */
	public void setInput(String input, boolean hasDefaultFocus) {
		
		if (hasDefaultFocus) {
			defaultFocusElement = inputBox;
		}
		
		inputBox.setText(input == null ? "" : input);
		inputBox.setVisible(true);
	}
	
	public void setInput(String input) {
		setInput(input, false);
	}
	
	public void setInputVisible(boolean visible) {
		inputBox.setVisible(visible);
	}
	
	
	public String getInput() {
		return inputBox.getText();
	}
	
	public String getSelectedButtonCaption() {
		return selectedButtonCaption;
	}
	
	public int getSelectedButtonType() {
		return selectedButtonType;
	}
	
	public void addButton(final String caption, final int buttonType, boolean hasDefaultFocus) {
		// Sets the default selected button title, the latest added button by default
		selectedButtonCaption = caption;
		
		JButton button = new JButton(caption);
		if (hasDefaultFocus) defaultFocusElement = button;
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectedButtonType = buttonType;
				selectedButtonCaption = caption;
				CustomDialog.this.setVisible(false);
			}
		});

		buttonsPanel.add(button);
	}
	

	public void addButton(final String caption, int buttonType) {
		addButton(caption, buttonType, false);
	}
	public void addButton(final String caption, boolean hasDefaultFocus) {
		addButton(caption, Option.OPTION_VALUE_GENERAL, hasDefaultFocus);
	}

	public void addButton(final String caption) {
		addButton(caption, Option.OPTION_VALUE_GENERAL, false);
	}
}
