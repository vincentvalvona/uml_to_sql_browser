package uk.co.solidcoresoftware.databaseapp.libraries.swing.views.functions;

public interface EditFunctions {
	public void doCopy();
	public void doCut();
	public void doPaste();
	public void doSelectAll();
}
