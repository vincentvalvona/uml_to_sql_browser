package uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;

public class Prompt {

	private static int constantValue = 0;
	public final static int STATUS_SUCCESSFUL = constantValue++;
	public final static int STATUS_FAILED = constantValue++;
	public final static int STATUS_NOT_SAVED = constantValue++;
	public final static int STATUS_ATTEMPTING_TO_SAVE = constantValue++;
	
	public final static int ACTION_ACCEPT = constantValue++;
	public final static int ACTION_CANCEL = constantValue++;
	public final static int ACTION_RETRY = constantValue++;
	
	private int action = ACTION_CANCEL;
	
	private Object[] row;
	private Column column;
	private Object potentialValue;
	
	public final int status;
	
	public final int rowIndex;
	public final int colIndex;
	
	public Prompt(int status, Object potentialValue, int rowIndex, int colIndex) {
		this.status = status;
		this.potentialValue = potentialValue;
		this.rowIndex = rowIndex;
		this.colIndex = colIndex;
	}
	
	public void setAction(int action) {
		this.action = action;
	}
	
	public int getAction() {
		return action;
	}

	public void setRow(Object[] row) {
		this.row = row;
	}

	public void setColumn(Column column) {
		this.column = column;
	}

	public void setPotentialValue(Object potentialValue) {
		this.potentialValue = potentialValue;
	}
	
	public Object[] getRow() {
		return row;
	}
	
	public Column getColumn() {
		return column;
	}
	
	public Object getPotentialValue() {
		return potentialValue;
	}
}
