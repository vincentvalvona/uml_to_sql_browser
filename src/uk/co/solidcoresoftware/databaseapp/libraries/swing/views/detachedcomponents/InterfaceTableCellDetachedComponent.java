package uk.co.solidcoresoftware.databaseapp.libraries.swing.views.detachedcomponents;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;

public interface InterfaceTableCellDetachedComponent {

	void setModelAndColumn(LightTableModel model, Column column);
	void setValue(int rowIndex, Object value);
	void setToNoRowSelected();
	void sendValueToTable();
	Column getColumn();
}
