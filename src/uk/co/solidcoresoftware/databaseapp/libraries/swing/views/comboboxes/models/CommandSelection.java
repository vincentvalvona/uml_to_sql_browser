package uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.models;

import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;

public interface CommandSelection {
	public void invokeCommand(LightTableModel model, Object newValue, int rowIndex, int columnIndex);
}
