package uk.co.solidcoresoftware.databaseapp.libraries.swing.views.detachedcomponents;

import java.awt.Point;

import javax.swing.text.JTextComponent;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;

public class TableCellDetachedTextComponentWrapper implements InterfaceTableCellDetachedComponent {
		
	private LightTableModel model = null;
	private Column column = null;
	
	private int rowIndex = -1;
	
	private Object value = null;
	
	public Point mousePressedPoint = null;
	public boolean mouseJustPressed = false;
	
	private final JTextComponent textComp;
	
	public TableCellDetachedTextComponentWrapper(final JTextComponent textComp) {
		this.textComp = textComp;
	}
	
	public void updateText() {
		if (textComp.isFocusOwner()) {
			textComp.setText(value == null ? "" : value.toString());
		}
		else {
			textComp.setText(value == null ? "" : value.toString());
		}
	}
	
	@Override
	public void setModelAndColumn(LightTableModel model, Column column) {
		this.model = model; this.column = column;
	}

	@Override
	public void setValue(int rowIndex, Object value) {
		this.rowIndex = rowIndex;
		this.value = value;
		updateText();
		textComp.setEnabled(true);
	}

	@Override
	public void setToNoRowSelected() {
		textComp.setText("");
		textComp.setEnabled(false);
	}
	
	@Override
	public void sendValueToTable() {
		model.updateValueFromDetachedComponent(rowIndex, column, textComp.getText());
	}


	@Override
	public Column getColumn() {
		return column;
	}
}
