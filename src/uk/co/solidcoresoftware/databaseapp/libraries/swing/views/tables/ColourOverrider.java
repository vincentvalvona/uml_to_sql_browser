package uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables;

import java.awt.Color;
import java.awt.Component;

public interface ColourOverrider {
	public Color setColourOverrides(Component comp, Object value, boolean isSelected, boolean hasFocus, int rowIndex, int columnIndex);
}
