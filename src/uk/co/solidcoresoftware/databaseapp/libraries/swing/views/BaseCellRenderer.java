package uk.co.solidcoresoftware.databaseapp.libraries.swing.views;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;

public class BaseCellRenderer extends DefaultTableCellRenderer {
	
	private Column column;
	private BaseTable table;
	private LightTableModel model;

	public BaseCellRenderer(BaseTable table, LightTableModel model) {
		this.table = table;
		this.model = model;
	}
	
	public void setColumn(Column column) {
		this.column = column;
	}
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		
		if (this.column.hasCalibrator()) {
			value = this.column.getCalibrator().getCellRenderingText(value);
		}
		
		int rowIndexInModel = this.table.convertRowIndexToModel(row);
		

		boolean cellIsActive = this.column.isCellActive(this.table.model.getRowAt(rowIndexInModel).getRowData());
		
		if (cellIsActive) {
			value = (value == null) ? "" : value.toString();
		}
		else {
			value = "";
		}
			
		Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
		
//		Color currentBackgroundColour = component.getBackground();
		
		final Color LIGHT_YELLOW = new Color(255, 255, 178);
		
		if (!cellIsActive) {
			component.setBackground(Color.GRAY);
		}
		else {
			
			if (table.isCellSelected(row, column)) {
				component.setBackground(Color.YELLOW);
			}
			else if (table.isRowSelected(row)) {
				component.setBackground(LIGHT_YELLOW);
			}
			else {
				component.setBackground(null);
			}
		}
		
		return component;
	}
}
