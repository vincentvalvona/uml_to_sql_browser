package uk.co.solidcoresoftware.databaseapp.libraries.swing.functionality;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.AbstractButton;
import javax.swing.text.JTextComponent;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.AbstractTableCellDetachedComponent;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.*;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.detachedcomponents.TableCellDetachedTextComponentWrapper;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;

public final class FunctionalityGenerator {
	private FunctionalityGenerator() {}
	
	public final static int NO_MAX = -1;
	
	public static void addActionAddRow(final AbstractButton button, final BaseTable table) {
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				table.addRow();
//				table.addRowWithDialog();
			}
		});
		
		final LightTableModel model = table.getTableModel();
		
		model.attachDetachedComponent(new AbstractTableCellDetachedComponent() {
			@Override
			public void updateComponent() {
				button.setEnabled(model.isActive());
			}
		}, null);	
	}
	
	/**
	 * Adds table-row deletion functionality to a button, menu, etc (any component which allows
	 * for ActionListener.
	 * 
	 * @param button The button to apply this functionality to.
	 * @param table The table to apply the row deletion to
	 * @param titleDefault The default title (null allowed)
	 * @param titleNoneSelected The title no items selected (null allowed)
	 * @param titleOneSelected The title if one item selected (null allowed)
	 * 
	 * If you want the title to contain the number of selected rows then use %n (eg, "Delete %n rows").
	 * Also, the button is disabled when the table contains no items.
	 */
	public static void addActionDeleteRow(final AbstractButton button, final BaseTable table,
			final String titleNoneSelected, final String titleOneSelected, final String titleDefault) {
		
		addActionDeleteRow(button, table);
		setEnabledBasedOnNumberOfRowsSelected(button, table, titleNoneSelected, titleOneSelected, titleDefault, 1, NO_MAX);
	}
	
	public static void setEnabledBasedOnNumberOfRowsSelected(final AbstractButton button, final BaseTable table,
			final String titleNoneSelected, final String titleOneSelected, final String titleDefault, int minSelected, int maxSelected) {
		
		final LightTableModel model = table.getTableModel();
		
		model.attachDetachedComponent( new AbstractTableCellDetachedComponent() {
			
			@Override
			public void updateComponent() {
				int numberOfRows = table.getSelectedRows().length;
				
				String newTitle = null;
				
				if (numberOfRows == 0) newTitle = titleNoneSelected;
				else if (numberOfRows == 1) newTitle = titleOneSelected;
				else newTitle = titleDefault;
				
				if (newTitle == null) newTitle = titleDefault;
				
				newTitle = newTitle.replace("%n", "" + numberOfRows);
				button.setText(newTitle);
				
				if (maxSelected == NO_MAX) {
					button.setEnabled(model.isActive() && numberOfRows >= minSelected);
				}
				else {
					button.setEnabled(model.isActive() && numberOfRows >= minSelected && numberOfRows <= maxSelected);
				}
			}
		}, null);
	}
	
	
	/**
	 * Adds table-row deletion functionality to a button, menu, etc (any component which allows
	 * for ActionListener.
	 * 
	 * @param button The button to apply this functionality to.
	 * @param table The table to apply the row deletion to
	 * @param titleDefault The default title (null allowed)
	 * @param titleNoneSelected The title no items selected (null allowed)
	 * @param titleOneSelected The title if one item selected (null allowed)
	 * 
	 * If you want the title to contain the number of selected rows then use %n (eg, "Delete %n rows").
	 * Also, the button is disabled when the table contains no items.
	 */
	public static void addActionDuplicateRows(final AbstractButton button, final BaseTable table,
			final String titleNoneSelected, final String titleOneSelected, final String titleDefault) {
		
		addActionDuplicateRows(button, table);
		setEnabledBasedOnNumberOfRowsSelected(button, table, titleNoneSelected, titleOneSelected, titleDefault, 1, NO_MAX);
	}

	private static void addActionDuplicateRows(final AbstractButton button, final BaseTable table) {
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				table.duplicateSelectedRows();
			}
		});
	}
	
	private static void addActionDeleteRow(final AbstractButton button, final BaseTable table) {
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				table.deleteSelectedRows();
			}
		});
	}
	
	public static void addActionRefreshRows(final AbstractButton button, final LightTableModel model) {
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				model.update();
			}
		});
		
		model.attachDetachedComponent( new AbstractTableCellDetachedComponent() {
			
			@Override
			public void updateComponent() {
				button.setEnabled(model.isActive());
			}
		}, null);	
	}
	
	private static void addTextFieldListeners(final JTextComponent textComp, final TableCellDetachedTextComponentWrapper wrapper) {

		textComp.addMouseListener(new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent e) {}
			
			@Override
			public void mousePressed(MouseEvent e) {
				wrapper.mousePressedPoint = e.getPoint();
				wrapper.mouseJustPressed = true;
			}
			
			@Override
			public void mouseExited(MouseEvent e) {}
			
			@Override
			public void mouseEntered(MouseEvent e) {}
			
			@Override
			public void mouseClicked(MouseEvent e) {}
		});
		
		textComp.addFocusListener(new FocusListener() {
			
			@Override
			public void focusLost(FocusEvent e) {
				wrapper.sendValueToTable();
				wrapper.updateText();
			}
			
			@Override
			public void focusGained(FocusEvent e) {
				
				/* The non-editing text value always has a "£" sign in front
				 * so caret-shifting is necessary
				 */
				int caretPosition = textComp.getCaretPosition();
				
				wrapper.updateText();
				
				if (wrapper.mouseJustPressed) {
					wrapper.mouseJustPressed = false;
					
					int textLength = textComp.getText().length();
					if (caretPosition > textLength) {
						caretPosition = textLength;
					}
					else if (caretPosition < 0) {
						caretPosition = 0;
					}
					
					textComp.setCaretPosition(caretPosition);
				}
				else {
					textComp.selectAll();
				}
			}
		});
	}
	
	public static TableCellDetachedTextComponentWrapper createDetachedTextEditorWrapper(final JTextComponent textComp) {
		
		final TableCellDetachedTextComponentWrapper wrapper = new TableCellDetachedTextComponentWrapper(textComp);
		
		addTextFieldListeners(textComp, wrapper);
		
		return wrapper;
	}
	
	
}
