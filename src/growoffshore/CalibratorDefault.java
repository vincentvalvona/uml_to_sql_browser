package growoffshore;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.AbstractColumnCalibrator;

public class CalibratorDefault extends AbstractColumnCalibrator {

	@Override
	public String getCellRenderingText(Object input) {
		return input == null ? "" : String.valueOf(input);
	}
	
	@Override
	public String getCellEditingText(Object input) {
		return input == null ? "" : String.valueOf(input);
	}
	
	@Override
	public Object calibrateToValue(Object input) {
		// TODO Auto-generated method stub
		return input;
	}
	
	@Override
	public Object calibrateFromValue(Object input) {
		// TODO Auto-generated method stub
		return input;
	}
}
