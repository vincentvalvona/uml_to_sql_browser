package growoffshore;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Point;
import java.util.Iterator;
import java.util.LinkedList;

import javax.accessibility.AccessibleEditableText;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.Highlighter;
import javax.swing.text.Highlighter.HighlightPainter;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLCodeParser;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLCodeParser.Segment;

import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

public class SQLParserWindow extends JFrame {
	
	JPanel mainPanel;
	JScrollPane textAreaScrollPane;
	JTextPane textArea;
	private DefaultStyledDocument doc;
	
	
	public SQLParserWindow() {
		setup();
	}

	private void setup() {
		setPreferredSize(new Dimension(600, 600));
		
		mainPanel = new JPanel();
		mainPanel.setBackground(Color.WHITE);
		mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		mainPanel.setLayout(new GridLayout(1, 1));
		
		doc = new DefaultStyledDocument() {
			@Override
			public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
				super.insertString(offs, str, a);
				parseAndHighlightText();
			}
			
			@Override
			public void remove(int offs, int len) throws BadLocationException {
				super.remove(offs, len);
				parseAndHighlightText();
			}
			
			@Override
			public void replace(int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
				super.replace(offset, length, text, attrs);
				parseAndHighlightText();
			}
		};
		
		textArea = new JTextPane(doc);
		textArea.setFont(new Font(Font.MONOSPACED, Font.BOLD, 14));
		textAreaScrollPane = new JScrollPane(textArea);
		mainPanel.add(textAreaScrollPane);

		getContentPane().add(mainPanel);
		
		pack();
		
//		doc.addDocumentListener(new DocumentListener() {
//			
//			@Override
//			public void removeUpdate(DocumentEvent e) {
//				parseAndHighlightText();
//			}
//			
//			@Override
//			public void insertUpdate(DocumentEvent e) {
//				parseAndHighlightText();
//			}
//			
//			@Override
//			public void changedUpdate(DocumentEvent e) {
//				parseAndHighlightText();
//			}
//		});
	}
	
	private void parseAndHighlightText() {
		final StyleContext cont = StyleContext.getDefaultStyleContext();

        final AttributeSet attrBlack = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.BLACK);

        final AttributeSet attrNorm = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.MAGENTA);
        final AttributeSet attrStringSingleQ = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.RED);
        final AttributeSet attrStringDoubleQ = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.RED);
        final AttributeSet attrComment = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.GREEN);
        final AttributeSet attrCommentLine = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.GRAY);
        
        final AttributeSet attrCharBracketLevel0 = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.ORANGE);
        final AttributeSet attrCharBracketLevel1 = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.BLUE);
        final AttributeSet attrCharBracketLevel2 = cont.addAttribute(cont.getEmptySet(), StyleConstants.Foreground, Color.BLACK);
        
		SQLCodeParser parser = new SQLCodeParser();
		
		parser.parseString(textArea.getText());
		
		LinkedList<Segment> segments = parser.getSegments();
		
		Iterator<Segment> segmentsIt = segments.iterator();

		int docLength = doc.getLength();
		doc.setCharacterAttributes(0, docLength, attrBlack, true);

//		AccessibleEditableText accessibleText = textArea.getAccessibleContext().getAccessibleEditableText();
//		
//		
//		System.out.println("textArea.getAccessibleContext().getAccessibleEditableText()): "
//				+ textArea.getAccessibleContext().getAccessibleEditableText().getIndexAtPoint(p));
		
		while (segmentsIt.hasNext()) {
			Segment nextSegment = segmentsIt.next();

			AttributeSet attSetToUse = null;
			
			if (nextSegment.segmentType == SQLCodeParser.SEGMENT_TYPE_NORMAL) {
				attSetToUse = attrNorm;
			}
			else if (nextSegment.segmentType == SQLCodeParser.SEGMENT_TYPE_COMMENT) {
				attSetToUse = attrComment;
			}
			else if (nextSegment.segmentType == SQLCodeParser.SEGMENT_TYPE_COMMENT_LINE) {
				attSetToUse = attrCommentLine;
			}
			else if (nextSegment.segmentType == SQLCodeParser.SEGMENT_TYPE_STRING_DOUBLE_Q) {
				attSetToUse = attrStringDoubleQ;
			}
			else if (nextSegment.segmentType == SQLCodeParser.SEGMENT_TYPE_STRING_SINGLE_Q) {
				attSetToUse = attrStringSingleQ;
			}
			else if (nextSegment.segmentType == SQLCodeParser.SEGMENT_TYPE_SEMICOLON
					|| nextSegment.segmentType == SQLCodeParser.SEGMENT_TYPE_OPEN_BRACKET
					|| nextSegment.segmentType == SQLCodeParser.SEGMENT_TYPE_CLOSED_BRACKET
					|| nextSegment.segmentType == SQLCodeParser.SEGMENT_TYPE_COMMA) {
				
				switch(nextSegment.getBracketLevel()) {
				case 0: attSetToUse = attrCharBracketLevel0; break;
				case 1: attSetToUse = attrCharBracketLevel1; break;
				default: attSetToUse = attrCharBracketLevel2; break;
				}
				
				
			}
			
			if (attSetToUse != null) {
				doc.setCharacterAttributes(nextSegment.startCharPos, nextSegment.length(), attSetToUse, true);
			}
		}
		
		
//		int docLength = doc.getLength();
//		
//
//		doc.setCharacterAttributes(0, docLength, attrBlack, true);
//		
//		if (docLength > 10) {
//			doc.setCharacterAttributes(2, 4, attr, true);
//		}
	}
}
