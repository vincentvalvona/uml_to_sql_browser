package growoffshore;

import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.Prompt;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.PromptGroup;

public interface UserAlerter {
	public void onPrompt(Prompt currentPrompt);
}
