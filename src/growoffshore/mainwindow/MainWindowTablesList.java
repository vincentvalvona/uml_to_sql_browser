package growoffshore.mainwindow;

import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import growoffshore.TableTreeNode;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;

public class MainWindowTablesList extends JScrollPane {
	
	private DefaultMutableTreeNode top = new DefaultMutableTreeNode("Root");
	private JTree tree = new JTree(top);
	private MainWindow mainWindow;
	
	public MainWindowTablesList(MainWindow mainWindow) {
		this.mainWindow = mainWindow;
		
		setViewportView(tree);
		setHorizontalScrollBarPolicy(HORIZONTAL_SCROLLBAR_NEVER);
		setVerticalScrollBarPolicy(VERTICAL_SCROLLBAR_ALWAYS);
		
		tree.setRootVisible(false);
		
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		
		tree.addTreeSelectionListener(new TreeSelectionListener() {
			
			@Override
			public void valueChanged(TreeSelectionEvent e) {
				
				if (e.isAddedPath()) {
				
					TreePath path = e.getPath();
					
					Object lastPathCompObj = path.getLastPathComponent();
					
					if (lastPathCompObj instanceof TableTreeNode) {
						TableTreeNode nodeSelected = (TableTreeNode) lastPathCompObj;
						nodeSelected.selectTable(mainWindow);
					}
				}
			}
		});
		
//		tree.setScrollsOnExpand(true);
	}
	
	public void expandAllNodes() {
		tree.setRootVisible(true);
		for (int i=0; i<tree.getRowCount(); i++) {
			tree.expandRow(i);
		}
		tree.setRootVisible(false);
	}

	public void clearTables() {
		top.removeAllChildren();
	}
	
	public void addTable(Table table) {
		TableTreeNode tableNode = new TableTreeNode(table);
		top.add(tableNode);
	}
	
}
