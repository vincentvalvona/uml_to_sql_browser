package growoffshore.mainwindow;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import growoffshore.DatabaseInfo;
import growoffshore.EnumComboBoxData;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Option;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLEngine;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.Row;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.models.ComboBoxData;

public class MainWindowFilterPanel extends JPanel {
	
	private MainWindow window;

	private JLabel allTablesComboBoxLabel;
	private ComboBoxData cBoxData;
	private JComboBox<Option> allTablesComboBox;
	
	private JLabel allValuesComboBoxLabel;
	private JComboBox<Option> allValuesComboBox;
	
	private JCheckBox nullsValidCheckBox;
	
	public MainWindowFilterPanel(MainWindow window) {
		this.window = window;
		setup();
	}
	
	private void setup() {
		setBackground(Color.RED);
		
		FlowLayout mainLayout = new FlowLayout(FlowLayout.LEFT);
		setLayout(mainLayout);
		
		allTablesComboBoxLabel = new JLabel("Filter table");
		allTablesComboBox = new JComboBox<>();
		allValuesComboBoxLabel = new JLabel("Filter value");
		allValuesComboBox = new JComboBox<>();
		
		nullsValidCheckBox = new JCheckBox("Nulls visible in current table");
		
		allValuesComboBox.setEnabled(false);
		
		nullsValidCheckBox.setOpaque(false);
		
		add(allTablesComboBoxLabel); add(allTablesComboBox);
		addLastBits();
	}
	
	private void addLastBits() {
		add(allValuesComboBoxLabel); add(allValuesComboBox);
		add(nullsValidCheckBox);
	}
	
	private void removeLastBits() {
		remove(allValuesComboBoxLabel); remove(allValuesComboBox);
		remove(nullsValidCheckBox);
	}
	
	public void reset() {
		allTablesComboBox.setSelectedIndex(-1);
		allValuesComboBox.setSelectedIndex(-1);
		
		ActionListener[] allActionListeners = allTablesComboBox.getActionListeners();
		for (ActionListener listener : allActionListeners) {
			allTablesComboBox.removeActionListener(listener);
		}
		
		allActionListeners = nullsValidCheckBox.getActionListeners();
		for (ActionListener listener : allActionListeners) {
			nullsValidCheckBox.removeActionListener(listener);
		}
		
		allTablesComboBox.removeAllItems();
		allValuesComboBox.removeAllItems();
		
		allTablesComboBox.addItem(new Option(null, "(no filter)"));
		
		DatabaseInfo dbInfo = window.getDbInfo();
		Table[] allTables = dbInfo.getTables();
		
		for (int i=0; i<allTables.length; i++) {
			Table nextTable = allTables[i];
			allTablesComboBox.addItem(new Option(nextTable, nextTable.tableName));
		}
		
		allTablesComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DatabaseInfo dbInfo = window.getDbInfo();
				
				Option selectedItem = (Option) allTablesComboBox.getSelectedItem();
				
				if (selectedItem == null || selectedItem.value == null) {
					dbInfo.setGlobalFilteredTable(null);
					refillValuesBox(null);
				}
				else {
					dbInfo.setGlobalFilteredTable(((Table) selectedItem.value));
					refillValuesBox((Table) selectedItem.value);
				}
				
				window.invokeRefreshOnCurrentlyOpenedTable();
			}
		});
		
		allValuesComboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				DatabaseInfo dbInfo = window.getDbInfo();

				Option selectedItem = (Option) allValuesComboBox.getSelectedItem();
				
				if (selectedItem == null || selectedItem.value == null) {
					dbInfo.setGlobalFilteredValue(null);
				}
				else {
					dbInfo.setGlobalFilteredValue((Long) selectedItem.value);
				}
				
				window.invokeRefreshOnCurrentlyOpenedTable();
			}
		});
		
		nullsValidCheckBox.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				DatabaseInfo dbInfo = window.getDbInfo();
				
				dbInfo.setNullsVisibleOnFilter(nullsValidCheckBox.isSelected());
				
				window.invokeRefreshOnCurrentlyOpenedTable();
			}
		});
	}
	
	private void refillValuesBox(Table table) {

		allValuesComboBox.removeAllItems();
		
		if (table == null) {
			cBoxData = null;
			allValuesComboBox.setEnabled(false);
		}
		else {
			allValuesComboBox.setEnabled(true);
			cBoxData = new EnumComboBoxData(window.getDbInfo(), table);
			
			final Column primaryColumn = new Column(table, table.idColumnName);
			cBoxData.addColumn(primaryColumn);

			Column[] allColumnsInModel = SQLEngine.getMainConnection().getAllColumnsInTable(primaryColumn.table);
			
			Column firstColumnAfterIDUniqueNotNull = Column.firstUniqueAndNotNullColumn(allColumnsInModel);
			
			Column displayColumn = primaryColumn;

			if (firstColumnAfterIDUniqueNotNull != null) {
				displayColumn = firstColumnAfterIDUniqueNotNull;
			}
			
			cBoxData.addColumn(displayColumn);
			
			JComboBox<Option> cBoxDataComboBox = cBoxData.getComboBox();
			cBoxData.update();
			DatabaseInfo dbInfo = window.getDbInfo();
			
			ArrayList<Row> allEntries = dbInfo.getValuesOfTable(table);
			
			allValuesComboBox.addItem(new Option((Long) null, "(all values)"));
			
			int cBoxDataComboBoxItemCount = cBoxDataComboBox.getItemCount();
			for (int i=0; i<cBoxDataComboBoxItemCount; i++) {
				Option nextEntry = cBoxDataComboBox.getItemAt(i);
				
				if (nextEntry.value != null && (nextEntry.value instanceof Long)) {
					allValuesComboBox.addItem(new Option(nextEntry.value, nextEntry.stringValue));
				}
			}
		}
	}
}
