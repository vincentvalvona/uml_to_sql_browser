package growoffshore.mainwindow;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MainTableWelcomeScreen extends JPanel {
	
	String html = "<html>"
			+ "<p><span style=\"font-family:Arial;font-size:20px;\">Welcome to your database editor</span></p>"
			+ "<br/><br/><br/><p><span style=\"font-family:Arial;font-size:13px;\">"
			+ "You will need the connection details of the database you want to use if you have not already provided them</span></p>"
			+ "</html>";
	
	private JLabel introContent;
	
	private JPanel loadButtonWrapper;
	private JButton loadButton;

	
	private MainWindow mainWindow;
	
	
	
	public MainTableWelcomeScreen(MainWindow mainWindow) {
		this.mainWindow = mainWindow;
		setup();
	}

	private void setup() {
		setLayout(new GridLayout(0, 1));
		
		introContent = new JLabel(html);
		
		loadButton = new JButton("Continue");
		
		loadButtonWrapper = new JPanel(new FlowLayout(FlowLayout.CENTER));
		loadButtonWrapper.add(loadButton);
		
		add(introContent);
		add(loadButtonWrapper);
		
		loadButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				mainWindow.promptForConnectionInfo("Load Database...");
			}
		});
	}
}
