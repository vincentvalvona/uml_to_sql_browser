package growoffshore.mainwindow;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;

public class MainWindowTableErrorFooter extends JPanel {

	JTextArea messageBox;
	JButton revertButton;
	private BaseTable baseTable;
	
	public MainWindowTableErrorFooter(BaseTable baseTable) {
		this.baseTable = baseTable;
		setup();
	}
	
	private void setup() {
		setBackground(Color.PINK);
		setOpaque(true);
		
		setBorder(new EmptyBorder(10, 10, 10, 10));
		
		setLayout(new GridBagLayout());
		messageBox = new JTextArea();
		messageBox.setEditable(false);
		messageBox.setLineWrap(true);
		messageBox.setWrapStyleWord(true);
		messageBox.enableInputMethods(false);
		revertButton = new JButton("Revert");
		
		GridBagConstraints mainLayoutConstaints = new GridBagConstraints();
		mainLayoutConstaints.anchor = GridBagConstraints.NORTH;
		
		mainLayoutConstaints.gridx = 0; mainLayoutConstaints.gridy = 0;
		mainLayoutConstaints.gridwidth = 1; mainLayoutConstaints.gridheight = 1;
		mainLayoutConstaints.weightx = 1; mainLayoutConstaints.weighty = 1;
		mainLayoutConstaints.fill = GridBagConstraints.BOTH;
		add(messageBox, mainLayoutConstaints);
		

		mainLayoutConstaints.gridx = 1; mainLayoutConstaints.gridy = 0;
		mainLayoutConstaints.gridwidth = 1; mainLayoutConstaints.gridheight = 1;
		mainLayoutConstaints.weightx = 0; mainLayoutConstaints.weighty = 1;
		mainLayoutConstaints.fill = GridBagConstraints.NONE;
		add(revertButton, mainLayoutConstaints);
		
		revertButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				baseTable.model.revertInvalidRows();
			}
		});
		
		hideMessage();
	}
	
	public void showMessage(String message) {
		messageBox.setText(message);
		setVisible(true);
	}
	
	public void hideMessage() {
		setVisible(false);
	}

	public void hideRevertButton() {
		revertButton.setVisible(false);
	}
}
