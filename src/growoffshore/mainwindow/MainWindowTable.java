package growoffshore.mainwindow;

import java.awt.Color;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellEditor;

import growoffshore.DatabaseInfo;
import growoffshore.PasteModifier;
import growoffshore.TablePanelWindow;
import growoffshore.UserAlerter;
import growoffshore.dialogs.ErrorFieldsDialog;
import growoffshore.dialogs.TableStacker;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.UniqueColumnGroup;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLEngine;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BadForeignConstraintValue;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableColumn;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.InterfaceBaseTableColumn;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.ModelDataChangedListener;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.Row;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.functions.Functions;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.ColourOverrider;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.EditorController;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.Prompt;

public class MainWindowTable extends BaseTable {
	
	private final static String ACTION_PASTE = "paste";
	
	private TablePanelWindow parentWindow;
	
	private final Table table;

	private boolean pastingErrorsPromptDialog = true;

	private String[] tableNamesStack;
	
	public MainWindowTable(TablePanelWindow parentWindow, final LightTableModel model, UserAlerter alerter, Table table) {
		super((Window) parentWindow, model, alerter);
		this.parentWindow = parentWindow;
		this.table = table;
		setup();
	}

	private void setup() {
		
		model.addModelDataChangedListener(new ModelDataChangedListener() {
			
			@Override
			public void beforeDataChanged(Object newValue, int rowIndex, int columnIndex, Object[] newRow, Row currentRow, Object extras) {}
			
			@Override
			public void afterDataChanged(Object newValue, int rowIndex, int columnIndex, Object[] newRow, Row currentRow, Object extras) {
				showOrHideErrorFooter(rowIndex);
			}

			@Override
			public void afterDataRefreshed() {
				showOrHideErrorFooter(-1);
			}

			@Override
			public void afterDataDeleted() {
				showOrHideErrorFooter(-1);
			}
			
			private void showOrHideErrorFooter(int rowIndex) {
				if (parentWindow instanceof MainWindow) {
					MainWindow mainWindow = (MainWindow) parentWindow;
					MainWindowTableErrorFooter errorFooter = mainWindow.getTableErrorFooter();
					
					if (errorFooter != null) {
						if (!model.allRowsValid()) {
							
							DatabaseInfo dbInfo = mainWindow.getDbInfo();
							String message = dbInfo.constructConstraintsMessage(table);
							
							errorFooter.showMessage(message);
						}
						else {
							errorFooter.hideMessage();
						}
						
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								if (rowIndex >= 0) {
//									selectAndShowRow(rowIndex);
								}
							}
						});
					}
				}
			}
		});
	}

	@Override
	protected void setupTable() {
		
	}
	

	public void setPastingErrorsPromptDialog(boolean pastingErrorsPromptDialog) {
		this.pastingErrorsPromptDialog = pastingErrorsPromptDialog;
	}
	
	public boolean isPastingErrorsPromptDialog() {
		return pastingErrorsPromptDialog;
	}
	
	@Override
	protected void addCustomMenuOptions(JPopupMenu masterMenu, int index, Column column) {
		JPopupMenu filterMenuMenu = new JPopupMenu();
		
		
		Object[][] rowData = model.getData();
		
		
//		for (int i=0; i<allColumns.length; i++) {
//			final InterfaceBaseTableColumn aTableColumn = allColumns[i];
//			final boolean columnVisible = tableColumnModel.columnVisible(aTableColumn);
//			  
//			JMenuItem menuItem = menu.add(new JCheckBoxMenuItem(aTableColumn.getHeaderValue().toString()));
//			menuItem.setSelected(columnVisible);
//			menuItem.addActionListener(new ActionListener() {
//				@Override
//				public void actionPerformed(ActionEvent e) {
//					tableColumnModel.setColumnVisible(aTableColumn, !columnVisible);
//					tableColumnModel.rearrangeColumns();
//				}
//			});
//		}
	}
	
	@Override
	protected Object[][] pasteData(int[] selectedRowIndices, int[] selectedColumnIndices, Object[][] dataToPaste, UserAlerter alerter,
			int modifyDataMode, String errorDialogTitle) {
		
		MainWindowAlerterPaste pasteAlerter = new MainWindowAlerterPaste();

		SQLEngine.getMainConnection().setCommitAndRollbackEnabled(false);
		dataToPaste = super.pasteData(selectedRowIndices, selectedColumnIndices, dataToPaste, pasteAlerter, modifyDataMode, errorDialogTitle);
		SQLEngine.getMainConnection().setCommitAndRollbackEnabled(true);
		
		if (pasteAlerter.pasteSuccessful()) {
			SQLEngine.getMainConnection().commit();
//			JOptionPane.showMessageDialog((Window) parentWindow, "Yay! Successful");
		}
		else {			
			ErrorFieldsDialog correctingItemsDialog = null;
			
			if (pastingErrorsPromptDialog) {
				// Making sure the number of rows and columns are big enough to fit selection (if possible)
				selectedRowIndices = matchIndicesLengthToSelection(selectedRowIndices, dataToPaste);
				
				Object[] dataToPasteColumns = dataToPaste[0];
				selectedColumnIndices = matchIndicesLengthToSelection(selectedColumnIndices, dataToPasteColumns, getColumnCount()-1);
				
				correctingItemsDialog = createErrorDialog(selectedRowIndices, selectedColumnIndices, dataToPaste, pasteAlerter, errorDialogTitle);
				
				setSelection(selectedRowIndices, selectedColumnIndices);
				correctingItemsDialog.display();
			}
			
			int selection = correctingItemsDialog == null ? ErrorFieldsDialog.SELECTION_CANCEL
					: correctingItemsDialog.getSelection();
			
			if (ErrorFieldsDialog.SELECTION_CANCEL == selection) {
				SQLEngine.getMainConnection().rollback();
				model.update();
			}
			else if (ErrorFieldsDialog.SELECTION_IGNORE == selection) {
				SQLEngine.getMainConnection().commit();
				model.update();
			}
			else if (ErrorFieldsDialog.SELECTION_OK == selection) {
				SQLEngine.getMainConnection().rollback();
				int numberOfRowsBefore = model.getRowCount(); 
				model.update();
				int numberOfRowsAfter = model.getRowCount();

				for (int i=numberOfRowsAfter; i<numberOfRowsBefore; i++) {
					addRow();
				}
				
				Object[][] newData = correctingItemsDialog.getDataAs2DStringArray();
				Object[][] newPasteData = new String[selectedRowIndices.length][selectedColumnIndices.length];
				
				PasteModifier[] pasteModifiers = getPasteModifiers(selectedColumnIndices);
				
				for (int rowIndex=0; rowIndex<selectedRowIndices.length; rowIndex++) {
					for (int colIndex=0; colIndex<selectedColumnIndices.length; colIndex++) {
						Object value = newData[rowIndex][selectedColumnIndices[colIndex]];
						
//						PasteModifier nextPasteModifier = pasteModifiers[colIndex];
//						String cellToCopy = nextPasteModifier == null ? (value == null ? "" : value.toString()) : nextPasteModifier.convertFromRealValueToStringValue(value);
						
						newPasteData[rowIndex][colIndex] = value;
					}
				}
				
				if (errorDialogTitle == DIALOG_TITLE_ADD_NEW_ROW) {
					errorDialogTitle = DIALOG_TITLE_ERROR_ADDING_NEW_ROW;
				}
				
				pasteData(selectedRowIndices, selectedColumnIndices, newPasteData, pasteAlerter, MODIFY_DATA_NONE, errorDialogTitle);
				model.update();
			}	
		}
		
		return dataToPaste;
	}

	private ErrorFieldsDialog createErrorDialog(int[] selectedRowIndices, int[] selectedColumnIndices,
			final Object[][] dataToPaste, MainWindowAlerterPaste pasteAlerter, String title) {
		
		int[] selectedModelRowIndices = convertRowIndexToModel(selectedRowIndices);
		int[] selectedModelColumnIndices = convertColumnIndexToModel(selectedColumnIndices);
		
		int[] rowIndicesToPaste = new int[selectedModelRowIndices.length];
		for (int i=0; i<rowIndicesToPaste.length; i++) {
			rowIndicesToPaste[i] = i;
		}
		Column[] columnsToPaste = model.getColumns(selectedModelColumnIndices);
		
		Column[] columnsToAdd = tableColumnModel.getAllRealColumns();

		final LightTableModel outerModel = model;
		
		ErrorFieldsDialog correctingItemsDialog = new ErrorFieldsDialog(parentWindow, table, columnsToAdd, parentWindow.getDbInfo()) {

			private int numberOfDataRowsToPaste;
			private int numberOfModelRows;
			private int numberOfRowsToAdd;
			private int[] columnIndicesToPaste;
			
			boolean[] columnsEnabledStatus;
			
			int totalNumberOfRows;
			int totalNumberOfColumns;
			
			boolean[][] errorPresentMatrix;
			
			@Override
			protected void doCustomForModel(LightTableModel model) {
				
				model.setAllowPreservedDataTypes(true);
				
				numberOfDataRowsToPaste = dataToPaste.length;
				numberOfModelRows = selectedModelRowIndices.length;
				
				numberOfRowsToAdd = Integer.max(numberOfDataRowsToPaste, numberOfModelRows);
				
				
				for (int rowIndex=0; rowIndex<numberOfRowsToAdd; rowIndex++) {
					model.addRow();
				}

				totalNumberOfRows = model.getRowCount();
				totalNumberOfColumns = model.getColumnCount();
				
				errorPresentMatrix = new boolean[totalNumberOfRows][totalNumberOfColumns];
				
				columnIndicesToPaste = model.getColumnIndices(columnsToPaste);
				
				/* Setting which columns are enabled and also putting markers for where
				 * the invalid values were put. 
				 */
				columnsEnabledStatus = new boolean[columnsToAdd.length];
				
				model.addModelDataChangedListener(new ModelDataChangedListener() {
					@Override
					public void beforeDataChanged(Object newValue, int rowIndex, int columnIndex, Object[] newRow, Row currentRow, Object extras) {
						
						if (columnIndex >= 0) {
						
							Column column = model.getColumn(columnIndex);
							
							for (int nextRowIndex=0; nextRowIndex<totalNumberOfRows; nextRowIndex++) {
								Object nextValue = model.getValueAt(nextRowIndex, columnIndex);
								
								if (nextValue == null) {
									errorPresentMatrix[nextRowIndex][columnIndex] = !column.isNullAllowed();
								}
								else {
									if (column.isUnique()) {
										errorPresentMatrix[nextRowIndex][columnIndex] = 
												outerModel.valueExistsInColumn(nextValue, selectedRowIndices[nextRowIndex], columnIndex)
												|| model.valueExistsInColumn(nextValue, nextRowIndex, columnIndex);
									}
									else {
										errorPresentMatrix[nextRowIndex][columnIndex] = false;
									}
								}
								model.fireTableDataChanged();
							}
						}
					}

					@Override
					public void afterDataChanged(Object newValue, int rowIndex, int columnIndex, Object[] newRow, Row currentRow, Object extras) {}

					@Override
					public void afterDataRefreshed() {}

					@Override
					public void afterDataDeleted() {}
				});
				
				for (int colIndex=0; colIndex<columnIndicesToPaste.length; colIndex++) {
					columnsEnabledStatus[columnIndicesToPaste[colIndex]] = true;
				}

				// Pasting the valid selected data into the model
				model.pasteDataToModel(rowIndicesToPaste, columnIndicesToPaste, dataToPaste, null);
				
				model.setAllowPreservedDataTypes(false);

				model.setRowAddingEnabled(false);
				model.setRowDeletingEnabled(false);
			}
			
			@Override
			protected void doCustomForTable(BaseTable baseTable) {
				
				((MainWindowTable)baseTable).setPastingErrorsPromptDialog(false);
				
				final ColourOverrider defaultColourOverrider = baseTable.getDefaultColourOverrider();
				final EditorController defaultEditorController = baseTable.getDefaultEditorController();
				
				baseTable.setColourOverrider(new ColourOverrider() {
					
					@Override
					public Color setColourOverrides(Component comp, Object value, boolean isSelected, boolean hasFocus, int rowIndex,
							int columnIndex) {
						
						Color normalColour = defaultColourOverrider.setColourOverrides(comp, value, isSelected, hasFocus, rowIndex, columnIndex); 
						 
						int modelColumnIndex = baseTable.convertColumnIndexToModel(columnIndex);
						int modelRowIndex = baseTable.convertRowIndexToModel(rowIndex);
						
						if (errorPresentMatrix[modelRowIndex][modelColumnIndex]) {
							if (normalColour.equals(COLOUR_SELECTED)) {
								normalColour = INVALID_COLOUR_SELECTED;
							}
							else if (normalColour.equals(COLOUR_HIGHLIGHTED)) {
								normalColour = INVALID_COLOUR_HIGHLIGHTED;
							}
							else {
								normalColour = INVALID_COLOUR_NORMAL;
							}
						}
						
						return normalColour;
					}
				});

				baseTable.setEditorController(new EditorController() {
					@Override
					public int isCellEditable(BaseTable baseTable, int rowIndex, int columnIndex) {
						return defaultEditorController.isCellEditable(baseTable, rowIndex, columnIndex);
					}
				});
			}
		};
		correctingItemsDialog.setTitle(title);
		return correctingItemsDialog;
	}
}
