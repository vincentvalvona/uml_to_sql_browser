package growoffshore.mainwindow;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;

import growoffshore.UserAlerter;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.Prompt;

public class MainWindowAlerterPaste implements UserAlerter {

	public final ArrayList<Prompt> allPrompts = new ArrayList<>();
	
	@Override
	public void onPrompt(Prompt currentPrompt) {
		allPrompts.add(currentPrompt);
	}

	public boolean pasteSuccessful() {
		int numberOfPrompts = allPrompts.size();
		for (int i=0; i<numberOfPrompts; i++) {
			if (allPrompts.get(i).status == Prompt.STATUS_FAILED) {
				return false;
			}
		}
		
		return true;
	}
	
	public Prompt[] getAllPromptsInCorrectPositions(final int status) {
		
		HashMap<Point, Integer> rowAndColumnIndexToSavedPromptPosition = new HashMap<>();
		
		int numberOfPastedItems = 0;
		
		// Counting the number of pasted items by looking out for the STATUS_ATTEMPTING_TO_SAVE status
		int allPromptsCount = allPrompts.size();
		for (int i=0; i<allPromptsCount; i++) {
			Prompt nextPrompt = allPrompts.get(i);
			if (nextPrompt.status == Prompt.STATUS_ATTEMPTING_TO_SAVE) {
				rowAndColumnIndexToSavedPromptPosition.put(new Point(nextPrompt.rowIndex, nextPrompt.colIndex), numberOfPastedItems++);
			}
		}

		Prompt[] promptsToReturn = new Prompt[numberOfPastedItems];
		for (int i=0; i<allPromptsCount; i++) {
			Prompt nextPrompt = allPrompts.get(i);
			if (nextPrompt.status == status) {
				Integer position = rowAndColumnIndexToSavedPromptPosition.get(new Point(nextPrompt.rowIndex, nextPrompt.colIndex));
				if (position != null) {
					promptsToReturn[position] = nextPrompt;
				}
			}
		}
		
		return promptsToReturn;
	}
}
