package growoffshore.mainwindow;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.InterfaceTableCellDetachedComponent;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;

public abstract class LightDetachedComponent implements InterfaceTableCellDetachedComponent {
	
	@Override
	public void setModelAndColumn(LightTableModel lightTableModel, Column column) {}

	@Override
	public void setToNoRowSelected() {
		updateComponent();
	}

	@Override
	public Column getColumn() { return null; }

	@Override
	public void setValue(int selectedRowIndex, Object value) {
		updateComponent();
	}

	@Override
	public void sendValueToTable() {}

	protected abstract void updateComponent();
}
