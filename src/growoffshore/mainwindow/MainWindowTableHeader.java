package growoffshore.mainwindow;

import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class MainWindowTableHeader extends JPanel {
	
	private JLabel headerLabel;
	private String title;
	
	public MainWindowTableHeader(String title) {
		this.title = title;
		setup();
	}
	
	private void setup() {
		headerLabel = new JLabel(title);
		Font headerLabelFont = headerLabel.getFont();
		headerLabel.setFont(new Font(headerLabelFont.getName(), headerLabelFont.getStyle(), 14));
		
		setBorder(new EmptyBorder(10, 10, 10, 10));
		
		setLayout(new GridLayout(1, 1));
		
		add(headerLabel);
	}
}
