package growoffshore.mainwindow;  

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.NumberFormatter;

import org.omg.CORBA.NO_MEMORY;

import growoffshore.DBInfoContainer;
import growoffshore.DatabaseInfo;
import growoffshore.ExtendedBaseTableModel;
import growoffshore.Main;
import growoffshore.TablePanel;
import growoffshore.TablePanelWindow;
import growoffshore.UserAlerter;
import growoffshore.dialogs.FilterFloatingFrameContent;
import growoffshore.dialogs.RowAsFormDialog;
import growoffshore.dialogs.TableStacker;
import growoffshore.generators.CellEditorAndRendererGenerator;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.DataType;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Option;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLEngine;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.functionality.FunctionalityGenerator;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.InterfaceTableCellDetachedComponent;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.ModelDataChangedListener;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.Row;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.models.ComboBoxData;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.Prompt;
import uk.co.solidcoresoftware.databaseapp.views.tables.UserTableScrollPane;

public class MainWindowTablePanel extends TablePanel implements UserAlerter {
	
	public final Table table;
	
	private UserTableScrollPane scrollPane;

	private JPanel buttonsPanel;

	private JPanel pagesPanel;
	private JButton pagePrev;
	private JLabel pageDisplay1;
	private JFormattedTextField pageNumberText;
	private JLabel pageDisplay2;
	private JButton pageNext;
	private JButton filtersButton;
	
	private JLabel pageRowInfo;
	
	private JLabel pageFilter;
	
	private JButton addRowButton;
	private JButton deleteRowButton;
	private JButton refreshButton;
	private JButton duplicateRowButton;
	private JButton editInFormButton;

	private int rowCount;

	private BaseTableModel baseModel;

	private int pageCount;

	private final boolean canModifyTable;

	private Integer rowPosInDBTable;

	public MainWindowTablePanel(final TablePanelWindow window, Table table) {
		this(window, table, true);
	}

	public MainWindowTablePanel(final TablePanelWindow window, Table table, boolean canModifyTable) {
		this(window, table, canModifyTable, null);
	}
	
	public MainWindowTablePanel(final TablePanelWindow window, Table table, boolean canModifyTable, Integer rowPosInDBTable) {
		super((Window) window);
		this.rowPosInDBTable = rowPosInDBTable;
		this.canModifyTable = canModifyTable;
		setBackground(Color.CYAN);
		this.table = table;
		
		final Column[] columns = SQLEngine.getMainConnection().getAllColumnsInTable(table);
		model = new ExtendedBaseTableModel(table, ((DBInfoContainer) window).getDbInfo()) {
			@Override
			protected void setupModel() {
				for (int i=0; i<columns.length; i++) {
					addColumn(columns[i]);
				}
			}
			
			@Override
			protected void setupCalibrators() {}
			
			@Override
			protected void defineIndices() {}
			
			@Override
			protected void postUpdate() {
				
				displayValues.clear();
				
				Column[] allColumns = dbInfo.getAllColumnsFromTable(table, true);
				
				for (int i=0; i<allColumns.length; i++) {
					Column nextColumn = allColumns[i];
					Column pKeyColumn = dbInfo.getPrimaryKeyOfForeignKey(nextColumn);
					if (pKeyColumn != null) {
						Column displayColumn = dbInfo.getDisplayColumnFromTable(pKeyColumn.table);
						HashMap<Long, String> options = SQLEngine.getMainConnection().getDisplayValues(pKeyColumn, displayColumn, model.getAllUniqueValuesInColumn(nextColumn));
						DisplayValueSet nextDVSet = new DisplayValueSet(options);
						displayValues.put(nextColumn, nextDVSet);
					}
				}
			}
		};
		
		
		baseModel = (BaseTableModel) this.model;
		
		model.addModelDataChangedListener(new ModelDataChangedListener() {
			
			private void doUpdates() {
				updateCustomDetailsFromModel();
			}
			
			@Override
			public void beforeDataChanged(Object newValue, int rowIndex, int columnIndex, Object[] newRow, Row currentRow, Object extras) {}
			
			@Override
			public void afterDataRefreshed() { doUpdates(); }
			
			@Override
			public void afterDataDeleted() { doUpdates(); }
			
			@Override
			public void afterDataChanged(Object newValue, int rowIndex, int columnIndex, Object[] newRow, Row currentRow,
					Object extras) {
				doUpdates();
			}
		});
		
		if (!canModifyTable) {
			model.setRowAddingEnabled(false);
			model.setRowDeletingEnabled(false);
		}
		
		baseModel.setRowsPerPage(Main.DATABASE_PAGES_PER_ROW);
		
		
		baseTable = new MainWindowTable(window, model, this, table) {
			@Override
			public boolean isCellEditable(EventObject anEvent) {
				if (canModifyTable) {
					return super.isCellEditable(anEvent);
				}
				else {
					return false;
				}
			}
			
			@Override
			public boolean isCellEditable(EventObject anEvent, boolean canUseKeys) {
				if (canModifyTable) {
					return super.isCellEditable(anEvent, canUseKeys);
				}
				else {
					return false;
				}
			}
			
			@Override
			public boolean isCellEditable(int row, int column) {
				if (canModifyTable) {
					return super.isCellEditable(row, column);
				}
				else {
					return false;
				}
			}
		};
		
		scrollPane = new UserTableScrollPane(baseTable);
		
		GridBagLayout mainLayout = new GridBagLayout();
		setLayout(mainLayout);
		
		GridBagConstraints mainLayoutConstraints = mainLayout.getConstraints(this);
		
		buttonsPanel = new JPanel(); 
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));

		addRowButton = new JButton("Add row");
		buttonsPanel.add(addRowButton);
		
		addRowButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int newRowIndex = baseTable.addRow();
				baseTable.changeSelection(newRowIndex, baseTable.getColumnCount(), false, false);
				baseTable.changeSelection(newRowIndex, 0, false, true);
				showEditInFormDialog(newRowIndex, window, "Add row...", RowAsFormDialog.BUTTONS_TYPE_OK_CANCEL);
			}
		});
		
		deleteRowButton = new JButton("Delete row");
		buttonsPanel.add(deleteRowButton);

		refreshButton = new JButton("Refresh");
		FunctionalityGenerator.addActionRefreshRows(refreshButton, baseTable.model);
		buttonsPanel.add(refreshButton);
		
		duplicateRowButton = new JButton("Duplicate 1 row");
		buttonsPanel.add(duplicateRowButton);
		
		duplicateRowButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				int numberOfSelectedRows = baseTable.getSelectedRowCount();
				if (numberOfSelectedRows == 1) {
					int selectedRow = baseTable.getSelectedRow();
					int modelIndex = baseTable.convertRowIndexToModel(selectedRow);
					
					Object[] copyOfSelectedRow = baseTable.model.getRowDataWithBlankPrimarys(modelIndex);
					int newRowIndex = baseTable.addRow();
					Object[] newRowData = baseTable.model.getRowAt(newRowIndex).getRowData();
					for (int i=0; i<copyOfSelectedRow.length; i++) {
						newRowData[i] = copyOfSelectedRow[i];
					}
					
					baseTable.changeSelection(newRowIndex, baseTable.getColumnCount(), false, false);
					baseTable.changeSelection(newRowIndex, 0, false, true);
					showEditInFormDialog(newRowIndex, window, "Duplicate row...", RowAsFormDialog.BUTTONS_TYPE_OK_CANCEL, copyOfSelectedRow);
				}
				else if (numberOfSelectedRows > 1) {
					baseTable.duplicateSelectedRows();
				}
			}
		});
		
		editInFormButton = new JButton("Edit row in form");
		buttonsPanel.add(editInFormButton);
		
		editInFormButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				int selectedRow = baseTable.getSelectedRow();
				showEditInFormDialog(selectedRow, window, "Edit row...", RowAsFormDialog.BUTTONS_TYPE_CLOSE);
			}
		});
		
		if (canModifyTable) {
			FunctionalityGenerator.setEnabledBasedOnNumberOfRowsSelected(addRowButton, baseTable, "Add row", "Add row", "Add row", 0, FunctionalityGenerator.NO_MAX);
			FunctionalityGenerator.addActionDeleteRow(deleteRowButton, baseTable, "Delete %n rows" , "Delete %n row", "Delete %n rows");
			FunctionalityGenerator.setEnabledBasedOnNumberOfRowsSelected(duplicateRowButton, baseTable, "Duplicate %n rows" , "Duplicate %n row", "Duplicate %n rows", 1, FunctionalityGenerator.NO_MAX);
			FunctionalityGenerator.setEnabledBasedOnNumberOfRowsSelected(editInFormButton, baseTable, "Edit row", "Edit row", "Edit row", 1, 1);
		}
		else {
			addRowButton.setEnabled(false);
			deleteRowButton.setEnabled(false);
			duplicateRowButton.setEnabled(false);
			editInFormButton.setEnabled(false);
		}
		
		
		// Adding page controls
		pagesPanel = new JPanel();
		pagesPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
		pagePrev = new JButton("<");
		pagePrev.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				addToPageNumberField(-1);
			}
		});
		pageDisplay1 = new JLabel("Page ");
		
		NumberFormat format = NumberFormat.getInstance();
	    NumberFormatter formatter = new NumberFormatter(format);
	    formatter.setValueClass(Integer.class);
	    formatter.setMinimum(1);
	    formatter.setMaximum(Integer.MAX_VALUE);
	    // If you want the value to be committed on each keystroke instead of focus lost
	    formatter.setCommitsOnValidEdit(true);
		pageNumberText = new JFormattedTextField(formatter);
		pageNumberText.setText("1");
		
		int pageNumberTextFixedWidth = 30;
		int pageNumberTextFixedHeight = pageNumberText.getPreferredSize().height;
		Dimension pageNumberTextDimension = new Dimension(pageNumberTextFixedWidth, pageNumberTextFixedHeight);
		pageNumberText.setMinimumSize(pageNumberTextDimension);
		pageNumberText.setMaximumSize(pageNumberTextDimension);
		pageNumberText.setPreferredSize(pageNumberTextDimension);
		pageNumberText.setSize(pageNumberTextDimension);
		
		pageNumberText.getDocument().addDocumentListener(new DocumentListener() {
			private void doMethod() {
				updatePageNumberOfModel();
			}
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				doMethod();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				doMethod();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				doMethod();
			}
		});
		pageDisplay2 = new JLabel(" of 1");
		pageNext = new JButton(">");
		pageNext.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				addToPageNumberField(1);
			}
		});
		
		
		pageRowInfo = new JLabel("Showing no rows");
		
		pagesPanel.add(pagePrev);
		pagesPanel.add(pageDisplay1);
		pagesPanel.add(pageNumberText);
		pagesPanel.add(pageDisplay2);
		pagesPanel.add(pageNext);
		pagesPanel.add(pageRowInfo);
		
		FilterFloatingFrameContent filterContent = null;
		
		if (window instanceof MainWindow) {
			System.out.println("A HERE");
			filtersButton = new JButton("Filtering and ordering..");
			filtersButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					Main.getMainWindow().createAndShowFilterFrame(table, (ExtendedBaseTableModel) baseModel);
				}
			});
			pagesPanel.add(filtersButton);
		}
		else {
			System.out.println("B HERE c length: " + columns.length);
			if (columns.length > 0) {
				DatabaseInfo dbInfo = Main.getMainWindow().getDbInfo();
				
				Column primaryColumn = null;
				
				for (int i=0; i<columns.length; i++) {
					Column nextColumn = columns[i];
					System.out.println("1 nextColumn: " + nextColumn.tableDotColumn());
					System.out.println("1 idName: " + nextColumn.table.idColumnName);
					if (nextColumn.isPrimary()) {
						primaryColumn = nextColumn;
						break;
					}
				}
				
				if (primaryColumn != null) {
					Column searchColumn = dbInfo.getDisplayColumnFromTable(primaryColumn.table);
					
					filterContent = new FilterFloatingFrameContent((Window)window, null, 3);
					add(filterContent);
					filterContent.setTablePanel(this);
					filterContent.display(dbInfo, table, baseModel);
					filterContent.setSectionFilterColumn(0, searchColumn);
				}
			}
		}
		
		int gridXPos = 0;
		
		if (filterContent != null) {
			mainLayoutConstraints.gridx = gridXPos++; mainLayoutConstraints.gridy = 0;
			mainLayoutConstraints.gridwidth = 1; mainLayoutConstraints.gridheight = 2;
			mainLayoutConstraints.weightx = 0; mainLayoutConstraints.weighty = 1;
			mainLayoutConstraints.fill = GridBagConstraints.BOTH;
			add(filterContent, mainLayoutConstraints);
		}

		mainLayoutConstraints.gridx = gridXPos; mainLayoutConstraints.gridy = 0;
		mainLayoutConstraints.gridwidth = 2; mainLayoutConstraints.gridheight = 1;
		mainLayoutConstraints.weightx = 1; mainLayoutConstraints.weighty = 0;
		mainLayoutConstraints.fill = GridBagConstraints.BOTH;
		add(pagesPanel, mainLayoutConstraints);
		
		mainLayoutConstraints.gridx = gridXPos; mainLayoutConstraints.gridy = 1;
		mainLayoutConstraints.gridwidth = 1; mainLayoutConstraints.gridheight = 1;
		mainLayoutConstraints.weightx = 1; mainLayoutConstraints.weighty = 1;
		mainLayoutConstraints.fill = GridBagConstraints.BOTH;
		add(scrollPane, mainLayoutConstraints);
		
		mainLayoutConstraints.gridx = ++gridXPos; mainLayoutConstraints.gridy = 1;
		mainLayoutConstraints.gridwidth = 1; mainLayoutConstraints.gridheight = 1;
		mainLayoutConstraints.weightx = 0; mainLayoutConstraints.weighty = 1;
		mainLayoutConstraints.fill = GridBagConstraints.BOTH;
		add(buttonsPanel, mainLayoutConstraints);

		updateCustomDetailsFromModel();
	}

	private void showEditInFormDialog(int selectedRowInTable, TablePanelWindow window, String title, int dialogType) {
		showEditInFormDialog(selectedRowInTable, window, title, dialogType, null);
	}
	
	private void showEditInFormDialog(int selectedRowInTable, TablePanelWindow window, String title, int dialogType, Object[] nextRowData) {
		if (selectedRowInTable >= 0) {
			int selectedRowInModel = baseTable.convertRowIndexToModel(selectedRowInTable);
			Column[] allColumns = baseTable.model.getAllColumns();
			CellEditorAndRendererGenerator[] generators = new CellEditorAndRendererGenerator[allColumns.length];

			Row nextRow = model.getRowAt(selectedRowInModel);
			
			if (nextRowData == null) {
				nextRowData = nextRow.getRowData();
			}
			Object[] nextRowDataCopy = Arrays.copyOf(nextRowData, nextRowData.length);
			int[] columnIndices = new int[allColumns.length];
			
			for (int i=0; i<allColumns.length; i++) {
				Column nextColumn = allColumns[i];
				CellEditorAndRendererGenerator nextGen = new CellEditorAndRendererGenerator(nextColumn, (Window) window, window.getDbInfo(), baseTable);
				generators[i] = nextGen;
				columnIndices[i] = i;
			}
			
			RowAsFormDialog dialog = new RowAsFormDialog(generators, nextRowDataCopy, baseTable, selectedRowInModel, columnIndices, window.getDbInfo(), window, dialogType);
			dialog.setTitle(title);
			
			dialog.setVisible(true);
		}
	}
	
	public void updateColumnProperties() {
		
		Column[] allColumnsInModel = model.getAllColumns();

		// First making sure the filtered column has an auto value set.
		DatabaseInfo dbInfo = null;

		if (window != null) {
			dbInfo = ((TablePanelWindow) window).getDbInfo();
		}
		
		for (int i=0; i<allColumnsInModel.length; i++) {
			final Column nextColumn = allColumnsInModel[i];
			
			nextColumn.removeAutomaticValue();
			
			// An automatic value is set if the column is a foreign key of the filtered table.
			if (dbInfo != null && dbInfo.columnIsPrimaryOfFilteredTable(nextColumn)) {
				nextColumn.setAutomaticValue(dbInfo.getGlobalFilteredValue());
			}
		}
		
		// The model is updated, old columns removed, new columns created and displayed

		System.out.println("initRowIndex: " + rowPosInDBTable);
		
		if (rowPosInDBTable != null) {
			int pageNumber = baseModel.getNumberOfPagesInTable(rowPosInDBTable);
			pageNumberText.setText("" + pageNumber);
		}
		else {
			pageNumberText.setText("1");
		}
		
		updateCustomDetailsFromModel();
		
		baseTable.removeAllColumns();

		// Setting the correct type of cell renderer and editor for each column.
		for (int i=0; i<allColumnsInModel.length; i++) {
			final Column nextColumn = allColumnsInModel[i];
			
			CellEditorAndRendererGenerator gen = new CellEditorAndRendererGenerator(nextColumn, window, ((TablePanelWindow) window).getDbInfo(), baseTable);
			baseTable.addColumn(nextColumn.tableDotColumn(), nextColumn.columnName, 100, 0, gen.cellRenderer, gen.cellEditor);
		}
		
		baseTable.showAllColumns();
		
		System.out.println("FD rowPosInDBTable: " + rowPosInDBTable);
		
		if (rowPosInDBTable == null) {
			// Do nothing
		}
		else {
			int rowIndexForCurrentPage = baseModel.getRowIndexOfForCurrentPage(rowPosInDBTable);
			baseTable.selectAndShowRow(rowIndexForCurrentPage - 1);
		}
	}

	private void updateRowCountFromModel() {
		rowCount = baseModel.getNumberOfRowsInTable(table);
	}
	
	private void updatePageCountFromModel() {
		pageCount = baseModel.getNumberOfPagesInTable(rowCount);
	}
	
	private void updateLabels() {

		Integer rowsPerPage = baseModel.getRowsPerPage();
		
		if (rowsPerPage != null) {
			pageDisplay2.setText(" of " + pageCount);
			
			if (rowCount == 0) {
				pageRowInfo.setText("Showing no rows");
			}
			else {
				int pageNumber = baseModel.getPageNumber();
				int lastRowNumber = pageNumber == pageCount ? rowCount : pageNumber * rowsPerPage.intValue();
				int firstRowNumber = (pageNumber - 1) * rowsPerPage.intValue() + 1;
				pageRowInfo.setText("Showing rows " + firstRowNumber + " to " + lastRowNumber + " of " + rowCount);
			}
		}
	}

	public void refreshTable() {
		updateColumnProperties();
	}

	@Override
	public void onPrompt(Prompt currentPrompt) {	
		
	}
	
	public UserTableScrollPane getScrollPane() {
		return scrollPane;
	}
	
	private void addToPageNumberField(int amount) {
		String pageNumberStr = pageNumberText.getText();
		
		int newPageNumber;
		
		try {
			newPageNumber = Integer.parseInt(pageNumberStr);
		}
		catch (NumberFormatException e) {
			return;
		}
		
		newPageNumber += amount;
		
		setPageNumber(newPageNumber);
	}
	
	public void setPageNumber(int newPageNumber) {
		System.out.println("setPageNumber " + newPageNumber);
		if (newPageNumber <= 0 || newPageNumber > pageCount) {
			// Do nothing
		}
		else {
			pageNumberText.setText("" + newPageNumber);
		}
	}
	
	private void updatePageNumberOfModel() {
		System.out.println("updatePageNumberOfModel");
		String pageNumberStr = pageNumberText.getText();
		
		int newPageNumber;
		
		try {
			newPageNumber = Integer.parseInt(pageNumberStr);
		}
		catch (NumberFormatException e) {
			/* Do nothing if page is invalid. Text box will revert to previous
			 * page number. This is a simple safeguard.
			 */
			return;
		}
		
		if (newPageNumber <= 0) {
			newPageNumber = 1;
			final int newPageNumberFixed = newPageNumber;
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					pageNumberText.setText("" + newPageNumberFixed);
				}
			});
		}
		else if (newPageNumber > pageCount) {
			newPageNumber = pageCount;
			final int newPageNumberFixed = newPageNumber;
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					pageNumberText.setText("" + newPageNumberFixed);
				}
			});
		}
		else {
			System.out.println("model.update()");
			baseModel.setPageNumber(newPageNumber);
			model.update();
		}
	}
	
	public void updateCustomDetailsFromModel() {
		updateRowCountFromModel();
		updatePageCountFromModel();
		updateLabels();
	}
}
