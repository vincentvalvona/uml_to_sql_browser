package growoffshore.mainwindow;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.xml.sax.SAXException;

import growoffshore.DBInfoContainer;
import growoffshore.DatabaseInfo;
import growoffshore.ExtendedBaseTableModel;
import growoffshore.Main;
import growoffshore.SQLParserWindow;
import growoffshore.TablePanelWindow;
import growoffshore.UserAlerter;
import growoffshore.XMLFileReader;
import growoffshore.dialogs.EnterConnectionInfoDialog;
import growoffshore.dialogs.FilterFloatingFrame;
import growoffshore.dialogs.ProgressBarDialog;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLEngine;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.Prompt;
import uk.co.solidcoresoftware.databaseapp.views.tables.UserTableScrollPane;

public class MainWindow extends JFrame implements UserAlerter, DBInfoContainer, TablePanelWindow{
	
	private static int modeCount = 0;
	public final static int MODE_INTRO = modeCount++;
	public final static int MODE_IN_DATABASE = modeCount++;
	
	JPanel mainPanel;
	
	JPanel topPartButtons;
	JButton importUMLFileButton;
	JButton changeDatabaseButton;
	JButton closeDatabaseButton;
	JButton exportDatabaseButton;
	JButton importDatabaseButton;
	JButton showSqlParserButton;
	
	JFileChooser xmlFileUploader = new JFileChooser();
	MainWindowTablesList tablesList;

//	private MainWindowFilterPanel filterPanel;
	private MainWindowTableHeader tableHeader;
	private MainWindowTablePanel tablePanel;
	private MainWindowTableErrorFooter tableErrorFooter;

	private JPanel leftPart;
	private JPanel tablePanelContainer;
	private JPanel introPartContainer;

	private JPanel rightPart;
	private GridBagLayout rightPartLayout;
	
	private DatabaseInfo dbInfo = null;
	private int mode = MODE_INTRO;
	private String[] tableNamesStack;
	
	FilterFloatingFrame filterFrame;
	
	public MainWindow() {
		setup();
	}

	private void setup() {
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(1300, 500));
		
		setLayout(new BorderLayout());
		
		GridBagLayout mainLayout = new GridBagLayout();
		mainPanel = new JPanel();
		mainPanel.setBackground(Color.DARK_GRAY);
		mainPanel.setLayout(mainLayout);
		
		getContentPane().add(mainPanel);

		importUMLFileButton = new JButton("Import UML File");
		importUMLFileButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clickedImportURLButton();
			}
		});
		
		changeDatabaseButton = new JButton("Change Database");
		ActionListener loadOrChangeDatabaseButtonActionListener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clickedChangeDatabaseButton();
			}
		};
		changeDatabaseButton.addActionListener(loadOrChangeDatabaseButtonActionListener);

		closeDatabaseButton = new JButton("Close Database");
		closeDatabaseButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				closeCurrentDatabase();
				hideFilterFrame();
			}
		});
		
		exportDatabaseButton = new JButton("Export to SQL file");
		exportDatabaseButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clickedExportDatabaseButton();
			}
		});
		

		importDatabaseButton = new JButton("Import from SQL File");
		importDatabaseButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clickedImportDatabaseButton();
			}
		});
		
		showSqlParserButton = new JButton("Show SQL parser");
		showSqlParserButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new SQLParserWindow().setVisible(true);
			}
		});

		topPartButtons = new JPanel();
		topPartButtons.setLayout(new FlowLayout(FlowLayout.LEFT));
		topPartButtons.add(importUMLFileButton);
		topPartButtons.add(changeDatabaseButton);
		topPartButtons.add(closeDatabaseButton);
//		topPartButtons.add(exportDatabaseButton);
//		topPartButtons.add(importDatabaseButton);
//		topPartButtons.add(showSqlParserButton);

		leftPart = new JPanel(); 
		leftPart.setLayout(new BoxLayout(leftPart, BoxLayout.Y_AXIS));
		
		rightPart = new JPanel();
		rightPartLayout = new GridBagLayout();
		rightPart.setLayout(rightPartLayout);
		rightPart.setAlignmentX(LEFT_ALIGNMENT);
		rightPart.setAlignmentY(TOP_ALIGNMENT);

		introPartContainer = new JPanel();
		introPartContainer.setLayout(new GridLayout(1, 1));
		introPartContainer.add(new MainTableWelcomeScreen(this));
		
		GridBagConstraints mainLayoutConstaints = new GridBagConstraints();
		mainLayoutConstaints.anchor = GridBagConstraints.NORTH;
		
		mainLayoutConstaints.gridx = 0; mainLayoutConstaints.gridy = 0;
		mainLayoutConstaints.gridwidth = 2; mainLayoutConstaints.gridheight = 1;
		mainLayoutConstaints.weightx = 1; mainLayoutConstaints.weighty = 0;
		mainLayoutConstaints.fill = GridBagConstraints.BOTH;
		mainPanel.add(topPartButtons, mainLayoutConstaints);

		mainLayoutConstaints.gridx = 0; mainLayoutConstaints.gridy = 1;
		mainLayoutConstaints.gridwidth = 2; mainLayoutConstaints.gridheight = 1;
		mainLayoutConstaints.weightx = 1; mainLayoutConstaints.weighty = 0;
		mainLayoutConstaints.fill = GridBagConstraints.BOTH;
		mainPanel.add(introPartContainer, mainLayoutConstaints);
		
		mainLayoutConstaints.gridx = 0; mainLayoutConstaints.gridy = 2;
		mainLayoutConstaints.gridwidth = 1; mainLayoutConstaints.gridheight = 1;
		mainLayoutConstaints.weightx = 0; mainLayoutConstaints.weighty = 1;
		mainLayoutConstaints.fill = GridBagConstraints.BOTH;
		mainPanel.add(leftPart, mainLayoutConstaints);
		
		mainLayoutConstaints.gridx = 1; mainLayoutConstaints.gridy = 2;
		mainLayoutConstaints.gridwidth = 1; mainLayoutConstaints.gridheight = 1;
		mainLayoutConstaints.weightx = 1; mainLayoutConstaints.weighty = 1;
		mainLayoutConstaints.fill = GridBagConstraints.BOTH;
		mainPanel.add(rightPart, mainLayoutConstaints);

		tablePanelContainer = new JPanel();
		
		tablePanelContainer.setOpaque(true);
		tablePanelContainer.setBackground(Color.YELLOW);
		
		setMode(MODE_INTRO);
		
		pack();
	}
	
	public void createAndShowFilterFrame(Table table, ExtendedBaseTableModel model) {
		if (filterFrame == null) {
			filterFrame = new FilterFloatingFrame(this);
		}
		
		if (model == filterFrame.getModel()) {
			filterFrame.displayExisting();
		}
		else {
			if (tablePanel != null) {
				filterFrame.content.setTablePanel(tablePanel);
				filterFrame.display(dbInfo, table, model);
			}
		}
	}
	
	public void hideFilterFrame() {
		if (filterFrame != null) {
			filterFrame.setVisible(false);
			filterFrame.dispose();
			filterFrame = null;
		}
	}
	
	private void clickedImportURLButton() {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
//				String msg = "Warning! All data from the current database will be deleted and replaced with new data.\n"
//						+ "This process is non-reversable (even if importing fails or is cancelled).\n\n"
//						+ "Before preceeding, it is strongly recommended that you backup this data first. Would \n"
//						+ "you like to save a backup?";
				
				String title = "Import new UML file?";
				
				int result = JOptionPane.showOptionDialog(MainWindow.this, title, null, JOptionPane.YES_NO_OPTION,
						JOptionPane.WARNING_MESSAGE, null, new String[] {"Yes", "No"}, "No");
				
				boolean cancelled = false;
				
				if (result == JOptionPane.NO_OPTION) {
					cancelled = true;
				}
		
				if (!cancelled) {
					int returnVal = xmlFileUploader.showOpenDialog(MainWindow.this);
					
					if (returnVal == JFileChooser.APPROVE_OPTION) {
						ProgressBarDialog progressDialog = new ProgressBarDialog(MainWindow.this);
						progressDialog.startMonitoring();
						
						progressDialog.setTitle("Importing new UML file into database");
						progressDialog.setMessage("Please be patient...");
						
						new Thread(new Runnable() {
							@Override
							public void run() {
								File file = xmlFileUploader.getSelectedFile();
								try {
									XMLFileReader xmlReader = new XMLFileReader(file);
									xmlReader.setUserAlerter(MainWindow.this);
									boolean successfulImport = xmlReader.importUMLFile(progressDialog);
									progressDialog.stopMonitoring();
									
									if (successfulImport) {
										update();
									}
								} catch (ParserConfigurationException | SAXException | IOException e1) {
									e1.printStackTrace();
									return;
								} catch (XPathExpressionException e1) {
									e1.printStackTrace();
									return;
								}
							}
						}).start();
					}
				}
			}
		});
	}

	private void clickedChangeDatabaseButton() {
		promptForConnectionInfo("Change Database...");
		setMode(MODE_IN_DATABASE);
	}
	
	private void closeCurrentDatabase() {
		setMode(MODE_INTRO);
	}
	
	private void clickedExportDatabaseButton() {
		
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				
				
				SQLEngine mainConnection = SQLEngine.getMainConnection();
				
				ArrayList<String> allTableNames = mainConnection.getTableNames(null);
				
				int numberOfTables = allTableNames.size();
				
				String[] allCreateStatements = new String[numberOfTables]; 
				
				for (int i=0; i<numberOfTables; i++) {
					String nextTableName = allTableNames.get(i);
					Table nextTable = new Table(nextTableName);
					
					// Getting CREATE statement for this table
					allCreateStatements[i] = mainConnection.getShowCreate(nextTable);
					
					// Getting all columns for this column
					Column[] allColumns = mainConnection.getShowColumns(nextTable);
				}
			}
		});
		
	}

	private void clickedImportDatabaseButton() {
		
	}
	
	public void update() {

		leftPart.removeAll();
		rightPart.removeAll();
		
		tablesList = null;
//		filterPanel = null;
	
		dbInfo = new DatabaseInfo();
		
		// This is a list of all of the tables.
		tablesList = new MainWindowTablesList(this);
		
		// This is the filter controls for at the top.
//		filterPanel = new MainWindowFilterPanel(this);

		tablesList.setMinimumSize(new Dimension(300, 1));
		tablesList.setPreferredSize(new Dimension(300, 1));

		leftPart.add(tablesList);
		
		populateTablesList();
		
//		filterPanel.reset();

//		rightPart.setPreferredSize(new Dimension(900, 500));
		GridBagConstraints rightPartLayoutConstraints = rightPartLayout.getConstraints(rightPart);
//		rightPartLayoutConstraints.gridx = 0; rightPartLayoutConstraints.gridy = 0;
//		rightPartLayoutConstraints.gridwidth = 1;
//		rightPartLayoutConstraints.weightx = 1;
//		rightPartLayoutConstraints.weighty = 0;
//		rightPartLayoutConstraints.fill = GridBagConstraints.BOTH;
//		rightPart.add(filterPanel, rightPartLayoutConstraints);
		
		rightPartLayoutConstraints.gridx = 0; rightPartLayoutConstraints.gridy = 0;
		rightPartLayoutConstraints.gridwidth = 1;
		rightPartLayoutConstraints.weightx = 1;
		rightPartLayoutConstraints.weighty = 1;
		rightPartLayoutConstraints.fill = GridBagConstraints.BOTH;
		rightPart.add(tablePanelContainer, rightPartLayoutConstraints);
		
		revalidate();
		repaint();
	}
	

	private void populateTablesList() {
		Table[] allTables = dbInfo.getTables();
		
		tablePanelContainer.removeAll();
		tablePanel = null;
		tablesList.clearTables();
		
		for (int i=0; i<allTables.length; i++) {
			tablesList.addTable(allTables[i]);
		}
		
		tablesList.expandAllNodes();
		
		tablesList.revalidate();
		tablesList.repaint();
	}
	
	public void showPanelForTable(Table table) {

		tableHeader = new MainWindowTableHeader(table.tableName);
		
		// Getting the correct panel for this table and updating it
		tablePanel = new MainWindowTablePanel(this, table);
		tablePanel.updateColumnProperties();
		
		BaseTable bTable = tablePanel.getBaseTable();
		tableErrorFooter = new MainWindowTableErrorFooter(bTable);
		
		tablePanelContainer.removeAll();
		
		tablePanelContainer.setLayout(new GridBagLayout());
		GridBagConstraints tablePanelContainerConstaints = new GridBagConstraints();
		
		tablePanelContainerConstaints.gridx = 0; tablePanelContainerConstaints.gridy = 0;
		tablePanelContainerConstaints.gridwidth = 1; tablePanelContainerConstaints.gridheight = 1;
		tablePanelContainerConstaints.weightx = 1; tablePanelContainerConstaints.weighty = 0;
		tablePanelContainerConstaints.fill = GridBagConstraints.HORIZONTAL;
		tablePanelContainer.add(tableHeader, tablePanelContainerConstaints);
		
		tablePanelContainerConstaints.gridx = 0; tablePanelContainerConstaints.gridy = 1;
		tablePanelContainerConstaints.gridwidth = 1; tablePanelContainerConstaints.gridheight = 1;
		tablePanelContainerConstaints.weightx = 1; tablePanelContainerConstaints.weighty = 1;
		tablePanelContainerConstaints.fill = GridBagConstraints.BOTH;
		tablePanelContainer.add(tablePanel, tablePanelContainerConstaints);
		
		tablePanelContainerConstaints.gridx = 0; tablePanelContainerConstaints.gridy = 2;
		tablePanelContainerConstaints.gridwidth = 1; tablePanelContainerConstaints.gridheight = 1;
		tablePanelContainerConstaints.weightx = 1; tablePanelContainerConstaints.weighty = 0;
		tablePanelContainerConstaints.fill = GridBagConstraints.BOTH;
		tablePanelContainer.add(tableErrorFooter, tablePanelContainerConstaints);
		
		tablePanelContainer.revalidate();
		tablePanelContainer.repaint();
		
		UserTableScrollPane scrollpane = tablePanel.getScrollPane();
		scrollpane.resizeTable();
		
		if (filterFrame != null && filterFrame.isVisible()) {
			createAndShowFilterFrame(table, (ExtendedBaseTableModel) bTable.model);
		}
	}

	public void display() {
		setVisible(true);
	}
	
	public void promptForConnectionInfo(String title) {
		while (true) {
			EnterConnectionInfoDialog enterConnectionInfoDlg = new EnterConnectionInfoDialog(title);
			enterConnectionInfoDlg.setVisible(true);
			
			int selectedButton = enterConnectionInfoDlg.getButtonSelected();
			
			if (selectedButton == enterConnectionInfoDlg.SELECTED_OK_BUTTON) {
				
				String host = enterConnectionInfoDlg.getFieldInput(EnterConnectionInfoDialog.HOST_FIELD);
				String username = enterConnectionInfoDlg.getFieldInput(EnterConnectionInfoDialog.USERNAME_FIELD);
				String password = enterConnectionInfoDlg.getFieldInput(EnterConnectionInfoDialog.PASSWORD_FIELD);
				String database = enterConnectionInfoDlg.getFieldInput(EnterConnectionInfoDialog.DATABASE_FIELD);
				
				String errorMessage = SQLEngine.attemptToConnect(host, username, password, database);
				
				if (errorMessage == null) {
					Main.setConnections(host, username, password, database);
					update();
					setMode(MODE_IN_DATABASE);
					break;
				}
				else {
					JOptionPane.showMessageDialog(this, errorMessage);
				}
			}
			else {
				break;
			}
		}
	}
	
	public DatabaseInfo getDbInfo() {
		return dbInfo;
	}

	public void invokeRefreshOnCurrentlyOpenedTable() {
		if (tablePanel != null) {
			tablePanel.refreshTable();
		}
	}

	public MainWindowTableErrorFooter getTableErrorFooter() {
		return tableErrorFooter;
	}

	@Override
	public void onPrompt(Prompt currentPrompt) {
		
	}

	@Override
	public TablePanelWindow getParentWindow() {
		return null;
	}

	
	public void setMode(int mode) {
		this.mode = mode;
		
		if (mode == MODE_INTRO) {
			introPartContainer.setVisible(true);
			leftPart.setVisible(false);
			rightPart.setVisible(false);
			topPartButtons.setVisible(false);
		}
		else {
			introPartContainer.setVisible(false);
			leftPart.setVisible(true);
			rightPart.setVisible(true);
			topPartButtons.setVisible(true);
		}
		
		setTitleWithDatabaseDetails();
	}
	
	public void setTitleWithDatabaseDetails() {
		
		final String TITLE_PREFIX = "Grow Offshore Wind Database Editor";
		
		if (mode == MODE_INTRO) {
			setTitle(TITLE_PREFIX);
		}
		else if (mode == MODE_IN_DATABASE) {
			SQLEngine mainConnection = SQLEngine.getMainConnection();
			
			String user = mainConnection.getUser();
			String urlPrefix = mainConnection.getUrlPrefix();
			String database = mainConnection.getDatabaseName();
			
			String titleSuffix = " - " + user + "@" + urlPrefix + "/" + database; 
			
			setTitle(TITLE_PREFIX + titleSuffix);
		}
	}
	
	public MainWindowTablePanel getTablePanel() {
		return tablePanel;
	}

}
