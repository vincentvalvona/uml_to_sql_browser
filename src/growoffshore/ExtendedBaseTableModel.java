package growoffshore;

import java.util.ArrayList;
import java.util.HashMap;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.OrderByClause;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLEngine;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.WhereClause;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableModel;

public class ExtendedBaseTableModel extends BaseTableModel {

	protected DatabaseInfo dbInfo;
	private Table table;
	
	protected HashMap<Column, DisplayValueSet> displayValues = new HashMap<>();
	private final Column displayColumn;
	
	public ExtendedBaseTableModel(Table table, DatabaseInfo databaseInfo) {
		super("SELECT * FROM " + table.tableNameWithTicks());
		this.table = table;
		setDatabaseInfo(databaseInfo);
		Column primaryColumn = new Column(table, table.idColumnName);
		displayColumn  = dbInfo.getDisplayColumnFromTable(table);
	}
	
	public void setDatabaseInfo(DatabaseInfo dbInfo) {
		this.dbInfo = dbInfo;
	}

	@Override
	protected void setupModel() {
		
	}

	@Override
	protected void setupCalibrators() {
		
	}

	@Override
	protected void defineIndices() {

	}

	@Override
	public void update() {
		super.update(customSQLForSelect());
	}
	
	@Override
	public void update(boolean invokePostUpdate) {
		super.update(invokePostUpdate, customSQLForSelect());
	}
	
	private String customSQLForSelect() {
		
		String sqlForSelect;
		
		if (dbInfo != null) {
			sqlForSelect = this.sqlForSelect + dbInfo.getJoinSQL(this.table);
		}
		else {
			sqlForSelect = this.sqlForSelect;
		}
		
		return sqlForSelect;
	}
	
	@Override
	public int getNumberOfRowsInTable(Table table) {
		if (dbInfo != null && (dbInfo.hasGlobalFilteredValue() || customWhereSql != null)) {
			
			int customJoinSQLSize = customJoinSQL.size();
			
			StringBuilder allJoinSQL = new StringBuilder();
			for (int i=0; i<customJoinSQLSize; i++) {
				String nextJoinSQL = customJoinSQL.get(i);
				allJoinSQL.append(" ");
				allJoinSQL.append(nextJoinSQL);
			}
			
			Column[] columns = new Column[modelIndexesToColumn.size()];
			modelIndexesToColumn.toArray(columns);
			
			ArrayList<WhereClause> whereClausesList = getWhereClausesList(columns);
			
			String sqlForSelect = this.sqlForSelect + dbInfo.getJoinSQL(this.table);
			int count = SQLEngine.getMainConnection().selectCount(null, sqlForSelect + allJoinSQL.toString(), null,
					whereClausesList.toArray(new WhereClause[whereClausesList.size()]), null, null); 
			
			return count;
		}
		else {
			return super.getNumberOfRowsInTable(table);
		}
	}
	
	public class DisplayValueSet {
		
		public final HashMap<Long, String> options;

		public DisplayValueSet(HashMap<Long, String> options) {
			this.options = options;
		}
		
		public String getDisplay(Long value) {
			return options.get(value);
		}
	}
	
	public String getDisplayValue(Column fKeyColumn, Long value) {
		DisplayValueSet dVSet = getDisplayValueSet(fKeyColumn);
		
		if (dVSet == null) return null;
		
		return dVSet.getDisplay(value);
	}
	
	public DisplayValueSet getDisplayValueSet(Column fKeyColumn) {
		return displayValues.get(fKeyColumn);
	}
	
	@Override
	public ArrayList<OrderByClause> getCustomOrderByClauses() {
		if (customOrderByClauses.isEmpty()) {
			ArrayList<OrderByClause> aList = new ArrayList<>();
			aList.add(new OrderByClause(displayColumn, OrderByClause.ORDER_BY_ASC));
			return aList;
		}
		else {
			return customOrderByClauses;
		}
	}
}
