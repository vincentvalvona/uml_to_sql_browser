package growoffshore;

import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JPanel;

import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;

public abstract class TablePanel extends JPanel {

	protected Window window;
	protected LightTableModel model;
	protected BaseTable baseTable;
	
	public TablePanel(Window window) {
		this.window = window;
	}
	
	public BaseTable getBaseTable() {
		return baseTable;
	}
	
	public LightTableModel getModel() {
		return model;
	}
}
