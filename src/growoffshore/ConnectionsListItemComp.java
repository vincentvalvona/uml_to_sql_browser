package growoffshore;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class ConnectionsListItemComp extends JPanel {
	
	private JLabel hostAndUserHeading;
	private JLabel databaseSubheading;

	public final ConnectionOption connectionOption;
	
	public ConnectionsListItemComp(ConnectionOption option) {
		this.connectionOption = option;
		setup();
	}
	
	private void setup() {
		hostAndUserHeading = new JLabel(connectionOption.getUser() + "@" + connectionOption.getHost());
		databaseSubheading = new JLabel(connectionOption.getDatabase());

		databaseSubheading.setHorizontalTextPosition(SwingConstants.RIGHT);
		
		Font hostAndUserHeadingFont = new Font(hostAndUserHeading.getFont().getName(), Font.BOLD, hostAndUserHeading.getFont().getSize());
		hostAndUserHeading.setFont(hostAndUserHeadingFont);
		
		Font databaseSubheadingFont = new Font(databaseSubheading.getFont().getName(), Font.PLAIN, databaseSubheading.getFont().getSize());
		databaseSubheading.setFont(databaseSubheadingFont);
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(hostAndUserHeading);
		add(databaseSubheading);
	}
}