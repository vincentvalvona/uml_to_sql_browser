package growoffshore.cellEditorsAndRenderers;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;

public class CheckBoxCellEditor extends DefaultCellEditor {

	private final JCheckBox checkBox;
	
	private BaseTable baseTable;
	
	public CheckBoxCellEditor(JCheckBox checkBox, BaseTable baseTable) {
		super(checkBox);
		this.checkBox = checkBox;
		this.baseTable = baseTable;
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int rowIndex, int columnIndex) {
		if (value instanceof Boolean) {
			checkBox.setSelected((Boolean) value);
		}
		else {
			checkBox.setSelected(false);
		}
		
		return checkBox;
	}
	
	@Override
	public boolean isCellEditable(EventObject anEvent) {
		return true;
	}
}
