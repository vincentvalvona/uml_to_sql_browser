package growoffshore.cellEditorsAndRenderers;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.EventObject;
import java.util.Iterator;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.SwingUtilities;

import growoffshore.DatabaseInfo;
import growoffshore.PasteModifier;
import growoffshore.TablePanelWindow;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.DataType;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Option;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.ValueToNeverSave;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BadForeignConstraintValue;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.models.ComboBoxData;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.models.ComboBoxSelectedAddRow;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.models.CommandSelection;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;

public class ComboBoxDataCellEditor extends DefaultCellEditor implements PasteModifier, EditRowPopupCommander {

	protected boolean editingIsOn = false;
	
	protected TablePanelWindow window;
	protected DatabaseInfo dbInfo;
	protected BaseTable baseTable;
	
	protected CommandSelection addRowOptionReturnCommand = null;

	protected Column column;

	protected Long initialValue = null;

	private static SpecialComboBox<String> cBox = new SpecialComboBox<String>();
	
	public ComboBoxDataCellEditor(TablePanelWindow window, DatabaseInfo dbInfo, Column column, BaseTable baseTable) {
		super(cBox);
		
		this.window = window;
		this.dbInfo = dbInfo;
		this.column = column;
		this.baseTable = baseTable;
		setClickCountToStart(2);
	}
	
	public boolean isEditing() {
		return editingIsOn;
	}
	

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		
		if (value == null || value instanceof Long) {
			initialValue  = (Long) value;
		}
		
		JLabel label = new JLabel();
		label.addMouseListener(new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent e) {}
			@Override
			public void mousePressed(MouseEvent e) {
				editingIsOn = false;
				stopCellEditing();
			}
			@Override
			public void mouseExited(MouseEvent e) {}
			@Override
			public void mouseEntered(MouseEvent e) {}
			@Override
			public void mouseClicked(MouseEvent e) {}
		});
		return label;
	}
	
	@Override
	public boolean isCellEditable(EventObject anEvent) {
		if (column.isAutomaticValueSet()) {
			return false;
		}
		
		boolean singleClickEdit = false;
		
		Integer rowIndex = null;
		Integer colIndex = null;
		
		if (anEvent instanceof MouseEvent) {
			MouseEvent mouseEv = (MouseEvent) anEvent;
			
			Object src = mouseEv.getSource();
			
			if (src instanceof JTable) {
				JTable jTable = (JTable) src;
				
				if (mouseEv.getClickCount() == 1) {
					
					if (cBox != null) {
						
						Point mousePoint = mouseEv.getPoint();
						int mouseX = mouseEv.getX();
						int mouseY = mouseEv.getY();
						
						rowIndex = jTable.rowAtPoint(mousePoint);
						colIndex = jTable.columnAtPoint(mousePoint);
						
						Rectangle cellRect = jTable.getCellRect(rowIndex, colIndex, false);
						
						if (mouseX >= cellRect.x + cellRect.width - cBox.getButtonWidth()) {
							singleClickEdit = true;
						}
					}
				}
			}
		}
		
		if (editingIsOn) {
			editingIsOn = false;
			cancelCellEditing();
			return false;
		}

		editingIsOn = true;
		
		if (singleClickEdit || super.isCellEditable(anEvent)) {
			if (rowIndex != null || colIndex != null) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public Object getCellEditorValue() {
		return new ComboBoxSelectedAddRow(this);
	}

	
	public void addRowOptionSelected(LightTableModel model, Object newValue, int rowIndex, int columnIndex) {
		
	}

	@Override
	public Object convertFromStringValueToRealValue(String value) {
		return 0;
//		// A value with all white space or null is considered null
//		if (value == null || value.trim().equals("")) {
//			return null;
//		}
//		
//		Iterator<Option> allOptionsIt = cBoxData.allOptions.iterator();
//		
//		Option matchingOptionExact = null;
//		Option matchingOptionCaseTrimmed = null;
//		Option matchingOption = null;
//		
//		while (allOptionsIt.hasNext()) {
//			Option nextOption = allOptionsIt.next();
//			String nextOptionDisplay = nextOption.stringValue;
//			
//			// An exact match takes priority
//			if (value.equals(nextOptionDisplay)) {
//				matchingOptionExact = nextOption;
//				break;
//			}
//			// Then does a trimmed match
//			else if (value.trim().equals(nextOptionDisplay.trim())) {
//				matchingOptionCaseTrimmed = nextOption;
//			}
//			// Then does a trimmed case-insensitive match
//			else if (value.toLowerCase().trim().equals(nextOptionDisplay.toLowerCase().trim())) {
//				matchingOption = nextOption;
//			}
//		}
//
//		if (matchingOptionExact != null) return matchingOptionExact.value;
//		else if (matchingOptionCaseTrimmed != null) return matchingOptionCaseTrimmed.value;
//		else if (matchingOption != null) return matchingOption.value;
//		
//		return new BadForeignConstraintValue(value);
	}
	
	@Override
	public String convertFromRealValueToStringValue(Object value) {
		return "" + value;
//		if (value instanceof BadForeignConstraintValue) {
//			BadForeignConstraintValue badValue = (BadForeignConstraintValue) value;
//			Option option = cBoxData.getOptionFromValue(badValue.value);
//			return option.stringValue;
//		}
//		else {
//			Option option = cBoxData.getOptionFromValue(value);
//			return option.stringValue;
//		}
	}

	public static class SpecialComboBox<T> extends JComboBox<T> {
		
		static int fixedWidth = 20;
		
		public Insets getInsets() {
			 return new Insets(-5,0,-5,0);
		}
		
		public int getButtonWidth() {
			JButton button = findButton(this);
			if (button != null) {
				Rectangle visibleRect = button.getVisibleRect();
				if (visibleRect.width > 0) {
					fixedWidth = button.getVisibleRect().width;
				}
			}
			
			return fixedWidth;
		}
		
		public JButton findButton(Component comp) {
			if (comp instanceof JButton) {
				return (JButton) comp;
			}
			
			if (comp instanceof Container) {
				Container container = (Container) comp;
				Component[] allComps = container.getComponents();
				for (int i=0; i<allComps.length; i++) {
					Component nextComp = allComps[i];
					JButton possibleButton = (JButton) findButton(nextComp);
					if (possibleButton != null) {
						return possibleButton;
					}
				}
			}
			
			return null;
		}
	}
	
	public void setAddRowOptionReturnCommand(CommandSelection addRowOptionReturnCommand) {
		this.addRowOptionReturnCommand = addRowOptionReturnCommand;
	}
}
