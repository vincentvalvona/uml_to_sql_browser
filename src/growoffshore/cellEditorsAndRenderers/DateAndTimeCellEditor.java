package growoffshore.cellEditorsAndRenderers;

import java.awt.Component;
import java.awt.Container;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Calendar;
import java.sql.Date;
import java.util.EventObject;

import javax.swing.AbstractCellEditor;
import javax.swing.BoxLayout;
import javax.swing.CellEditor;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerDateModel;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

import growoffshore.ContainsManualStop;
import growoffshore.DateChooserForPopup;
import growoffshore.Main;
import growoffshore.TimeChooserForPopup;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.DataType;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;

public class DateAndTimeCellEditor extends AbstractCellEditor implements TableCellEditor, ContainsManualStop {

	private Window window;
	
	private JPanel elementsContainer;
	private DateChooserForPopup dateChooser;
	private TimeChooserForPopup timeChooser;
	private BaseTable baseTable;

	private Column column;
	
	public DateAndTimeCellEditor(Window window, BaseTable baseTable, Column column) {
		this.window = window;
		this.baseTable = baseTable;
		this.column = column;
	}
	

	@Override
	protected void fireEditingStopped() {
		// TODO Auto-generated method stub
//		super.fireEditingStopped();
	}
	
	public void manuallyEditingStopped() {
		super.fireEditingStopped();
	}
	
	@Override
	public Object getCellEditorValue() {
		
		// Creates a new calendar using the default date and time initially
		
		
		Calendar dateChooserCal = null;
		Calendar timeChooserCal = null;
		
		if (dateChooser != null) {
			dateChooserCal = dateChooser.getCalendar();
		}
		
		if (timeChooser != null) {
			timeChooserCal = Calendar.getInstance();
			timeChooserCal.setTime((java.util.Date) timeChooser.getValue());
		}
		
		Calendar cal = Main.combineDateAndTimeCalendars(dateChooserCal, timeChooserCal);
		
		return column.getCurrentTimeValue(cal.getTimeInMillis());
	}
	

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int rowIndex, int columnIndex) {

		java.util.Date valueToShowInEditor = column.getCurrentTimeValue(value);
		
		if (elementsContainer != null) {
			Container parentContainer = elementsContainer.getParent();
			if (parentContainer != null) parentContainer.remove(elementsContainer);
		}
		
		elementsContainer = null;
		dateChooser = null;
		timeChooser = null;
//		timeSpinner = null;
		
		// The container to return as the cell-editor
		elementsContainer = new JPanel();
		elementsContainer.setLayout(new BoxLayout(elementsContainer, BoxLayout.X_AXIS));

		int columnType = column.getType();
		
		// Creates a date-chooser part if a date is in the data-type
		if (columnType == DataType.INT_TYPE_DATETIME || columnType == java.sql.Types.TIMESTAMP || columnType == java.sql.Types.DATE) {
			dateChooser = new DateChooserForPopup(valueToShowInEditor, window);
			dateChooser.addPropertyChangeListener(new PropertyChangeListener() {
				
				@Override
				public void propertyChange(PropertyChangeEvent evt) {
					if (evt.getPropertyName().equals("date")) {
						manuallyEditingStopped();
					}
				}
			});
			elementsContainer.add(dateChooser);
		}
		
		// Creates a time-chooser part if a time is in the data-type
		if (columnType == DataType.INT_TYPE_DATETIME || columnType == java.sql.Types.TIMESTAMP || columnType == java.sql.Types.TIME) {
			timeChooser = new TimeChooserForPopup(valueToShowInEditor, window, this);
			elementsContainer.add(timeChooser);
		}

		return elementsContainer;
	}

	@Override
	public boolean isCellEditable(EventObject e) {
		if (super.isCellEditable(e)) {
			return baseTable.isCellEditable(e);
		}
		return false;
	}
}
