package growoffshore.cellEditorsAndRenderers;

import java.awt.Color;
import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Calendar;
import java.sql.Date;
import java.sql.Time;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerDateModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import com.toedter.calendar.JDateChooser;

import growoffshore.DateChooserForPopup;
import growoffshore.TimeChooserForPopup;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.DataType;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;

public class PopUpTableFormCellRenderer implements TableCellRenderer {

	private Window window;
	
	private JPanel elementsContainer;
	private DateChooserForPopup dateChooser;
//	private JSpinner timeSpinner;
	private Column column;

	private TimeChooserForPopup timeChooser;
	
	public PopUpTableFormCellRenderer(Window window, Column column) {
		this.window = window;
		this.column = column;
	}
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int rowIndex, int columnIndex) {
		
		BaseTableColourer baseTable = (BaseTableColourer) table;
		
		java.util.Date valueToShowInEditor = column.getCurrentTimeValue(value);
				
		elementsContainer = null;
		dateChooser = null;
		timeChooser = null;
		
		// The container to return as the cell-editor
		elementsContainer = new JPanel();
		elementsContainer.setOpaque(false);
		elementsContainer.setLayout(new BoxLayout(elementsContainer, BoxLayout.X_AXIS));

		int columnType = column.getType();
		
		// Creates a date-chooser part if a date is in the data-type
		if (columnType == DataType.INT_TYPE_DATETIME || columnType == java.sql.Types.TIMESTAMP || columnType == java.sql.Types.DATE) {
			dateChooser = new DateChooserForPopup((java.util.Date) valueToShowInEditor, window);
			elementsContainer.add(dateChooser);
			baseTable.setColour(dateChooser, value, isSelected, hasFocus, rowIndex, columnIndex);
		}
		
		// Creates a time-chooser part if a time is in the data-type
		if (columnType == DataType.INT_TYPE_DATETIME || columnType == java.sql.Types.TIMESTAMP || columnType == java.sql.Types.TIME) {
			
			timeChooser = new TimeChooserForPopup(valueToShowInEditor, window, null);
			elementsContainer.add(timeChooser);
			
//			timeSpinner = new JSpinner( new SpinnerDateModel() );
//			JSpinner.DateEditor timeEditor = new JSpinner.DateEditor(timeSpinner, "HH:mm:ss");
//			timeSpinner.setEditor(timeEditor);
//			timeSpinner.setValue((java.util.Date) valueToShowInEditor); // will only show the current time
//			elementsContainer.add(timeSpinner);
			
			baseTable.setColour(timeChooser, value, isSelected, hasFocus, rowIndex, columnIndex);
//			baseTable.setColour(timeSpinner.getEditor(), value, isSelected, hasFocus, rowIndex, columnIndex);
//			baseTable.setColour( ((JSpinner.DefaultEditor)timeSpinner.getEditor()).getTextField(), value, isSelected, hasFocus, rowIndex, columnIndex);
//			baseTable.setColour(timeEditor, value, isSelected, hasFocus, rowIndex, columnIndex);
		}

		return elementsContainer;
	}
}
