package growoffshore.cellEditorsAndRenderers;

import java.awt.Component;
import java.awt.Container;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Calendar;
import java.sql.Date;
import java.util.EventObject;
import java.util.Iterator;

import javax.swing.AbstractCellEditor;
import javax.swing.BoxLayout;
import javax.swing.CellEditor;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.SpinnerDateModel;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

import growoffshore.ContainsManualStop;
import growoffshore.DatabaseInfo;
import growoffshore.DateChooserForPopup;
import growoffshore.PasteModifier;
import growoffshore.TablePanelWindow;
import growoffshore.TimeChooserForPopup;
import growoffshore.cellEditorsAndRenderers.ComboBoxDataCellEditor.SpecialComboBox;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.DataType;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Option;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.ValueToNeverSave;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BadForeignConstraintValue;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.models.ComboBoxData;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.models.ComboBoxSelectedAddRow;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.models.CommandSelection;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;

public class PopUpTableFormCellEditor extends DefaultCellEditor implements EditRowPopupCommander {

	private boolean editingIsOn = false;
	
	protected TablePanelWindow window;
	private DatabaseInfo dbInfo;
	private BaseTable baseTable;
	
	protected CommandSelection addRowOptionReturnCommand = null;

	private Column column;
	
	public PopUpTableFormCellEditor(TablePanelWindow window, DatabaseInfo dbInfo, Column column, BaseTable baseTable) {
		super(new SpecialComboBox<String>());
		this.window = window;
		this.dbInfo = dbInfo;
		this.column = column;
		this.baseTable = baseTable;
	}
	
	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		return editorComponent;
	}
	
	@Override
	public boolean isCellEditable(EventObject anEvent) {
		if (column.isAutomaticValueSet()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public Object getCellEditorValue() {
		return new ComboBoxSelectedAddRow(this);
	}

	public void addRowOptionSelected(LightTableModel model, Object newValue, int rowIndex, int columnIndex) {
		
	}
}
