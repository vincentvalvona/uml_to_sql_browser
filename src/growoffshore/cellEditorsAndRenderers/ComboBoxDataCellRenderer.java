package growoffshore.cellEditorsAndRenderers;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Window;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.table.TableCellRenderer;

import growoffshore.DatabaseInfo;
import growoffshore.ExtendedBaseTableModel;
import growoffshore.ExtendedBaseTableModel.DisplayValueSet;
import growoffshore.TablePanelWindow;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Option;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLEngine;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.WhereClause;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BadForeignConstraintValue;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.Row;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.models.ComboBoxData;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;

public class ComboBoxDataCellRenderer implements TableCellRenderer {

	public final TablePanelWindow window;
	public final DatabaseInfo dbInfo;
	public final BaseTable baseTable;
	
	public final Column column;
	public final Column primaryColumn;
	public final Column displayColumn;

	private final String sql;
	
	public ComboBoxDataCellRenderer(TablePanelWindow window, DatabaseInfo dbInfo, Column column, BaseTable baseTable) {
		this.window = window;
		this.dbInfo = dbInfo;
		this.baseTable = baseTable;
		
		this.column = column;
		
		if (dbInfo.isAForeignKey(column)) {
			this.primaryColumn = dbInfo.getPrimaryKeyOfForeignKey(column);
			this.displayColumn = dbInfo.getDisplayColumnFromTable(primaryColumn.table);
		}
		else {
			this.primaryColumn = column;
			this.displayColumn = column;
		}
		
		
		sql = "SELECT * FROM " + primaryColumn.tableNameWithTicks(); 
	}
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int rowIndex, int columnIndex) {
		
		String displayValue = "";
		
		if (value == null) {
			// Do nothing
		}
		else if (primaryColumn != displayColumn) {

			ExtendedBaseTableModel exBModel = null;
			String dValue = null;
			
			
			if (baseTable.model instanceof ExtendedBaseTableModel && value instanceof Long) {
				exBModel = (ExtendedBaseTableModel) baseTable.model;
				dValue = exBModel.getDisplayValue(column, (Long) value);
				displayValue = dValue == null ? "" : dValue.toString();
			}
			
			if (dValue == null) {
				WhereClause clause = new WhereClause(primaryColumn, value);
				
				ArrayList<Row> data = SQLEngine.getMainConnection().select(null, sql, new Column[]{displayColumn}, new WhereClause[] {clause}, null, 1);
				
				if (data != null && data.size() > 0) {
					Object val = data.get(0).getRowData()[0];
					dValue = val == null ? "" : val.toString();
					
					if (dValue != null && exBModel != null) {
						DisplayValueSet dVSet = exBModel.getDisplayValueSet(column);
						if (dVSet != null) {
							dVSet.options.put((Long) value, dValue);
						}
					}	
					displayValue = dValue == null ? "" : dValue.toString();
				}
				else {
					displayValue = value == null ? "" : value.toString();
				}
			}
		}
		else {
			displayValue = value.toString();
		}
		
		ComboBoxDataCellEditor.SpecialComboBox<String> cBox = new ComboBoxDataCellEditor.SpecialComboBox<>();
		
		BaseTableColourer baseTable = (BaseTableColourer) table;
		baseTable.setColour(cBox, value, isSelected, hasFocus, rowIndex, columnIndex);
		
		if (value instanceof BadForeignConstraintValue) {
			cBox.addItem(displayValue);
			cBox.setSelectedIndex(0);
		}
		else {
			if (value == null) {
				if (column.isPrimary()) {
					cBox.addItem("UNSAVED ROW");
				}
				else {
					cBox.addItem("NOT SET");	
				}
				
				Font f = cBox.getFont();
				cBox.setFont(new Font(f.getName(), Font.ITALIC | Font.BOLD, f.getSize()));
				cBox.setForeground(new Color(192, 64, 0));
			}
			else {
				cBox.addItem(displayValue);
			}
			
			
			cBox.setSelectedIndex(0);
		}
		
		if (column.isAutomaticValueSet()) {
			JButton button = cBox.findButton(cBox);
			
			if (button != null) {
				button.setEnabled(false);
			}
		}
			
		return cBox;
	}
}
