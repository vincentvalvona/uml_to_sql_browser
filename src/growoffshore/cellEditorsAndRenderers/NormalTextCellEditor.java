package growoffshore.cellEditorsAndRenderers;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.EventObject;
import java.util.HashSet;

import javax.swing.AbstractCellEditor;
import javax.swing.DefaultCellEditor;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;

public class NormalTextCellEditor extends DefaultCellEditor {

	private final JTextField textField;
	
	private BaseTable baseTable;
	private Column column;
	
	private static HashSet<Character> validKeyTypeChars = new HashSet<>();
	
	static {
		validKeyTypeChars.add('!');
		validKeyTypeChars.add('"');
		validKeyTypeChars.add('£');
		validKeyTypeChars.add('$');
		validKeyTypeChars.add('€');
		validKeyTypeChars.add('%');
		validKeyTypeChars.add('^');
		validKeyTypeChars.add('&');
		validKeyTypeChars.add('*');
		validKeyTypeChars.add('(');
		validKeyTypeChars.add(')');
		validKeyTypeChars.add('-');
		validKeyTypeChars.add('_');
		validKeyTypeChars.add('+');
		validKeyTypeChars.add('=');
		validKeyTypeChars.add('`');
		validKeyTypeChars.add('¬');
		validKeyTypeChars.add('¦');
		validKeyTypeChars.add('|');
		validKeyTypeChars.add('\\');
		validKeyTypeChars.add('[');
		validKeyTypeChars.add(']');
		validKeyTypeChars.add('{');
		validKeyTypeChars.add('}');
		validKeyTypeChars.add(':');
		validKeyTypeChars.add(';');
		validKeyTypeChars.add('\'');
		validKeyTypeChars.add('@');
		validKeyTypeChars.add('~');
		validKeyTypeChars.add('#');
		validKeyTypeChars.add(',');
		validKeyTypeChars.add('.');
		validKeyTypeChars.add('/');
		validKeyTypeChars.add('<');
		validKeyTypeChars.add('>');
		validKeyTypeChars.add('?');
		validKeyTypeChars.add(' ');
	}
	
	private Character startWithKeyChar = null;
	
	public NormalTextCellEditor(JTextField textField, BaseTable baseTable, Column column) {
		super(textField);
		this.textField = textField;
		this.baseTable = baseTable;
		this.column = column;
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int rowIndex, int columnIndex) {
		if (startWithKeyChar == null) {
			textField.setText(value == null ? "" : value.toString());
		}
		else {
			textField.setText("");
			startWithKeyChar = null;
		}
		return textField;
	}
	
	 private Character startWithKeyEvent(KeyEvent e) {
        // Check for modifiers
        if ((e.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) != 0) {
            return null;
        }
        char keyChar = e.getKeyChar();
        
        if (validKeyTypeChars.contains(keyChar) || Character.isDigit(keyChar) || Character.isLetter(keyChar)) {
        	return keyChar;
        }
        
        return null;
    }
	
	@Override
	public boolean isCellEditable(EventObject anEvent) {
		startWithKeyChar = null;
		
		if (column.isPrimary()) {
			return false;
		}
		
		if (anEvent instanceof KeyEvent) {
			Character charTyped = startWithKeyEvent((KeyEvent) anEvent);

			if (charTyped != null) {
				System.out.println("232k A");
				boolean isEditable = baseTable.isCellEditable(anEvent, true);
				System.out.println("isEditable: " + isEditable);
				if (isEditable) {
					startWithKeyChar = charTyped;
				}
				return isEditable;
			}
		}
		
		if (super.isCellEditable(anEvent)) {
			return baseTable.isCellEditable(anEvent);
		}
		return false;
	}
}
