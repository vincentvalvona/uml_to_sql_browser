package growoffshore.cellEditorsAndRenderers;

import java.awt.Component;

import javax.swing.JLabel;

import growoffshore.DateChooserForPopup;
import growoffshore.TimeChooserForPopup;
import growoffshore.cellEditorsAndRenderers.ComboBoxDataCellEditor.SpecialComboBox;

public interface BaseTableColourer {
	void setColour(Component comp, Object value, boolean isSelected, boolean hasFocus, int rowIndex, int columnIndex);
}
