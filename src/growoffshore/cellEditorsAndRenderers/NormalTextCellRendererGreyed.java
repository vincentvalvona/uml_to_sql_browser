package growoffshore.cellEditorsAndRenderers;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;

public class NormalTextCellRendererGreyed implements TableCellRenderer {

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int rowIndex, int columnIndex) {

		BaseTableColourer baseTable = (BaseTableColourer) table;

		JLabel label = new JLabel();
		label.setOpaque(true);
		label.setBackground(Color.GRAY);
//		baseTable.setColour(label, value, isSelected, hasFocus, rowIndex, columnIndex);
		label.setText(value == null ? "" : value.toString());
		
		return label;
	}
}
