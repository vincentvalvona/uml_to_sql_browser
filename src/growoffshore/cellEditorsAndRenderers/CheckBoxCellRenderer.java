package growoffshore.cellEditorsAndRenderers;

import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;

public class CheckBoxCellRenderer implements TableCellRenderer {

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int rowIndex, int columnIndex) {

		BaseTableColourer baseTable = (BaseTableColourer) table;

		JCheckBox checkBox = new JCheckBox();
		baseTable.setColour(checkBox, value, isSelected, hasFocus, rowIndex, columnIndex);
		
		if (value instanceof Boolean) {
			checkBox.setSelected((Boolean) value);
		}
		else {
			checkBox.setSelected(false);
		}
		
		return checkBox;
	}
}
