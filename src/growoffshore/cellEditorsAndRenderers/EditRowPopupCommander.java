package growoffshore.cellEditorsAndRenderers;

import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;

public interface EditRowPopupCommander {
	public void addRowOptionSelected(LightTableModel model, Object newValue, int rowIndex, int columnIndex);
}
