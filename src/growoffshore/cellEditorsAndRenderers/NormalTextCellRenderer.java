package growoffshore.cellEditorsAndRenderers;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;

public class NormalTextCellRenderer implements TableCellRenderer {

	
	private Column column;

	public NormalTextCellRenderer(Column column) {
		this.column = column;
	}
	
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
			int rowIndex, int columnIndex) {

		BaseTableColourer baseTable = (BaseTableColourer) table;

		JLabel label = new JLabel();
		
		baseTable.setColour(label, value, isSelected, hasFocus, rowIndex, columnIndex);
		
		if (value == null) {
			if (column.isPrimary()) {
				label.setText("UNSAVED ROW");
			}
			else {
				label.setText("NOT SET");				
			}
			
			Font f = label.getFont();
			label.setFont(new Font(f.getName(), Font.ITALIC | Font.BOLD, f.getSize()));
			label.setForeground(new Color(192, 64, 0));
		}
		else {
			label.setText(value.toString());
		}
		
		return label;
	}
}
