package growoffshore;
import java.util.ArrayList;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableModel;

public class MyTableModel extends BaseTableModel {

	private ArrayList<Column> myColumns = new ArrayList<>();
	
	public MyTableModel(String sqlForSelect) {
		super(sqlForSelect);
	}

	@Override
	protected void setupModel() {
		for (int i=0; i<myColumns.size(); i++) {
			super.addColumn(myColumns.get(i));
		}
	}
	
	@Override
	public void addColumn(Column column) {
		myColumns.add(column);
	}

	@Override
	protected void setupCalibrators() {
		// TODO Auto-generated method stub
	}

	@Override
	protected void defineIndices() {
		// TODO Auto-generated method stub
	}
}
