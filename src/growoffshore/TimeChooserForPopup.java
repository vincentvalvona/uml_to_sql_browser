package growoffshore;

import java.awt.Component;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;

import com.toedter.calendar.JDateChooser;

import growoffshore.cellEditorsAndRenderers.DateAndTimeCellEditor;

public class TimeChooserForPopup extends JDateChooser {

	private Date timeValue;
	
	private JSpinner timeSpinner;
	private JSpinner.DateEditor timeEditor;
	private JButton okButton;
	
	private DateAndTimeCellEditor cellEditor;
	
	private Component invoker;

	public TimeChooserForPopup(Date date, Component invoker) {
		this(date, invoker, null);
	}
	
	public TimeChooserForPopup(Date date, Component invoker, DateAndTimeCellEditor cellEditor) {
		super(date);
		this.cellEditor = cellEditor;
		this.invoker = invoker;
		setup(date);
	}
	
	private void setup(Date date) {
		
		this.timeValue = date;
		
		setDateFormatString("HH:mm:ss");
		
		timeSpinner = new JSpinner( new SpinnerDateModel() );
		timeEditor = new JSpinner.DateEditor(timeSpinner, "HH:mm:ss");
		timeSpinner.setEditor(timeEditor);
		timeSpinner.setValue(date);
		
		okButton = new JButton("Save");
		
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				timeValue = (Date) timeSpinner.getValue();
				
				setDate(timeValue);
				
				dateSelected = true;
				popup.setVisible(false);
				
				if (cellEditor != null) {
					cellEditor.manuallyEditingStopped();
				}
				
				TimeChooserForPopup.this.actionPerformed(e);
			}
		});
		
		final ActionListener[] oldActionListeners = calendarButton.getActionListeners();
		
		
		calendarButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				timeSpinner.setValue(getDateEditor().getDate());
				
				for (int i=0; i<oldActionListeners.length; i++) {
					oldActionListeners[i].actionPerformed(e);
				}
			}
		});
		
		popup.removeAll();
		popup.add(timeSpinner);
		popup.add(okButton);
	}

	public void showPopupMenu(int x, int y) {
		popup.show(invoker, x, y);
	}

	
	public Date getValue() {
		return getDate();
	}
}
