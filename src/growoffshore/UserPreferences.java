package growoffshore;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.prefs.Preferences;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLEngine;

public class UserPreferences {
	public final static String PREF_CONNECTION_OPTIONS = "CONNECTION_OPTIONS";
	
	private static int indexCount = 0;
	public final static int HOST_INDEX = indexCount++;
	public final static int USER_INDEX = indexCount++;
	public final static int PASSWORD_INDEX = indexCount++;
	public final static int DATABASE_INDEX = indexCount++;
	
	private final static int MAX_NUMBER_OF_ITEMS_TO_SAVE = 10;
	
	public final static String NODE = UserPreferences.class.getName();
	
	private ArrayList<ConnectionOption> allConnections = new ArrayList<>();
	
	private static UserPreferences newInstance = null;
	
	public UserPreferences() {
		updateConnectionsFromDatabase();
	}
	
	public static UserPreferences getInstance() {
		if (newInstance == null) {
			newInstance = new UserPreferences();
		}
		return newInstance;
	}
	
	public void savePreferences() {
		int allConnectionsCount = allConnections.size();
		
		String[] connectionsEscaped = new String[allConnectionsCount];
		
		int connectionsCount = connectionsEscaped.length;
		
		// Limit the number of past entries to MAX_NUMBER_OF_ITEMS_TO_SAVE items max
		if (connectionsCount > MAX_NUMBER_OF_ITEMS_TO_SAVE) {
			connectionsCount = MAX_NUMBER_OF_ITEMS_TO_SAVE;
		}
		
		for (int i=0; i<connectionsCount; i++) {
			connectionsEscaped[i] = allConnections.get(i).escapeToString();
		}
		
		String allPrefs = String.join("\n", connectionsEscaped);
		
		Preferences prefs = Preferences.userRoot().node(NODE);
		prefs.put(PREF_CONNECTION_OPTIONS, allPrefs);
	}
	
	public void setCurrentConnectionDetails(String host, String user, String password, String database) {
		
		ConnectionOption newConnectionOption = new ConnectionOption(host, user, password, database);
		
		/* If any previous entry has the same host, user and database then delete it. The password
		 * doesn't need to match though.
		 */
		allConnections.remove(newConnectionOption);
		
		allConnections.add(0, newConnectionOption);
		savePreferences();
	}
	
	public void updateConnectionsFromDatabase() {
		Preferences prefs = Preferences.userRoot().node(NODE);
		String optionsJoined = prefs.get(PREF_CONNECTION_OPTIONS, null);
		
		allConnections.clear();
		
		if (optionsJoined != null) {
			String[] options = StringUtils.split(optionsJoined, "\n");
			
			for (int i=0; i<options.length; i++) {
				allConnections.add(new ConnectionOption(options[i]));
			}
		}
	}
	
	public ArrayList<ConnectionOption> getAllConnections() {
		return allConnections;
	}
}
