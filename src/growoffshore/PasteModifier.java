package growoffshore;

public interface PasteModifier {
	public Object convertFromStringValueToRealValue(String value);
	public String convertFromRealValueToStringValue(Object value);
}
