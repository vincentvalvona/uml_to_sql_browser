package growoffshore;
import java.awt.Window;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLData;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.FutureTask;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import growoffshore.dialogs.ProgressBarDialog;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.DataType;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.UniqueColumnGroup;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLEngine;

public class XMLFileReader {
	
	public final static String ID_STRING = "id";
	
	public final static String XMI_NAME_SPACE = "http://www.omg.org/spec/XMI/20131001";

	private File file;

	private HashMap<String, PrimitiveType> primitiveTypes = new HashMap<>();
	private HashMap<String, Enumeration> enums = new HashMap<>();
	private ArrayList<Association> associations = new ArrayList<>();
	
	private HashMap<String, Table> allTables = new HashMap<>();
	
	private XPath xPath;

	private UserAlerter alerter;
	
	public XMLFileReader(File file) {
		this.file = file;
	}
	
	public void setUserAlerter(UserAlerter alerter) {
		this.alerter = alerter;
	}
	
	public Collection<Table> getAllTables() {
		return allTables.values();
	}
	
	public boolean importUMLFile(ProgressBarDialog progressDialog) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException {
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setNamespaceAware(true);
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc;
		
		try {
			doc = dBuilder.parse(file);
		}
		catch(SAXParseException e) {
			progressDialog.stopMonitoring();
			JOptionPane.showMessageDialog(null, "Cannot import the selected UML file, there are errors in the syntax! \n\n" + e.getMessage());
			return false;
		}
		
		XPathFactory xPathFactory = XPathFactory.newInstance();
		xPath = xPathFactory.newXPath();
		
		xPath.setNamespaceContext(new NamespaceContext() {
			@Override
			public Iterator getPrefixes(String namespaceURI) { return null; }
			
			@Override
			public String getPrefix(String namespaceURI) { return null; }
			
			@Override
			public String getNamespaceURI(String prefix) {
				if ("xmi".equals(prefix)) return XMI_NAME_SPACE;
				return XMLConstants.NULL_NS_URI;
			}
		});
		
		
		while (true) {
			// First, foreign-keys must be removed and then old tables cleared out.
			
			progressDialog.setTitle("1/4 - Dropping all foreign keys");
			if (!SQLEngine.getMainConnection().dropAllForiegnKeys(progressDialog)) {
				break;
			}
			
			if (progressDialog.pendingCancelled()) break;
	
			progressDialog.setTitle("2/4 - Dropping all tables");
			if (!SQLEngine.getMainConnection().dropAllTables(progressDialog)) {
				break;
			};
			
			if (progressDialog.pendingCancelled()) break;
			
			progressDialog.setTitle("3/4 - Adding information to database");
			// Getting all primitive type codes
			NodeList primitiveTypeNodes = (NodeList) xPath.evaluate("//packagedElement[@xmi:type='uml:PrimitiveType'][@xmi:id][@name]", doc, XPathConstants.NODESET);
			
			// Getting all enum type codes
			NodeList enumNodes = (NodeList) xPath.evaluate("//packagedElement[@xmi:type='uml:Enumeration'][@xmi:id][@name]", doc, XPathConstants.NODESET);
			
			// Getting all classes and their variables
			NodeList packagedElementNodes = (NodeList) xPath.evaluate("//packagedElement[@xmi:type='uml:Class'][@name]", doc, XPathConstants.NODESET);
			
			int numberOfStepsToTake = primitiveTypeNodes.getLength() + enumNodes.getLength() + packagedElementNodes.getLength();
	
			progressDialog.setNewMaxValue(numberOfStepsToTake);
			
			if (progressDialog.pendingCancelled()) break;
			
			populatePrimitiveTypes(primitiveTypeNodes, progressDialog);
			if (progressDialog.pendingCancelled()) break;
			populateEnums(enumNodes, progressDialog);
			if (progressDialog.pendingCancelled()) break;
			populateClassesAndFields(xPath, packagedElementNodes, progressDialog);
			if (progressDialog.pendingCancelled()) break;
			
			progressDialog.setTitle("4/4 - Adding associations to database");
			populateAssociations(progressDialog);
			
			break;
		}

		if (progressDialog.pendingCancelled()) {
			SQLEngine.getMainConnection().rollback();
			return false;
		}
		else {
			SQLEngine.getMainConnection().commit();
			return true;
		}
	}

	private void populateClassesAndFields(XPath xPath, NodeList packagedElementNodes, ProgressBarDialog progressDialog)
			throws XPathExpressionException {
		
		associations.clear();
		
		// Each iterations here represents a new table
		for (int tableIndex=0; tableIndex<packagedElementNodes.getLength(); tableIndex++) {
			if (progressDialog.pendingCancelled()) {
				return;
			}
			
			Node packagedElementItem = packagedElementNodes.item(tableIndex);
			NamedNodeMap attributes = packagedElementItem.getAttributes();

			// Gets attribute-name
			String nameTag = attributes.getNamedItem("name").getNodeValue();
			String xmiID = attributes.getNamedItemNS(XMI_NAME_SPACE, "id").getNodeValue();
			
			Table classTable = Table.addTable(nameTag, "id");
			allTables.put(xmiID, classTable);			

			if (progressDialog != null) {
				progressDialog.setMessage("Adding table \"" + classTable.tableName + "\"");
			}
			
			NodeList ownedAttributeNodes = (NodeList) xPath.evaluate("./ownedAttribute[@xmi:type='uml:Property'][@xmi:id][@name][@type]", packagedElementItem, XPathConstants.NODESET);
			
			int ownedAttributeNodesLength = ownedAttributeNodes.getLength();
			
			ArrayList<Column> columnsToAddToThisTable = new ArrayList<>(ownedAttributeNodesLength);
			
			/* Searching for xmi:type="uml:Property" elements. These are the
			 * variables for each class
			 */
			// Each iterations here represents a new column
			for (int attIndex=0; attIndex<ownedAttributeNodesLength; attIndex++) {
				Node nextOwnedAttribute = ownedAttributeNodes.item(attIndex);
				NamedNodeMap ownedAttributeAttr = nextOwnedAttribute.getAttributes();
				String type = ownedAttributeAttr.getNamedItem("type").getNodeValue();
				String name = ownedAttributeAttr.getNamedItem("name").getNodeValue();
				Node visibilityAttr = ownedAttributeAttr.getNamedItem("visibility");
				
				NodeList defaultValueNodes = (NodeList) xPath.evaluate("./defaultValue", nextOwnedAttribute, XPathConstants.NODESET);
				
				Column column = new Column(classTable, name);
				
				if (progressDialog != null) {
					progressDialog.setMessage("Adding column \"" + column.columnName + "\" to \"" + classTable.tableName + "\"");
				}
				
				Node associationNamedItem = ownedAttributeAttr.getNamedItem("association");
				
				column.setUnique(false); column.setNullAllowed(true);
				if (visibilityAttr != null) {
					String visibilityAttrValue = visibilityAttr.getNodeValue();
					
					if (visibilityAttrValue.equals("private")) { column.setUnique(true); column.setNullAllowed(false); }
					else if (visibilityAttrValue.equals("package")) { column.setUnique(true); column.setNullAllowed(true); }
					else if (visibilityAttrValue.equals("protected")) { column.setUnique(false); column.setNullAllowed(false); }
				}
				
				// Id columns are automatically skipped
				if (!isId(column)) {
					
					boolean defaultValueExists = false;
					String defaultValue = null;
					
					if (primitiveTypes.containsKey(type)) {
						PrimitiveType pType = primitiveTypes.get(type);
						column.setTypeString(pType.name);

						if (defaultValueNodes.getLength() > 0) {
							Node defaultValueNode = defaultValueNodes.item(0);
							NamedNodeMap defaultValueNodeAttr = defaultValueNode.getAttributes();
							defaultValueExists = true;
							column.setHasDefaultValue(true);
							
							if (defaultValueNodeAttr != null) {
								Node defaultValueNodeValueAttr = defaultValueNodeAttr.getNamedItem("value");

								if (defaultValueNodeValueAttr == null) {
									column.setDefaultValue(column.getAbsoluteDefault());
								}
								else {
									defaultValue = defaultValueNodeValueAttr.getNodeValue();
									column.setDefaultValue(column.convertToCorrectDataType(defaultValue));
								}
							}
						}
						else {
							column.setHasDefaultValue(false);
						}
					}
					else if (associationNamedItem != null) {
						column.setTypeString("BIGINT(20)");
						column.setType(java.sql.Types.BIGINT);
						column.setDefaultValue(null);
						String associationNamedItemStr = associationNamedItem.getNodeValue();

						Association association = new Association(name, type, associationNamedItemStr, column);
						associations.add(association);
					}
					else {
						continue;
					}

					columnsToAddToThisTable.add(column);
				}
			}

			// Column groups defined here 
			ArrayList<UniqueColumnGroup> uniqueColumnGroups = new ArrayList<>();
			
			Column previousColumn = null;
			UniqueColumnGroup currentUniqueColumnGroup = null;
			
			int columnsToAddToThisTableSize = columnsToAddToThisTable.size();
			for (int columnIndex=0; columnIndex<columnsToAddToThisTableSize; columnIndex++) {
				Column nextColumn = columnsToAddToThisTable.get(columnIndex);
				
				// Only unique columns are allowed in the currentUniqueColumnGroup
				if (nextColumn.isUnique()) {

					/* If the previous column and this column share the same null
					 * status then continue using the same currentUniqueColumnGroup.
					 */
					if (previousColumn != null && (previousColumn.isNullAllowed() == nextColumn.isNullAllowed())) {
						// Keep using same currentUniqueColumnGroup
					}
					// Otherwise, start a new currentUniqueColumnGroup.
					else {
						currentUniqueColumnGroup = new UniqueColumnGroup();
						uniqueColumnGroups.add(currentUniqueColumnGroup);
					}
					
					currentUniqueColumnGroup.addColumn(nextColumn);
					
					previousColumn = nextColumn;
				}
				else {
					previousColumn = null;
					currentUniqueColumnGroup = null;
				}
			}
			
			Column[] columnsToAddToThisTableArray = columnsToAddToThisTable.toArray(new Column[columnsToAddToThisTableSize]);
			SQLEngine.getMainConnection().createTable(classTable, false, columnsToAddToThisTableArray, uniqueColumnGroups);
			
			if (progressDialog != null) {
				progressDialog.increment();
			}
		}
	}

	private void populatePrimitiveTypes(NodeList primitiveTypeNodes, ProgressBarDialog progressDialog) {
		
		primitiveTypes.clear();
		
		int primitiveTypeNodesCount = primitiveTypeNodes.getLength();
		
		for (int i=0; i<primitiveTypeNodesCount; i++) {

			if (progressDialog.pendingCancelled()) {
				return;
			}
			
			Node primitiveTypeItem = primitiveTypeNodes.item(i);
			NamedNodeMap primitiveTypeItemAttrs = primitiveTypeItem.getAttributes();
			
			String xmiID = primitiveTypeItemAttrs.getNamedItemNS(XMI_NAME_SPACE, "id").getNodeValue();
			String name = primitiveTypeItemAttrs.getNamedItem("name").getNodeValue();

			if (progressDialog != null) {
				progressDialog.setMessage("Adding primitive type \"" + name + "\"");
			}
			
			PrimitiveType pType = new PrimitiveType(name);
			primitiveTypes.put(xmiID, pType);

			if (progressDialog != null) {
				progressDialog.increment();
			}
		}
	}
	
	private void populateEnums(NodeList enumNodes, ProgressBarDialog progressDialog) throws XPathExpressionException {
		enums.clear();
		
		Table enumsTable = Table.addTable(EnumsTableModel.ID_COLUMN.table.tableName, EnumsTableModel.ID_COLUMN.columnName);
		SQLEngine.getMainConnection().createTable(enumsTable, false);
		SQLEngine.getMainConnection().createColumn(EnumsTableModel.TABLE_DOT_COLUMN_COLUMN, false);
		SQLEngine.getMainConnection().createColumn(EnumsTableModel.VALUE_COLUMN, false);

		int enumNodesLength = enumNodes.getLength();
		
		for (int i=0; i<enumNodesLength; i++) {
			
			if (progressDialog.pendingCancelled()) {
				return;
			}
			
			Node enumItem = enumNodes.item(i);
			NamedNodeMap enumItemAttrs = enumItem.getAttributes();

			String xmiID = enumItemAttrs.getNamedItemNS(XMI_NAME_SPACE, "id").getNodeValue();
			String name = enumItemAttrs.getNamedItem("name").getNodeValue();
			
			Enumeration enumeration = new Enumeration(name);
			
			NodeList ownedAttributeNodes = (NodeList) xPath.evaluate("./ownedLiteral[@xmi:type='uml:EnumerationLiteral'][@name]", enumItem, XPathConstants.NODESET);
			
			for (int attrIndex=0; attrIndex<ownedAttributeNodes.getLength(); attrIndex++) {
				Node ownedAttributeItem = ownedAttributeNodes.item(attrIndex);
				NamedNodeMap ownedAttributeAttrs = ownedAttributeItem.getAttributes();
				Node ownedAttributeAttrsName = ownedAttributeAttrs.getNamedItem("name");
				enumeration.addStringLiteral(ownedAttributeAttrsName.getNodeValue());
			}

			enums.put(xmiID, enumeration);

			if (progressDialog != null) {
				progressDialog.increment();
			}
		}
	}

	private void populateAssociations(ProgressBarDialog progressDialog) {
		
		int associationCount = associations.size();

		progressDialog.setNewMaxValue(associationCount);

		for (int i=0; i<associationCount; i++) {
			if (progressDialog.pendingCancelled()) {
				return;
			}
			
			Association nextAssociation = associations.get(i);

			Table primaryKeyTable = allTables.get(nextAssociation.type);
			
			
			if (primaryKeyTable != null) {
				if (progressDialog != null) {
					progressDialog.setMessage("Adding associations for table \"" + primaryKeyTable.tableName + "\"");
				}
				
				SQLEngine.getMainConnection().setColumnDataTypeToMatchPrimaryKey(nextAssociation.column);
				SQLEngine.getMainConnection().setColumnAsForeignKeyToTable(nextAssociation.column, primaryKeyTable);
			}
			
			if (progressDialog != null) {
				progressDialog.increment();
			}
		}
	}
	
	private static boolean isId(Column column) {
		String name = column.columnName;
		return name.toLowerCase().equals(ID_STRING);
	}
	
	private static String referencesTable(Column column) {

		String name = column.columnName;
		
		int numberOfChars = name.length();
		
		if (numberOfChars > ID_STRING.length()) {
			String firstPart = name.substring(0, numberOfChars - ID_STRING.length());
			String secondPart = name.substring(numberOfChars - ID_STRING.length(), numberOfChars);

			if (secondPart.toLowerCase().equals(ID_STRING)) {
				char firstPartLastChar = firstPart.charAt(firstPart.length() - 1);
				char secondPartFirstChar = secondPart.charAt(0);
				
				/* A table is defined to be a reference to a table if the case of the first letter in "id" is
				 * different to the previous character.
				 */
				if ((Character.isLowerCase(secondPartFirstChar) && !Character.isLowerCase(firstPartLastChar))
						|| Character.isUpperCase(secondPartFirstChar) && !Character.isUpperCase(firstPartLastChar)) {
					return firstPart;
				}
			}
		}
		
		return null;
	}

}
