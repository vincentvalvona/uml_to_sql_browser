package growoffshore;

import javax.swing.tree.DefaultMutableTreeNode;

import growoffshore.mainwindow.MainWindow;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;

public class TableTreeNode extends DefaultMutableTreeNode {
	
	private Table table;

	public TableTreeNode(Table table) {
		super(table.tableName);
		this.table = table;
	}
	
	{
		setAllowsChildren(false);
	}
	
	public void selectTable(MainWindow mainWindow) {
		mainWindow.showPanelForTable(table);
	}
}
