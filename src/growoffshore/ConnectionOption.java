package growoffshore;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

public class ConnectionOption implements Serializable {
	private String host;
	private String user;
	private String password;
	private String database;
	
	
	public ConnectionOption(String host, String user, String password, String database) {
		setHost(host); setUser(user); setPassword(password); setDatabase(database);
	}
	
	public ConnectionOption(String concat) {
		String host = ""; String user = ""; String password = ""; String database = "";
		
		if (concat != null) {
			String[] concatSplit = StringUtils.split(concat, "\t");
			
			if (concatSplit.length == 4) {
				host = concatSplit[0];
				user = concatSplit[1];
				password = concatSplit[2];
				database = concatSplit[3];
			}
		}
		
		setHost(host); setUser(user); setPassword(password); setDatabase(database);
	}
	
	
	public String escapeToString() {
		return String.join("\t", new String[] { host, user, password, database });
	}
	
	public static String filter(String str) {
		return str == null ? "" : str.replace("\t", "").replace("\n", "");
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		String[] strings = new String[] { host, user, database };
		
		for ( String s : strings ) {
		    result *= prime + s.hashCode();
		}
	
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ConnectionOption) {
			ConnectionOption option = (ConnectionOption) obj;
			return this.host.equals(option.host)
				&& this.user.equals(option.user)
				&& this.database.equals(option.database);
		}

		return false;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("host: "); str.append(host.toString()); str.append('\n');
		str.append("user: "); str.append(user.toString()); str.append('\n');
		str.append("password: "); str.append(password.toString()); str.append('\n');
		str.append("database: "); str.append(database.toString());
		
		return str.toString();
	}
	
	public void setHost(String host) { this.host = filter(host); }
	public void setUser(String user) { this.user = filter(user); }
	public void setPassword(String password) { this.password = filter(password); }
	public void setDatabase(String database) { this.database = filter(database); }
	
	public String getHost() { return host; }
	public String getUser() { return user; }
	public String getPassword() { return password; }
	public String getDatabase() { return database; }
}