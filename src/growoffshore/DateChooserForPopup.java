package growoffshore;

import java.awt.Window;
import java.util.Date;

import com.toedter.calendar.JDateChooser;

public class DateChooserForPopup extends JDateChooser {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Window window;

	public DateChooserForPopup(Date date, Window window) {
		super(date);
		this.window = window;
	}

	public void showPopupMenu(int x, int y) {
		popup.show(window, x, y);
	}
}
