package growoffshore;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.DataType;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.UniqueColumnGroup;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLEngine;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.Row;

public class DatabaseInfo {
	
	private ArrayList<Table> tables = new ArrayList<>();
	private HashMap<Column, Column> fKeyToPColumn = new HashMap<>();
	private HashMap<Table, Column> tableNameToPColumn = new HashMap<>();
	
	private HashMap<UniqueColumnGroup, Table> uniqueColumnGroupToTable = new HashMap<>();
	
	private Table globalFilteredTable = null;
	private Long globalFilteredValue = null;
	private boolean nullsVisibleOnFilter;
	
	public DatabaseInfo() {
		setup();
	}
	
	private void setup() {
		populateTableToAllColumnsMap();
		populateForeignKeysMap();
		populateTableNames();
		populateUniqueColumnsMap();
	}
	
	private void populateUniqueColumnsMap() {
		uniqueColumnGroupToTable = SQLEngine.getInfoSchemaConnection().getAllUniqueColumnGroups(
				SQLEngine.getMainConnection().getDatabaseName()
		);	
	}

	public boolean isAForeignKey(Column column) {
		return fKeyToPColumn.containsKey(column);
	}
	
	public Column getPrimaryKeyOfForeignKey(Column column) {
		return fKeyToPColumn.get(column);
	}

	private void populateTableToAllColumnsMap() {
		
	}
	
	private void populateForeignKeysMap() {
		
		Table keyColumnUsageTable = new Table("KEY_COLUMN_USAGE");

		Column refInfoTable = new Column(keyColumnUsageTable, SQLEngine.META_COLUMNS_REFERENCED_TABLE_NAME);
		Column refInfoColumn = new Column(keyColumnUsageTable, SQLEngine.META_COLUMNS_REFERENCED_COLUMN_NAME);
		Column thisInfoTable = new Column(keyColumnUsageTable, SQLEngine.META_COLUMNS_TABLE_NAME);
		Column thisInfoColumn = new Column(keyColumnUsageTable, SQLEngine.META_COLUMNS_COLUMN_NAME);
		Column constraintColumn = new Column(keyColumnUsageTable, SQLEngine.META_COLUMNS_CONSTRAINT_NAME);
		
		int numberOfInfoColumns = 0;
		int refInfoTableIndex = numberOfInfoColumns++;
		int refInfoColumnIndex = numberOfInfoColumns++;
		int thisInfoTableIndex = numberOfInfoColumns++;
		int thisInfoColumnIndex = numberOfInfoColumns++;
		int constraintColumnIndex = numberOfInfoColumns++;
		
		Column[] infoColumns = new Column[numberOfInfoColumns];
		infoColumns[refInfoTableIndex] = refInfoTable;
		infoColumns[refInfoColumnIndex] = refInfoColumn;
		infoColumns[thisInfoTableIndex] = thisInfoTable;
		infoColumns[thisInfoColumnIndex] = thisInfoColumn;
		infoColumns[constraintColumnIndex] = constraintColumn;
		
		StringBuilder sql = new StringBuilder("SELECT * FROM ");
		sql.append("KEY_COLUMN_USAGE");
		sql.append(" WHERE `TABLE_SCHEMA`=\"");
		sql.append(SQLEngine.getMainConnection().getDatabaseName());
		sql.append("\"  AND ((`REFERENCED_TABLE_NAME` IS NOT NULL AND `REFERENCED_COLUMN_NAME` IS NOT NULL) OR `CONSTRAINT_NAME`=\"");
		sql.append(DataType.CONSTRAINT_PRIMARY);
		sql.append("\") ORDER BY `CONSTRAINT_NAME`!=\"PRIMARY\"");
		System.out.println(sql);
		
		ArrayList<Row> keyInfo = SQLEngine.getInfoSchemaConnection().select(null, sql.toString(), infoColumns, null, null, null);
		
		fKeyToPColumn.clear();
		
		if (keyInfo != null && !keyInfo.isEmpty()) {

			Iterator<Row> keyInfoIt = keyInfo.iterator();
			
			while (keyInfoIt.hasNext()) {
				Object[] nextInfo = keyInfoIt.next().getRowData();

				String constraintName = (String) nextInfo[constraintColumnIndex];
				
				// This is a primary key
				if (constraintName != null && constraintName.equals(DataType.CONSTRAINT_PRIMARY)) {
					Table table = new Table((String) nextInfo[thisInfoTableIndex], (String) nextInfo[thisInfoColumnIndex]);
					tableNameToPColumn.put(table, new Column(table, table.idColumnName));
				}
				// This is a foreign key
				else {
					Column fKeyColumn = new Column((String) nextInfo[thisInfoTableIndex], (String) nextInfo[thisInfoColumnIndex]);
					Column pColumn = new Column((String) nextInfo[refInfoTableIndex], (String) nextInfo[refInfoColumnIndex]);
					fKeyToPColumn.put(fKeyColumn, pColumn);
				}
				
			}
		}
		


		
	}

	private void populateTableNames() {
		ArrayList<String> tableNames = SQLEngine.getMainConnection().getTableNames("");
		
		Iterator<String> tableNamesIt = tableNames.iterator();
		
		while (tableNamesIt.hasNext()) {
			String nextTableName = tableNamesIt.next();
			Table nextTable = new Table(nextTableName);
			tables.add(nextTable);
		}
	}
	
	public Table[] getTables() {
		return tables.toArray(new Table[tables.size()]);
	}
	
	public ArrayList<Row> getValuesOfTable(Table table) {
		
		Column idColumn = new Column(table, table.idColumnName);
		
		Column[] columns = new Column[] {idColumn};
		
		String sql = "SELECT * FROM " + table.tableNameWithTicks();
		
		return SQLEngine.getMainConnection().select(null, sql, columns, null, null, null);
	}
	

	public String getJoinSQL(Table table) {

		if (globalFilteredTable == null || globalFilteredValue == null) {
			return "";
		}
		
		TableLink initialLink = new TableLink(null, null, null);
		
		findForeignKeyColumns(fKeyToPColumn.entrySet(), initialLink, table);
		
		ArrayList<TableLink> allLinks = initialLink.getAllValidLinks();
		
		int allLinksSize = allLinks.size();
		
		// Creating joiner
		Joiner joiner = new Joiner(table, globalFilteredTable);
		
		// Adding all tables first
		for (int i=0; i<allLinksSize; i++) {
			final TableLink nextLink = allLinks.get(i);
			
			if (nextLink != null) {
				joiner.addTable(nextLink.getFKeyTable());
				joiner.addTable(nextLink.getPKeyTable());
			}
		}
		
		// Then all column pairs next
		for (int i=0; i<allLinksSize; i++) {
			final TableLink nextLink = allLinks.get(i);

			if (nextLink != null) {
				joiner.addColumnPair(new Entry<Column, Column>() {
					@Override
					public Column setValue(Column value) { return nextLink.pKeyColumn; }
					@Override
					public Column getValue() { return nextLink.pKeyColumn; }
					@Override
					public Column getKey() { return nextLink.fKeyColumn; }
				});
			}
		}
		
		String joinSQL = joiner.createJoinStatement(nullsVisibleOnFilter);
		
		// Then adding all sets
		if (!joinSQL.equals("")) {
			StringBuilder whereStatement = new StringBuilder();
			whereStatement.append(" WHERE ");
			whereStatement.append(globalFilteredTable.primaryKeyTableDotColumnWithTicks());
			whereStatement.append("=");
			whereStatement.append(globalFilteredValue);
//			whereStatement.append(" GROUP BY ");
//			whereStatement.append(table.primaryKeyTableDotColumnWithTicks());
			
			joinSQL += whereStatement;
		}
		
		System.out.println("joinSQL: " + joinSQL);
		return joinSQL;
	}
	
	@SuppressWarnings("unchecked")
	private void findForeignKeyColumns(Set<Entry<Column, Column>> allFKeySets, TableLink chain, final Table table) {
		
		/* If table is global filter table then return true which means the chain
		 * from the original table is valid.
		 */
		if (table.tableName.equals(globalFilteredTable.tableName)) {
			// Do nothing
		}
		else {
//			Set<Entry<Column, Column>> entrySet = fKeyToPColumn.entrySet();
			Iterator<Entry<Column, Column>> allFKeySetsIt = allFKeySets.iterator();
			
			while (allFKeySetsIt.hasNext()) {
				Entry<Column, Column> nextEntrySet = allFKeySetsIt.next();
	
				Column nextFColumn = nextEntrySet.getKey(); String nextFTableName = nextFColumn.table.tableName;
				Column nextPColumn = nextEntrySet.getValue(); String nextPTableName = nextPColumn.table.tableName;

				
				// Current foreign key is in this table and therefore maybe is part of the chain
				if (nextFColumn.table.tableName.equals(table.tableName)) {
					System.out.println("nextFColumn: " + nextFColumn + "    nextPColumn: " + nextPColumn + "    table.tableName: " + table.tableName);
					
					if (nextPColumn.table.equals(globalFilteredTable)) {
						System.out.println("A1");
						TableLink newLink = new TableLink(chain, nextFColumn, nextPColumn);
						newLink.formAsEndOfValidChain();
					}
					else if (chain.tableAlreadyInChain(table)) {
						System.out.println("A2");
						// Ignore tables which already exist in the chain
					}
					else if (nextFTableName.equals(nextPTableName)) {
						System.out.println("A3");
						// Any link with both columns with the same table are ignored
					}
					else {
						System.out.println("A4");
						TableLink newLink = new TableLink(chain, nextFColumn, nextPColumn);
						findForeignKeyColumns(allFKeySets, newLink, nextPColumn.table);
					}
				}
			}
		}
	}
	
	public void setGlobalFilteredTable(Table globalFilteredTable) {
		this.globalFilteredTable = globalFilteredTable;
	}
	
	public void setGlobalFilteredValue(Long globalFilteredValue) {
		this.globalFilteredValue = globalFilteredValue;
	}
	
	public boolean columnIsPrimaryOfFilteredTable(Column column) {
		if (globalFilteredTable != null && globalFilteredValue != null) {
			Column pColumn = getPrimaryKeyOfForeignKey(column);
			
			if (pColumn != null && pColumn.table.equals(globalFilteredTable)) {
				return true;
			}
		}
		
		return false;
	}
	

	public boolean hasGlobalFilteredTable() {
		return globalFilteredTable != null;
	}
	

	public boolean hasGlobalFilteredValue() {
		return globalFilteredValue != null;
	}
	
	public Table getGlobalFilteredTable() {
		return globalFilteredTable;
	}
	
	public Long getGlobalFilteredValue() {
		return globalFilteredValue;
	}

	public void setNullsVisibleOnFilter(boolean nullsShouldBeVisible) {
		this.nullsVisibleOnFilter = nullsShouldBeVisible;
	}
	
//	private Column[] columnChainToPrimary() {
//		
//	}
//	
//	public String getWhereClause(Column column) {
//		
//	}
	
	private class TableNameWithLevel {
		public final String tableName;
		public final int level;

		public TableNameWithLevel(String tableName, int level) {
			this.tableName = tableName;
			this.level = level;
		}
		
		@Override
		public int hashCode() {
		// TODO Auto-generated method stub
			return tableName.hashCode();
		}
		
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof TableNameWithLevel) {
				TableNameWithLevel objCast = (TableNameWithLevel) obj;
				return tableName.equals(objCast.tableName);
			}
			return false;
		}
	}

	public UniqueColumnGroup[] getAllUniqueGroupsInTable(Table table) {
		return getAllUniqueGroupsInTable(table, false);
	}
	
	public UniqueColumnGroup[] getAllUniqueGroupsInTable(Table table, boolean excludePrimaryKey) {
		Set<Entry<UniqueColumnGroup, Table>> allGroups = uniqueColumnGroupToTable.entrySet();
		
		ArrayList<UniqueColumnGroup> allGroupsInTable = new ArrayList<>(allGroups.size());
		
		Iterator<Entry<UniqueColumnGroup, Table>> allGroupsIt = allGroups.iterator();
		
		while(allGroupsIt.hasNext()) {
			Entry<UniqueColumnGroup, Table> nextEntry = allGroupsIt.next();
			
			if (excludePrimaryKey && nextEntry.getKey().containsSinglePrimaryKey()) {
				continue;
			}
			
			UniqueColumnGroup nextUniqueGroup = nextEntry.getKey();
			Table nextTable = nextEntry.getValue();
			
			if (table.equals(nextTable)) {
				allGroupsInTable.add(nextUniqueGroup);
			}
		}
		
		return allGroupsInTable.toArray(new UniqueColumnGroup[allGroupsInTable.size()]);
	}
	
	public Column[] getAllNotNullColumnsTable(Table table, boolean excludePrimaryKey) {
		
		Column[] allColumns = getAllColumnsFromTable(table, true);
		
		ArrayList<Column> allColumnsAList = new ArrayList<>(allColumns.length);
		
		for (int i=0; i<allColumns.length; i++) {
			Column nextColumn = allColumns[i];
			
			if (nextColumn.isPrimary()) {
				continue;
			}
			
			if (!nextColumn.isNullAllowed()) {
				allColumnsAList.add(nextColumn);
			}
		}
		
		return allColumnsAList.toArray(new Column[allColumnsAList.size()]);
	}
	
	public String constructConstraintsMessage(Table table) {
		UniqueColumnGroup[] allUniqueGroups = getAllUniqueGroupsInTable(table, true);
		
		StringBuilder message = new StringBuilder("Cannot save row. One or more of the following contraints may be violated.");
		
		for (int uniqueGroupIndex=0; uniqueGroupIndex<allUniqueGroups.length; uniqueGroupIndex++) {
			message.append('\n');
			
			UniqueColumnGroup nextUniqueGroup = allUniqueGroups[uniqueGroupIndex];
			int columnCount = nextUniqueGroup.getColumnCount();
			
			for (int columnIndex=0; columnIndex<columnCount; columnIndex++) {
				Column nextColumn = nextUniqueGroup.getColumnAt(columnIndex);
				
				if (columnIndex != 0) {
					message.append(", ");
				}
				
				message.append(nextColumn.columnNameWithTicks());
			}
			
			if (columnCount == 1) {
				message.append(" must be unique for each row");
			}
			else {
				message.append(" must form a unique combination for each row");
			}
		}
		
		// Construct not null columns list
		message.append("\n\n");
		Column[] allNotNullColumns = getAllNotNullColumnsTable(table, true);
		
		if (allNotNullColumns.length > 0) {
			for (int i=0; i<allNotNullColumns.length; i++) {
				Column nextColumn = allNotNullColumns[i];
	
				if (i != 0) {
					message.append(", ");
				}
				
				message.append(nextColumn.columnNameWithTicks());
			}
			
			if (allNotNullColumns.length > 1) {
				message.append(" must be non-null values");
			}
			else {
				message.append(" must be a non-null value");
			}
		}
		
		return message.toString().trim();
	}
	
	private static class TableLink {
		
		private boolean partOfValidChain = false;
		private boolean partOfInvalidChain = false;
		
		private final TableLink parentLink;
		private final ArrayList<TableLink> childLinks;

		public final Column fKeyColumn;
		public final Column pKeyColumn;
		
		public TableLink(TableLink parentLink, Column fKeyColumn, Column pKeyColumn) {
			// The parent (if it exists) of this link has this link as a child. 
			this.parentLink = parentLink;
			if (parentLink != null) {
				parentLink.childLinks.add(this);
			}
			
			this.fKeyColumn = fKeyColumn;
			this.pKeyColumn = pKeyColumn;
			
			this.childLinks = new ArrayList<>();
		}

		public ArrayList<TableLink> getAllValidLinks() {
			return getAllValidLinks(null);
		}
		
		public ArrayList<TableLink> getAllValidLinks(ArrayList<TableLink> allLinks) {
			if (allLinks == null) {
				allLinks = new ArrayList<>();
			}

			int childLinksSize = childLinks.size();
			
			// First adding all child links
			for (int i=0; i<childLinksSize; i++) {
				TableLink nextChild = childLinks.get(i);
				if (nextChild.partOfValidChain) {
					allLinks.add(nextChild);
//				}
//			}
//			
//			// Then doing the same for all children
//			for (int i=0; i<childLinksSize; i++) {
//				TableLink nextChild = childLinks.get(i);
//				if (nextChild.partOfValidChain) {
					nextChild.getAllValidLinks(allLinks);
				}
			}
			
			return allLinks;
		}

		public Table getFKeyTable() {
			return fKeyColumn.table;
		}
		
		public Table getPKeyTable() {
			return pKeyColumn.table;
		}
		
		public boolean isEndOfChain() {
			return partOfValidChain || partOfInvalidChain;
		}
		
		public void formAsEndOfValidChain() {
			partOfValidChain = true;
			
			if (parentLink != null) {
				parentLink.formAsEndOfValidChain();
			}
		}
		
		public void formAsEndOfInvalidChain() {
			partOfInvalidChain = true;
		}
		
		public boolean tableAlreadyInChain(Table table) {
			
			if (fKeyColumn == null) {
				return false;
			}
			else if (parentLink != null) {
				return fKeyColumn.table.equals(table)
						|| parentLink.tableAlreadyInChain(table);
			}
			else {
				return fKeyColumn.table.equals(table);
			}
		}
	}
	

	public Column[] getAllColumnsFromTable(Table table, boolean includeKeyColumns) {
		Column[] allColumns = SQLEngine.getMainConnection().getAllColumnsInTable(table);
		
		if (!includeKeyColumns) {
			ArrayList<Column> allColumnsAListFiltered = new ArrayList<>(allColumns.length);
			
			for (int i=0; i<allColumns.length; i++) {
				Column nextColumn = allColumns[i];
				if (!isAForeignKey(nextColumn) && !nextColumn.isPrimary()) {
					allColumnsAListFiltered.add(nextColumn);
				}
			}
			
			return allColumnsAListFiltered.toArray(new Column[allColumnsAListFiltered.size()]);
		}
		
		return allColumns;
	}
	/**
	 * 
	 * @param table
	 * @return display
	 * 
	 * return the first column after the id column (primary key) which must be in column 0
	 * as long as it is not a foreign key itself and does not accept null 
	 */
	public Column getDisplayColumnFromTable(Table table) {
		Column[] allColumns = SQLEngine.getMainConnection().getAllColumnsInTable(table);
		Column display= null;
		if(allColumns.length > 0 ){	
			display=allColumns[0];
			if(allColumns.length >1 ){			
				if(!isAForeignKey(allColumns[1])){
					if(! allColumns[1].isNullAllowed()){
						display=allColumns[1];
					}				
				}
			}
		}
			
		
		return display;
	}

	public static class Joiner {

		public final Table mainTable;
		public final Table filterTable;
		public final ArrayList<Table> tables;
		public final HashMap<Table, ArrayList<Entry<Column, Column>>> tableToPairs;
		
		public Joiner(Table mainTable, Table filterTable) {
			this.mainTable = mainTable;
			this.filterTable = filterTable;
			tables = new ArrayList<>();
			tableToPairs = new HashMap<>();
		}
		
		
		public void addTable(Table table) {
			// The same table isn't allowed to be added twice
			if (!tables.contains(table)) {
				System.out.println("table.tableName: " + table.tableName);
				tables.add(table);
				tableToPairs.put(table, new ArrayList<>());
			}
		}
		
		private Table getJoinTable(Entry<Column, Column> pair) {
			int tablesSize = tables.size();
			
			Column fKeyColumn = pair.getKey();
			Column pKeyColumn = pair.getValue();
			
			Table firstTable = null;
			
			for (int i=0; i<tablesSize; i++) {
				Table nextTable = tables.get(i);
				
				if (fKeyColumn.table.equals(nextTable) || pKeyColumn.table.equals(nextTable)) {
					
					if (firstTable == null) {
						firstTable = nextTable;
					}
					else {
						return nextTable;
					}
				}
			}
			
			return null;
		}
		
		public void addColumnPair(Entry<Column, Column> pair) {
			System.out.println();
			System.out.println("getJoinTable(pair): " + getJoinTable(pair));
			System.out.println("pair.getKey(): " + pair.getKey());
			System.out.println("pair.getValue(): " + pair.getValue());
			System.out.println("tableToPairs == null: " + (tableToPairs == null));
			System.out.println("tableToPairs.get(getJoinTable(pair)) == null: " + (tableToPairs.get(getJoinTable(pair)) == null));
			tableToPairs.get(getJoinTable(pair)).add(pair);
		}
		
		public String createJoinStatement(boolean nullsVisibleOnFilter) {
			
//			String joinWord = nullsVisibleOnFilter ? " LEFT JOIN " : " JOIN ";

			String joinWord = " LEFT JOIN ";
			
			HashSet<String> joinsUsedAlready = new HashSet<>(); 
			
			int tablesSize = tables.size();
			
			if (tablesSize <= 1) return "";
			
			StringBuilder joinSQL = new StringBuilder();
			
			for (int i=1; i<tablesSize; i++) {
				Table nextTable = tables.get(i);
				
				boolean tableJoinPerformedYet = false;
				
				ArrayList<Entry<Column, Column>> allPairs = tableToPairs.get(nextTable);
				int allPairsSize = allPairs.size();
				
				for (int pairIndex=0; pairIndex<allPairsSize; pairIndex++) {
					
					Entry<Column, Column> nextPair = allPairs.get(pairIndex);
					Column fKeyColumn = nextPair.getKey();
					Column pKeyColumn = nextPair.getValue();
					
					String joinCombo1 = fKeyColumn.tableDotColumn() + "," + pKeyColumn.tableDotColumn();
					String joinCombo2 = pKeyColumn.tableDotColumn() + "," + fKeyColumn.tableDotColumn();

					/* Prevents the same joins being repeated. It doesn't matter whether
					 *  joinCombo1 or joinCombo2 is used for checking
					 */
					if (joinsUsedAlready.contains(joinCombo1)) {
						continue;
					}
					
					if (!tableJoinPerformedYet) {
						joinSQL.append(joinWord);
						joinSQL.append(nextTable.tableNameWithTicks());
						tableJoinPerformedYet = true;
					}
					
					// Marks already used join combinations. Both column orders are marked
					joinsUsedAlready.add(joinCombo1);
					joinsUsedAlready.add(joinCombo2);
					
					joinSQL.append(pairIndex == 0 ? " ON " : " OR ");
					joinSQL.append(fKeyColumn.tableDotColumnWithTicks());
					joinSQL.append('=');
					joinSQL.append(pKeyColumn.tableDotColumnWithTicks());
					
					if (nullsVisibleOnFilter && fKeyColumn.table.equals(mainTable) && pKeyColumn.table.equals(filterTable)) {
						joinSQL.append(" OR ");
						joinSQL.append(fKeyColumn.tableDotColumnWithTicks());
						joinSQL.append(" IS NULL");
					}
				}
			}
				
			return joinSQL.toString();
		}
	}
}
