package growoffshore;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

public class ConnectionsList extends JList<ConnectionsListItemComp> {

	
	public ConnectionsList() {
		setCellRenderer(new ListItemRenderer());
	}
	
	private class ListItemRenderer extends JPanel implements ListCellRenderer<ConnectionsListItemComp> {

		@Override
		public Component getListCellRendererComponent(JList<? extends ConnectionsListItemComp> list, ConnectionsListItemComp value, int index,
				boolean isSelected, boolean cellHasFocus) {
			ConnectionsListItemComp item = value;
			
			if (isSelected) {
				item.setOpaque(true);
				item.setBackground(Color.LIGHT_GRAY);
			}
			else {
				item.setOpaque(false);
			}
			
			return item;
		}
	}
}
