package growoffshore;
import java.util.ArrayList;

import org.apache.commons.lang3.StringEscapeUtils;

import com.mysql.jdbc.StringUtils;

public class Enumeration {

	private String name;
	private ArrayList<String> stringLiterals = new ArrayList<>();
	
	public Enumeration(String name) {
		this.name = name;
	}
	
	public void addStringLiteral(String literal) {
		stringLiterals.add(literal);
	}
	
	public ArrayList<String> getStringLiterals() {
		return stringLiterals;
	}
	
	public String getEnumAsType() {
		int numberOfLiteralsToReturn = stringLiterals.size() + 1;
		
		ArrayList<String> stringLiteralsEscaped = new ArrayList<>(numberOfLiteralsToReturn);
		
		stringLiteralsEscaped.add("\"\"");
		for (int i=1; i<numberOfLiteralsToReturn; i++) {
			stringLiteralsEscaped.add("\"" + StringEscapeUtils.escapeJava(stringLiterals.get(i-1)) + "\"");
		}
		
		return "ENUM(" + String.join(",", stringLiteralsEscaped) + ")";
		
	}
}
