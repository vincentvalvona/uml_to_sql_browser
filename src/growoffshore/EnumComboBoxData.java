package growoffshore;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Option;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.models.ComboBoxData;

public class EnumComboBoxData extends ComboBoxData {
	
	private Table table;
	private DatabaseInfo dbInfo;

	public EnumComboBoxData(DatabaseInfo dbInfo, Table table) {
		super("SELECT * FROM " + table.tableNameWithTicks());
		this.dbInfo = dbInfo;
		this.table = table;
	}

	@Override
	protected void postUpdateData() {
		
	}

	@Override
	public String getComboBoxDisplay(Object[] row) {
		if (row == null || row[0] == null) {
			return "Please select...";
		}
		
		return row[1] == null ? table.tableName + " " + row[0] : row[1].toString();
	}

	@Override
	public Object getValue(Object[] row) {
		return row == null ? (Long) null : (Long) row[0];
	}

	@Override
	public Option getOptionFromValue(Object value) {
		int allOptionsCount = allOptions.size();
		
		for (int i=0; i<allOptionsCount; i++) {
			Option option = allOptions.get(i);
			if (option.value != null && option.value.equals(value)) {
				return option;
			}
		}
		
		return new Option(null, "");
	}
	
	@Override
	public void updateData() {
		String joinSQL = dbInfo == null ? "" : dbInfo.getJoinSQL(table);
		updateData(query + joinSQL);
	}
}
