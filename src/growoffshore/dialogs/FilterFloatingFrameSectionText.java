package growoffshore.dialogs;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;

public class FilterFloatingFrameSectionText extends FilterFloatingFrameSection {

	JPanel radioOptionsPart;
	JRadioButton allWordsOption;
	JRadioButton anyWordOption;

	JPanel textFilterPanel;
	JLabel textFilterPanelLabel;
	JTextField textFilter;
	
	public FilterFloatingFrameSectionText(FilterFloatingFrameContent owner, Column column, boolean isRemovable) {
		super(owner, column, isRemovable);
	}

	@Override
	protected void setup() {
		setLayout(new GridBagLayout());
		
		// Adding radio options part
		radioOptionsPart = new JPanel();
		radioOptionsPart.setBorder(new TitledBorder("Match type"));
		radioOptionsPart.setLayout(new FlowLayout(FlowLayout.LEFT));

		allWordsOption = new JRadioButton("All words");
		anyWordOption = new JRadioButton("Any word");

		ButtonGroup radioOptionsGroup = new ButtonGroup();
		radioOptionsGroup.add(allWordsOption);
		radioOptionsGroup.add(anyWordOption);
		
		radioOptionsPart.add(allWordsOption);
		radioOptionsPart.add(anyWordOption);
		
//		showBothOption.addActionListener(new ShowOptionActionListener(SHOW_TYPE_BOTH));
//		showFromOnlyOption.addActionListener(new ShowOptionActionListener(SHOW_TYPE_FROM));
//		showToOnlyOption.addActionListener(new ShowOptionActionListener(SHOW_TYPE_TO));
		
		allWordsOption.setSelected(true);
		
		// Adding date and time boxes
		textFilterPanel = new JPanel();
		textFilterPanel.setLayout(new BoxLayout(textFilterPanel, BoxLayout.Y_AXIS));
		textFilter = new JTextField();
		textFilterPanel.add(textFilter);
		textFilterPanelLabel = new JLabel("Filter:");

		GridBagConstraints gbConstraints = new GridBagConstraints();
		gbConstraints.fill = GridBagConstraints.HORIZONTAL;
		
		// Options part
		gbConstraints.gridx = 0; gbConstraints.gridy = 0;
		gbConstraints.weightx = 1; gbConstraints.weighty = 0;
		gbConstraints.gridwidth = 2;
		add(radioOptionsPart, gbConstraints);

		gbConstraints.gridwidth = 1;
		// From part
		gbConstraints.gridx = 0; gbConstraints.gridy = 1;
		gbConstraints.weightx = 0;
		add(textFilterPanelLabel, gbConstraints);
		
		gbConstraints.gridx = 1; gbConstraints.gridy = 1;
		gbConstraints.weightx = 1;
		add(textFilterPanel, gbConstraints);
	}

	@Override
	public String getFilterString() {
		if (textFilter != null) {
			String text = textFilter.getText().trim();
			if (!text.equals("")) {
				if (text.length() > 2) {
					String[] textSplit = text.split(Pattern.quote(" "));
					
					ArrayList<String> allValidWords = new ArrayList<>(textSplit.length);
					
					for (int i=0; i<textSplit.length; i++) {
						String nextWord = Column.makeAlphanumeric(textSplit[i].trim());
						
						if (!nextWord.equals("")) {
							allValidWords.add(nextWord);
						}
					}
					
					String sqlConj = anyWordOption.isSelected() ? " OR " : " AND ";
					StringBuilder sql = new StringBuilder();
					sql.append("(");
					int allValidWordsLength = allValidWords.size();
					//  eg.      AND (field LIKE '%tea%')
					for (int i=0; i<allValidWordsLength; i++) {
						if (0 != i) {
							sql.append(sqlConj);
						}
						sql.append("(");
						sql.append(column.tableDotColumnWithTicks());
						sql.append(" LIKE '%");
						sql.append(allValidWords.get(i));
						sql.append("%')");
					}
					sql.append(")");
					return sql.toString();
				}
				else {
					String nextWord = Column.makeAlphanumeric(text);
					if (!nextWord.equals("")) {
						StringBuilder sql = new StringBuilder();
						sql.append("(");
						sql.append(column.tableDotColumnWithTicks());
						sql.append(" LIKE '");
						sql.append(nextWord);
						sql.append("%')");
						return sql.toString();
					}
				}
			}
		}
		return "";
	}	
}
