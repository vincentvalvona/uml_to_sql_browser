package growoffshore.dialogs;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import growoffshore.DatabaseInfo;
import growoffshore.TablePanelWindow;
import growoffshore.mainwindow.MainWindowTableErrorFooter;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;
import uk.co.solidcoresoftware.databaseapp.views.tables.UserTableScrollPane;

public class ErrorFieldsDialog extends LightTableDialog {

	private static int numberOfActions = 0;
	public static final int SELECTION_CANCEL = numberOfActions++;
	public static final int SELECTION_IGNORE = numberOfActions++;
	public static final int SELECTION_OK = numberOfActions++;
	
	private JButton okButton;
	private JButton ignoreButton;
	private JButton cancelButton;
	
	private int selection = SELECTION_CANCEL;
	
	public ErrorFieldsDialog(TablePanelWindow parent, Table table, Column[] columns, DatabaseInfo dbInfo) {
		super(parent, table, columns, dbInfo);
	}

	@Override
	protected void setup() {
		super.setup();
		
		MainWindowTableErrorFooter errorFooter = new MainWindowTableErrorFooter(mainPanel.getBaseTable());
		errorFooter.showMessage(dbInfo.constructConstraintsMessage(table));
		errorFooter.hideRevertButton();
		
		add(errorFooter, 0);
	}
	
	@Override
	protected void setupButtons(JPanel buttonsPanel) {
		okButton = new JButton("OK");
		ignoreButton = new JButton("Ignore");
		cancelButton = new JButton("Cancel");

		okButton.addActionListener(new SelectionListener(SELECTION_OK));
		ignoreButton.addActionListener(new SelectionListener(SELECTION_IGNORE));
		cancelButton.addActionListener(new SelectionListener(SELECTION_CANCEL));

		buttonsPanel.add(okButton);
		buttonsPanel.add(ignoreButton);
		buttonsPanel.add(cancelButton);
	}
	
	private class SelectionListener implements ActionListener {

		private final int selection; 
		
		public SelectionListener(int selection) {
			this.selection = selection;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			ErrorFieldsDialog.this.selection = selection;
			ErrorFieldsDialog.this.setVisible(false);
		}
	}

	public int getSelection() {
		return selection;
	}
	
	public Object[][] getDataAs2DStringArray() {
		LightTableModel model = getModel();

		Object[][] allDataObject = model.getData();

		int rowCount = model.getRowCount();
		int columnCount = model.getColumnCount();
		Object[][] allData = BaseTable.Array2DObjectToString(allDataObject, rowCount, columnCount);
		
		return allData;
	}

	public Object[][] getDataAs2DArray() {
		LightTableModel model = getModel();

		Object[][] allDataObject = model.getData();

		int rowCount = model.getRowCount();
		int columnCount = model.getColumnCount();
		Object[][] allData = BaseTable.Array2DObject(allDataObject, rowCount, columnCount);
		
		return allData;
	}
}
