package growoffshore.dialogs;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import growoffshore.DatabaseInfo;
import growoffshore.TablePanelWindow;
import growoffshore.cellEditorsAndRenderers.BaseTableColourer;
import growoffshore.cellEditorsAndRenderers.ComboBoxDataCellEditor;
import growoffshore.cellEditorsAndRenderers.NormalTextCellRendererGreyed;
import growoffshore.generators.CellEditorAndRendererGenerator;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableColumn;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.Row;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.models.ComboBoxSelectedAddRow;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.models.CommandSelection;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;

public class RowAsFormDialog extends JDialog implements TablePanelWindow {
	

	private Object[] values;
	private CellEditorAndRendererGenerator[] generators;
	private BaseTable baseTable;
	private int rowIndex;
	private int[] columnIndices;

	private JPanel rowAsForm;
	
	BaseTable jTable;
	LightTableModel jTableModel;
	
	private DatabaseInfo dbInfo;
	private TablePanelWindow parentWindow;
	
	private JPanel buttonsPanel;
	private JButton okButton;
	private JButton cancelButton;
	private JButton closeButton;
	private int buttonsType;
	
	private static int buttonsTypeCount = 0;
	public final static int BUTTONS_TYPE_OK_CANCEL = buttonsTypeCount++;
	public final static int BUTTONS_TYPE_CLOSE = buttonsTypeCount++;
	
	public RowAsFormDialog(CellEditorAndRendererGenerator[] generators, Object[] values, BaseTable baseTable, int rowIndex, int[] columnIndices, DatabaseInfo dbInfo, TablePanelWindow parentWindow, int buttonsType) {
		this.generators = generators;
		this.values = values;
		this.baseTable = baseTable;
		this.rowIndex = rowIndex;
		this.columnIndices = columnIndices;
		this.dbInfo = dbInfo;
		this.parentWindow = parentWindow;
		
		if (BUTTONS_TYPE_CLOSE == buttonsType) {
			// No nothing
		}
		else {
			buttonsType = BUTTONS_TYPE_OK_CANCEL;
		}
		
		this.buttonsType = buttonsType;
		setup();
	}

	private void setup() {
		setModal(true);
		
		setLayout(new GridBagLayout());
		
		GridBagConstraints layoutConstraints = new GridBagConstraints();
		
		rowAsForm = new JPanel();
		
		rowAsForm.setLayout(new GridBagLayout());
		
		final CellEditorAndRendererGenerator[] generatorsToUse = new CellEditorAndRendererGenerator[generators.length];
		
		jTableModel = new LightTableModel() {
			{
				for (int i=0; i<values.length; i++) {
					data.add(new Row(new Object[] {generators[i].column.columnName, values[i]}));
				}
			}

			@Override
			protected void setupModel() {
				Column labelColumn = new Column("Form", "Label");
				Column entryColumn = new Column("Form", "Entry");
				addColumn(labelColumn);
				addColumn(entryColumn);
			}

			@Override
			protected void setupCalibrators() {}

			@Override
			protected void defineIndices() {}
			
			@Override
			public boolean isCellEditable(int row, int column) {
				if (0 == column) {
					return false;
				}

				return super.isCellEditable(row, column);
			}
			
			@Override
			public void setValueAt(final Object newValue, final int rowIndex, final int columnIndex) {
				
				CommandSelection returnCommand = new CommandSelection() {
					@Override
					public void invokeCommand(LightTableModel unused1, Object unused2, int unused3, int unused4) {
						int numberOfRows = getRowCount();
						
						for (int i=0; i<numberOfRows; i++) {
							Object parentValue = baseTable.model.getValueAt(RowAsFormDialog.this.rowIndex, i);
							data.get(i).getRowData()[1] = parentValue;
						}
						
						baseTable.model.fireTableDataChanged();
						fireTableDataChanged();
					}
				};
				
				boolean invodeReturnCommandManually = true;
				
				if (newValue instanceof ComboBoxSelectedAddRow) {
					((ComboBoxDataCellEditor) generators[rowIndex].cellEditor).setAddRowOptionReturnCommand(returnCommand);
					invodeReturnCommandManually = false;
				}
				
				baseTable.model.setValueAt(newValue, RowAsFormDialog.this.rowIndex, rowIndex);
				
				if (invodeReturnCommandManually) {
					returnCommand.invokeCommand(baseTable.model, newValue, rowIndex, columnIndex);
				}
			}
		};
		
		jTableModel.setRowAddingEnabled(false);
		jTableModel.setRowDeletingEnabled(false);
		
		final NormalTextCellRendererGreyed labelRenderer = new NormalTextCellRendererGreyed();
		
		jTable = new BaseTable(this, jTableModel, null) {
			
			@Override
			protected void setupTable() {
				for (int i=0; i<generatorsToUse.length; i++) {
					CellEditorAndRendererGenerator nextGen = generators[i]; 
					generatorsToUse[i] = new CellEditorAndRendererGenerator(nextGen.column, RowAsFormDialog.this, nextGen.dbInfo, this);
				}

				BaseTableColumn lableTableColumn = new BaseTableColumn(0, 100, null, null);
				BaseTableColumn entryTableColumn = new BaseTableColumn(1, 100, null, null);
				
				addColumn(lableTableColumn);
				addColumn(entryTableColumn);
			}
			
			@Override
			public TableCellRenderer getCellRenderer(int rowIndex, int columnIndex) {
				if (0 == columnIndex) {
					return labelRenderer;
				}
				else {
					return generators[rowIndex].cellRenderer;
				}
			}
			
			@Override
			public TableCellEditor getCellEditor(int rowIndex, int columnIndex) {
				if (0 == columnIndex) {
					return super.getCellEditor(rowIndex, columnIndex);					
				}
				else {
					return generators[rowIndex].cellEditor;
				}
			}
		};
		jTable.showLines(false);
		int oldRowHeight = jTable.getRowHeight();
		jTable.setRowMargin(4);
		jTable.setRowHeight(oldRowHeight + 4);
		jTable.setBackground(Color.LIGHT_GRAY);
		
		buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		
		if (BUTTONS_TYPE_OK_CANCEL == buttonsType) { 
			okButton = new JButton("OK");
			cancelButton = new JButton("Cancel");
			buttonsPanel.add(okButton);
			buttonsPanel.add(cancelButton);

			okButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					okButtonClicked();
				}
			});
			cancelButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					cancelButtonClicked();
				}
			});
		}
		else if (BUTTONS_TYPE_CLOSE == buttonsType) {
			closeButton = new JButton("Close");
			buttonsPanel.add(closeButton);

			closeButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					closeButtonClicked();
				}
			});
		}

		layoutConstraints.gridx = 0; layoutConstraints.gridy = 0;
		layoutConstraints.weightx = 1; layoutConstraints.weighty = 1;
		layoutConstraints.fill = GridBagConstraints.BOTH;
		add(jTable, layoutConstraints);

		layoutConstraints.gridx = 0; layoutConstraints.gridy = 1;
		layoutConstraints.weightx = 1; layoutConstraints.weighty = 0;
		layoutConstraints.fill = GridBagConstraints.BOTH;
		add(buttonsPanel, layoutConstraints);
		
		pack();
		int fixedHeight = getHeight();
		setMinimumSize(new Dimension(200, fixedHeight));
		setSize(400, fixedHeight);
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		addWindowListener(new WindowListener() {
			
			@Override
			public void windowOpened(WindowEvent e) {}
			
			@Override
			public void windowIconified(WindowEvent e) {}
			
			@Override
			public void windowDeiconified(WindowEvent e) {}
			
			@Override
			public void windowDeactivated(WindowEvent e) {}
			
			@Override
			public void windowClosing(WindowEvent e) {
				if (BUTTONS_TYPE_CLOSE == buttonsType) {
					closeButtonClicked();
				}
				else if (BUTTONS_TYPE_OK_CANCEL == buttonsType) {
					cancelButtonClicked();
				}
			}
			
			@Override
			public void windowClosed(WindowEvent e) {}
			
			@Override
			public void windowActivated(WindowEvent e) {}
		});
	}

	@Override
	public DatabaseInfo getDbInfo() {
		return dbInfo;
	}

	@Override
	public TablePanelWindow getParentWindow() {
		return parentWindow;
	}

	private void okButtonClicked() {
		baseTable.model.saveRowAt(baseTable.convertRowIndexToModel(rowIndex));
		setVisible(false);
	}
	
	private void cancelButtonClicked() {
		baseTable.model.deleteRowAt(baseTable.convertRowIndexToModel(rowIndex), null);
		setVisible(false);
	}
	
	private void closeButtonClicked() {
		setVisible(false);
	}
	
	private class VeryLightTable extends JTable implements BaseTableColourer {
		private VeryLightTable(TableModel dm) {
			super(dm);
		}

		@Override
		public TableCellRenderer getCellRenderer(int rowIndex, int columnIndex) {
			if (0 == columnIndex) {
				return super.getCellRenderer(rowIndex, columnIndex);
			}
			else {
				return generators[rowIndex].cellRenderer;
			}
		}

		@Override
		public TableCellEditor getCellEditor(int rowIndex, int columnIndex) {
			if (0 == columnIndex) {
				return super.getCellEditor(rowIndex, columnIndex);
			}
			else {
				return generators[rowIndex].cellEditor;
			}
		}

		@Override
		public void setColour(Component comp, Object value, boolean isSelected, boolean hasFocus, int rowIndex,
				int columnIndex) {
		}
	}
}
