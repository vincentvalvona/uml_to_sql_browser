package growoffshore.dialogs;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Time;
import java.util.Calendar;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.toedter.calendar.JDateChooser;

import growoffshore.Main;
import growoffshore.TimeChooser;
import growoffshore.TimeChooserForPopup;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.WhereClause;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableModel;

public class FilterFloatingFrameSectionDateOrTime extends FilterFloatingFrameSection {

	private boolean containsDate;
	private boolean containsTime;
	
	private JPanel radioOptionsPart;
	private JRadioButton showBothOption;
	private JRadioButton showFromOnlyOption;
	private JRadioButton showToOnlyOption;
	
	private JPanel rangePartFrom;
	private JLabel rangePartFromLabel;
	private JDateChooser dateChooserFrom;
	private TimeChooserForPopup timeChooserFrom;
	
	private JPanel rangePartTo;
	private JLabel rangePartToLabel;
	private JDateChooser dateChooserTo;
	private TimeChooserForPopup timeChooserTo;
	
	public FilterFloatingFrameSectionDateOrTime(FilterFloatingFrameContent owner, Column column, boolean isRemovable) {
		super(owner, column, isRemovable);
	}
	
	@Override
	protected void setup() {
		int sectionType = column.getType();
		
		containsDate = java.sql.Types.TIME != sectionType;
		containsTime = java.sql.Types.DATE != sectionType;
		
		setLayout(new GridBagLayout());
		
		// Adding radio options part
		radioOptionsPart = new JPanel();
		radioOptionsPart.setBorder(new TitledBorder("Show range type"));
		radioOptionsPart.setLayout(new FlowLayout(FlowLayout.LEFT));

		showBothOption = new JRadioButton("Full range");
		showFromOnlyOption = new JRadioButton("From only");
		showToOnlyOption = new JRadioButton("To only");

		ButtonGroup radioOptionsGroup = new ButtonGroup();
		radioOptionsGroup.add(showBothOption);
		radioOptionsGroup.add(showFromOnlyOption);
		radioOptionsGroup.add(showToOnlyOption);
		
		radioOptionsPart.add(showBothOption);
		radioOptionsPart.add(showFromOnlyOption);
		radioOptionsPart.add(showToOnlyOption);

		showBothOption.addActionListener(new ShowOptionActionListener(SHOW_TYPE_BOTH));
		showFromOnlyOption.addActionListener(new ShowOptionActionListener(SHOW_TYPE_FROM));
		showToOnlyOption.addActionListener(new ShowOptionActionListener(SHOW_TYPE_TO));
		
		showBothOption.setSelected(true);
				
		// Adding date and time boxes
		rangePartFrom = new JPanel();
		rangePartFrom.setLayout(new FlowLayout(FlowLayout.LEFT));
		rangePartFromLabel = new JLabel("From:");
		if (containsDate) {
			java.sql.Date dateChooserFromDate = new java.sql.Date(System.currentTimeMillis());
			dateChooserFrom = new JDateChooser(dateChooserFromDate);
			rangePartFrom.add(dateChooserFrom);
		}
		if (containsTime) {
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 0); cal.set(Calendar.MINUTE, 0); cal.set(Calendar.SECOND, 0);
			timeChooserFrom = new TimeChooserForPopup(new Time(cal.getTimeInMillis()), this);
			rangePartFrom.add(timeChooserFrom);
		}
		
		rangePartTo = new JPanel();
		rangePartTo.setLayout(new FlowLayout(FlowLayout.LEFT));
		rangePartToLabel = new JLabel("To:");
		if (containsDate) {
			dateChooserTo = new JDateChooser(new java.sql.Date(System.currentTimeMillis()));
			rangePartTo.add(dateChooserTo);
		}
		if (containsTime) {
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.HOUR_OF_DAY, 0); cal.set(Calendar.MINUTE, 0); cal.set(Calendar.SECOND, 0);
			timeChooserTo = new TimeChooserForPopup(new Time(cal.getTimeInMillis()), this);
			rangePartTo.add(timeChooserTo);
		}

		GridBagConstraints gbConstraints = new GridBagConstraints();
		gbConstraints.fill = GridBagConstraints.HORIZONTAL;
		
		// Options part
		gbConstraints.gridx = 0; gbConstraints.gridy = 0;
		gbConstraints.weightx = 1; gbConstraints.weighty = 0;
		gbConstraints.gridwidth = 2;
		add(radioOptionsPart, gbConstraints);

		gbConstraints.gridwidth = 1;
		// From part
		gbConstraints.gridx = 0; gbConstraints.gridy = 1;
		gbConstraints.weightx = 1;
		add(rangePartFromLabel, gbConstraints);
		
		gbConstraints.gridx = 1; gbConstraints.gridy = 1;
		gbConstraints.weightx = 0;
		add(rangePartFrom, gbConstraints);

		// To part
		gbConstraints.gridx = 0; gbConstraints.gridy = 2;
		gbConstraints.weightx = 1;
		add(rangePartToLabel, gbConstraints);
		
		gbConstraints.gridx = 1; gbConstraints.gridy = 2;
		gbConstraints.weightx = 0;
		add(rangePartTo, gbConstraints);
	}

	public boolean showingToField() {
		return rangePartTo.isVisible();
	}
	
	public boolean showingFromField() {
		return rangePartFrom.isVisible();
	}
	
	private static int showTypeCount = 0; 
	public static final int SHOW_TYPE_BOTH = showTypeCount++;
	public static final int SHOW_TYPE_FROM = showTypeCount++;
	public static final int SHOW_TYPE_TO = showTypeCount++;
	
	private class ShowOptionActionListener implements ActionListener {
		private int showType;

		public ShowOptionActionListener(int showType) {
			this.showType = showType;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			if (SHOW_TYPE_BOTH == showType) {
				rangePartFrom.setVisible(true);
				rangePartTo.setVisible(true);
			}
			else if (SHOW_TYPE_FROM == showType) {
				rangePartFrom.setVisible(true);
				rangePartTo.setVisible(false);
			}
			else if (SHOW_TYPE_TO == showType) {
				rangePartFrom.setVisible(false);
				rangePartTo.setVisible(true);
			}

			rangePartFromLabel.setVisible(rangePartFrom.isVisible());
			rangePartToLabel.setVisible(rangePartTo.isVisible());
		}
	}

	private Date convertCalendarToCorrectType(Calendar cal) {
		if (containsDate && !containsTime) {
			return new java.sql.Date(cal.getTimeInMillis());
		}
		else if (!containsDate && containsTime) {
			return new java.sql.Time(cal.getTimeInMillis());
		}
		else {
			return new java.sql.Timestamp(cal.getTimeInMillis());
		}
	}
	
	@Override
	public String getFilterString() {

		BaseTableModel model = owner.getModel();

		// Getting from fields
		Calendar dateChooserFromCal = dateChooserFrom == null ? null : dateChooserFrom.getCalendar();
		Calendar timeChooserFromCal = timeChooserFrom == null ? null : timeChooserFrom.getCalendar();

		// Getting too fields
		Calendar dateChooserToCal = dateChooserTo == null ? null : dateChooserTo.getCalendar();
		Calendar timeChooserToCal = timeChooserTo == null ? null : timeChooserTo.getCalendar();
		
		if (showingFromField()) {
			Calendar fromCal = Main.combineDateAndTimeCalendars(containsDate ? dateChooserFromCal : null,
					containsTime ? timeChooserFromCal : null);
			Date fromCorrectType = convertCalendarToCorrectType(fromCal);
			WhereClause fromClause = new WhereClause(column, fromCorrectType, WhereClause.WHERE_CLAUSE_TYPE_GREATER_THAN_OR_EQUAL_TO);
			model.addCustomWhereClause(fromClause);
		}

		if (showingToField()) {
			Calendar toCal = Main.combineDateAndTimeCalendars(containsDate ? dateChooserToCal : null,
					containsTime ? timeChooserToCal : null);
			Date toCorrectType = convertCalendarToCorrectType(toCal);
			WhereClause toClause = new WhereClause(column, toCorrectType, WhereClause.WHERE_CLAUSE_TYPE_LESS_THAN_OR_EQUAL_TO);
			model.addCustomWhereClause(toClause);
		}
		
		return "";
	}
}
