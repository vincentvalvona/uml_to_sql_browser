package growoffshore.dialogs;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Window;

import javax.swing.BoxLayout;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import growoffshore.DatabaseInfo;
import growoffshore.TablePanel;
import growoffshore.TablePanelWindow;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;

public abstract class LightTableDialog extends JDialog implements TablePanelWindow {

	protected TablePanelWindow parent;
	protected Column[] columns;
	protected Table table;
	protected DatabaseInfo dbInfo;
	protected TablePanel mainPanel;
	protected JPanel buttonsPanel;
	protected final boolean canModifyTable;
	protected Integer rowPosInDBTable;


	public LightTableDialog(TablePanelWindow parent, Table table, Column[] columns, DatabaseInfo dbInfo, boolean canModifyTable) {
		this(parent, table, columns, dbInfo, canModifyTable, null);
	}
	public LightTableDialog(TablePanelWindow parent, Table table, Column[] columns, DatabaseInfo dbInfo, boolean canModifyTable, Integer rowPosInDBTable) {
		super((Window) parent);
		this.parent = parent;
		this.table = table;
		this.columns = columns;
		
		this.dbInfo = dbInfo;
		this.canModifyTable = canModifyTable;
		this.rowPosInDBTable = rowPosInDBTable;
		
		setup();
	}

	public LightTableDialog(TablePanelWindow parent, Table table, Column[] columns, DatabaseInfo dbInfo) {
		this(parent, table, columns, dbInfo, true);
	}

	protected void addMainPanel() {
		mainPanel = new LightTableDialogPanel(parent, table, columns, dbInfo) {
			@Override
			protected void doCustomForModel(LightTableModel model) {
				LightTableDialog.this.doCustomForModel(model);
			}
			
			@Override
			protected void doCustomForTable(BaseTable baseTable) {
				LightTableDialog.this.doCustomForTable(baseTable);
			}
		};
	}
	
	protected void setup() {
		setPreferredSize(new Dimension(900, 300));
		pack();
		
		addMainPanel();
		
		getContentPane().setLayout(new GridBagLayout());
		
		GridBagConstraints mainLayoutConstraints = new GridBagConstraints();

		mainLayoutConstraints.gridx = 0; mainLayoutConstraints.gridy = 0;
		mainLayoutConstraints.weightx = 1; mainLayoutConstraints.weighty = 1;
		mainLayoutConstraints.fill = GridBagConstraints.BOTH;
		getContentPane().add(mainPanel, mainLayoutConstraints);
		
		buttonsPanel = new JPanel();
		buttonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

		mainLayoutConstraints.gridx = 0; mainLayoutConstraints.gridy = 1;
		mainLayoutConstraints.weightx = 1; mainLayoutConstraints.weighty = 0;
		mainLayoutConstraints.fill = GridBagConstraints.BOTH;
		getContentPane().add(buttonsPanel, mainLayoutConstraints);
		
		setupButtons(buttonsPanel);
		
		setModal(true);
	}

	protected abstract void setupButtons(JPanel buttonsPanel);
	
	protected void doCustomForModel(LightTableModel model) {
		
	}

	protected void doCustomForTable(BaseTable baseTable) {
		
	}

	public TablePanelWindow getParentWindow() {
		return parent;
	}
	
	public TablePanel getMainPanel() {
		return mainPanel;
	}

	@Override
	public DatabaseInfo getDbInfo() {
		return dbInfo;
	}

	public LightTableModel getModel() {
		return mainPanel.getModel();
	}
	

	public void display() {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				mainPanel.getBaseTable().getScrollPane().resizeTable();
			}
		});
		setVisible(true);
	}
}
