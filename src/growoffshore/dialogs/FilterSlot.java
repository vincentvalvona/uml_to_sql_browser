package growoffshore.dialogs;

import javax.swing.JPanel;

public class FilterSlot extends JPanel {
	
	private FilterFloatingFrameSection section;

	public void setFilterFloatingFrameSection(FilterFloatingFrameSection section) {
		removeAll();
		this.section = section;
		add(section);
	}
	
	public void removeFilterFloatingFrameSection() {
		removeAll();
		this.section = null;
	}
	
	public FilterFloatingFrameSection getSection() {
		return section;
	}
}
