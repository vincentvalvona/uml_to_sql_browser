package growoffshore.dialogs;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.regex.Pattern;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import com.toedter.calendar.JDateChooser;

//import javafx.scene.chart.PieChart.Data;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.DataType;

public class FilterFloatingFrameSectionBoolean extends FilterFloatingFrameSection {

	JPanel radioOptionsPart;
	JRadioButton bothOption;
	JRadioButton trueOption;
	JRadioButton falseOption;
	
	public FilterFloatingFrameSectionBoolean(FilterFloatingFrameContent owner, Column column, boolean isRemovable) {
		super(owner, column, isRemovable);
	}

	@Override
	protected void setup() {
		setLayout(new GridBagLayout());
		
		// Adding radio options part
		radioOptionsPart = new JPanel();
		radioOptionsPart.setBorder(new TitledBorder("True, false or both?"));
		radioOptionsPart.setLayout(new FlowLayout(FlowLayout.LEFT));

		bothOption = new JRadioButton("Both");
		trueOption = new JRadioButton("True");
		falseOption = new JRadioButton("False");

		ButtonGroup radioOptionsGroup = new ButtonGroup();
		radioOptionsGroup.add(bothOption);
		radioOptionsGroup.add(trueOption);
		radioOptionsGroup.add(falseOption);
		
		radioOptionsPart.add(bothOption);
		radioOptionsPart.add(trueOption);
		radioOptionsPart.add(falseOption);
		
//		showBothOption.addActionListener(new ShowOptionActionListener(SHOW_TYPE_BOTH));
//		showFromOnlyOption.addActionListener(new ShowOptionActionListener(SHOW_TYPE_FROM));
//		showToOnlyOption.addActionListener(new ShowOptionActionListener(SHOW_TYPE_TO));
		
		bothOption.setSelected(true);
		
		GridBagConstraints gbConstraints = new GridBagConstraints();
		gbConstraints.fill = GridBagConstraints.HORIZONTAL;
		
		// Options part
		gbConstraints.gridx = 0; gbConstraints.gridy = 0;
		gbConstraints.weightx = 1; gbConstraints.weighty = 0;
		gbConstraints.gridwidth = 2;
		add(radioOptionsPart, gbConstraints);
	}

	@Override
	public String getFilterString() {
		StringBuilder sql = new StringBuilder();
		if (trueOption.isSelected()) {
			sql.append(column.tableDotColumnWithTicks());
			sql.append("=1");
		}
		else if (falseOption.isSelected()) {
			sql.append(column.tableDotColumnWithTicks());
			sql.append("=0");
		}
		return sql.toString();
	}	
}
