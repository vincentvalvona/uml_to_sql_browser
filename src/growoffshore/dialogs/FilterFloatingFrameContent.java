package growoffshore.dialogs;

import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import growoffshore.DatabaseInfo;
import growoffshore.Main;
import growoffshore.mainwindow.MainWindowTablePanel;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Option;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.OrderByClause;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableModel;

public class FilterFloatingFrameContent extends JPanel {
	
	private DatabaseInfo dbInfo;
	private Table table;
	private BaseTableModel model;

	final int numberOfSections;
	
	private ArrayList<FilterSlot> allFilterSectionWrappers;
	private final Window owner;
	private FilterFloatingFrame floatingFrame;
	private ArrayList<JComboBox<Option>> comboBoxes;

	private MainWindowTablePanel tablePanel;
	private JButton updateButton;
	
	public FilterFloatingFrameContent(Window owner, FilterFloatingFrame floatingFrame) {
		this(owner, floatingFrame, null);
	}
	
	public FilterFloatingFrameContent(Window owner, FilterFloatingFrame floatingFrame, Integer numberOfSections) {
		this.owner = owner;
		this.floatingFrame = floatingFrame;
		this.numberOfSections = numberOfSections == null ? 4 : numberOfSections;
		setup();	
	}
	
	public void setSectionFilterColumn(int sectionIndex, Column column) {
		JComboBox<Option> cBox = comboBoxes.get(sectionIndex);
		
		int itemCount = cBox.getItemCount();
		for (int i=0; i<itemCount; i++) {
			Option nextItem = cBox.getItemAt(i);
			if (column.equals(nextItem.value)) {
				cBox.setSelectedIndex(i);
				break;
			}
		}
	}
	
	public void setSectionFilterColumnToIndex(int sectionIndex, int columnIndex) {
		JComboBox<Option> cBox = comboBoxes.get(sectionIndex);
		
		if (columnIndex < cBox.getComponentCount()) {
			cBox.setSelectedIndex(columnIndex);
		}
	}
	
	private void setup() {
		setLayout(new GridBagLayout());
		allFilterSectionWrappers = new ArrayList<>();
		comboBoxes = new ArrayList<>();
	}
	
	public void display(DatabaseInfo dbInfo, Table table, BaseTableModel model) {
		this.dbInfo = dbInfo;
		this.table = table;
		this.model = model;

		System.out.println("table: " + table);
		
		allFilterSectionWrappers.clear();
		comboBoxes.clear();
		removeAll();
		
		GridBagConstraints gbConstraints = new GridBagConstraints();
		gbConstraints.gridx = 0; gbConstraints.weightx = 1;
		gbConstraints.fill = GridBagConstraints.HORIZONTAL;
		
		for (int sectionIndex=0; sectionIndex<numberOfSections; sectionIndex++) {
			final JPanel nextSection = new JPanel();
			nextSection.setLayout(new GridBagLayout());
			nextSection.setBorder(new TitledBorder("Filter and order " + (sectionIndex+1)));
			
			Column[] allColumns = dbInfo.getAllColumnsFromTable(table, true);
			
			final JComboBox<Option> allTablesComboBox = new JComboBox<>();
			allTablesComboBox.addItem(new Option(null, "Select column..."));
			for (int columnIndex=0; columnIndex<allColumns.length; columnIndex++) {
				Column nextColumn = allColumns[columnIndex];
				if (nextColumn.isPrimary() || dbInfo.isAForeignKey(nextColumn) || nextColumn.isDateOrTime()
						|| nextColumn.isTextType() || nextColumn.isNonBooleanNumericInput()
						|| nextColumn.isBoolean() ) {
					allTablesComboBox.addItem(new Option(nextColumn, nextColumn.columnName));
				}
			}
			
			final FilterSlot filterSlot = new FilterSlot();
			allFilterSectionWrappers.add(filterSlot);
			filterSlot.setVisible(false);
			
			allTablesComboBox.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					filterSlot.removeFilterFloatingFrameSection();
					filterSlot.setVisible(false);

					Object selectedItemObj = allTablesComboBox.getSelectedItem();
					if (selectedItemObj != null) {
						Option selectedItem = (Option) selectedItemObj;
						if (selectedItem.value != null) {
							Column selectedColumn = (Column) selectedItem.value;

							if (selectedColumn.isPrimary()) {
								FilterFloatingFrameSectionNumberRange nextFrameSection = new FilterFloatingFrameSectionNumberRange(FilterFloatingFrameContent.this, selectedColumn, false);
								filterSlot.setFilterFloatingFrameSection(nextFrameSection);
							}
							else if (dbInfo.isAForeignKey(selectedColumn)) {
								Column pKeyColumn = dbInfo.getPrimaryKeyOfForeignKey(selectedColumn);
								Column displayColumn =dbInfo.getDisplayColumnFromTable(pKeyColumn.table);
								FilterFloatingFrameSectionFKeyText nextFrameSection = new FilterFloatingFrameSectionFKeyText(FilterFloatingFrameContent.this, selectedColumn, pKeyColumn, false, displayColumn);
								filterSlot.setFilterFloatingFrameSection(nextFrameSection);
							}
							else if (selectedColumn.isDateOrTime()) {
								FilterFloatingFrameSectionDateOrTime nextFrameSection = new FilterFloatingFrameSectionDateOrTime(FilterFloatingFrameContent.this, selectedColumn, false);
								filterSlot.setFilterFloatingFrameSection(nextFrameSection);
							}
							else if (selectedColumn.isTextType()) {
								FilterFloatingFrameSectionText nextFrameSection = new FilterFloatingFrameSectionText(FilterFloatingFrameContent.this, selectedColumn, false);
								filterSlot.setFilterFloatingFrameSection(nextFrameSection);
							}
							else if (selectedColumn.isNonBooleanNumericInput()) {
								FilterFloatingFrameSectionNumberRange nextFrameSection = new FilterFloatingFrameSectionNumberRange(FilterFloatingFrameContent.this, selectedColumn, false);
								filterSlot.setFilterFloatingFrameSection(nextFrameSection);
							}
							else if (selectedColumn.isBoolean()) {
								FilterFloatingFrameSectionBoolean nextFrameSection = new FilterFloatingFrameSectionBoolean(FilterFloatingFrameContent.this, selectedColumn, false);
								filterSlot.setFilterFloatingFrameSection(nextFrameSection);
							}
							

							filterSlot.setVisible(true);
							filterSlot.revalidate();
							revalidate();
							if (floatingFrame != null) {
								floatingFrame.pack();
							}
							Dimension newSize = FilterFloatingFrameContent.this.getSize();
							int newWidth = Math.max(newSize.width, 300);
							setSize(newWidth, getSize().height);
						}
					}
				}
			});
			
			gbConstraints.gridy = sectionIndex;
			add(nextSection, gbConstraints);
			
			gbConstraints.gridy = 0;
			comboBoxes.add(allTablesComboBox);
			nextSection.add(allTablesComboBox, gbConstraints);
			
			gbConstraints.gridy = 1;
			nextSection.add(filterSlot, gbConstraints);
			
			updateButton = new JButton("Update");
			updateButton.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					updateFiltersOnModel();
				}
			});
			
			gbConstraints.gridy = 2;
			nextSection.add(updateButton, gbConstraints);
		}
		
	}
	
	public void updateFiltersOnModel() {
		model.clearCustomJoinSQL();
		model.clearCustomWhereClauses();
		model.clearCustomOrderByClauses();
		
		int allFilterSectionsSize = allFilterSectionWrappers.size();
		
		StringBuilder whereSql = new StringBuilder();
		
		boolean whereSQLFilledYet = false;
		
		for (int i=0; i<allFilterSectionsSize; i++) {
			FilterFloatingFrameSection nextSection = allFilterSectionWrappers.get(i).getSection();
			
			if (nextSection != null) {
				if (nextSection instanceof FilterFloatingFrameSectionFKeyText) {
					FilterFloatingFrameSectionFKeyText nextSectionFKey = (FilterFloatingFrameSectionFKeyText) nextSection;
					
					Table joinTable = new Table(table.tableName + "_join_" + i);
					nextSectionFKey.setAliasTable(joinTable);
					
					model.addCustomJoinSQL(nextSectionFKey.getJoinSQL());
				}
				
				String nextFilterString = nextSection.getFilterString();
				
				if (!nextFilterString.equals("")) {
					if (whereSQLFilledYet) {
						whereSql.append(" AND ");
					}
					else {
						whereSQLFilledYet = true;
						whereSql.append(" ");
					}
					whereSql.append(nextFilterString);
				}
				if (nextSection instanceof FilterFloatingFrameSectionFKeyText) {
					model.addCustomOrderByClause(new OrderByClause(((FilterFloatingFrameSectionFKeyText)nextSection).getDisplayColumnAlias(), OrderByClause.ORDER_BY_ASC));
				}
				else {
					model.addCustomOrderByClause(new OrderByClause(nextSection.column, OrderByClause.ORDER_BY_ASC));
				}
			}
		}
		
		String sqlToUse = whereSql.toString().trim();
		
		if (sqlToUse.equals("")) {
			model.setCustomWhereSQL(null);
		}
		else {
			model.setCustomWhereSQL(sqlToUse);
		}

		if (tablePanel != null) {
			tablePanel.updateCustomDetailsFromModel();
			tablePanel.setPageNumber(1);
		}
	}
	
	public void setTablePanel(MainWindowTablePanel tablePanel) {
		this.tablePanel = tablePanel;
	}
	
	public BaseTableModel getModel() {
		return model;
	}
}
