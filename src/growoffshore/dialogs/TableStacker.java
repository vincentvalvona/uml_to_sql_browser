package growoffshore.dialogs;

import growoffshore.TablePanelWindow;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;

public interface TableStacker {
	public void setTableStack(Table table, TablePanelWindow parent);
	public boolean tableNameAlreadyInStack(String tableName);
	public String[] getTableNamesStack();
}
