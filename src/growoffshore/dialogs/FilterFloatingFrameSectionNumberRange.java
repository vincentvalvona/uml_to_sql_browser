package growoffshore.dialogs;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.math.BigDecimal;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;

public class FilterFloatingFrameSectionNumberRange extends FilterFloatingFrameSection {

	JPanel textFilterPanel;
	JLabel textFilterPanelLabel;
	
	JLabel fromLabel;
	JTextField textFilterFrom;
	JLabel toLabel;
	JTextField textFilterTo;
	
	public FilterFloatingFrameSectionNumberRange(FilterFloatingFrameContent owner, Column column, boolean isRemovable) {
		super(owner, column, isRemovable);
	}

	@Override
	protected void setup() {
		setLayout(new GridBagLayout());
		// Adding date and time boxes
		textFilterPanel = new JPanel();
		textFilterPanel.setLayout(new GridBagLayout());
		fromLabel = new JLabel("From ");
		textFilterFrom = new JTextField();
		Dimension size1 = new Dimension(200, 30);
		textFilterFrom.setMinimumSize(size1);
		textFilterFrom.setPreferredSize(size1);
		toLabel = new JLabel(" to ");
		textFilterTo = new JTextField();
		Dimension size2 = new Dimension(200, 30);
		textFilterTo.setMinimumSize(size2);
		textFilterTo.setPreferredSize(size2);

		GridBagConstraints gbConstraints1 = new GridBagConstraints();
		gbConstraints1.fill = GridBagConstraints.BOTH;
		gbConstraints1.gridy = 0; textFilterPanel.add(fromLabel, gbConstraints1);
		gbConstraints1.gridy = 1; textFilterPanel.add(textFilterFrom, gbConstraints1);
		gbConstraints1.gridy = 2; textFilterPanel.add(toLabel, gbConstraints1);
		gbConstraints1.gridy = 3; textFilterPanel.add(textFilterTo, gbConstraints1);

		GridBagConstraints gbConstraints = new GridBagConstraints();
		gbConstraints.fill = GridBagConstraints.HORIZONTAL;

		gbConstraints.gridwidth = 1;
		
		gbConstraints.gridx = 0; gbConstraints.gridy = 0;
		gbConstraints.weightx = 1;
		add(textFilterPanel, gbConstraints);
	}

	@Override
	public String getFilterString() {

		String fromText = textFilterFrom.getText().trim();
		String toText = textFilterTo.getText().trim();
		
		BigDecimal fromVal = null;
		BigDecimal toVal = null;
		
		if (!fromText.equals("")) {
			try {
				fromVal = new BigDecimal(fromText);
			}
			catch (NumberFormatException e) {
				fromVal = null;
			}
		}
		if (!toText.equals("")) {
			try {
				toVal = new BigDecimal(toText);
			}
			catch (NumberFormatException e) {
				toVal = null;
			}
		}
		
		boolean sqlEntered = false;
		StringBuilder sql = new StringBuilder();
		
		if (fromVal != null) {
			sql.append(column.tableDotColumnWithTicks());
			sql.append(">=");
			sql.append(fromVal);
			
			sqlEntered = true;
		}
		
		if (toVal != null) {
			if (sqlEntered) {
				sql.append(" AND ");
			}
			sql.append(column.tableDotColumnWithTicks());
			sql.append("<=");
			sql.append(toVal);
		}

		return sql.toString().trim();
	}	
}
