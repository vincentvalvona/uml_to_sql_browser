package growoffshore.dialogs;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Window;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

import growoffshore.DBInfoContainer;
import growoffshore.DatabaseInfo;
import growoffshore.TablePanel;
import growoffshore.TablePanelWindow;
import growoffshore.UserAlerter;
import growoffshore.generators.CalibratorGenerator;
import growoffshore.generators.CellEditorAndRendererGenerator;
import growoffshore.mainwindow.MainWindow;
import growoffshore.mainwindow.MainWindowTable;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLEngine;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.functionality.FunctionalityGenerator;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.Prompt;
import uk.co.solidcoresoftware.databaseapp.views.tables.UserTableScrollPane;

public class LightTableDialogPanel extends TablePanel implements DBInfoContainer, UserAlerter {

	private final TablePanelWindow parent;

	private final DatabaseInfo dbInfo;

	private UserTableScrollPane scrollPane;

	private JPanel buttonsPanel;

	private JButton addRowButton;
	private JButton deleteRowButton;
	private JButton refreshButton;

	private Table table;
	private Column[] columns;

	public LightTableDialogPanel(LightTableDialog parent, Column[] columns, DatabaseInfo dbInfo) {
		this((TablePanelWindow) parent, null, columns, dbInfo);
	}
	
	public LightTableDialogPanel(LightTableDialog parent, Table table, DatabaseInfo dbInfo) {
		this((TablePanelWindow) parent, table, null, dbInfo);
	}
	
	public LightTableDialogPanel(MainWindow parent, Column[] columns, DatabaseInfo dbInfo) {
		this((TablePanelWindow) parent, null, columns, dbInfo);
	}
	
	public LightTableDialogPanel(MainWindow parent, Table table, DatabaseInfo dbInfo) {
		this((TablePanelWindow) parent, table, null, dbInfo);
	}

	public LightTableDialogPanel(TablePanelWindow parent, Column[] columns, DatabaseInfo dbInfo) {
		this(parent, null, columns, dbInfo);
	}
	
	public LightTableDialogPanel(TablePanelWindow parent, Table table, DatabaseInfo dbInfo) {
		this(parent, table, null, dbInfo);
	}

	LightTableDialogPanel(TablePanelWindow parent, Table table, Column[] columns, DatabaseInfo dbInfo) {
		super((Window) parent);
		this.parent = parent;
		this.dbInfo = dbInfo;
		this.table = table;
		this.columns = columns;

		doCustomCellRendererGeneration();
		
		model = createTableModel();
		doCustomForModel(model);
		
		baseTable = createTable();
		doCustomForTable(baseTable);
		
		setup();
	}
	

	protected void doCustomForTable(BaseTable baseTable) {
		// To override if necessary
	}

	protected void doCustomForModel(LightTableModel model) {
		// To override if necessary
	}

	protected void doCustomCellRendererGeneration() {
		// To override if necessary
	}
	
	private LightTableModel createTableModel() {
		
		ArrayList<Object[]> columnInfo = SQLEngine.getMainConnection().getColumnInfo("COLUMNS", table, new String[] {
				SQLEngine.META_COLUMNS_COLUMN_NAME,
				SQLEngine.META_COLUMNS_DATA_TYPE,
				SQLEngine.META_COLUMNS_IS_NULLABLE,
				SQLEngine.META_COLUMNS_COLUMN_DEFAULT });
		
		Iterator<Object[]> columnInfoIt = columnInfo.iterator();
		
		int columnNameIndex = 0; int dataTypeIndex = 1; int isNullableIndex = 2; int columnDefaultIndex = 3;
		
		return new LightTableModel() {
			@Override
			protected void setupModel() {
				if (columns != null) {
					this.addColumns();
				}
				else if (table != null) {
					this.extractColumns(columnInfoIt, columnNameIndex, dataTypeIndex, isNullableIndex, columnDefaultIndex);
				}
			}

			private void addColumns() {
				for (int i=0; i<columns.length; i++) {
					addColumn(columns[i]);
				}
			}
			
			private void extractColumns(Iterator<Object[]> columnInfoIt, int columnNameIndex, int dataTypeIndex,
					int isNullableIndex, int columnDefaultIndex) {
				while (columnInfoIt.hasNext()) {
					Object[] nextInfoSet = columnInfoIt.next();
					
					String columnName = (String) nextInfoSet[columnNameIndex];
					String dataType = (String) nextInfoSet[dataTypeIndex];
					String isNullable = (String) nextInfoSet[isNullableIndex];
					String columnDefault = (String) nextInfoSet[columnDefaultIndex];
					
					Column column = new Column(table, columnName);
					column.setTypeString(dataType);
					
					CalibratorGenerator.addCalibrator(column);
					
					if (columnName.toLowerCase().equals("id")) {
						column.setType(java.sql.Types.BIGINT);
						column.setDefaultValue(null);
					}
					
					column.setNullAllowed(isNullable.toUpperCase().equals("YES"));
					
					if (columnDefault != null && columnDefault.equals("CURRENT_TIMESTAMP")) {
						column.setDefaultValue(null);
					}
					else {
						column.setDefaultValue(columnDefault);
					}
					
					addColumn(column);
				}
			}
			
			@Override
			protected void setupCalibrators() {}
			
			@Override
			protected void defineIndices() {}
		};
	}
	
	private MainWindowTable createTable() {
		return new MainWindowTable((TablePanelWindow) parent, model, (UserAlerter) parent, table);
	}
	
	private void setup() {
		
		scrollPane = new UserTableScrollPane(baseTable);
//		scrollPane.setPreferredSize(new Dimension(500, 400));
//		scrollPane.setBounds(12, 12, 858, 257);
		
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		add(scrollPane);
		
		buttonsPanel = new JPanel(); 
		buttonsPanel.setLayout(new BoxLayout(buttonsPanel, BoxLayout.Y_AXIS));
//
//		addRowButton = new JButton("Add row");
//		FunctionalityGenerator.addActionAddRow(addRowButton, baseTable);
//		buttonsPanel.add(addRowButton);
//		
//		deleteRowButton = new JButton("Delete row");
//		FunctionalityGenerator.addActionDeleteRow(deleteRowButton, baseTable, "Delete %n rows" , "Delete %n row", "Delete %n rows");
//		buttonsPanel.add(deleteRowButton);
//
//		refreshButton = new JButton("Refresh");
//		FunctionalityGenerator.addActionRefreshRows(refreshButton, baseTable.model);
//		buttonsPanel.add(refreshButton);
		
		add(buttonsPanel);
		
		model.setRowAddingEnabled(false);
		model.setRowDeletingEnabled(false);
		refreshTable();
	}

	public void updateColumnProperties() {
		// The model is updated, old columns removed, new columns created and displayed 
		model.update();

		Column[] allColumnsInModel = model.getAllColumns();
		
		baseTable.removeAllColumns();

		// Setting the correct type of cell renderer and editor for each column.
		for (int i=0; i<allColumnsInModel.length; i++) {
			final Column nextColumn = allColumnsInModel[i];
			
			CellEditorAndRendererGenerator gen = new CellEditorAndRendererGenerator(nextColumn, (Window) parent, getDbInfo(), baseTable);
			baseTable.addColumn(nextColumn.tableDotColumn(), nextColumn.columnName, 100, 0, gen.cellRenderer, gen.cellEditor);
		}
		
		baseTable.showAllColumns();
	}

	public void refreshTable() {
		updateColumnProperties();
	}

	@Override
	public DatabaseInfo getDbInfo() {
		return dbInfo;
	}

	@Override
	public void onPrompt(Prompt currentPrompt) {
		
	}
}
