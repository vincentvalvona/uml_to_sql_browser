package growoffshore.dialogs;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;

public class FilterFloatingFrameSectionPrimary extends FilterFloatingFrameSection {

	public FilterFloatingFrameSectionPrimary(FilterFloatingFrameContent owner, Column column, boolean isRemovable) {
		super(owner, column, isRemovable);
	}

	@Override
	protected void setup() {
		
	}

	@Override
	public String getFilterString() {
		return "";
	}	
}
