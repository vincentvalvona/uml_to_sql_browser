package growoffshore.dialogs;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.ProgressMonitor;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.border.EmptyBorder;

public class ProgressBarDialog extends JDialog {
	Timer timer;
	
	private int minValue;
	private int maxValue;
	private int newMaxValue;
	
	private int counter;
	
	private boolean pendingCancelled = false;
	
	private String message;
	
	private JPanel mainContainer;
	
	private JLabel mainMessageLabel;
	private JProgressBar pBar;
	private JButton cancelButton;

//	private Runnable cancelOperation;
	
	public ProgressBarDialog(Window window) {
		super(window);
		setup();
	}

	private void setup() {
		setModal(true);
		
		mainContainer = new JPanel();
		BoxLayout mainLayout = new BoxLayout(mainContainer, BoxLayout.Y_AXIS);
		mainContainer.setLayout(mainLayout);
		mainContainer.setBorder(new EmptyBorder(10, 10, 10, 10));
		getContentPane().add(mainContainer);
		
		mainMessageLabel = new JLabel(" ");
		pBar = new JProgressBar();
		pBar.setStringPainted(true);
		cancelButton = new JButton("Cancel");

		mainMessageLabel.setAlignmentX(LEFT_ALIGNMENT);
		pBar.setAlignmentX(LEFT_ALIGNMENT);
		cancelButton.setAlignmentX(LEFT_ALIGNMENT);

		mainContainer.add(mainMessageLabel);
		mainContainer.add(pBar);
		mainContainer.add(cancelButton);
		
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				pendingCancelled = true;
			}
		});
		
		pack();
		setSize(400, getHeight());
		setResizable(false);
	}
	
	public void increment() {
		if (counter < maxValue) {
			counter++;
		}
	}

	
	private class Update implements Runnable {
		@Override
		public void run() {
			if (message != null) {
				mainMessageLabel.setText(message);
			}
			
			if (newMaxValue != -1) {
				if(newMaxValue != maxValue) {
					maxValue = newMaxValue;
					pBar.setMaximum(maxValue);
				}
				
				if (counter >= maxValue) {
					// Do nothing
				}
				else {
					pBar.setValue(counter);
					int percentageComplete = (counter * 100 / maxValue);
					if (percentageComplete < 0) percentageComplete = 0;
					else if (percentageComplete > 100) percentageComplete = 100;
					
					pBar.setString("" + percentageComplete + "% (" + counter +  "/" + maxValue + ")");
				}
			}
		}
	}

	public void startMonitoring() {
		
		newMaxValue = -1;
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				setVisible(true);
			}
		}).start();
		
		timer = new Timer(100, new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				new Update().run();
			}
		});
		timer.start();
		
	}
	
	public void stopMonitoring() {
		if (timer != null && timer.isRunning()) {
			timer.stop();
		}
		timer = null;
		
		setVisible(false);
	}
	
	public void setNewMaxValue(int newMaxValue) {
		this.newMaxValue = newMaxValue;
		this.counter = 0;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
//
//	public void setCancelOperation(Runnable cancelOperation) {
//		this.cancelOperation = cancelOperation;
//	}

	public boolean pendingCancelled() {
		return pendingCancelled;
	}
}
