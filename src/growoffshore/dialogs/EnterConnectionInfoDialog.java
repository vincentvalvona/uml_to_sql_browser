package growoffshore.dialogs;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import growoffshore.ConnectionOption;
import growoffshore.ConnectionsList;
import growoffshore.ConnectionsListItemComp;
import growoffshore.UserPreferences;

public class EnterConnectionInfoDialog extends JDialog {

	private static int inputFieldsCount = 0;
	public static final int HOST_FIELD = inputFieldsCount++;
	public static final int USERNAME_FIELD = inputFieldsCount++;
	public static final int PASSWORD_FIELD = inputFieldsCount++;
	public static final int DATABASE_FIELD = inputFieldsCount++;
	
	private JLabel[] labels = new JLabel[inputFieldsCount];
	private JTextField[] inputFields = new JTextField[inputFieldsCount];
	
	private JButton okButton = new JButton("OK");
	private JButton cancelButton = new JButton("Cancel");
	private JButton clearButton = new JButton("Clear");
	
	private ConnectionsList previousConnectionsSelection;
	
	private int buttonSelected = 0;
	
	private static int buttonCount = 0;
	public final static int SELECTED_CANCEL_BUTTON = buttonCount++;
	public final static int SELECTED_OK_BUTTON = buttonCount++;
	
	public EnterConnectionInfoDialog(String title) {
		setTitle(title);
		setup();
	}
	
	private void setup() {
		
		setModal(true);
		
		FlowLayout mainLayout = new FlowLayout(FlowLayout.LEFT);
		setLayout(mainLayout);
		
		EmptyBorder mainBorder = new EmptyBorder(10, 10, 10, 10);
		JPanel mainPart = new JPanel();
		mainPart.setLayout(new BoxLayout(mainPart, BoxLayout.Y_AXIS));
		mainPart.setBorder(mainBorder);
		
		JPanel rightPart = new JPanel();
		
		previousConnectionsSelection = new ConnectionsList();
		DefaultListModel<ConnectionsListItemComp> listModel = new DefaultListModel<>();
		previousConnectionsSelection.setModel(listModel);
		
		
		previousConnectionsSelection.setPreferredSize(new Dimension(200, 300));

		ArrayList<ConnectionOption> allConnections = UserPreferences.getInstance().getAllConnections();
		
		Iterator<ConnectionOption> allConnectionsIt = allConnections.iterator();
		while (allConnectionsIt.hasNext()) {
			ConnectionOption nextConnection = allConnectionsIt.next();
			listModel.addElement(new ConnectionsListItemComp(nextConnection));
		}
		
		previousConnectionsSelection.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				ConnectionsListItemComp listItem = previousConnectionsSelection.getSelectedValue();
				
				if (listItem != null) {
					ConnectionOption connectionOption = listItem.connectionOption;
					inputFields[HOST_FIELD].setText(connectionOption.getHost());
					inputFields[USERNAME_FIELD].setText(connectionOption.getUser());
					inputFields[PASSWORD_FIELD].setText(connectionOption.getPassword());
					inputFields[DATABASE_FIELD].setText(connectionOption.getDatabase());
				}
			}
		});
		
		rightPart.add(new JScrollPane(previousConnectionsSelection));
		
		add(mainPart);
		add(rightPart);
		
		JPanel fieldsPart = new JPanel();
		
		GridBagLayout fieldsPartLayout = new GridBagLayout();
		GridBagConstraints c = new GridBagConstraints();
		c.anchor = GridBagConstraints.WEST;
		c.fill = GridBagConstraints.VERTICAL;
		fieldsPart.setLayout(fieldsPartLayout);
		
		for (int i=0; i<inputFieldsCount; i++) {
			
			labels[i] = new JLabel();
			
			if (i == PASSWORD_FIELD) {
				inputFields[i] = new JPasswordField();
			}
			else {
				inputFields[i] = new JTextField();
			}
			
			
			final JLabel nextLabel = labels[i];
			nextLabel.setOpaque(true);
			nextLabel.setHorizontalAlignment(SwingConstants.LEFT);
			final JTextField nextInputField = inputFields[i];
			
			nextInputField.addFocusListener(new FocusListener() {
				
				@Override
				public void focusLost(FocusEvent e) {}
				
				@Override
				public void focusGained(FocusEvent e) {
					nextInputField.selectAll();
				}
			});
			
			
			
			Dimension preferedSize = nextInputField.getPreferredSize();

			nextInputField.setPreferredSize(new Dimension(400, preferedSize.height));
			
//			fieldsPartLayout.
			c.gridy = i;
			c.gridx = 0;
			fieldsPart.add(nextLabel, c);
			c.gridx = 1;
			fieldsPart.add(nextInputField, c);
//			fieldsPartLayout.putConstraint(SpringLayout.WEST, c1, pad, e2, c2);
			
		}
		
		// Setting fields
		labels[USERNAME_FIELD].setText("Username");
		labels[PASSWORD_FIELD].setText("Password");
		labels[HOST_FIELD].setText("Host");
		labels[DATABASE_FIELD].setText("Database");
		clearFields();
		
		JPanel buttonsPart = new JPanel();
		buttonsPart.setLayout(new FlowLayout(FlowLayout.CENTER));
		buttonsPart.add(okButton);
		buttonsPart.add(cancelButton);
		buttonsPart.add(clearButton);
		
		mainPart.add(fieldsPart);
		mainPart.add(buttonsPart);

		okButton.addActionListener(new ButtonSelectedListener(SELECTED_OK_BUTTON));
		okButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				UserPreferences.getInstance().setCurrentConnectionDetails(
						inputFields[HOST_FIELD].getText().trim(),
						inputFields[USERNAME_FIELD].getText().trim(),
						inputFields[PASSWORD_FIELD].getText(),
						inputFields[DATABASE_FIELD].getText().trim());
			}
		});
		cancelButton.addActionListener(new ButtonSelectedListener(SELECTED_CANCEL_BUTTON));
		clearButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clearFields();
			}
		});

		buttonsPart.add(okButton);
		buttonsPart.add(cancelButton);
		
		pack();
		setResizable(false);
		
		populateConnectionsDetails();
	}
	
	private void clearFields() {
		for (int i=0; i<inputFieldsCount; i++) {
			inputFields[i].setText("");
		}
		deselectListItems();
	}
	
	private void deselectListItems() {
		previousConnectionsSelection.clearSelection();
	}
	
	private void populateConnectionsDetails() {
		
		
	}

	private class ButtonSelectedListener implements ActionListener {

		private final int buttonSelected;
		
		public ButtonSelectedListener(int buttonSelected) {
			this.buttonSelected = buttonSelected;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			EnterConnectionInfoDialog.this.buttonSelected = this.buttonSelected;
			EnterConnectionInfoDialog.this.setVisible(false);
		}
	}
	
	public String getFieldInput(int field) {
		if (field == PASSWORD_FIELD) {
			return inputFields[field].getText();
		}
		else {
			return inputFields[field].getText().trim();
		}
	}
	
	public int getButtonSelected() {
		return buttonSelected;
	}
}
