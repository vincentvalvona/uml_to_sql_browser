package growoffshore.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;

public abstract class FilterFloatingFrameSection extends JPanel {

	public final Column column;
	private int columnType;
	public final boolean isRemovable;
	
	
	public final FilterFloatingFrameContent owner;
	
	public FilterFloatingFrameSection(FilterFloatingFrameContent owner, Column column, boolean isRemovable) {
		this.owner = owner;
		this.column = column;
		this.columnType = column.getType();
		this.isRemovable = isRemovable;
		
		setup();
	}
	
	protected abstract void setup();
	public abstract String getFilterString();
}