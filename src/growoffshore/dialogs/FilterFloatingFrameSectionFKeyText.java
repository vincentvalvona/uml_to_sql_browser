package growoffshore.dialogs;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.regex.Pattern;

import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;

public class FilterFloatingFrameSectionFKeyText extends FilterFloatingFrameSection {

	JPanel radioOptionsPart;
	JRadioButton allWordsOption;
	JRadioButton anyWordOption;

	JPanel textFilterPanel;
	JLabel textFilterPanelLabel;
	JTextField textFilter;
	public final Column pKeyColumn;
	public final Column displayColumn;
	
	private Table aliasTable;
	private Column pKeyColumnAlias;
	private Column displayColumnAlias;
	
	
	public FilterFloatingFrameSectionFKeyText(FilterFloatingFrameContent owner, Column fKeyColumn, Column pKeyColumn, boolean isRemovable, Column displayColumn) {
		super(owner, fKeyColumn, isRemovable);
		this.pKeyColumn = pKeyColumn;
		this.displayColumn = displayColumn;
		doSetup();
	}
	
	@Override
	protected void setup() {
		// doSetup is used instead
	}
	
	protected void doSetup() {
		setLayout(new GridBagLayout());
		
		// Adding radio options part
		radioOptionsPart = new JPanel();
		radioOptionsPart.setBorder(new TitledBorder("Match type"));
		radioOptionsPart.setLayout(new FlowLayout(FlowLayout.LEFT));

		allWordsOption = new JRadioButton("All words");
		anyWordOption = new JRadioButton("Any word");

		ButtonGroup radioOptionsGroup = new ButtonGroup();
		radioOptionsGroup.add(allWordsOption);
		radioOptionsGroup.add(anyWordOption);
		
		radioOptionsPart.add(allWordsOption);
		radioOptionsPart.add(anyWordOption);
		
//		showBothOption.addActionListener(new ShowOptionActionListener(SHOW_TYPE_BOTH));
//		showFromOnlyOption.addActionListener(new ShowOptionActionListener(SHOW_TYPE_FROM));
//		showToOnlyOption.addActionListener(new ShowOptionActionListener(SHOW_TYPE_TO));
		
		allWordsOption.setSelected(true);
		
		// Adding date and time boxes
		textFilterPanel = new JPanel();
		textFilterPanel.setLayout(new BoxLayout(textFilterPanel, BoxLayout.Y_AXIS));
		textFilter = new JTextField();
		textFilterPanel.add(textFilter);
		textFilterPanelLabel = new JLabel("Filter:");

		GridBagConstraints gbConstraints = new GridBagConstraints();
		gbConstraints.fill = GridBagConstraints.HORIZONTAL;
		
		// Options part
		gbConstraints.gridx = 0; gbConstraints.gridy = 0;
		gbConstraints.weightx = 1; gbConstraints.weighty = 0;
		gbConstraints.gridwidth = 2;
		add(radioOptionsPart, gbConstraints);

		gbConstraints.gridwidth = 1;
		// From part
		gbConstraints.gridx = 0; gbConstraints.gridy = 1;
		gbConstraints.weightx = 0;
		add(textFilterPanelLabel, gbConstraints);
		
		gbConstraints.gridx = 1; gbConstraints.gridy = 1;
		gbConstraints.weightx = 1;
		add(textFilterPanel, gbConstraints);
	}

	public String getJoinSQL() {
		StringBuilder joinSQL = new StringBuilder("JOIN ");
		joinSQL.append(pKeyColumn.tableNameWithTicks());
		joinSQL.append(" AS ");
		joinSQL.append(aliasTable.tableNameWithTicks());
		joinSQL.append(" ON ");
		joinSQL.append(column.tableDotColumnWithTicks());
		joinSQL.append("=");
		joinSQL.append(pKeyColumnAlias.tableDotColumnWithTicks());
		return joinSQL.toString();
	}
	
	@Override
	public String getFilterString() {
		if (textFilter != null) {
			String text = textFilter.getText().trim();
			if (!text.equals("")) {
				
				if (text.length() > 2) {
					String[] textSplit = text.split(Pattern.quote(" "));
					
					ArrayList<String> allValidWords = new ArrayList<>(textSplit.length);
					
					for (int i=0; i<textSplit.length; i++) {
						String nextWord = Column.makeAlphanumeric(textSplit[i].trim());
						
						if (!nextWord.equals("")) {
							allValidWords.add(nextWord);
						}
					}
					
					String sqlConj = anyWordOption.isSelected() ? " OR " : " AND ";
					StringBuilder sql = new StringBuilder();
					sql.append("(");
					int allValidWordsLength = allValidWords.size();
					//  eg.      AND (field LIKE '%tea%')
					for (int i=0; i<allValidWordsLength; i++) {
						if (0 != i) {
							sql.append(sqlConj);
						}
						sql.append("(");
						sql.append(displayColumnAlias.tableDotColumnWithTicks());
						sql.append(" LIKE '%");
						sql.append(allValidWords.get(i));
						sql.append("%')");
					}
					sql.append(")");
					return sql.toString();
				}
				else {
					String nextWord = Column.makeAlphanumeric(text);
					if (!nextWord.equals("")) {
						StringBuilder sql = new StringBuilder();
						sql.append("(");
						sql.append(displayColumnAlias.tableDotColumnWithTicks());
						sql.append(" LIKE '");
						sql.append(nextWord);
						sql.append("%')");
						return sql.toString();
					}
				}
			}
		}
		return "";
	}

	public void setAliasTable(Table aliasTable) {
		this.aliasTable = aliasTable;
		this.pKeyColumnAlias = new Column(aliasTable, pKeyColumn.columnName);
		this.displayColumnAlias = new Column(aliasTable, displayColumn.columnName);
	}
	
	public Table getAliasTable() {
		return aliasTable;
	}
	
	public Column getPKeyColumnAlias() {
		return pKeyColumnAlias;
	}
	
	public Column getDisplayColumnAlias() {
		return displayColumnAlias;
	}
}
