package growoffshore.dialogs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;

import growoffshore.DatabaseInfo;
import growoffshore.TablePanelWindow;
import growoffshore.UserAlerter;
import growoffshore.mainwindow.MainWindowTablePanel;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.functionality.FunctionalityGenerator;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.Prompt;

public class OrdinaryTableDialog extends LightTableDialog implements TablePanelWindow, UserAlerter, TableStacker {

	private JButton selectButton;
	private JButton cancelButton;
	
	protected MainWindowTablePanel mainPanel;
	
	private String[] tableNamesStack;
	
	private static int numberOfOptions = 0;
	public static final int OPTION_CANCEL = numberOfOptions++;
	public static final int OPTION_SELECT = numberOfOptions++; 
	
	private int selectedOption = OPTION_CANCEL;
	
	private long selectedValue = -1;
	
//	public OrdinaryTableDialog(TablePanelWindow parent, Column[] columns, DatabaseInfo dbInfo) {
//		super(parent, columns, dbInfo);
//	}
	

	public OrdinaryTableDialog(TablePanelWindow parent, Table table, DatabaseInfo dbInfo, boolean canModifyTable) {
		this(parent, table, dbInfo, canModifyTable, null);
	}
	
	public OrdinaryTableDialog(TablePanelWindow parent, Table table, DatabaseInfo dbInfo, boolean canModifyTable, Integer rowPosInDBTable) {
		super(parent, table, null, dbInfo, canModifyTable, rowPosInDBTable);
		
		setTitle("Add, edit or select row(s)...");
		setTableStack(table, parent);
		setSize(800, 400);
	}
	
	public void setTableStack(Table table, TablePanelWindow parent) {
		if (parent instanceof TableStacker) {
			String[] oldTableNamesStack = ((TableStacker)parent).getTableNamesStack();
			
			tableNamesStack = new String[oldTableNamesStack.length+1];
			
			// Copying all the values from the old table stack to the new one.
			for (int i=0; i<oldTableNamesStack.length; i++) {
				tableNamesStack[i] = oldTableNamesStack[i];
			}
			
			// Adding the newest table name to the last position of the stack.
			tableNamesStack[tableNamesStack.length-1] = table.tableName;
		}
		else {
			tableNamesStack = new String[] { table.tableName };
		}
	}
//	
//	public OrdinaryTableDialog(TablePanelWindow parent, Table table, Column[] columns, DatabaseInfo dbInfo) {
//		super(parent, table, columns, dbInfo);
//	}

	@Override
	protected void addMainPanel() {
		super.mainPanel = new MainWindowTablePanel(this, table, canModifyTable, rowPosInDBTable);
		mainPanel = (MainWindowTablePanel) super.mainPanel;
		mainPanel.updateColumnProperties();
	}
	
	@Override
	protected void setupButtons(JPanel buttonsPanel) {
		selectButton = new JButton("Select");
		cancelButton = new JButton("Cancel");
		
		selectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean couldSelectTable = setSelectedTable();
				
				if (couldSelectTable) {
					selectedOption = OPTION_SELECT;
					setVisible(false);
				}
			}
		});
		
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectedOption = OPTION_CANCEL;
				setVisible(false);
			}
		});
		
		FunctionalityGenerator.setEnabledBasedOnNumberOfRowsSelected(selectButton, mainPanel.getBaseTable(), null, null, "Select", 1, 1);
		
		buttonsPanel.add(selectButton);
		buttonsPanel.add(cancelButton);
	}

	private boolean setSelectedTable() {
		BaseTable baseTable = mainPanel.getBaseTable();
		LightTableModel tableModel = baseTable.model;
		
		int selectedRowIndex = baseTable.getSelectedRow();
		
		if (selectedRowIndex > -1) {
			int modelRowIndex = baseTable.convertRowIndexToModel(selectedRowIndex);
			int idColumnIndex = tableModel.getColumnIndex(table.tableName, table.idColumnName);
			
			if (idColumnIndex > -1) {
				Object value = baseTable.getValueAt(modelRowIndex, idColumnIndex);
				if (value != null && value instanceof Long) {
					selectedValue = (long) value;
					return true;
				}
			}
		}
		
		return false;
	}
	
	@Override
	public void onPrompt(Prompt currentPrompt) {
		// TODO Auto-generated method stub	
	}
	
	public boolean tableNameAlreadyInStack(String tableName) {
		for (int i=0; i<tableNamesStack.length; i++) {
			if (tableNamesStack[i].equals(tableName)) {
				return true;
			}
		}
		
		return false;
	}
	
	public String getTableNamesInStackAsStr() {
		
		StringBuilder str = new StringBuilder();
		
		for (int i=0; i<tableNamesStack.length; i++) {
			str.append("" + i + ": " + tableNamesStack[i]);
			str.append("\n");
		}
		
		return str.toString();
	}
	
	public long getSelectedValue() {
		return selectedValue;
	}
	
	public int getSelectedOption() {
		return selectedOption;
	}

	@Override
	public String[] getTableNamesStack() {
		return tableNamesStack;
	}
}
