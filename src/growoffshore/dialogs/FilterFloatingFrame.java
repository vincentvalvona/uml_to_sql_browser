package growoffshore.dialogs;

import java.awt.Frame;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JDialog;

import growoffshore.DatabaseInfo;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableModel;

public class FilterFloatingFrame extends JDialog {

	public final FilterFloatingFrameContent content;
	
	public FilterFloatingFrame(Frame owner) {
		super(owner);
		content = new FilterFloatingFrameContent(owner, this);
		setup(owner);
	}
	
	private void setup(Frame owner) {
		setDefaultCloseOperation(HIDE_ON_CLOSE);
		setResizable(false);
		
		add(content);
	}
	
	public void display(DatabaseInfo dbInfo, Table table, BaseTableModel model) {
		content.display(dbInfo, table, model);
		pack();
		setSize(300, getSize().height);
		
		setTitle("Filters and ordering for table " + table.tableNameWithTicks());

		setVisible(true);
	}


	public void displayExisting() {
		setVisible(true);
	}

	public BaseTableModel getModel() {
		return content.getModel();
	}
}
