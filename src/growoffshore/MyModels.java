package growoffshore;

import java.math.BigDecimal;
import java.util.Iterator;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Option;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.items.ItemOfType;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.comboboxes.models.ComboBoxData;

public final class MyModels {
	private MyModels() {}
	
	public static final EnumsTableModel enumsModel = new EnumsTableModel();
	
	public static final class PossibleContainedItemsComboBoxData extends ComboBoxData {
		
		int idColumnIndex = -1;
		int tableColumnIndex = -1;
		int nameColumnIndex = -1;
		int unitPriceColumnIndex = -1;
		int unitTimeColumnIndex = -1;
		
		Column columnItemId;
		Column columnName;
		Column columnTable;
		Column columnUnitPrice;
		Column columnUnitTime;
		
		{
			columnItemId = new Column("all_items", "item_id");
			columnItemId.setType(java.sql.Types.BIGINT);
			columnName = new Column("all_items", "name");
			columnTable = new Column("all_items", "table");
			
			columnUnitPrice = new Column("all_items", "unit_cost");
			columnUnitPrice.makeColumnFake();
			columnUnitPrice.setType(java.sql.Types.DECIMAL);
			
			columnUnitTime = new Column("all_items", "unit_time");
			columnUnitTime.makeColumnFake();
			columnUnitTime.setType(java.sql.Types.BIGINT);
			
			addColumn(columnItemId);
			addColumn(columnName);
			addColumn(columnTable);
			addColumn(columnUnitPrice);
			addColumn(columnUnitTime);
			
			idColumnIndex = getColumnIndex("all_items.item_id");
			nameColumnIndex = getColumnIndex("all_items.name");
			tableColumnIndex = getColumnIndex("all_items.table");
			unitPriceColumnIndex = getColumnIndex("all_items.unit_cost");
			unitTimeColumnIndex = getColumnIndex("all_items.unit_time");
		}

		private PossibleContainedItemsComboBoxData(String query) {
			super(query);
		}


		@Override
		public String getComboBoxDisplay(Object[] row) {
			return row[tableColumnIndex] + " - " + row[nameColumnIndex] + "(£" + row[unitPriceColumnIndex] + ")";
		}

		@Override
		protected void postUpdateData() {
			
		}


		@Override
		public String getValue(Object[] row) {
			// TODO Auto-generated method stub
			return null;
		}


		@Override
		public Option getOptionFromValue(Object value) {
			// TODO Auto-generated method stub
			return null;
		}
	}
}
