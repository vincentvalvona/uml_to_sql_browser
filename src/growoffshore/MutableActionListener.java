package growoffshore;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MutableActionListener implements ActionListener  {

	private ActionListener innerListener;
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (innerListener != null) {
			innerListener.actionPerformed(e);
		}
	}

	public void setInnerListener(ActionListener innerListener) {
		this.innerListener = innerListener;
	}
}
