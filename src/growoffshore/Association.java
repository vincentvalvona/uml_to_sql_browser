package growoffshore;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;

public class Association {

	public final String name;
	public final String type;
	public final String associationStr;
	public final Column column;

	public Association(String name, String type, String associationStr, Column column) {
		this.name = name;
		this.type = type;
		this.associationStr = associationStr;
		this.column = column;
	}

	@Override
	public String toString() {
		StringBuilder sBuilder = new StringBuilder("Name: ");
		sBuilder.append(this.name);
		sBuilder.append("\nType: ");
		sBuilder.append(this.type);
		sBuilder.append("\nAssociation: ");
		sBuilder.append(this.associationStr);
		
		return sBuilder.toString();
	}
}
