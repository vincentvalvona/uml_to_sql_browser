package growoffshore.generators;

import java.awt.Window;
import javax.swing.JCheckBox;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import growoffshore.DBInfoContainer;
import growoffshore.DatabaseInfo;
import growoffshore.ExtendedBaseTableModel;
import growoffshore.ExtendedBaseTableModel.DisplayValueSet;
import growoffshore.TablePanelWindow;
import growoffshore.cellEditorsAndRenderers.CheckBoxCellEditor;
import growoffshore.cellEditorsAndRenderers.CheckBoxCellRenderer;
import growoffshore.cellEditorsAndRenderers.ComboBoxDataCellEditor;
import growoffshore.cellEditorsAndRenderers.ComboBoxDataCellRenderer;
import growoffshore.cellEditorsAndRenderers.DateAndTimeCellEditor;
import growoffshore.cellEditorsAndRenderers.DateAndTimeCellRenderer;
import growoffshore.cellEditorsAndRenderers.NormalTextCellEditor;
import growoffshore.cellEditorsAndRenderers.NormalTextCellRenderer;
import growoffshore.dialogs.OrdinaryTableDialog;
import growoffshore.mainwindow.MainWindow;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.DataType;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLEngine;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.LightTableModel;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.views.tables.BaseTable;

public class CellEditorAndRendererGenerator {
	
	public final TableCellRenderer cellRenderer;
	public final TableCellEditor cellEditor;
	public final Column column;
	public final DatabaseInfo dbInfo;
	
	public CellEditorAndRendererGenerator(Column nextColumn, Window jFrame, DatabaseInfo dbInfo, BaseTable baseTable) {
		
		this.column = nextColumn;
		this.dbInfo = dbInfo;
		
		int nextColumnType = nextColumn.getType();
		
		if (dbInfo != null && dbInfo.isAForeignKey(nextColumn)) {
			
			final Column primaryColumn = dbInfo.getPrimaryKeyOfForeignKey(nextColumn);
			final Column displayColumn = dbInfo.getDisplayColumnFromTable(primaryColumn.table);
			
			cellRenderer = new ComboBoxDataCellRenderer((TablePanelWindow) jFrame, dbInfo, nextColumn, baseTable);
			cellEditor = new ComboBoxDataCellEditor((TablePanelWindow) jFrame, dbInfo, nextColumn, baseTable) {
				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public void addRowOptionSelected(final LightTableModel model, final Object newValue, final int rowIndex, final int columnIndex) {
					SwingUtilities.invokeLater(new Runnable() {
						
						@Override
						public void run() {
							if (window instanceof DBInfoContainer) {
								
								boolean errorInsteadOfWindow = false;
								
								if (window instanceof OrdinaryTableDialog && ((OrdinaryTableDialog) window).tableNameAlreadyInStack(primaryColumn.table.tableName)) {
									errorInsteadOfWindow = true;
								}
								else if (window instanceof MainWindow) {
									if (((MainWindow) window).getTablePanel().table.tableName.equals(primaryColumn.table.tableName)) {
										errorInsteadOfWindow = true;
									}
								}
								
								Integer rowPosInDBTable = null;
								if (initialValue != null) {
									rowPosInDBTable = SQLEngine.getMainConnection().getRowPosition(primaryColumn, displayColumn, initialValue);
								}
								System.out.println("initialValue: " + initialValue);
								System.out.println("rowPosInDBTable: " + rowPosInDBTable);
								
								OrdinaryTableDialog dialog = new OrdinaryTableDialog(window, primaryColumn.table, dbInfo, !errorInsteadOfWindow, rowPosInDBTable);
								dialog.display();
								
								if (dialog.getSelectedOption() == OrdinaryTableDialog.OPTION_SELECT) {
									long selectedValue = dialog.getSelectedValue();
									
									if (model instanceof ExtendedBaseTableModel) {
										ExtendedBaseTableModel exBModel = (ExtendedBaseTableModel) model;
										DisplayValueSet displaySet = exBModel.getDisplayValueSet(column);
										if (displaySet != null) {
											displaySet.options.remove(selectedValue);
										}
									}
									
									model.setValueAt(selectedValue, rowIndex, columnIndex);
									
									if (addRowOptionReturnCommand != null) {
										addRowOptionReturnCommand.invokeCommand(model, newValue, rowIndex, columnIndex);
									}
								}
							}
						}
					});
				}
			};
		}
		else if (nextColumnType == java.sql.Types.BOOLEAN) {
			cellRenderer = new CheckBoxCellRenderer();
			cellEditor = new CheckBoxCellEditor(new JCheckBox(), baseTable);
		}
		else if (nextColumnType == java.sql.Types.DATE
				|| nextColumnType == java.sql.Types.TIME
				|| nextColumnType == java.sql.Types.TIMESTAMP
				|| nextColumnType == DataType.INT_TYPE_DATETIME) {
			cellRenderer = new DateAndTimeCellRenderer(jFrame, nextColumn);
			cellEditor = new DateAndTimeCellEditor(jFrame, baseTable, nextColumn);
		}
		else {
			cellRenderer = new NormalTextCellRenderer(nextColumn);
			cellEditor = new NormalTextCellEditor(new JTextField(), baseTable, nextColumn);
		}
	}
}
