package growoffshore;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.DataType;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableModel;

public class AssociationsTableModel extends BaseTableModel {
	
	private final static Table TABLE = Table.getTable("__Associations");
	
	public static final Column ID_COLUMN = new Column(TABLE, "id");
	
	static {
		ID_COLUMN.setType(java.sql.Types.BIGINT);
		ID_COLUMN.setDefaultValue(null);
		
		ID_COLUMN.setValueReadOnly(true);
	}
	
	public static final Column BASE_TABLE_DOT_COLUMN_COLUMN = new Column(TABLE, "base_table_dot_column");
	public static final Column TARGET_TABLE_DOT_COLUMN_COLUMN = new Column(TABLE, "target_table_dot_column");

	public static final int ID_COLUMN_INDEX = 0;
	public static final int BASE_TABLE_DOT_COLUMN_COLUMN_INDEX = 1;
	public static final int TARGET_TABLE_DOT_COLUMN_COLUMN_INDEX = 2;
	
	public AssociationsTableModel() {
		super(getSQLSelect());
	}
	
	private static String getSQLSelect() {
		StringBuilder sBuild = new StringBuilder("SELECT * FROM ");
		sBuild.append(TABLE.tableNameWithTicks());
		return sBuild.toString();
	}

	@Override
	protected void setupModel() {
		addColumn(ID_COLUMN);
		addColumn(BASE_TABLE_DOT_COLUMN_COLUMN);
		addColumn(TARGET_TABLE_DOT_COLUMN_COLUMN);
	}

	@Override
	protected void setupCalibrators() {
		
	}

	@Override
	protected void defineIndices() {
		
	}

}
