package growoffshore;
import java.util.Calendar;

import growoffshore.mainwindow.MainWindow;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.sql.SQLEngine;

public class Main {
	
	public final static int DATABASE_PAGES_PER_ROW = 20;
	private static MainWindow mainWindow;
	
	public static void main(String[] args) {
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		mainWindow = new MainWindow();
		mainWindow.display();
	}
	
	public static void setConnections(String host, String user, String password, String database) {
		SQLEngine.setInfoSchemaConnection(SQLEngine.getSQLEngineForSchema(
				host,
				user,
				password
		));
		
		SQLEngine.setMainConnection(SQLEngine.getSQLEngine(
				host,
				user,
				password,
				database
		));
	}
	
	public static String convertVisibilityToAttributes(String visibility) {
		
		String attributes;
		
		if ("private".equals(visibility)) {
			attributes = " UNIQUE NOT NULL";
		}
		else if ("package".equals(visibility)) {
			attributes = " UNIQUE";
		}
		else if ("protected".equals(visibility)) {
			attributes = " NOT NULL";
		}
		else {
			attributes = ""; // Public
		}
		
		return attributes;
	}
	
	public static MainWindow getMainWindow() {
		return mainWindow;
	}
	
	public static Calendar combineDateAndTimeCalendars(Calendar dateCalendar, Calendar timeCalendar) {
		// Creates a new calendar using the default date and time initially
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(0);
		
		if (dateCalendar != null) {
			cal.set(Calendar.YEAR, dateCalendar.get(Calendar.YEAR));
			cal.set(Calendar.MONTH, dateCalendar.get(Calendar.MONTH));
			cal.set(Calendar.DAY_OF_MONTH, dateCalendar.get(Calendar.DAY_OF_MONTH));
		}
		
		if (timeCalendar != null) {
			cal.set(Calendar.HOUR_OF_DAY, timeCalendar.get(Calendar.HOUR_OF_DAY));
			cal.set(Calendar.MINUTE, timeCalendar.get(Calendar.MINUTE));
			cal.set(Calendar.SECOND, timeCalendar.get(Calendar.SECOND));
			cal.set(Calendar.MILLISECOND, timeCalendar.get(Calendar.MILLISECOND));
		}
		return cal;
	}
}
