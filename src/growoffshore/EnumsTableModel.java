package growoffshore;

import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Column;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.DataType;
import uk.co.solidcoresoftware.databaseapp.libraries.core.models.Table;
import uk.co.solidcoresoftware.databaseapp.libraries.swing.models.BaseTableModel;

public class EnumsTableModel extends BaseTableModel {
	
	private final static Table TABLE = Table.getTable("__Enums");
	
	public static final Column ID_COLUMN = new Column(TABLE, "id");
	
	
	public static final Column TABLE_DOT_COLUMN_COLUMN = new Column(TABLE, "table_dot_column");
	public static final Column VALUE_COLUMN = new Column(TABLE, "value");

	static {
		ID_COLUMN.setType(java.sql.Types.BIGINT);
		ID_COLUMN.setDefaultValue(null);
		ID_COLUMN.setValueReadOnly(true);
		
		TABLE_DOT_COLUMN_COLUMN.setType(java.sql.Types.VARCHAR);
		TABLE_DOT_COLUMN_COLUMN.setMaxStringLength(100);

		VALUE_COLUMN.setType(java.sql.Types.VARCHAR);
		VALUE_COLUMN.setMaxStringLength(100);
	}
	
	public static final int ID_COLUMN_INDEX = 0;
	public static final int TABLE_DOT_COLUMN_COLUMN_INDEX = 1;
	public static final int VALUE_COLUMN_INDEX = 2;
	
	public EnumsTableModel() {
		super(getSQLSelect());
	}
	
	private static String getSQLSelect() {
		StringBuilder sBuild = new StringBuilder("SELECT * FROM ");
		sBuild.append(TABLE.tableNameWithTicks());
		return sBuild.toString();
	}

	@Override
	protected void setupModel() {
		addColumn(ID_COLUMN);
		addColumn(TABLE_DOT_COLUMN_COLUMN);
		addColumn(VALUE_COLUMN);
	}

	@Override
	protected void setupCalibrators() {
		
	}

	@Override
	protected void defineIndices() {
		
	}
}
