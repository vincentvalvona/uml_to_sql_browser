package growoffshore;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;

import com.toedter.calendar.JDateChooser;

import growoffshore.cellEditorsAndRenderers.DateAndTimeCellEditor;

public class TimeChooser extends JDateChooser implements ActionListener {

	private Date timeValue;
	
	private JSpinner timeSpinner;
	private JSpinner.DateEditor timeEditor;
	private JButton okButton;
	
	private DateAndTimeCellEditor cellEditor;
	
	private Component invoker;

	public TimeChooser(Date timeValue) {
		super(timeValue);
		this.timeValue = timeValue;
		setup();
	}

	private void setup() {
		setDateFormatString("HH:mm:ss");
		
		timeSpinner = new JSpinner( new SpinnerDateModel() );
		timeEditor = new JSpinner.DateEditor(timeSpinner, "HH:mm:ss");
		timeSpinner.setEditor(timeEditor);
		timeSpinner.setValue(timeValue);
		
		okButton = new JButton("Save");
		
		okButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				timeValue = (Date) timeSpinner.getValue();
				
				setDate(timeValue);
				
				dateSelected = true;
				popup.setVisible(false);
				
				if (cellEditor != null) {
					cellEditor.manuallyEditingStopped();
				}
				
				TimeChooser.this.actionPerformed(e);
			}
		});
		
		final ActionListener[] oldActionListeners = calendarButton.getActionListeners();
		
		
		calendarButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				timeSpinner.setValue(getDateEditor().getDate());
				
				for (int i=0; i<oldActionListeners.length; i++) {
					oldActionListeners[i].actionPerformed(e);
				}
			}
		});
		
		popup.removeAll();
		popup.add(timeSpinner);
		popup.add(okButton);
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		
	}
}
